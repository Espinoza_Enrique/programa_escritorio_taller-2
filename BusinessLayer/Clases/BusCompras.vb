﻿Imports Entities
Imports DataAccess
Public Class BusCompras

    Private Shared Function getUltimosIdDetallesCompra(ByVal idCompra As Integer) As List(Of Integer)
        Try
            Dim listaIdDetalles As New List(Of Integer)
            For Each det In DatCompras.getDetallesIdCompra(idCompra)
                listaIdDetalles.Add(det.idDetalleCompra)
            Next
            If listaIdDetalles.Count() > 0 Then
                Return listaIdDetalles
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los detalles de la última compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error Ventana")
            Return Nothing
        End Try
    End Function
    
    Public Shared Sub insertarCabeceraMod(ByVal montoMod As Double)
        Dim mensaje As String
        mensaje = ""
        Try
            DatCompras.insertarCabeceraModCompra(montoMod)
        Catch ex As Exception
            MsgBox("Error al colocar la cabecera de la modificación de monto para la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub
    Public Shared Sub insertarCabeceraCompra(ByVal montoTotal As Double, ByVal idUsuario As Integer)
        Try
            DatCompras.insertarCabeceraCompra(montoTotal, idUsuario)
        Catch ex As Exception
            MsgBox("Error al colocar la cabecera de la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub insertarDetallesCompra(ByVal idCabecera As Integer, ByRef precio As List(Of Double), ByRef unidades As List(Of Integer), ByRef idProductos As List(Of Integer), ByRef idProveedores As List(Of Integer))
        Try
            Dim detalles As New List(Of DetallesCompras)
            Dim cantidadElementos As Integer
            Dim elementoAnalizado As Integer
           
            cantidadElementos = idProductos.LongCount()
            elementoAnalizado = 0
            If idCabecera <> 0 Then
                While elementoAnalizado < cantidadElementos
                    Dim detalle As New DetallesCompras
                    detalle.idCompra = idCabecera
                    'detalle.idDetalleCompra = ultimoDetalleCompra

                    detalle.precioCompra = precio(elementoAnalizado)
                    detalle.unidadesCompra = unidades(elementoAnalizado)
                    detalle.idProducto = idProductos(elementoAnalizado)
                    detalle.idProveedor = idProveedores(elementoAnalizado)
                    detalle.estado = True
                    detalles.Add(detalle)
                    elementoAnalizado = elementoAnalizado + 1
                End While
                DatCompras.insertarDetallesCompras(detalles)
            Else
                MsgBox("Error al insertar los detalles de compras. Hubo problemas en recuperar el identificador de la cabecera de compra", vbOKOnly + vbCritical, "Error de recuperación de datos")
            End If
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub insertarDetallesMod(ByRef idCabeceraCompra As Integer, ByVal idCabeceraMod As Integer, ByRef precioMod As List(Of Double), ByRef motivos As List(Of Integer), ByRef descripciones As List(Of String))
        Try

            Dim detalles As New List(Of DetallesModCompras)
            Dim idDetallesCompra As New List(Of Integer)
            Dim cantidadElementos As Integer
            Dim elementoAnalizado As Integer

            cantidadElementos = precioMod.Count()
            idDetallesCompra = BusCompras.getUltimosIdDetallesCompra(idCabeceraCompra)
            elementoAnalizado = 0
            If idCabeceraCompra <> 0 And idCabeceraMod <> 0 Then
                While elementoAnalizado < cantidadElementos
                    Dim detalle As New DetallesModCompras
                    detalle.idCabeceraModCompras = idCabeceraMod
                    detalle.idCompra = idCabeceraCompra
                    detalle.idDetalleCompra = idDetallesCompra(elementoAnalizado)
                    detalle.montoDescuento = precioMod(elementoAnalizado)
                    detalle.idMotivo = motivos(elementoAnalizado)
                    detalle.fechaModDetalle = Date.Now()
                    detalle.estado = True
                    detalle.descModCompras = descripciones(elementoAnalizado)
                    detalles.Add(detalle)
                    elementoAnalizado = elementoAnalizado + 1
                End While
                DatCompras.insertarDetallesMod(detalles)
            Else
                MsgBox("Error al insertar los detalles de compras. Hubo problemas en recuperar el identificador de la cabecera de compra", vbOKOnly + vbCritical, "Error de recuperación de datos")
            End If
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub
    Public Shared Function getUltimaCabeceraCompra() As Integer
        Try
            Return DatCompras.getUltimaCabecera()
        Catch ex As Exception
            MsgBox("Error al obtener el último identificador de la factura de compra. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
            Return 0
        End Try
    End Function
    Public Shared Function getUltimaCabeceraMod() As Integer
        Try
            Return DatCompras.getUltimaCabeceraMod()
        Catch ex As Exception
            MsgBox("Error al obtener el último identificador de las modificaciones de precio para la última factura. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
            Return 0
        End Try
    End Function
    Public Shared Function getMotivosModCompras() As List(Of MotivosModCompras)
        Try
            Return DatCompras.getMotivosModCompras()
        Catch ex As Exception
            MsgBox("Error al obtener los motivos de modificación de precios en la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperción de datos")
            Return Nothing
        End Try
    End Function

    Public Shared Sub actuatlizaStockPorCompra(ByVal idProd As List(Of Integer), ByRef stock As List(Of Integer))
        Try
            Dim idTotales As Integer
            Dim idAnalizado As Integer

            idTotales = idProd.Count
            idAnalizado = 0
            While idAnalizado < idTotales
                DatCompras.actualizarStockPorCompra(idProd(idAnalizado), stock(idAnalizado))
                idAnalizado = idAnalizado + 1
            End While
            'contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al actualizar el stock de productos al realizar la compra. Los errores producidos fueron: " + +vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de actualización de datos")
        End Try
    End Sub
End Class
