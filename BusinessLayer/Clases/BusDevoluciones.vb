﻿Imports Entities
Imports DataAccess


Public Class BusDevoluciones

    Public Shared Function getDevolucionesProductos() As List(Of AuxDevolucionesProd)
        Try
            Return DatDevoluciones.getDevolucionesProductos()
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de las devoluciones. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Sub insertarCabeceraDev(ByVal montoTotalDev As Double)
        Try
            DatDevoluciones.insertarCabeceraDev(montoTotalDev)
        Catch ex As Exception
            MsgBox("Error al insertar los datos para la devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")

        End Try
    End Sub

    Public Shared Function getUltimoIdCabecera() As Integer
        Try
            Return DatDevoluciones.getUltimoIdCabecera()
        Catch ex As Exception
            MsgBox("Error al obtener el último identificador de la cabecera. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return -1
        End Try
    End Function
    Public Shared Function devPermitida(ByVal idVenta As Integer, ByVal idDetalle As Integer, ByVal cantidad As Integer) As Boolean
        Try
            Dim cantidadDev As Integer
            Dim devoluciones As List(Of DevolucionesDetalles)
            Dim unidadesVendidas As Integer
            cantidadDev = 0

            unidadesVendidas = DatVentas.getUnidadesVendidaDetalle(idVenta, idDetalle)
            devoluciones = DatDevoluciones.getDevPorIdVenta(idVenta, idDetalle)
            If Not IsNothing(devoluciones) And unidadesVendidas > -1 Then
                For Each det In DatDevoluciones.getDevPorIdVenta(idVenta, idDetalle)
                    cantidadDev = cantidadDev + det.unidadesDev
                Next
                cantidadDev = cantidadDev + cantidad
                If cantidadDev <= unidadesVendidas Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return True
            End If
        Catch ex As Exception
            MsgBox("Error al controlar si la devolución está permitida. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return False
        End Try
    End Function
    Public Shared Sub insertarDetallesDev(ByRef idVentas As List(Of Integer), ByRef idDetalles As List(Of Integer), ByRef unidadesDev As List(Of Integer), ByRef montos As List(Of Double), ByRef idMotivos As List(Of Integer), ByRef descripciones As List(Of String))
        Try
            Dim cabecera As Integer
            Dim cantidadDet As Integer
            Dim detAnalizado As Integer
            Dim detalle As New DevolucionesDetalles
            Dim detalles As New List(Of DevolucionesDetalles)

            cabecera = DatDevoluciones.getUltimoIdCabecera()
            cantidadDet = idVentas.Count
            detAnalizado = 0
            If cabecera > 0 Then
                While detAnalizado < cantidadDet
                    detalle.idVenta = idVentas(detAnalizado)
                    detalle.idDevolucion = cabecera
                    detalle.idDetalleVenta = idDetalles(detAnalizado)
                    detalle.unidadesDev = unidadesDev(detAnalizado)
                    detalle.montoDetalleDev = montos(detAnalizado)
                    detalle.idMotivo = idMotivos(detAnalizado)
                    detalle.estado = True
                    detalle.descripcion = descripciones(detAnalizado)
                    detAnalizado = detAnalizado + 1
                    detalles.Add(detalle)
                End While
                DatDevoluciones.insertarDetallesDev(detalles)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function getMotivosDev() As List(Of MotivosDevoluciones)
        Try
            Return DatDevoluciones.getMotivosDev()
        Catch ex As Exception
            MsgBox("Error al recuperar los motivos de devoluciones. Los errores producidos fueron: " + vbNewLine + ex.Message, "Ventana Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function getTotalDevPendientes(ByVal idVenta As Integer, ByVal idDetalle As Integer, ByVal unidadesDev As Integer) As Boolean
        Try
            Dim devPorIdVenta As New List(Of DevolucionesDetalles)
            Dim respuesta As Boolean
            Dim totalDev As Integer

            respuesta = True
            totalDev = 0
            devPorIdVenta = DatDevoluciones.getDevPorIdVenta(idVenta, idDetalle)

            For Each det In devPorIdVenta
                totalDev = totalDev + det.unidadesDev
            Next

            If totalDev >= unidadesDev Then
                respuesta = False
            End If
            Return respuesta
        Catch ex As Exception
            MsgBox("Error al controlar las unidades devueltas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
End Class
