﻿Imports Entities
Imports DataAccess
Public Class BusLogueos

    Public Shared Function getInformesPorFechaYDni(ByVal dni As Integer, ByVal fecha As DateTime)
        Try
            Return DatLogueos.getLogueosPorDniYFecha(dni, fecha)
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function getInformesPorFecha(ByVal fecha As DateTime)
        Try
            Return DatLogueos.getInformeLogueosPorFecha(fecha)
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getInformesPorDni(ByVal dni As Integer)
        Try
            Return DatLogueos.getInformeLogueosPorDni(dni)
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getInformeLogueos() As List(Of AuxLogueos)
        Try
            Return DatLogueos.getInformeLogueos()
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Sub desloguear(ByVal idLogueo As Integer)
        Try
            DatLogueos.establecerDeslogueo(idLogueo)
        Catch ex As Exception
            MsgBox("Error al establecer los datos del cierre de sesión. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")

        End Try
    End Sub
    Public Shared Function nuevoLogueo(ByVal idUsuario As Integer) As Integer
        Try
            Return DatLogueos.insertarNuevoLogueo(idUsuario)
        Catch ex As Exception
            MsgBox("Error al establecer los datos de logueo. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return 0
        End Try

    End Function

    Public Shared Function idLogueoSesionActual() As Integer
        Try
            Return DatLogueos.siguienteIdLogueo()
        Catch ex As Exception
            MsgBox("Error al establecer los datos de logueo. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return 0
        End Try

    End Function
End Class
