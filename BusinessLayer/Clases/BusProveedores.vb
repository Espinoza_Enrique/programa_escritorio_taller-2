﻿Imports Entities
Imports DataAccess
Public Class BusProveedores
    Public Shared Function getProveedorPorNombre(ByVal nombre As String) As List(Of Proveedores)
        Try
            Return DatProveedores.getProveedorPorNombre(nombre)
        Catch ex As Exception
            MsgBox("Error al filtrar los producos. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getProveedores() As List(Of Proveedores)
        Try
            Return DatProveedores.getProveedores()
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los proveedores. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de obtencion de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarEstado(ByVal idProv As Integer, ByVal estado As Boolean) As String
        Try
            Dim mensaje As String
            Dim resultado As Boolean
            mensaje = ""
            resultado = DatProveedores.modificarEstado(idProv, estado)
            If resultado Then
                mensaje = mensaje + "Estado del producto modificado" + vbNewLine
            Else
                mensaje = mensaje + "El producto indicado no se encuentra almacenado. Por favor, revise la lista de productos." + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al modificar el estado del producto. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de modificación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarProveedor(ByVal idProv As Integer, ByVal nombre As String, ByVal direccion As String, ByVal telefono As String, ByVal codArea As String, ByVal celular As String, ByVal mail As String, ByVal modNom As Boolean) As String
        Try
            Dim repetido As Boolean
            Dim mensaje As String
            Dim resultado As Boolean
            repetido = False
            mensaje = ""
            If modNom Then
                repetido = DatProveedores.controlarNombreRepetido(nombre)
            End If
            If repetido Then
                mensaje = mensaje + "Ya existe un proveedor con el nuevo nombre que desea ingresar." + vbNewLine
            Else
                resultado = DatProveedores.modificarProveedor(idProv, nombre, direccion, telefono, codArea, celular, mail, modNom)
            End If
            If Not resultado And Not IsNothing(resultado) Then
                mensaje = mensaje + "Error al modificar el proveedor" + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al modificar los datos del proveedor.Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al modificar datos del proveedor")
            Return Nothing
        End Try
    End Function
    Public Shared Function insertarProveedor(ByVal nombre As String, ByVal direccion As String, ByVal telefono As String, ByVal codArea As String, ByVal celular As String, ByVal mail As String) As String
        Try
            Dim contexto As New CalabozoEntities
            Dim mensaje As String
            Dim repetido As Boolean
            mensaje = ""
            repetido = DatProveedores.controlarNombreRepetido(nombre)
            If Not repetido Then
                DatProveedores.insertarProveedores(nombre, direccion, telefono, codArea, celular, mail)
            Else
                mensaje = mensaje + "Ya existe un proveedor con el nombre indicado" + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al insertar un nuevo proveedor. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de inserción de datos")
            Return Nothing
        End Try
    End Function
End Class
