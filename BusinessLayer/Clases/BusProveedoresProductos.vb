﻿Imports Entities
Imports DataAccess
Public Class BusProveedoresProductos

    Public Shared Function getDatos(ByVal nombrePro As String, ByVal nombreProv As String) As List(Of ProveedoresProductos)
        Try
            Return DatProductosProveedores.filtrarProvProdProNombre(nombrePro, nombreProv)
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de las relaciones productos/proveedores. Los errores fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getDatosPorProveedor(ByVal nombreProv As String) As List(Of ProveedoresProductos)
        Try
            Return DatProductosProveedores.filtrarProvProdPorProv(nombreProv)
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de las relaciones productos/proveedores. Los errores fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getDatosPorProducto(ByVal nombreProd As String) As List(Of ProveedoresProductos)
        Try
            Return DatProductosProveedores.filtrarProvProdPorProducto(nombreProd)
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de las relaciones productos/proveedores. Los errores fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function insertarProductoProveedor(ByVal idProd As Integer, ByVal idProv As Integer, ByVal precioCompra As Double)
        Try
            Dim contexto As New CalabozoEntities
            Dim repetido As Boolean
            Dim mensaje As String

            mensaje = ""
            repetido = DatProductosProveedores.comprobarRepetido(idProd, idProv)
            If Not repetido Then
                DatProductosProveedores.insertarProductoProveedor(idProd, idProv, precioCompra)
            Else
                mensaje = mensaje + "Ya existe una relación para el proveedor y producto seleccionado." + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al insertar una nueva relación producto/proveedor. Los errores producidos fueron: " + ex.Message, vbOKOnly + vbCritical, "Error de alta")
            Return Nothing
        End Try
    End Function
    Public Shared Sub modificarDatos(ByVal idProd As Integer, ByVal idProv As Integer, ByVal precioCompra As Double, ByVal modProdProv As Boolean)
        Try
            DatProductosProveedores.modificarProductoProveedor(idProd, idProv, precioCompra, modProdProv)
        Catch ex As Exception
            MsgBox("Error al modificar los datos de la relación. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de actualización de datos")
        End Try
    End Sub
    Public Shared Function gerProveedoresProductos() As List(Of ProveedoresProductos)
        Try
            Return DatProductosProveedores.gerProdProv()
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las relaciones productos/proveedores. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function
End Class
