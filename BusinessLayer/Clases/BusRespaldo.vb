﻿Imports DataAccess
Public Class BusRespaldo
    Public Shared Sub respaldarBD(ByVal ruta As String)
        Try
            DatRespaldo.respaldarBD(ruta)
        Catch ex As Exception
            MsgBox("Error al respaldar la base de datos. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
End Class
