﻿Imports Entities
Imports DataAccess
Public Class BusUsuarios

    Public Shared Function filtrarPorDni(ByVal dni As Integer)
        Try
            Return DatUsuarios.filtrarPorDniEmp(dni)
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function filtrarPorApellido(ByVal ape As String)
        Try
            Return DatUsuarios.filtrarPorApellido(ape)
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarAcceso(ByVal nombreUsuario As String, ByVal contra As String) As Usuarios
        Return DatUsuarios.controlarAccesso(nombreUsuario, contra)
    End Function

    Public Shared Function getUsuarios() As List(Of Usuarios)
        Return DatUsuarios.getUsuarios()
    End Function

    Public Shared Sub modificarEstadoUsuario(ByVal idUsuario As Integer, ByVal valorEstado As Byte)
        DatUsuarios.modificarEstadoUsuario(idUsuario, valorEstado)
    End Sub

    Public Shared Sub crearNuevoUsuario(ByVal nombreUsu As String, ByVal password As String, ByVal idEmpleado As Integer, ByVal idPerfil As Integer)
        DatUsuarios.nuevoUsuario(nombreUsu, password, idEmpleado, idPerfil)
    End Sub

    Public Shared Sub modificarContrasenia(ByVal password As String, ByVal idUsuario As Integer)
        DatUsuarios.modificarContrasenia(idUsuario, password)
    End Sub

    Public Shared Function controlarUsuarioRepetido(ByVal nombreUsuario As String, ByVal idEmpleado As Integer, ByVal idPerfil As Integer) As String
        Return DatUsuarios.controlarUsuarioRepetido(nombreUsuario, idEmpleado, idPerfil)
    End Function


End Class
