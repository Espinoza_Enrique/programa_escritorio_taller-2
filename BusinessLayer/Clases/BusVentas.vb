﻿Imports Entities
Imports DataAccess

Public Class BusVentas

    Public Shared Function getCabecerasPorFiltro(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime) As List(Of VentasDevoluciones)
        Try
            Return DatVentas.getCabecerasParaFiltro(fechaInicio, fechaFin)
        Catch ex As Exception
            MsgBox("Error al recuperar las cabeceras de venta. Los errores producidos fueron: " + vbNewLine + ex.Message, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getDetallesVentasDev(ByVal idVenta As Integer)
        Try
            Return DatVentas.getDetallesParaDevPorId(idVenta)
        Catch ex As Exception
            MsgBox("Error al recuperar los detalles de venta. Los errores producidos fueron: " + vbNewLine + ex.Message, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getCabeceraParaDev() As List(Of VentasDevoluciones)
        Try
            Return DatVentas.getCabecerasParaDev()
        Catch ex As Exception
            MsgBox("Error al recuperar los datos para listar las ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getVentas(ByVal fechaInicio As DateTime, fechaFin As DateTime) As List(Of RepVentas)
        Try
            Return DatVentas.getVentas(fechaInicio, fechaFin)
        Catch ex As Exception
            MsgBox("Error al recuperar las ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Private Shared Function getUltimosIdVenta(ByVal idVenta As Integer) As List(Of Integer)
        Try
            Dim listaIdDetVentas As New List(Of Integer)
            For Each det In DatVentas.getDetallesIdVenta(idVenta)
                listaIdDetVentas.Add(det.idDetalle)
            Next
            If listaIdDetVentas.Count > 0 Then
                Return listaIdDetVentas
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los detalles de venta para la modificación de precios. Los erorres producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getMotivosModVentas() As List(Of MotivosModVentas)
        Try
            Return DatVentas.getMotivosModVentas()
        Catch ex As Exception
            MsgBox("Error al obtener la lista de motivos de modificación para ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Sub insertarCabeceraVenta(ByVal idUsuario As Integer, ByVal monto As Double)
        Try
            DatVentas.insertarCabeceraVenta(monto, idUsuario)
        Catch ex As Exception
            MsgBox("Error al insertar la cabecera de la factura de venta. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Public Shared Sub insertarCabeceraMod(ByVal monto As Double)
        Try
            DatVentas.insertarCabeceraModVenta(monto)
        Catch ex As Exception
            MsgBox("Error al insertar la cabecera de descuento para la venta. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Public Shared Sub insertarDetallesVenta(ByVal idCabecera As Integer, ByRef precio As List(Of Double), ByRef unidades As List(Of Integer), ByRef idProductos As List(Of Integer))
        Try
            Dim detalles As New List(Of DetallesVentas)
            Dim cantidadElementos As Integer
            Dim elementoAnalizado As Integer

            cantidadElementos = idProductos.LongCount()
            elementoAnalizado = 0
            If idCabecera <> 0 Then
                While elementoAnalizado < cantidadElementos
                    Dim detalle As New DetallesVentas
                    detalle.idVenta = idCabecera
                    'detalle.idDetalleCompra = elementoAnalizado + 1
                    detalle.precioDetalle = precio(elementoAnalizado)
                    detalle.cantidad = unidades(elementoAnalizado)
                    detalle.idProducto = idProductos(elementoAnalizado)
                    detalle.estado = True
                    detalles.Add(detalle)
                    elementoAnalizado = elementoAnalizado + 1
                End While
                DatVentas.insertarDetallesVentas(detalles)
            Else
                MsgBox("Error al insertar los detalles de ventas. Hubo problemas en recuperar el identificador de la cabecera de compra", vbOKOnly + vbCritical, "Error de recuperación de datos")
            End If
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de ventas. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub insertarDetallesMod(ByRef idCabeceraVenta As Integer, ByVal idCabeceraMod As Integer, ByRef precioMod As List(Of Double), ByRef motivos As List(Of Integer), ByRef descripciones As List(Of String))
        Try

            Dim contexto As New CalabozoEntities
            Dim detalles As New List(Of DetallesModVentas)
            Dim listaIdDetallesVenta As New List(Of Integer)
            Dim cantidadElementos As Integer
            Dim elementoAnalizado As Integer

            cantidadElementos = precioMod.LongCount()
            listaIdDetallesVenta = BusVentas.getUltimosIdVenta(idCabeceraVenta)
            elementoAnalizado = 0
            If idCabeceraVenta <> 0 And idCabeceraMod <> 0 Then
                While elementoAnalizado < cantidadElementos
                    Dim detalle As New DetallesModVentas
                    detalle.idModVentas = idCabeceraMod
                    detalle.idVenta = idCabeceraVenta
                    detalle.idDetalleVenta = listaIdDetallesVenta(elementoAnalizado)
                    detalle.monto = precioMod(elementoAnalizado)
                    detalle.idMotivo = motivos(elementoAnalizado)
                    detalle.estado = True
                    detalle.descModVentas = descripciones(elementoAnalizado)
                    detalles.Add(detalle)
                    elementoAnalizado = elementoAnalizado + 1
                End While
                DatVentas.insertarDetallesMod(detalles)
            Else
                MsgBox("Error al insertar los detalles de compras. Hubo problemas en recuperar el identificador de la cabecera de compra", vbOKOnly + vbCritical, "Error de recuperación de datos")
            End If
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub
    Public Shared Sub actualizarStockPorVenta(ByRef idProductos As List(Of Integer), ByRef unidadesVendidas As List(Of Integer))
        Try
            Dim cantidadProd As Integer
            Dim elementoAnalizado As Integer

            cantidadProd = idProductos.Count
            elementoAnalizado = 0
            While elementoAnalizado < cantidadProd
                DatVentas.actualizarStockPorVenta(idProductos(elementoAnalizado), unidadesVendidas(elementoAnalizado))
                elementoAnalizado = elementoAnalizado + 1
            End While
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function getUltimoIdCabecera() As Integer
        Try
            Return DatVentas.getUltimoIdCabecera()

        Catch ex As Exception
            MsgBox("Error al obtener la última factura de venta. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return -1
        End Try
    End Function

    Public Shared Function getUltimoIdCabeceraMod() As Integer
        Try
            Return DatVentas.getUltimaCabeceraMod()

        Catch ex As Exception
            MsgBox("Error al obtener el último registro para asentar las modificaciones en los precios de ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return -1
        End Try
    End Function
End Class
