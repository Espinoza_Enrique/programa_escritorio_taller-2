﻿Imports Entities
Imports DataAccess
Public Class BusCategorias
    Public Shared Function modificarDatosSubcategoria(ByVal idSub As Integer, ByVal nombreSub As String, ByVal descripcion As String, ByVal soloDescripcion As Boolean) As String
        Dim mensaje As String
        Dim repetidos As Boolean
        mensaje = ""
        If Not soloDescripcion Then
            repetidos = DatCategorias.controlarRepetidosSub(nombreSub)
        End If
        If repetidos Then
            mensaje = mensaje + "Ya existe una categoría con el nombre: " + nombreSub + "."
        Else
            DatCategorias.modificarSubcategoria(idSub, nombreSub, descripcion, soloDescripcion)
        End If
        Return mensaje
    End Function
    Public Shared Function modificarDatosCategoria(ByVal idCategoria As Integer, ByVal nombreCat As String, ByVal descripcion As String, ByVal soloDescripcion As Boolean) As String
        Dim mensaje As String
        Dim repetidos As Boolean
        mensaje = ""
        If Not soloDescripcion Then
            repetidos = DatCategorias.controlarRepetidosCategoria(nombreCat)
        End If
        If repetidos Then
            mensaje = mensaje + "Ya existe una categoría con el nombre: " + nombreCat + "."
        Else
            DatCategorias.modificarCategoria(idCategoria, nombreCat, descripcion, soloDescripcion)
        End If
        Return mensaje
    End Function
    Public Shared Function insertarNuevaCategoria(ByVal nombreCat As String, ByVal descripcion As String) As String
        Dim mensaje As String
        mensaje = ""
        If Not DatCategorias.controlarRepetidosCategoria(nombreCat) Then
            DatCategorias.insertarCategoria(nombreCat, descripcion)
            Return mensaje
        Else
            mensaje = mensaje + "Ya existe una categoría con el nombre: " + nombreCat + vbNewLine
            Return mensaje
        End If
    End Function

    Public Shared Function insertarNuevaSubcategoria(ByVal nombreSub As String, ByVal descripcion As String) As String
        Dim mensaje As String
        mensaje = ""
        If Not DatCategorias.controlarRepetidosSub(nombreSub) Then
            DatCategorias.insertarSubcategoria(nombreSub, descripcion)
            Return mensaje
        Else
            mensaje = mensaje + "Ya existe una subcategoría con el nombre: " + nombreSub + vbNewLine
            Return mensaje
        End If
    End Function

    Public Shared Sub modificarEstadoCategoria(ByVal idCat As Integer, ByVal estado As Boolean)
        DatCategorias.modificarEstadoCategoria(idCat, estado)
    End Sub

    Public Shared Sub modificarEstadoSubcategoria(ByVal idSub As Integer, ByVal estado As Boolean)
        DatCategorias.modificarEstadoSub(idSub, estado)
    End Sub
    Public Shared Function getCategorias() As List(Of Categorias)
        Return DatCategorias.getCategorias()
    End Function

    Public Shared Function getSubcategorias() As List(Of Subcategorias)
        Return DatCategorias.getSubcategorias()
    End Function

    Public Shared Function getCategoriasSubcategorias() As List(Of CategoriasSubcategorias)
        Return DatCategorias.getCategoriasSubcategorias()
    End Function

    Public Shared Function getCategoriasActivas() As List(Of Categorias)
        Return DatCategorias.getCategoriasActivas()
    End Function

    Public Shared Function getSubCatActivas() As List(Of Subcategorias)
        Return DatCategorias.getSubActivas()
    End Function
    Public Shared Function controlarCatSubRepetidos(ByVal idCat As Integer, ByVal idSub As Integer) As String
        Try
            Dim mensaje As String
            Dim catsub As New CategoriasSubcategorias
            mensaje = ""
            catsub = DatCategorias.controlarCatSubRepetidos(idCat, idSub)
            If Not IsNothing(catsub) Then
                mensaje = mensaje + "Ya existe la relación entre: " + vbNewLine + catsub.Categorias.nombre + vbNewLine + "Subcategoria: " + catsub.Subcategorias.nombre
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar si hay relaciones categoría/subcategoría repetidas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error De Control")
            Return Nothing
        End Try
    End Function

    Public Shared Function insertarNuevaCatSub(ByVal idCat As Integer, ByVal idSub As Integer, ByVal descripcion As String) As String
        Try
            Dim mensaje = controlarCatSubRepetidos(idCat, idSub)
            If mensaje = "" Then
                DatCategorias.insertarNuevaCatSub(idCat, idSub, descripcion)
            End If
            Return mensaje

        Catch ex As Exception
            MsgBox("Error al insertar la nueva relación categoría/subcategoría.Los errores presentados fueron: " + ex.Message, vbOKOnly + vbCritical, "Error de ingreso de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Sub modificarEstadoCatSub(ByVal idCat As Integer, ByVal idSub As Integer, ByVal estado As Boolean)
        DatCategorias.modificarEstadoCatSub(idCat, idSub, estado)
    End Sub

    Public Shared Function modificarCatSub(ByVal idCat As Integer, ByVal idSub As Integer, ByVal descripcion As String, ByVal modificacion As Boolean) As String
        Try
            Dim mensaje As String
            Dim elemento As New Entities.CategoriasSubcategorias
            mensaje = ""
            If modificacion Then
                elemento = DatCategorias.controlarCatSubRepetidos(idCat, idSub)
                If IsNothing(elemento) Then
                    DatCategorias.modificarCatSub(idCat, idSub, descripcion, modificacion)
                Else
                    mensaje = " Ya existe una relación: " + vbNewLine + "Categoria: " + elemento.Categorias.nombre + vbNewLine + "Subcategoria: " + elemento.Subcategorias.nombre + vbNewLine
                End If
            Else
                DatCategorias.modificarCatSub(idCat, idSub, descripcion, modificacion)
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al querer modificar los datos de las relaciones categorias/subcategorias. Los errores presentados fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function
    Public Shared Function getCategoriasConSubcategorias() As List(Of Categorias)
        Dim categorias As List(Of Categorias)
        categorias = DatCategorias.getCategoriasConSubcategorias()
        'cantidadCatRepetidas = categorias.Count()
        'elementoAnalizado = 0
        'elementoSalteado = 0
        'categoriasNoRepetidas.Add(categorias.Item(elementoAnalizado))
        'While elementoAnalizado < cantidadCatRepetidas
        'If categoriasNoRepetidas.Item(elementoSalteado).idCategoria = categorias.Item(elementoAnalizado).idCategoria Then
        'elementoAnalizado = elementoAnalizado + 1
        'Else
        'categoriasNoRepetidas.Add(categorias.Item(elementoAnalizado))
        'elementoSalteado = elementoSalteado + 1
        'elementoAnalizado = elementoAnalizado + 1
        'End If
        'End While
        Return categorias

    End Function
    Public Shared Function getSubcatPorCategorias(ByVal idCategoria As Integer) As List(Of Subcategorias)
        Dim subcategorias As New List(Of Subcategorias)
        Dim listaCatSub As New List(Of CategoriasSubcategorias)
        listaCatSub = DatCategorias.getSubcatPorCategorias(idCategoria)
        If Not IsNothing(listaCatSub) Then
            For Each subcat In listaCatSub
                subcategorias.Add(subcat.Subcategorias)
            Next
            Return subcategorias
        Else
            Return Nothing
        End If


    End Function
End Class
