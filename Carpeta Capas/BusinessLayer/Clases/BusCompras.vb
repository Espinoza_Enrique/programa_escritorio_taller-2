﻿Imports Entities
Imports DataAccess
Public Class BusCompras
    Public Shared Sub insertarCabeceraCompra(ByVal montoTotal As Double, ByVal idUsuario As Integer)
        Try
            DatCompras.insertarCabeceraCompra(montoTotal, idUsuario)
        Catch ex As Exception
            MsgBox("Error al colocar la cabecera de la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub insertarDetallesCompra(ByRef precio As List(Of Double), ByRef unidades As List(Of Integer), ByRef idProductos As List(Of Integer), ByRef idProveedores As List(Of Integer))
        Try

            Dim contexto As New CalabozoEntities
            Dim detalles As New List(Of DetallesCompras)
            Dim cantidadElementos As Integer
            Dim elementoAnalizado As Integer
            Dim idCabecera As Integer

            cantidadElementos = idProductos.LongCount()
            elementoAnalizado = 0
            idCabecera = DatCompras.getUltimaCabecera()
            If idCabecera <> 0 Then
                While elementoAnalizado < cantidadElementos
                    Dim detalle As New DetallesCompras
                    detalle.idCompra = idCabecera
                    detalle.idDetalleCompra = elementoAnalizado + 1
                    detalle.precioCompra = precio(elementoAnalizado)
                    detalle.unidadesCompra = unidades(elementoAnalizado)
                    detalle.idProducto = idProductos(elementoAnalizado)
                    detalle.idProveedor = idProveedores(elementoAnalizado)
                    detalle.estado = True
                    detalles.Add(detalle)
                    elementoAnalizado = elementoAnalizado + 1
                End While
                DatCompras.insertarDetallesCompras(detalles)
            Else
                MsgBox("Error al insertar los detalles de compras. Hubo problemas en recuperar el identificador de la cabecera de compra", vbOKOnly + vbCritical, "Error de recuperación de datos")
            End If
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub insertarDetallesMod(ByRef precioMod As List(Of Double), ByRef motivos As List(Of Integer))
        Try

            Dim contexto As New CalabozoEntities
            Dim detalles As New List(Of DetallesModCompras)
            Dim cantidadElementos As Integer
            Dim elementoAnalizado As Integer
            Dim idCabeceraCompra As Integer
            Dim idCabeceraMod As Integer
            cantidadElementos = precioMod.LongCount()
            elementoAnalizado = 0
            idCabeceraCompra = DatCompras.getUltimaCabecera()
            idCabeceraMod = DatCompras.getUltimaCabeceraMod()
            If idCabeceraCompra <> 0 And idCabeceraMod <> 0 Then
                While elementoAnalizado < cantidadElementos
                    Dim detalle As New DetallesModCompras
                    detalle.idCabeceraModCompras = idCabeceraMod
                    detalle.idCompra = idCabeceraCompra
                    detalle.idDetalleCompra = elementoAnalizado + 1
                    detalle.montoDescuento = precioMod(elementoAnalizado)
                    detalle.idMotivo = motivos(elementoAnalizado)
                    detalle.fechaModDetalle = Date.Now()
                    detalle.estado = True
                    detalles.Add(detalle)
                    elementoAnalizado = elementoAnalizado + 1
                End While
                DatCompras.insertarDetallesMod(detalles)
            Else
                MsgBox("Error al insertar los detalles de compras. Hubo problemas en recuperar el identificador de la cabecera de compra", vbOKOnly + vbCritical, "Error de recuperación de datos")
            End If
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Function getMotivosModCompras() As List(Of MotivosModCompras)
        Try
            Return DatCompras.getMotivosModCompras()
        Catch ex As Exception
            MsgBox("Error al obtener los motivos de modificación de precios en la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperción de datos")
            Return Nothing
        End Try
    End Function
End Class
