﻿Imports Entities
Imports DataAccess
Public Class BusEmpleados
    Public Shared Function getEmpleadoPorId(ByVal idEmpleado As Integer)
        Return DatEmpleados.getEmpleadoPorId(idEmpleado)
    End Function
    Public Shared Sub modificarEmpleadoSinDni(ByVal idEmpleado As Integer, ByVal apellido As String, ByVal nombre As String, ByVal telefono As String, ByVal mail As String)
        DatEmpleados.modificarDatosEmpleadoSinDni(idEmpleado, apellido, nombre, telefono, mail)
    End Sub
    Public Shared Function getEmpleados() As List(Of Empleados)
        Return DatEmpleados.getEmpleados()
    End Function

    Public Shared Sub agregarNuevoEmpleado(ByVal apellido As String, ByVal nombre As String, ByVal dni As Integer, ByVal telefono As String, ByVal mail As String)
        DatEmpleados.agregarNuevoEmpleado(apellido, nombre, dni, telefono, mail)
    End Sub

    Public Shared Sub modificarDatosEmpleado(ByVal idEmpleado As Integer, ByVal apellido As String, ByVal nombre As String, ByVal dni As Integer, ByVal telefono As String, ByVal mail As String)
        DatEmpleados.modificarDatosEmpleado(idEmpleado, apellido, nombre, dni, telefono, mail)
    End Sub

    Public Shared Sub cambiarEstadoEmpleado(ByVal idEmpleado As Integer, ByVal estado As String)
        If estado = "Inactivo" Then
            DatEmpleados.altaEmpleado(idEmpleado)
        Else
            DatEmpleados.bajaEmpleado(idEmpleado)
        End If
    End Sub

    Public Shared Function controlarDniRepetido(ByVal dni As Integer)
        Return DatEmpleados.controlarDni(dni)
    End Function
End Class
