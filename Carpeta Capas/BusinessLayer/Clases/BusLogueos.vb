﻿Imports Entities
Imports DataAccess
Public Class BusLogueos
    Public Shared Function nuevoLogueo(ByVal idUsuario As Integer) As Integer
        Return DatLogueos.insertarNuevoLogueo(idUsuario)
    End Function

    Public Shared Function idLogueoSesionActual() As Integer
        Return DatLogueos.siguienteIdLogueo()
    End Function
End Class
