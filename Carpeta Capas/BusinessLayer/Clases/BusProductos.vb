﻿Imports Entities
Imports DataAccess
Public Class BusProductos

    Public Shared Function getMotivosModStock() As List(Of MotivosModStock)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From mot In contexto.MotivosModStock Where mot.estado = True Select mot
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If

        Catch ex As Exception
            MsgBox("Error al obtener los motivos de modificación de precio. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de obtención de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function getMotivosModPrecio() As List(Of MotivosModPrecio)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From mot In contexto.MotivosModPrecio Where mot.estado = True Select mot
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If

        Catch ex As Exception
            MsgBox("Error al obtener los motivos de modificación de precio. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de obtención de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarPrecio(ByVal idProd As Integer, ByVal nuevoPrecio As Double, ByVal motivo As Integer, ByVal descripcion As String, ByVal dni As Integer, ByVal empleado As String) As String
        Try
            Dim mensaje As String
            Dim correcto As Boolean

            mensaje = ""
            correcto = DatProductos.modificarPrecio(idProd, nuevoPrecio, motivo, descripcion, dni, empleado)
            If correcto Then
                mensaje = mensaje + "Modificación De Precio Exitosa" + vbNewLine
            Else
                mensaje = mensaje + "Error al modificar el precio del producto" + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al modificar el precio del producto. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de modificación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarStock(ByVal idProd As Integer, ByVal nuevoStock As Integer, ByVal motivo As Integer, ByVal descripcion As String, ByVal dni As Integer, ByVal empleado As String) As String
        Try
            Dim mensaje As String
            Dim correcto As Boolean

            mensaje = ""
            correcto = DatProductos.modificarStock(idProd, nuevoStock, motivo, descripcion, dni, empleado)
            If correcto Then
                mensaje = mensaje + "Modificación De Stock Exitosa" + vbNewLine
            Else
                mensaje = mensaje + "Error al modificar el stock del producto" + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al modificar el stock del producto. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de modificación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Sub modificarEstadoProducto(ByVal idProd As Integer, ByVal estado As Boolean)
        DatProductos.modificarEstadoProducto(idProd, estado)
    End Sub
    Public Shared Function modificarProducto(ByVal idProd As Integer, ByVal nombre As String, ByVal idCat As Integer, ByVal idSub As Integer, ByVal stockMin As Integer, ByVal garantia As Integer, ByVal descripcion As String, ByVal nombreFoto As String, ByVal modNombre As Boolean, ByVal modCatSub As Boolean, ByVal habModCatSub As Boolean, ByVal cambiarFoto As Boolean) As String
        Try
            Dim prodRepetido As Boolean
            Dim mensaje As String

            mensaje = ""
            prodRepetido = True
            If modNombre Then
                prodRepetido = DatProductos.controlarNombreRepetido(nombre)
            End If

            If Not prodRepetido Then
                mensaje = mensaje + "Ya existe un producto con el nombre indicado."
            End If

            If mensaje = "" Then
                DatProductos.modificarProducto(idProd, nombre, idCat, idSub, stockMin, garantia, descripcion, nombreFoto, modNombre, modCatSub, habModCatSub, cambiarFoto)
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al modificar los datos del producto. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al modificar datos")
            Return Nothing
        End Try
    End Function

    Public Shared Function insertarProducto(ByVal nombre As String, ByVal idCat As Integer, ByVal idSub As Integer, ByVal precio As Double, ByVal stockMin As Integer, ByVal garantia As Integer, ByVal nombreFoto As String, ByVal descripcion As String) As String
        Try
            Dim respuesta As String
            Dim prodRepetido As Boolean
            respuesta = ""
            prodRepetido = DatProductos.controlarNombreRepetido(nombre)
            If Not prodRepetido And Not IsNothing(prodRepetido) Then
                DatProductos.insertarProducto(nombre, idCat, idSub, precio, stockMin, garantia, nombreFoto, descripcion)
            Else
                respuesta = respuesta + "Ya existe un producto con el nombre: " + vbNewLine + nombre
            End If
            Return respuesta
        Catch ex As Exception
            MsgBox("Error al acceder a la base de datos para guardar datos. Los errores producidos son: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function getProductos() As List(Of Productos)
        Return DatProductos.getProductos()
    End Function
End Class
