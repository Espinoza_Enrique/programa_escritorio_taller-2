﻿Imports Entities
Public Class DatCompras

    Public Shared Sub insertarCabeceraModCompra(ByVal montoTotal As Double)
        Try
            Dim contexto As New CalabozoEntities
            Dim cabecera As New CabeceraModCompras
            cabecera.fechaDesc = Date.Now()
            cabecera.montoTotal = montoTotal
            cabecera.estado = True
            contexto.CabeceraModCompras.Add(cabecera)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al colocar la cabecera de la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub insertarDetallesMod(ByRef detalles As List(Of DetallesModCompras))
        Try
            Dim contexto As New CalabozoEntities
            contexto.DetallesModCompras.AddRange(detalles)
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de modificación de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Function getUltimaCabeceraMod() As Integer
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From comp In contexto.CabeceraModCompras Select comp).ToList().LastOrDefault().idCabeceraModCompras
            Return consulta
        Catch ex As Exception
            MsgBox("Error al obtener la última cabecera de modificación insertada. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return 0
        End Try
    End Function
    Public Shared Sub insertarCabeceraCompra(ByVal montoTotal As Double, ByVal idUsuario As Integer)
        Try
            Dim contexto As New CalabozoEntities
            Dim cabecera As New ComprasProductos
            cabecera.precioTotal = montoTotal
            cabecera.idUsuarioCompra = idUsuario
            cabecera.estado = True
            cabecera.fechaCompra = Date.Now()
            contexto.ComprasProductos.Add(cabecera)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al colocar la cabecera de la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Function getUltimaCabecera() As Integer
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From comp In contexto.ComprasProductos Select comp).ToList().LastOrDefault().idCompra
            Return consulta
        Catch ex As Exception
            MsgBox("Error al obtener la última cabecera insertada. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return 0
        End Try
    End Function

    Public Shared Sub insertarDetallesCompras(ByRef detalles As List(Of DetallesCompras))
        Try
            Dim contexto As New CalabozoEntities
            contexto.DetallesCompras.AddRange(detalles)
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Function getDetallesPorIdCompra(ByVal idCompra As Integer) As List(Of DetallesCompras)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From det In contexto.DetallesCompras Where det.idCompra = idCompra Select det
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al recuperar dato")
            Return Nothing
        End Try
    End Function

    Public Shared Function getMotivosModCompras() As List(Of MotivosModCompras)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From mot In contexto.MotivosModCompras Where mot.estado = True Select mot Order By mot.idMotivoModCompras Descending
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If

        Catch ex As Exception
            MsgBox("Error al obtener los motivos de modificación de precios en la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperción de datos")
            Return Nothing
        End Try
    End Function
End Class
