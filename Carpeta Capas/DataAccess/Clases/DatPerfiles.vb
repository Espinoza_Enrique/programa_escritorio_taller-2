﻿Imports Entities
Public Class DatPerfiles
    Public Shared Function getPerfiles() As List(Of Perfiles)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From per In contexto.Perfiles Select per Order By per.descripcionPerfil
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar la información de perfiles. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al recuperar datos")
            Return Nothing
        End Try
    End Function
End Class
