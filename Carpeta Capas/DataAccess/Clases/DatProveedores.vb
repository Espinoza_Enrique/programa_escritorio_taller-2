﻿Imports Entities
Public Class DatProveedores
    Public Shared Function getProveedores() As List(Of Proveedores)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From prov In contexto.Proveedores Select prov)
            Return consulta.ToList()
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los proveedores. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarEstado(ByVal idProv As Integer, ByVal estado As Boolean) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim provMod As New Proveedores
            provMod = (From prov In contexto.Proveedores Where prov.idProveedor = idProv Select prov).Single()
            If Not IsNothing(provMod) Then
                provMod.estado = estado
                provMod.fechaModProv = Date.UtcNow()
                contexto.SaveChanges()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al cambiar el estado del proveedor de Activo a Inactivo, o viceversa. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de modificación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarProveedor(ByVal idProv As Integer, ByVal nombre As String, ByVal direccion As String, ByVal telefono As String, ByVal codArea As String, ByVal celular As String, ByVal mail As String, ByVal modNom As Boolean) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim provMod As New Proveedores
            provMod = (From prov In contexto.Proveedores Where prov.idProveedor = idProv Select prov).Single()
            If Not IsNothing(provMod) Then
                If modNom Then
                    provMod.nombreComercial = nombre
                Else
                    provMod.direccionProveedor = direccion
                    provMod.telefonoProv = telefono
                    provMod.celularProv = codArea + "-" + celular
                    provMod.mailProv = mail
                End If
                contexto.SaveChanges()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al modificar los datos del proveedor. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al modificar datos")
            Return Nothing
        End Try
    End Function
    Public Shared Sub insertarProveedores(ByVal nombre As String, ByVal direccion As String, ByVal telefono As String, ByVal codArea As String, ByVal celular As String, ByVal mail As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim nuevoProv As New Proveedores
            nuevoProv.nombreComercial = nombre
            nuevoProv.direccionProveedor = direccion
            nuevoProv.telefonoProv = telefono
            nuevoProv.celularProv = codArea + "-" + celular
            nuevoProv.mailProv = mail
            contexto.Proveedores.Add(nuevoProv)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al dar de alta al nuevo proveedor. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error en ingreso de datos")
        End Try
    End Sub
    Public Shared Function controlarNombreRepetido(ByVal nombre As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim proveedor As New Proveedores
            Dim consulta = From prov In contexto.Proveedores Where prov.nombreComercial = nombre Select prov

            If consulta.Count() > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al controlar si hay proveedores con el mismo nombre. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de control de datos")
            Return Nothing
        End Try
    End Function

    Public Shared Function getProveedoresActivos() As List(Of Proveedores)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From prov In contexto.Proveedores Where prov.estado = True Select prov).ToList()
            Return consulta
        Catch ex As Exception
            MsgBox("Error al recuperar los proveedores activos. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function
End Class
