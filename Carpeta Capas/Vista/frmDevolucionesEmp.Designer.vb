﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDevolucionesEmp
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDevolucionesEmp))
        Me.grbDevolucionesEmp = New System.Windows.Forms.GroupBox()
        Me.grbListaDevEmp = New System.Windows.Forms.GroupBox()
        Me.btnRealizarDevEmp = New System.Windows.Forms.Button()
        Me.dgvCarritoDev = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblCodDetVentaDev = New System.Windows.Forms.Label()
        Me.lblCodVentaDev = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbMotivoDevEmp = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnDevolverDev = New System.Windows.Forms.Button()
        Me.lblMontoDev = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nudCantidadDev = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblNomProdDev = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grbFiltrarDev = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnFiltrarFechaDev = New System.Windows.Forms.Button()
        Me.lblFechaFinDev = New System.Windows.Forms.Label()
        Me.lblFechaIniDev = New System.Windows.Forms.Label()
        Me.dtpFechaFinDevFil = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaIniDevFil = New System.Windows.Forms.DateTimePicker()
        Me.grbDetallesVentaAdm = New System.Windows.Forms.GroupBox()
        Me.btnAnteriorDetallesDevEmp = New System.Windows.Forms.Button()
        Me.btnSiguienteDetallesDevEmp = New System.Windows.Forms.Button()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbListaVentasAdm = New System.Windows.Forms.GroupBox()
        Me.btnAnteriorVentasDevEmp = New System.Windows.Forms.Button()
        Me.btnSiguienteVentasDevEmp = New System.Windows.Forms.Button()
        Me.dgvVentasDev = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbDevolucionesEmp.SuspendLayout()
        Me.grbListaDevEmp.SuspendLayout()
        CType(Me.dgvCarritoDev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nudCantidadDev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbFiltrarDev.SuspendLayout()
        Me.grbDetallesVentaAdm.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbListaVentasAdm.SuspendLayout()
        CType(Me.dgvVentasDev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grbDevolucionesEmp
        '
        Me.grbDevolucionesEmp.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grbDevolucionesEmp.AutoSize = True
        Me.grbDevolucionesEmp.BackColor = System.Drawing.Color.DarkGray
        Me.grbDevolucionesEmp.Controls.Add(Me.grbListaDevEmp)
        Me.grbDevolucionesEmp.Controls.Add(Me.GroupBox1)
        Me.grbDevolucionesEmp.Controls.Add(Me.grbFiltrarDev)
        Me.grbDevolucionesEmp.Controls.Add(Me.grbDetallesVentaAdm)
        Me.grbDevolucionesEmp.Controls.Add(Me.grbListaVentasAdm)
        Me.grbDevolucionesEmp.Location = New System.Drawing.Point(0, 0)
        Me.grbDevolucionesEmp.Name = "grbDevolucionesEmp"
        Me.grbDevolucionesEmp.Size = New System.Drawing.Size(1060, 665)
        Me.grbDevolucionesEmp.TabIndex = 0
        Me.grbDevolucionesEmp.TabStop = False
        Me.grbDevolucionesEmp.Text = "Devolución De Ventas"
        '
        'grbListaDevEmp
        '
        Me.grbListaDevEmp.BackColor = System.Drawing.Color.DodgerBlue
        Me.grbListaDevEmp.Controls.Add(Me.btnRealizarDevEmp)
        Me.grbListaDevEmp.Controls.Add(Me.dgvCarritoDev)
        Me.grbListaDevEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbListaDevEmp.Location = New System.Drawing.Point(6, 437)
        Me.grbListaDevEmp.Name = "grbListaDevEmp"
        Me.grbListaDevEmp.Size = New System.Drawing.Size(647, 209)
        Me.grbListaDevEmp.TabIndex = 8
        Me.grbListaDevEmp.TabStop = False
        Me.grbListaDevEmp.Text = "Lista Devoluciones Actuales"
        '
        'btnRealizarDevEmp
        '
        Me.btnRealizarDevEmp.Location = New System.Drawing.Point(531, 72)
        Me.btnRealizarDevEmp.Name = "btnRealizarDevEmp"
        Me.btnRealizarDevEmp.Size = New System.Drawing.Size(110, 72)
        Me.btnRealizarDevEmp.TabIndex = 4
        Me.btnRealizarDevEmp.Text = "Realizar Devoluciones"
        Me.btnRealizarDevEmp.UseVisualStyleBackColor = True
        '
        'dgvCarritoDev
        '
        Me.dgvCarritoDev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCarritoDev.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5})
        Me.dgvCarritoDev.Location = New System.Drawing.Point(21, 23)
        Me.dgvCarritoDev.Name = "dgvCarritoDev"
        Me.dgvCarritoDev.Size = New System.Drawing.Size(504, 179)
        Me.dgvCarritoDev.TabIndex = 1
        '
        'Column1
        '
        Me.Column1.HeaderText = "Producto"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Unidades Devueltas"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "Monto devuelto"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Código Compra"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Código Detalle"
        Me.Column5.Name = "Column5"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.SkyBlue
        Me.GroupBox1.Controls.Add(Me.lblCodDetVentaDev)
        Me.GroupBox1.Controls.Add(Me.lblCodVentaDev)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cmbMotivoDevEmp)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.btnDevolverDev)
        Me.GroupBox1.Controls.Add(Me.lblMontoDev)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.nudCantidadDev)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.lblNomProdDev)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(659, 206)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(395, 440)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Devolución"
        '
        'lblCodDetVentaDev
        '
        Me.lblCodDetVentaDev.AutoSize = True
        Me.lblCodDetVentaDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCodDetVentaDev.Location = New System.Drawing.Point(182, 172)
        Me.lblCodDetVentaDev.Name = "lblCodDetVentaDev"
        Me.lblCodDetVentaDev.Size = New System.Drawing.Size(204, 22)
        Me.lblCodDetVentaDev.TabIndex = 24
        Me.lblCodDetVentaDev.Text = "aquí va el código del detalle"
        '
        'lblCodVentaDev
        '
        Me.lblCodVentaDev.AutoSize = True
        Me.lblCodVentaDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCodVentaDev.Location = New System.Drawing.Point(193, 113)
        Me.lblCodVentaDev.Name = "lblCodVentaDev"
        Me.lblCodVentaDev.Size = New System.Drawing.Size(193, 22)
        Me.lblCodVentaDev.TabIndex = 23
        Me.lblCodVentaDev.Text = "aquí va el código de venta"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 172)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 20)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Número Detalle"
        '
        'cmbMotivoDevEmp
        '
        Me.cmbMotivoDevEmp.FormattingEnabled = True
        Me.cmbMotivoDevEmp.Location = New System.Drawing.Point(193, 292)
        Me.cmbMotivoDevEmp.Name = "cmbMotivoDevEmp"
        Me.cmbMotivoDevEmp.Size = New System.Drawing.Size(193, 28)
        Me.cmbMotivoDevEmp.TabIndex = 21
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 295)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(137, 20)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Motivo Devolución"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 115)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 20)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Número Venta"
        '
        'btnDevolverDev
        '
        Me.btnDevolverDev.Location = New System.Drawing.Point(114, 395)
        Me.btnDevolverDev.Name = "btnDevolverDev"
        Me.btnDevolverDev.Size = New System.Drawing.Size(176, 38)
        Me.btnDevolverDev.TabIndex = 17
        Me.btnDevolverDev.Text = "Agregar Devolución"
        Me.btnDevolverDev.UseVisualStyleBackColor = True
        '
        'lblMontoDev
        '
        Me.lblMontoDev.AutoSize = True
        Me.lblMontoDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMontoDev.Location = New System.Drawing.Point(147, 353)
        Me.lblMontoDev.Name = "lblMontoDev"
        Me.lblMontoDev.Size = New System.Drawing.Size(239, 22)
        Me.lblMontoDev.TabIndex = 16
        Me.lblMontoDev.Text = "Aquí mostrará el monto devuelto"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 355)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(134, 20)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Monto A Devolver"
        '
        'nudCantidadDev
        '
        Me.nudCantidadDev.Location = New System.Drawing.Point(193, 229)
        Me.nudCantidadDev.Name = "nudCantidadDev"
        Me.nudCantidadDev.Size = New System.Drawing.Size(193, 26)
        Me.nudCantidadDev.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 231)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(140, 20)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Cantidad Devuelta"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(650, 29)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 45)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Sólo Categoría/Subcategoria"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblNomProdDev
        '
        Me.lblNomProdDev.AutoSize = True
        Me.lblNomProdDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNomProdDev.Location = New System.Drawing.Point(157, 54)
        Me.lblNomProdDev.Name = "lblNomProdDev"
        Me.lblNomProdDev.Size = New System.Drawing.Size(229, 22)
        Me.lblNomProdDev.TabIndex = 4
        Me.lblNomProdDev.Text = "Aquí va el nombre del producto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 20)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nombre Producto"
        '
        'grbFiltrarDev
        '
        Me.grbFiltrarDev.BackColor = System.Drawing.Color.CadetBlue
        Me.grbFiltrarDev.Controls.Add(Me.Button3)
        Me.grbFiltrarDev.Controls.Add(Me.btnFiltrarFechaDev)
        Me.grbFiltrarDev.Controls.Add(Me.lblFechaFinDev)
        Me.grbFiltrarDev.Controls.Add(Me.lblFechaIniDev)
        Me.grbFiltrarDev.Controls.Add(Me.dtpFechaFinDevFil)
        Me.grbFiltrarDev.Controls.Add(Me.dtpFechaIniDevFil)
        Me.grbFiltrarDev.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltrarDev.Location = New System.Drawing.Point(659, 19)
        Me.grbFiltrarDev.Name = "grbFiltrarDev"
        Me.grbFiltrarDev.Size = New System.Drawing.Size(395, 181)
        Me.grbFiltrarDev.TabIndex = 6
        Me.grbFiltrarDev.TabStop = False
        Me.grbFiltrarDev.Text = "Opciones De Filtrado"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(650, 29)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(133, 45)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Sólo Categoría/Subcategoria"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnFiltrarFechaDev
        '
        Me.btnFiltrarFechaDev.Location = New System.Drawing.Point(157, 138)
        Me.btnFiltrarFechaDev.Name = "btnFiltrarFechaDev"
        Me.btnFiltrarFechaDev.Size = New System.Drawing.Size(75, 35)
        Me.btnFiltrarFechaDev.TabIndex = 6
        Me.btnFiltrarFechaDev.Text = "Filtrar"
        Me.btnFiltrarFechaDev.UseVisualStyleBackColor = True
        '
        'lblFechaFinDev
        '
        Me.lblFechaFinDev.AutoSize = True
        Me.lblFechaFinDev.Location = New System.Drawing.Point(152, 81)
        Me.lblFechaFinDev.Name = "lblFechaFinDev"
        Me.lblFechaFinDev.Size = New System.Drawing.Size(80, 20)
        Me.lblFechaFinDev.TabIndex = 4
        Me.lblFechaFinDev.Text = "Fecha Fin"
        '
        'lblFechaIniDev
        '
        Me.lblFechaIniDev.AutoSize = True
        Me.lblFechaIniDev.Location = New System.Drawing.Point(143, 22)
        Me.lblFechaIniDev.Name = "lblFechaIniDev"
        Me.lblFechaIniDev.Size = New System.Drawing.Size(95, 20)
        Me.lblFechaIniDev.TabIndex = 3
        Me.lblFechaIniDev.Text = "Fecha Inicio"
        '
        'dtpFechaFinDevFil
        '
        Me.dtpFechaFinDevFil.Location = New System.Drawing.Point(50, 104)
        Me.dtpFechaFinDevFil.Name = "dtpFechaFinDevFil"
        Me.dtpFechaFinDevFil.Size = New System.Drawing.Size(310, 26)
        Me.dtpFechaFinDevFil.TabIndex = 1
        '
        'dtpFechaIniDevFil
        '
        Me.dtpFechaIniDevFil.Location = New System.Drawing.Point(50, 45)
        Me.dtpFechaIniDevFil.Name = "dtpFechaIniDevFil"
        Me.dtpFechaIniDevFil.Size = New System.Drawing.Size(310, 26)
        Me.dtpFechaIniDevFil.TabIndex = 0
        '
        'grbDetallesVentaAdm
        '
        Me.grbDetallesVentaAdm.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.grbDetallesVentaAdm.Controls.Add(Me.btnAnteriorDetallesDevEmp)
        Me.grbDetallesVentaAdm.Controls.Add(Me.btnSiguienteDetallesDevEmp)
        Me.grbDetallesVentaAdm.Controls.Add(Me.DataGridView2)
        Me.grbDetallesVentaAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDetallesVentaAdm.Location = New System.Drawing.Point(6, 226)
        Me.grbDetallesVentaAdm.Name = "grbDetallesVentaAdm"
        Me.grbDetallesVentaAdm.Size = New System.Drawing.Size(647, 205)
        Me.grbDetallesVentaAdm.TabIndex = 5
        Me.grbDetallesVentaAdm.TabStop = False
        Me.grbDetallesVentaAdm.Text = "Detalles Venta Seleccionada"
        '
        'btnAnteriorDetallesDevEmp
        '
        Me.btnAnteriorDetallesDevEmp.Image = CType(resources.GetObject("btnAnteriorDetallesDevEmp.Image"), System.Drawing.Image)
        Me.btnAnteriorDetallesDevEmp.Location = New System.Drawing.Point(569, 126)
        Me.btnAnteriorDetallesDevEmp.Name = "btnAnteriorDetallesDevEmp"
        Me.btnAnteriorDetallesDevEmp.Size = New System.Drawing.Size(72, 72)
        Me.btnAnteriorDetallesDevEmp.TabIndex = 5
        Me.btnAnteriorDetallesDevEmp.UseVisualStyleBackColor = True
        '
        'btnSiguienteDetallesDevEmp
        '
        Me.btnSiguienteDetallesDevEmp.Image = CType(resources.GetObject("btnSiguienteDetallesDevEmp.Image"), System.Drawing.Image)
        Me.btnSiguienteDetallesDevEmp.Location = New System.Drawing.Point(569, 31)
        Me.btnSiguienteDetallesDevEmp.Name = "btnSiguienteDetallesDevEmp"
        Me.btnSiguienteDetallesDevEmp.Size = New System.Drawing.Size(72, 72)
        Me.btnSiguienteDetallesDevEmp.TabIndex = 4
        Me.btnSiguienteDetallesDevEmp.UseVisualStyleBackColor = True
        '
        'DataGridView2
        '
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.DataGridView2.Location = New System.Drawing.Point(21, 31)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(542, 167)
        Me.DataGridView2.TabIndex = 1
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Código Detalle"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Producto"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Descuento"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Precio Neto"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'grbListaVentasAdm
        '
        Me.grbListaVentasAdm.BackColor = System.Drawing.Color.Aquamarine
        Me.grbListaVentasAdm.Controls.Add(Me.btnAnteriorVentasDevEmp)
        Me.grbListaVentasAdm.Controls.Add(Me.btnSiguienteVentasDevEmp)
        Me.grbListaVentasAdm.Controls.Add(Me.dgvVentasDev)
        Me.grbListaVentasAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbListaVentasAdm.Location = New System.Drawing.Point(6, 19)
        Me.grbListaVentasAdm.Name = "grbListaVentasAdm"
        Me.grbListaVentasAdm.Size = New System.Drawing.Size(647, 201)
        Me.grbListaVentasAdm.TabIndex = 4
        Me.grbListaVentasAdm.TabStop = False
        Me.grbListaVentasAdm.Text = "Lista De Ventas"
        '
        'btnAnteriorVentasDevEmp
        '
        Me.btnAnteriorVentasDevEmp.Image = CType(resources.GetObject("btnAnteriorVentasDevEmp.Image"), System.Drawing.Image)
        Me.btnAnteriorVentasDevEmp.Location = New System.Drawing.Point(569, 122)
        Me.btnAnteriorVentasDevEmp.Name = "btnAnteriorVentasDevEmp"
        Me.btnAnteriorVentasDevEmp.Size = New System.Drawing.Size(72, 72)
        Me.btnAnteriorVentasDevEmp.TabIndex = 3
        Me.btnAnteriorVentasDevEmp.UseVisualStyleBackColor = True
        '
        'btnSiguienteVentasDevEmp
        '
        Me.btnSiguienteVentasDevEmp.Image = CType(resources.GetObject("btnSiguienteVentasDevEmp.Image"), System.Drawing.Image)
        Me.btnSiguienteVentasDevEmp.Location = New System.Drawing.Point(569, 31)
        Me.btnSiguienteVentasDevEmp.Name = "btnSiguienteVentasDevEmp"
        Me.btnSiguienteVentasDevEmp.Size = New System.Drawing.Size(72, 72)
        Me.btnSiguienteVentasDevEmp.TabIndex = 2
        Me.btnSiguienteVentasDevEmp.UseVisualStyleBackColor = True
        '
        'dgvVentasDev
        '
        Me.dgvVentasDev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVentasDev.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.dgvVentasDev.Location = New System.Drawing.Point(21, 31)
        Me.dgvVentasDev.Name = "dgvVentasDev"
        Me.dgvVentasDev.Size = New System.Drawing.Size(542, 164)
        Me.dgvVentasDev.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Código Venta"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha venta"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Vendedor"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Monto Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'frmDevolucionesEmp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1084, 671)
        Me.Controls.Add(Me.grbDevolucionesEmp)
        Me.Name = "frmDevolucionesEmp"
        Me.Text = "Devoluciones"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.grbDevolucionesEmp.ResumeLayout(False)
        Me.grbListaDevEmp.ResumeLayout(False)
        CType(Me.dgvCarritoDev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nudCantidadDev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbFiltrarDev.ResumeLayout(False)
        Me.grbFiltrarDev.PerformLayout()
        Me.grbDetallesVentaAdm.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbListaVentasAdm.ResumeLayout(False)
        CType(Me.dgvVentasDev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grbDevolucionesEmp As System.Windows.Forms.GroupBox
    Friend WithEvents grbDetallesVentaAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnAnteriorDetallesDevEmp As System.Windows.Forms.Button
    Friend WithEvents btnSiguienteDetallesDevEmp As System.Windows.Forms.Button
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents grbListaVentasAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnAnteriorVentasDevEmp As System.Windows.Forms.Button
    Friend WithEvents btnSiguienteVentasDevEmp As System.Windows.Forms.Button
    Friend WithEvents dgvVentasDev As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnDevolverDev As System.Windows.Forms.Button
    Friend WithEvents lblMontoDev As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nudCantidadDev As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblNomProdDev As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grbFiltrarDev As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarFechaDev As System.Windows.Forms.Button
    Friend WithEvents lblFechaFinDev As System.Windows.Forms.Label
    Friend WithEvents lblFechaIniDev As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFinDevFil As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaIniDevFil As System.Windows.Forms.DateTimePicker
    Friend WithEvents grbListaDevEmp As System.Windows.Forms.GroupBox
    Friend WithEvents btnRealizarDevEmp As System.Windows.Forms.Button
    Friend WithEvents dgvCarritoDev As System.Windows.Forms.DataGridView
    Friend WithEvents lblCodVentaDev As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbMotivoDevEmp As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblCodDetVentaDev As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
