﻿
Public Class frmDevolucionesEmp

    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'control-Tipo: TabControl-Representa el tab control cuyas tab pages se quiere seleccionar
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de devoluciones:" + vbNewLine + ex.Message, vbOKOnly, "Error De Carga")
        End Try
    End Sub
    Private Sub frmDevolucionesEmp_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al cerrar la ventana de devoluciones:" + vbNewLine + ex.Message, vbOKOnly, "Error De Cierre De Ventana")
        End Try
    End Sub

    Private Sub frmDevolucionesEmp_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.prepararFormulario()
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub prepararFormulario()
        Try

            Me.btnAnteriorDetallesDevEmp.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.btnAnteriorVentasDevEmp.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.cmbMotivoDevEmp.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cargarMotivosDev(Me.cmbMotivoDevEmp)
        Catch ex As Exception
            MsgBox("Error al dar formato a la ventana de devoluciones:" + vbNewLine + ex.Message, vbOKOnly, "Error De Formato De Tabla")
        End Try
    End Sub



    Public Function controlarDatosDev() As String
        Dim mensaje As String
        Try
            mensaje = ""
            If Me.nudCantidadDev.Value <= 0 Then
                mensaje = mensaje + "Por favor que la cantidad devuelta sea mayor a cero" + vbNewLine
            End If

            If Me.cmbMotivoDevEmp.Text = "-" Or IsNothing(Me.cmbMotivoDevEmp.Text) Then
                mensaje = mensaje + "Por favor seleccione un motivo para la devolución" + vbNewLine
            End If

            If Not General.controlarEnterosPos(Me.lblCodVentaDev.Text) Or Not General.controlarEnterosPos(Me.lblCodDetVentaDev.Text) Then
                mensaje = mensaje + "Seleccione por favor una venta y un detalle. Si está en el prototipo, siempre aparecerá este error debido a la falta de datos." + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar datos de devolución:" + vbNewLine + ex.Message, vbOKOnly, "Error De Control")
            Return ""
        End Try
    End Function

    Private Sub btnDevolverDev_Click(sender As Object, e As EventArgs) Handles btnDevolverDev.Click
        Dim mensaje As String
        mensaje = Me.controlarDatosDev()
        If mensaje = "" Then
            MsgBox("Datos De Devolución Correctos", vbOKOnly, "Datos Correctos")
        Else
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly, "Datos Correctos")
        End If
    End Sub

    Private Sub cargarMotivosDev(ByRef motivos As ComboBox)
        Try
            motivos.Items.Add("-")
            motivos.Items.Add("Rotura")
            motivos.Items.Add("Defectuoso")
            motivos.Items.Add("No cumple las expectativas")
            motivos.Items.Add("Peligroso o inapropiado")
            motivos.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al cargar motivos de devoluciones: " + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
End Class