﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGestionarDevolucionesAdm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGestionarDevolucionesAdm))
        Me.tbcDevolucionesAdm = New System.Windows.Forms.TabControl()
        Me.tpgEstadisticaDevAdm = New System.Windows.Forms.TabPage()
        Me.btnFiltrarMotivoDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarSubAnioDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarSubMesDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarCatAnioDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarCatMesDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarAnioDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarMesDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarSubDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltrarCatDevAdm = New System.Windows.Forms.Button()
        Me.grbFiltrarDev = New System.Windows.Forms.GroupBox()
        Me.btnFiltradoCatSubDevAdm = New System.Windows.Forms.Button()
        Me.btnFiltradoFechaDevAdm = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbSubFiltradoDevAdm = New System.Windows.Forms.ComboBox()
        Me.cmbCatFiltradoDevAdm = New System.Windows.Forms.ComboBox()
        Me.btnFiltradoTodoDevAdm = New System.Windows.Forms.Button()
        Me.lblFechaFinDev = New System.Windows.Forms.Label()
        Me.lblFechaIniDev = New System.Windows.Forms.Label()
        Me.dtpFechaFinDevAdm = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaInicioDevAdm = New System.Windows.Forms.DateTimePicker()
        Me.chtEstadisticasDevAdm = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.tpgListaDevAdm = New System.Windows.Forms.TabPage()
        Me.grbInformacionDev = New System.Windows.Forms.GroupBox()
        Me.lblTituloDetallesDev = New System.Windows.Forms.Label()
        Me.txbDetDevList = New System.Windows.Forms.TextBox()
        Me.lblDatosMontoDev = New System.Windows.Forms.Label()
        Me.lblTituloPrecioDev = New System.Windows.Forms.Label()
        Me.lblDatosMotivoDev = New System.Windows.Forms.Label()
        Me.lblTituloMotivoDev = New System.Windows.Forms.Label()
        Me.lblNomProdDevList = New System.Windows.Forms.Label()
        Me.lblTituloProdDev = New System.Windows.Forms.Label()
        Me.lblDatosCodDetalle = New System.Windows.Forms.Label()
        Me.lblTituloCodDetalle = New System.Windows.Forms.Label()
        Me.lblMontoDevList = New System.Windows.Forms.Label()
        Me.lblTituloMontoTotalDev = New System.Windows.Forms.Label()
        Me.lblDatosFechaDev = New System.Windows.Forms.Label()
        Me.lblTituloFechaDev = New System.Windows.Forms.Label()
        Me.lblDatosCodDev = New System.Windows.Forms.Label()
        Me.lblTituloCodDev = New System.Windows.Forms.Label()
        Me.grbDetallesDevAdm = New System.Windows.Forms.GroupBox()
        Me.btnSigDetallesDev = New System.Windows.Forms.Button()
        Me.btnAntDetallesDev = New System.Windows.Forms.Button()
        Me.dgvDetallesDevAdm = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbDevListAdm = New System.Windows.Forms.GroupBox()
        Me.btnSiguienteDev = New System.Windows.Forms.Button()
        Me.btnAnteriorDev = New System.Windows.Forms.Button()
        Me.dgvDevAdm = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbcDevolucionesAdm.SuspendLayout()
        Me.tpgEstadisticaDevAdm.SuspendLayout()
        Me.grbFiltrarDev.SuspendLayout()
        CType(Me.chtEstadisticasDevAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgListaDevAdm.SuspendLayout()
        Me.grbInformacionDev.SuspendLayout()
        Me.grbDetallesDevAdm.SuspendLayout()
        CType(Me.dgvDetallesDevAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDevListAdm.SuspendLayout()
        CType(Me.dgvDevAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbcDevolucionesAdm
        '
        Me.tbcDevolucionesAdm.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbcDevolucionesAdm.Controls.Add(Me.tpgEstadisticaDevAdm)
        Me.tbcDevolucionesAdm.Controls.Add(Me.tpgListaDevAdm)
        Me.tbcDevolucionesAdm.Location = New System.Drawing.Point(2, 12)
        Me.tbcDevolucionesAdm.Name = "tbcDevolucionesAdm"
        Me.tbcDevolucionesAdm.SelectedIndex = 0
        Me.tbcDevolucionesAdm.Size = New System.Drawing.Size(1192, 635)
        Me.tbcDevolucionesAdm.TabIndex = 0
        '
        'tpgEstadisticaDevAdm
        '
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarMotivoDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarSubAnioDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarSubMesDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarCatAnioDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarCatMesDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarAnioDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarMesDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarSubDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.btnFiltrarCatDevAdm)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.grbFiltrarDev)
        Me.tpgEstadisticaDevAdm.Controls.Add(Me.chtEstadisticasDevAdm)
        Me.tpgEstadisticaDevAdm.Location = New System.Drawing.Point(4, 22)
        Me.tpgEstadisticaDevAdm.Name = "tpgEstadisticaDevAdm"
        Me.tpgEstadisticaDevAdm.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgEstadisticaDevAdm.Size = New System.Drawing.Size(1184, 609)
        Me.tpgEstadisticaDevAdm.TabIndex = 0
        Me.tpgEstadisticaDevAdm.Text = "Estadisticas Devoluciones"
        Me.tpgEstadisticaDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarMotivoDevAdm
        '
        Me.btnFiltrarMotivoDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarMotivoDevAdm.Location = New System.Drawing.Point(1011, 353)
        Me.btnFiltrarMotivoDevAdm.Name = "btnFiltrarMotivoDevAdm"
        Me.btnFiltrarMotivoDevAdm.Size = New System.Drawing.Size(165, 44)
        Me.btnFiltrarMotivoDevAdm.TabIndex = 25
        Me.btnFiltrarMotivoDevAdm.Text = "Motivo"
        Me.btnFiltrarMotivoDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarSubAnioDevAdm
        '
        Me.btnFiltrarSubAnioDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarSubAnioDevAdm.Location = New System.Drawing.Point(840, 354)
        Me.btnFiltrarSubAnioDevAdm.Name = "btnFiltrarSubAnioDevAdm"
        Me.btnFiltrarSubAnioDevAdm.Size = New System.Drawing.Size(165, 44)
        Me.btnFiltrarSubAnioDevAdm.TabIndex = 24
        Me.btnFiltrarSubAnioDevAdm.Text = "Subcaterogia/Año"
        Me.btnFiltrarSubAnioDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarSubMesDevAdm
        '
        Me.btnFiltrarSubMesDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarSubMesDevAdm.Location = New System.Drawing.Point(669, 353)
        Me.btnFiltrarSubMesDevAdm.Name = "btnFiltrarSubMesDevAdm"
        Me.btnFiltrarSubMesDevAdm.Size = New System.Drawing.Size(165, 45)
        Me.btnFiltrarSubMesDevAdm.TabIndex = 23
        Me.btnFiltrarSubMesDevAdm.Text = "Subcaterogia/Mes"
        Me.btnFiltrarSubMesDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarCatAnioDevAdm
        '
        Me.btnFiltrarCatAnioDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarCatAnioDevAdm.Location = New System.Drawing.Point(1011, 268)
        Me.btnFiltrarCatAnioDevAdm.Name = "btnFiltrarCatAnioDevAdm"
        Me.btnFiltrarCatAnioDevAdm.Size = New System.Drawing.Size(165, 45)
        Me.btnFiltrarCatAnioDevAdm.TabIndex = 22
        Me.btnFiltrarCatAnioDevAdm.Text = "Categoria/Año"
        Me.btnFiltrarCatAnioDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarCatMesDevAdm
        '
        Me.btnFiltrarCatMesDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarCatMesDevAdm.Location = New System.Drawing.Point(840, 268)
        Me.btnFiltrarCatMesDevAdm.Name = "btnFiltrarCatMesDevAdm"
        Me.btnFiltrarCatMesDevAdm.Size = New System.Drawing.Size(165, 45)
        Me.btnFiltrarCatMesDevAdm.TabIndex = 21
        Me.btnFiltrarCatMesDevAdm.Text = "Categoria/Mes"
        Me.btnFiltrarCatMesDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarAnioDevAdm
        '
        Me.btnFiltrarAnioDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarAnioDevAdm.Location = New System.Drawing.Point(669, 268)
        Me.btnFiltrarAnioDevAdm.Name = "btnFiltrarAnioDevAdm"
        Me.btnFiltrarAnioDevAdm.Size = New System.Drawing.Size(165, 44)
        Me.btnFiltrarAnioDevAdm.TabIndex = 20
        Me.btnFiltrarAnioDevAdm.Text = "Por Año"
        Me.btnFiltrarAnioDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarMesDevAdm
        '
        Me.btnFiltrarMesDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarMesDevAdm.Location = New System.Drawing.Point(1011, 182)
        Me.btnFiltrarMesDevAdm.Name = "btnFiltrarMesDevAdm"
        Me.btnFiltrarMesDevAdm.Size = New System.Drawing.Size(165, 44)
        Me.btnFiltrarMesDevAdm.TabIndex = 19
        Me.btnFiltrarMesDevAdm.Text = "Por Mes"
        Me.btnFiltrarMesDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarSubDevAdm
        '
        Me.btnFiltrarSubDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarSubDevAdm.Location = New System.Drawing.Point(840, 182)
        Me.btnFiltrarSubDevAdm.Name = "btnFiltrarSubDevAdm"
        Me.btnFiltrarSubDevAdm.Size = New System.Drawing.Size(165, 44)
        Me.btnFiltrarSubDevAdm.TabIndex = 18
        Me.btnFiltrarSubDevAdm.Text = "Subcategoria"
        Me.btnFiltrarSubDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltrarCatDevAdm
        '
        Me.btnFiltrarCatDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarCatDevAdm.Location = New System.Drawing.Point(669, 182)
        Me.btnFiltrarCatDevAdm.Name = "btnFiltrarCatDevAdm"
        Me.btnFiltrarCatDevAdm.Size = New System.Drawing.Size(165, 44)
        Me.btnFiltrarCatDevAdm.TabIndex = 17
        Me.btnFiltrarCatDevAdm.Text = "Categorias"
        Me.btnFiltrarCatDevAdm.UseVisualStyleBackColor = True
        '
        'grbFiltrarDev
        '
        Me.grbFiltrarDev.BackColor = System.Drawing.Color.LightSkyBlue
        Me.grbFiltrarDev.Controls.Add(Me.btnFiltradoCatSubDevAdm)
        Me.grbFiltrarDev.Controls.Add(Me.btnFiltradoFechaDevAdm)
        Me.grbFiltrarDev.Controls.Add(Me.Label2)
        Me.grbFiltrarDev.Controls.Add(Me.Label1)
        Me.grbFiltrarDev.Controls.Add(Me.cmbSubFiltradoDevAdm)
        Me.grbFiltrarDev.Controls.Add(Me.cmbCatFiltradoDevAdm)
        Me.grbFiltrarDev.Controls.Add(Me.btnFiltradoTodoDevAdm)
        Me.grbFiltrarDev.Controls.Add(Me.lblFechaFinDev)
        Me.grbFiltrarDev.Controls.Add(Me.lblFechaIniDev)
        Me.grbFiltrarDev.Controls.Add(Me.dtpFechaFinDevAdm)
        Me.grbFiltrarDev.Controls.Add(Me.dtpFechaInicioDevAdm)
        Me.grbFiltrarDev.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltrarDev.Location = New System.Drawing.Point(6, 6)
        Me.grbFiltrarDev.Name = "grbFiltrarDev"
        Me.grbFiltrarDev.Size = New System.Drawing.Size(1170, 120)
        Me.grbFiltrarDev.TabIndex = 16
        Me.grbFiltrarDev.TabStop = False
        Me.grbFiltrarDev.Text = "Opciones De Filtrado"
        '
        'btnFiltradoCatSubDevAdm
        '
        Me.btnFiltradoCatSubDevAdm.Location = New System.Drawing.Point(812, 42)
        Me.btnFiltradoCatSubDevAdm.Name = "btnFiltradoCatSubDevAdm"
        Me.btnFiltradoCatSubDevAdm.Size = New System.Drawing.Size(133, 45)
        Me.btnFiltradoCatSubDevAdm.TabIndex = 12
        Me.btnFiltradoCatSubDevAdm.Text = "Sólo Categoría/Subcategoria"
        Me.btnFiltradoCatSubDevAdm.UseVisualStyleBackColor = True
        '
        'btnFiltradoFechaDevAdm
        '
        Me.btnFiltradoFechaDevAdm.Location = New System.Drawing.Point(603, 42)
        Me.btnFiltradoFechaDevAdm.Name = "btnFiltradoFechaDevAdm"
        Me.btnFiltradoFechaDevAdm.Size = New System.Drawing.Size(133, 45)
        Me.btnFiltradoFechaDevAdm.TabIndex = 11
        Me.btnFiltradoFechaDevAdm.Text = "Sólo Fecha"
        Me.btnFiltradoFechaDevAdm.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(308, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 18)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Subcategoría"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 18)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Categoría"
        '
        'cmbSubFiltradoDevAdm
        '
        Me.cmbSubFiltradoDevAdm.FormattingEnabled = True
        Me.cmbSubFiltradoDevAdm.Location = New System.Drawing.Point(409, 76)
        Me.cmbSubFiltradoDevAdm.Name = "cmbSubFiltradoDevAdm"
        Me.cmbSubFiltradoDevAdm.Size = New System.Drawing.Size(160, 26)
        Me.cmbSubFiltradoDevAdm.TabIndex = 8
        '
        'cmbCatFiltradoDevAdm
        '
        Me.cmbCatFiltradoDevAdm.FormattingEnabled = True
        Me.cmbCatFiltradoDevAdm.Location = New System.Drawing.Point(101, 77)
        Me.cmbCatFiltradoDevAdm.Name = "cmbCatFiltradoDevAdm"
        Me.cmbCatFiltradoDevAdm.Size = New System.Drawing.Size(176, 26)
        Me.cmbCatFiltradoDevAdm.TabIndex = 7
        '
        'btnFiltradoTodoDevAdm
        '
        Me.btnFiltradoTodoDevAdm.Location = New System.Drawing.Point(1008, 42)
        Me.btnFiltradoTodoDevAdm.Name = "btnFiltradoTodoDevAdm"
        Me.btnFiltradoTodoDevAdm.Size = New System.Drawing.Size(133, 45)
        Me.btnFiltradoTodoDevAdm.TabIndex = 6
        Me.btnFiltradoTodoDevAdm.Text = "Filtrar"
        Me.btnFiltradoTodoDevAdm.UseVisualStyleBackColor = True
        '
        'lblFechaFinDev
        '
        Me.lblFechaFinDev.AutoSize = True
        Me.lblFechaFinDev.Location = New System.Drawing.Point(377, 14)
        Me.lblFechaFinDev.Name = "lblFechaFinDev"
        Me.lblFechaFinDev.Size = New System.Drawing.Size(73, 18)
        Me.lblFechaFinDev.TabIndex = 4
        Me.lblFechaFinDev.Text = "Fecha Fin"
        '
        'lblFechaIniDev
        '
        Me.lblFechaIniDev.AutoSize = True
        Me.lblFechaIniDev.Location = New System.Drawing.Point(148, 11)
        Me.lblFechaIniDev.Name = "lblFechaIniDev"
        Me.lblFechaIniDev.Size = New System.Drawing.Size(87, 18)
        Me.lblFechaIniDev.TabIndex = 3
        Me.lblFechaIniDev.Text = "Fecha Inicio"
        '
        'dtpFechaFinDevAdm
        '
        Me.dtpFechaFinDevAdm.Location = New System.Drawing.Point(283, 32)
        Me.dtpFechaFinDevAdm.Name = "dtpFechaFinDevAdm"
        Me.dtpFechaFinDevAdm.Size = New System.Drawing.Size(286, 24)
        Me.dtpFechaFinDevAdm.TabIndex = 1
        '
        'dtpFechaInicioDevAdm
        '
        Me.dtpFechaInicioDevAdm.Location = New System.Drawing.Point(6, 32)
        Me.dtpFechaInicioDevAdm.Name = "dtpFechaInicioDevAdm"
        Me.dtpFechaInicioDevAdm.Size = New System.Drawing.Size(271, 24)
        Me.dtpFechaInicioDevAdm.TabIndex = 0
        '
        'chtEstadisticasDevAdm
        '
        ChartArea1.Name = "ChartArea1"
        Me.chtEstadisticasDevAdm.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.chtEstadisticasDevAdm.Legends.Add(Legend1)
        Me.chtEstadisticasDevAdm.Location = New System.Drawing.Point(6, 182)
        Me.chtEstadisticasDevAdm.Name = "chtEstadisticasDevAdm"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.chtEstadisticasDevAdm.Series.Add(Series1)
        Me.chtEstadisticasDevAdm.Size = New System.Drawing.Size(657, 379)
        Me.chtEstadisticasDevAdm.TabIndex = 15
        Me.chtEstadisticasDevAdm.Text = "Chart1"
        '
        'tpgListaDevAdm
        '
        Me.tpgListaDevAdm.Controls.Add(Me.grbInformacionDev)
        Me.tpgListaDevAdm.Controls.Add(Me.grbDetallesDevAdm)
        Me.tpgListaDevAdm.Controls.Add(Me.grbDevListAdm)
        Me.tpgListaDevAdm.Location = New System.Drawing.Point(4, 22)
        Me.tpgListaDevAdm.Name = "tpgListaDevAdm"
        Me.tpgListaDevAdm.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgListaDevAdm.Size = New System.Drawing.Size(1184, 609)
        Me.tpgListaDevAdm.TabIndex = 1
        Me.tpgListaDevAdm.Text = "Lista Devoluciones"
        Me.tpgListaDevAdm.UseVisualStyleBackColor = True
        '
        'grbInformacionDev
        '
        Me.grbInformacionDev.BackColor = System.Drawing.Color.DodgerBlue
        Me.grbInformacionDev.Controls.Add(Me.lblTituloDetallesDev)
        Me.grbInformacionDev.Controls.Add(Me.txbDetDevList)
        Me.grbInformacionDev.Controls.Add(Me.lblDatosMontoDev)
        Me.grbInformacionDev.Controls.Add(Me.lblTituloPrecioDev)
        Me.grbInformacionDev.Controls.Add(Me.lblDatosMotivoDev)
        Me.grbInformacionDev.Controls.Add(Me.lblTituloMotivoDev)
        Me.grbInformacionDev.Controls.Add(Me.lblNomProdDevList)
        Me.grbInformacionDev.Controls.Add(Me.lblTituloProdDev)
        Me.grbInformacionDev.Controls.Add(Me.lblDatosCodDetalle)
        Me.grbInformacionDev.Controls.Add(Me.lblTituloCodDetalle)
        Me.grbInformacionDev.Controls.Add(Me.lblMontoDevList)
        Me.grbInformacionDev.Controls.Add(Me.lblTituloMontoTotalDev)
        Me.grbInformacionDev.Controls.Add(Me.lblDatosFechaDev)
        Me.grbInformacionDev.Controls.Add(Me.lblTituloFechaDev)
        Me.grbInformacionDev.Controls.Add(Me.lblDatosCodDev)
        Me.grbInformacionDev.Controls.Add(Me.lblTituloCodDev)
        Me.grbInformacionDev.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbInformacionDev.Location = New System.Drawing.Point(664, 6)
        Me.grbInformacionDev.Name = "grbInformacionDev"
        Me.grbInformacionDev.Size = New System.Drawing.Size(517, 600)
        Me.grbInformacionDev.TabIndex = 17
        Me.grbInformacionDev.TabStop = False
        Me.grbInformacionDev.Text = "Información Devolución"
        '
        'lblTituloDetallesDev
        '
        Me.lblTituloDetallesDev.AutoSize = True
        Me.lblTituloDetallesDev.Location = New System.Drawing.Point(6, 364)
        Me.lblTituloDetallesDev.Name = "lblTituloDetallesDev"
        Me.lblTituloDetallesDev.Size = New System.Drawing.Size(139, 18)
        Me.lblTituloDetallesDev.TabIndex = 15
        Me.lblTituloDetallesDev.Text = "Detalles Devolución"
        '
        'txbDetDevList
        '
        Me.txbDetDevList.Location = New System.Drawing.Point(6, 385)
        Me.txbDetDevList.Multiline = True
        Me.txbDetDevList.Name = "txbDetDevList"
        Me.txbDetDevList.Size = New System.Drawing.Size(505, 209)
        Me.txbDetDevList.TabIndex = 14
        '
        'lblDatosMontoDev
        '
        Me.lblDatosMontoDev.AutoSize = True
        Me.lblDatosMontoDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDatosMontoDev.Location = New System.Drawing.Point(261, 311)
        Me.lblDatosMontoDev.Name = "lblDatosMontoDev"
        Me.lblDatosMontoDev.Size = New System.Drawing.Size(250, 20)
        Me.lblDatosMontoDev.TabIndex = 13
        Me.lblDatosMontoDev.Text = "Aquí va el monto devuelto por detalle"
        '
        'lblTituloPrecioDev
        '
        Me.lblTituloPrecioDev.AutoSize = True
        Me.lblTituloPrecioDev.Location = New System.Drawing.Point(6, 311)
        Me.lblTituloPrecioDev.Name = "lblTituloPrecioDev"
        Me.lblTituloPrecioDev.Size = New System.Drawing.Size(113, 18)
        Me.lblTituloPrecioDev.TabIndex = 12
        Me.lblTituloPrecioDev.Text = "Monto Devuelto"
        '
        'lblDatosMotivoDev
        '
        Me.lblDatosMotivoDev.AutoSize = True
        Me.lblDatosMotivoDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDatosMotivoDev.Location = New System.Drawing.Point(280, 268)
        Me.lblDatosMotivoDev.Name = "lblDatosMotivoDev"
        Me.lblDatosMotivoDev.Size = New System.Drawing.Size(231, 20)
        Me.lblDatosMotivoDev.TabIndex = 11
        Me.lblDatosMotivoDev.Text = "Aquí va el motivo de la devolución"
        '
        'lblTituloMotivoDev
        '
        Me.lblTituloMotivoDev.AutoSize = True
        Me.lblTituloMotivoDev.Location = New System.Drawing.Point(6, 268)
        Me.lblTituloMotivoDev.Name = "lblTituloMotivoDev"
        Me.lblTituloMotivoDev.Size = New System.Drawing.Size(53, 18)
        Me.lblTituloMotivoDev.TabIndex = 10
        Me.lblTituloMotivoDev.Text = "Motivo"
        '
        'lblNomProdDevList
        '
        Me.lblNomProdDevList.AutoSize = True
        Me.lblNomProdDevList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNomProdDevList.Location = New System.Drawing.Point(298, 221)
        Me.lblNomProdDevList.Name = "lblNomProdDevList"
        Me.lblNomProdDevList.Size = New System.Drawing.Size(213, 20)
        Me.lblNomProdDevList.TabIndex = 9
        Me.lblNomProdDevList.Text = "Aquí va el nombre del producto"
        '
        'lblTituloProdDev
        '
        Me.lblTituloProdDev.AutoSize = True
        Me.lblTituloProdDev.Location = New System.Drawing.Point(6, 221)
        Me.lblTituloProdDev.Name = "lblTituloProdDev"
        Me.lblTituloProdDev.Size = New System.Drawing.Size(69, 18)
        Me.lblTituloProdDev.TabIndex = 8
        Me.lblTituloProdDev.Text = "Producto"
        '
        'lblDatosCodDetalle
        '
        Me.lblDatosCodDetalle.AutoSize = True
        Me.lblDatosCodDetalle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDatosCodDetalle.Location = New System.Drawing.Point(324, 176)
        Me.lblDatosCodDetalle.Name = "lblDatosCodDetalle"
        Me.lblDatosCodDetalle.Size = New System.Drawing.Size(187, 20)
        Me.lblDatosCodDetalle.TabIndex = 7
        Me.lblDatosCodDetalle.Text = "Aquí va el código de detalle"
        '
        'lblTituloCodDetalle
        '
        Me.lblTituloCodDetalle.AutoSize = True
        Me.lblTituloCodDetalle.Location = New System.Drawing.Point(6, 176)
        Me.lblTituloCodDetalle.Name = "lblTituloCodDetalle"
        Me.lblTituloCodDetalle.Size = New System.Drawing.Size(105, 18)
        Me.lblTituloCodDetalle.TabIndex = 6
        Me.lblTituloCodDetalle.Text = "Código Detalle"
        '
        'lblMontoDevList
        '
        Me.lblMontoDevList.AutoSize = True
        Me.lblMontoDevList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMontoDevList.Location = New System.Drawing.Point(301, 126)
        Me.lblMontoDevList.Name = "lblMontoDevList"
        Me.lblMontoDevList.Size = New System.Drawing.Size(210, 20)
        Me.lblMontoDevList.TabIndex = 5
        Me.lblMontoDevList.Text = "Aquí va el monto total devuelto"
        '
        'lblTituloMontoTotalDev
        '
        Me.lblTituloMontoTotalDev.AutoSize = True
        Me.lblTituloMontoTotalDev.Location = New System.Drawing.Point(6, 128)
        Me.lblTituloMontoTotalDev.Name = "lblTituloMontoTotalDev"
        Me.lblTituloMontoTotalDev.Size = New System.Drawing.Size(88, 18)
        Me.lblTituloMontoTotalDev.TabIndex = 4
        Me.lblTituloMontoTotalDev.Text = "Monto Total"
        '
        'lblDatosFechaDev
        '
        Me.lblDatosFechaDev.AutoSize = True
        Me.lblDatosFechaDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDatosFechaDev.Location = New System.Drawing.Point(304, 75)
        Me.lblDatosFechaDev.Name = "lblDatosFechaDev"
        Me.lblDatosFechaDev.Size = New System.Drawing.Size(207, 20)
        Me.lblDatosFechaDev.TabIndex = 3
        Me.lblDatosFechaDev.Text = "Aquí va la fecha de devolución"
        '
        'lblTituloFechaDev
        '
        Me.lblTituloFechaDev.AutoSize = True
        Me.lblTituloFechaDev.Location = New System.Drawing.Point(6, 77)
        Me.lblTituloFechaDev.Name = "lblTituloFechaDev"
        Me.lblTituloFechaDev.Size = New System.Drawing.Size(127, 18)
        Me.lblTituloFechaDev.TabIndex = 2
        Me.lblTituloFechaDev.Text = "Fecha Devolución"
        '
        'lblDatosCodDev
        '
        Me.lblDatosCodDev.AutoSize = True
        Me.lblDatosCodDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDatosCodDev.Location = New System.Drawing.Point(295, 31)
        Me.lblDatosCodDev.Name = "lblDatosCodDev"
        Me.lblDatosCodDev.Size = New System.Drawing.Size(216, 20)
        Me.lblDatosCodDev.TabIndex = 1
        Me.lblDatosCodDev.Text = "Aquí va el código de devolución"
        '
        'lblTituloCodDev
        '
        Me.lblTituloCodDev.AutoSize = True
        Me.lblTituloCodDev.Location = New System.Drawing.Point(6, 31)
        Me.lblTituloCodDev.Name = "lblTituloCodDev"
        Me.lblTituloCodDev.Size = New System.Drawing.Size(134, 18)
        Me.lblTituloCodDev.TabIndex = 0
        Me.lblTituloCodDev.Text = "Código Devolución"
        '
        'grbDetallesDevAdm
        '
        Me.grbDetallesDevAdm.BackColor = System.Drawing.Color.SteelBlue
        Me.grbDetallesDevAdm.Controls.Add(Me.btnSigDetallesDev)
        Me.grbDetallesDevAdm.Controls.Add(Me.btnAntDetallesDev)
        Me.grbDetallesDevAdm.Controls.Add(Me.dgvDetallesDevAdm)
        Me.grbDetallesDevAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDetallesDevAdm.Location = New System.Drawing.Point(3, 298)
        Me.grbDetallesDevAdm.Name = "grbDetallesDevAdm"
        Me.grbDetallesDevAdm.Size = New System.Drawing.Size(655, 308)
        Me.grbDetallesDevAdm.TabIndex = 16
        Me.grbDetallesDevAdm.TabStop = False
        Me.grbDetallesDevAdm.Text = "Detalles Devoluciones"
        '
        'btnSigDetallesDev
        '
        Me.btnSigDetallesDev.Image = CType(resources.GetObject("btnSigDetallesDev.Image"), System.Drawing.Image)
        Me.btnSigDetallesDev.Location = New System.Drawing.Point(577, 93)
        Me.btnSigDetallesDev.Name = "btnSigDetallesDev"
        Me.btnSigDetallesDev.Size = New System.Drawing.Size(72, 72)
        Me.btnSigDetallesDev.TabIndex = 6
        Me.btnSigDetallesDev.UseVisualStyleBackColor = True
        '
        'btnAntDetallesDev
        '
        Me.btnAntDetallesDev.Image = CType(resources.GetObject("btnAntDetallesDev.Image"), System.Drawing.Image)
        Me.btnAntDetallesDev.Location = New System.Drawing.Point(577, 171)
        Me.btnAntDetallesDev.Name = "btnAntDetallesDev"
        Me.btnAntDetallesDev.Size = New System.Drawing.Size(72, 72)
        Me.btnAntDetallesDev.TabIndex = 5
        Me.btnAntDetallesDev.UseVisualStyleBackColor = True
        '
        'dgvDetallesDevAdm
        '
        Me.dgvDetallesDevAdm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetallesDevAdm.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Column7, Me.DataGridViewTextBoxColumn2, Me.Column5, Me.Column6, Me.DataGridViewTextBoxColumn3, Me.Column4})
        Me.dgvDetallesDevAdm.Location = New System.Drawing.Point(6, 19)
        Me.dgvDetallesDevAdm.Name = "dgvDetallesDevAdm"
        Me.dgvDetallesDevAdm.Size = New System.Drawing.Size(565, 283)
        Me.dgvDetallesDevAdm.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Código Detalle"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'Column7
        '
        Me.Column7.HeaderText = "Motivo"
        Me.Column7.Name = "Column7"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Producto"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Categoria"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Subcategoria"
        Me.Column6.Name = "Column6"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Unidades Devueltas"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Monto Devuelto"
        Me.Column4.Name = "Column4"
        '
        'grbDevListAdm
        '
        Me.grbDevListAdm.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.grbDevListAdm.Controls.Add(Me.btnSiguienteDev)
        Me.grbDevListAdm.Controls.Add(Me.btnAnteriorDev)
        Me.grbDevListAdm.Controls.Add(Me.dgvDevAdm)
        Me.grbDevListAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDevListAdm.Location = New System.Drawing.Point(3, 6)
        Me.grbDevListAdm.Name = "grbDevListAdm"
        Me.grbDevListAdm.Size = New System.Drawing.Size(655, 286)
        Me.grbDevListAdm.TabIndex = 15
        Me.grbDevListAdm.TabStop = False
        Me.grbDevListAdm.Text = "Lista Devoluciones"
        '
        'btnSiguienteDev
        '
        Me.btnSiguienteDev.Image = CType(resources.GetObject("btnSiguienteDev.Image"), System.Drawing.Image)
        Me.btnSiguienteDev.Location = New System.Drawing.Point(577, 74)
        Me.btnSiguienteDev.Name = "btnSiguienteDev"
        Me.btnSiguienteDev.Size = New System.Drawing.Size(72, 72)
        Me.btnSiguienteDev.TabIndex = 4
        Me.btnSiguienteDev.UseVisualStyleBackColor = True
        '
        'btnAnteriorDev
        '
        Me.btnAnteriorDev.Image = CType(resources.GetObject("btnAnteriorDev.Image"), System.Drawing.Image)
        Me.btnAnteriorDev.Location = New System.Drawing.Point(577, 150)
        Me.btnAnteriorDev.Name = "btnAnteriorDev"
        Me.btnAnteriorDev.Size = New System.Drawing.Size(72, 72)
        Me.btnAnteriorDev.TabIndex = 3
        Me.btnAnteriorDev.UseVisualStyleBackColor = True
        '
        'dgvDevAdm
        '
        Me.dgvDevAdm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDevAdm.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3})
        Me.dgvDevAdm.Location = New System.Drawing.Point(6, 19)
        Me.dgvDevAdm.Name = "dgvDevAdm"
        Me.dgvDevAdm.Size = New System.Drawing.Size(565, 261)
        Me.dgvDevAdm.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = "Código Devolución"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Fecha Devolución"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "Monto Devuelto"
        Me.Column3.Name = "Column3"
        '
        'frmGestionarDevolucionesAdm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(1206, 647)
        Me.Controls.Add(Me.tbcDevolucionesAdm)
        Me.Name = "frmGestionarDevolucionesAdm"
        Me.Text = "Devoluciones Administrador"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcDevolucionesAdm.ResumeLayout(False)
        Me.tpgEstadisticaDevAdm.ResumeLayout(False)
        Me.grbFiltrarDev.ResumeLayout(False)
        Me.grbFiltrarDev.PerformLayout()
        CType(Me.chtEstadisticasDevAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgListaDevAdm.ResumeLayout(False)
        Me.grbInformacionDev.ResumeLayout(False)
        Me.grbInformacionDev.PerformLayout()
        Me.grbDetallesDevAdm.ResumeLayout(False)
        CType(Me.dgvDetallesDevAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDevListAdm.ResumeLayout(False)
        CType(Me.dgvDevAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbcDevolucionesAdm As System.Windows.Forms.TabControl
    Friend WithEvents tpgEstadisticaDevAdm As System.Windows.Forms.TabPage
    Friend WithEvents btnFiltrarMotivoDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarSubAnioDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarSubMesDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarCatAnioDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarCatMesDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarAnioDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarMesDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarSubDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarCatDevAdm As System.Windows.Forms.Button
    Friend WithEvents grbFiltrarDev As System.Windows.Forms.GroupBox
    Friend WithEvents btnFiltradoCatSubDevAdm As System.Windows.Forms.Button
    Friend WithEvents btnFiltradoFechaDevAdm As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbSubFiltradoDevAdm As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCatFiltradoDevAdm As System.Windows.Forms.ComboBox
    Friend WithEvents btnFiltradoTodoDevAdm As System.Windows.Forms.Button
    Friend WithEvents lblFechaFinDev As System.Windows.Forms.Label
    Friend WithEvents lblFechaIniDev As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFinDevAdm As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaInicioDevAdm As System.Windows.Forms.DateTimePicker
    Friend WithEvents chtEstadisticasDevAdm As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents tpgListaDevAdm As System.Windows.Forms.TabPage
    Friend WithEvents grbInformacionDev As System.Windows.Forms.GroupBox
    Friend WithEvents lblMontoDevList As System.Windows.Forms.Label
    Friend WithEvents lblTituloMontoTotalDev As System.Windows.Forms.Label
    Friend WithEvents lblDatosFechaDev As System.Windows.Forms.Label
    Friend WithEvents lblTituloFechaDev As System.Windows.Forms.Label
    Friend WithEvents lblDatosCodDev As System.Windows.Forms.Label
    Friend WithEvents lblTituloCodDev As System.Windows.Forms.Label
    Friend WithEvents grbDetallesDevAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnSigDetallesDev As System.Windows.Forms.Button
    Friend WithEvents btnAntDetallesDev As System.Windows.Forms.Button
    Friend WithEvents dgvDetallesDevAdm As System.Windows.Forms.DataGridView
    Friend WithEvents grbDevListAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnSiguienteDev As System.Windows.Forms.Button
    Friend WithEvents btnAnteriorDev As System.Windows.Forms.Button
    Friend WithEvents dgvDevAdm As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblTituloDetallesDev As System.Windows.Forms.Label
    Friend WithEvents txbDetDevList As System.Windows.Forms.TextBox
    Friend WithEvents lblDatosMontoDev As System.Windows.Forms.Label
    Friend WithEvents lblTituloPrecioDev As System.Windows.Forms.Label
    Friend WithEvents lblDatosMotivoDev As System.Windows.Forms.Label
    Friend WithEvents lblTituloMotivoDev As System.Windows.Forms.Label
    Friend WithEvents lblNomProdDevList As System.Windows.Forms.Label
    Friend WithEvents lblTituloProdDev As System.Windows.Forms.Label
    Friend WithEvents lblDatosCodDetalle As System.Windows.Forms.Label
    Friend WithEvents lblTituloCodDetalle As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
