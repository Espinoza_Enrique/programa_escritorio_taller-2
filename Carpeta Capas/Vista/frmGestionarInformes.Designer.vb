﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGestionarInformesAdm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rtvInformes = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.grbInformesProductos = New System.Windows.Forms.GroupBox()
        Me.clbOpcionesProductos = New System.Windows.Forms.CheckedListBox()
        Me.grbInformesVentas = New System.Windows.Forms.GroupBox()
        Me.clbOpcionesVentas = New System.Windows.Forms.CheckedListBox()
        Me.grbAuditorias = New System.Windows.Forms.GroupBox()
        Me.clbOpcionesAuditorias = New System.Windows.Forms.CheckedListBox()
        Me.grbDevoluciones = New System.Windows.Forms.GroupBox()
        Me.clbOpcionesDev = New System.Windows.Forms.CheckedListBox()
        Me.btnCrearInforme = New System.Windows.Forms.Button()
        Me.grbInformesProductos.SuspendLayout()
        Me.grbInformesVentas.SuspendLayout()
        Me.grbAuditorias.SuspendLayout()
        Me.grbDevoluciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'rtvInformes
        '
        Me.rtvInformes.Location = New System.Drawing.Point(28, 357)
        Me.rtvInformes.Name = "rtvInformes"
        Me.rtvInformes.Size = New System.Drawing.Size(1098, 318)
        Me.rtvInformes.TabIndex = 0
        '
        'grbInformesProductos
        '
        Me.grbInformesProductos.BackColor = System.Drawing.Color.Cyan
        Me.grbInformesProductos.Controls.Add(Me.clbOpcionesProductos)
        Me.grbInformesProductos.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbInformesProductos.Location = New System.Drawing.Point(12, 12)
        Me.grbInformesProductos.Name = "grbInformesProductos"
        Me.grbInformesProductos.Size = New System.Drawing.Size(263, 158)
        Me.grbInformesProductos.TabIndex = 6
        Me.grbInformesProductos.TabStop = False
        Me.grbInformesProductos.Text = "Opciones De Informes"
        '
        'clbOpcionesProductos
        '
        Me.clbOpcionesProductos.FormattingEnabled = True
        Me.clbOpcionesProductos.Items.AddRange(New Object() {"Reportes De Stock", "Reportes De Modificaciones", "Historial De Altas", "Clasificados Por Categorías", "Logueos"})
        Me.clbOpcionesProductos.Location = New System.Drawing.Point(6, 19)
        Me.clbOpcionesProductos.Name = "clbOpcionesProductos"
        Me.clbOpcionesProductos.Size = New System.Drawing.Size(241, 118)
        Me.clbOpcionesProductos.TabIndex = 0
        '
        'grbInformesVentas
        '
        Me.grbInformesVentas.BackColor = System.Drawing.Color.Cyan
        Me.grbInformesVentas.Controls.Add(Me.clbOpcionesVentas)
        Me.grbInformesVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbInformesVentas.Location = New System.Drawing.Point(328, 12)
        Me.grbInformesVentas.Name = "grbInformesVentas"
        Me.grbInformesVentas.Size = New System.Drawing.Size(263, 158)
        Me.grbInformesVentas.TabIndex = 7
        Me.grbInformesVentas.TabStop = False
        Me.grbInformesVentas.Text = "Opciones De Ventas"
        '
        'clbOpcionesVentas
        '
        Me.clbOpcionesVentas.FormattingEnabled = True
        Me.clbOpcionesVentas.Items.AddRange(New Object() {"Ventas Por Empleado", "Ventas Por Categorias", "Ventas Por Mes", "Media De Ventas Por Mes"})
        Me.clbOpcionesVentas.Location = New System.Drawing.Point(6, 19)
        Me.clbOpcionesVentas.Name = "clbOpcionesVentas"
        Me.clbOpcionesVentas.Size = New System.Drawing.Size(241, 99)
        Me.clbOpcionesVentas.TabIndex = 0
        '
        'grbAuditorias
        '
        Me.grbAuditorias.BackColor = System.Drawing.Color.Cyan
        Me.grbAuditorias.Controls.Add(Me.clbOpcionesAuditorias)
        Me.grbAuditorias.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbAuditorias.Location = New System.Drawing.Point(305, 193)
        Me.grbAuditorias.Name = "grbAuditorias"
        Me.grbAuditorias.Size = New System.Drawing.Size(359, 158)
        Me.grbAuditorias.TabIndex = 8
        Me.grbAuditorias.TabStop = False
        Me.grbAuditorias.Text = "Opciones De Auditoria"
        '
        'clbOpcionesAuditorias
        '
        Me.clbOpcionesAuditorias.FormattingEnabled = True
        Me.clbOpcionesAuditorias.Items.AddRange(New Object() {"Operaciones En Productos", "Operaciones En Empleados", "Operaciones En Usuarios", "Operaciones En Devoluciones", "Operaciones En Categorias/Subcategorias"})
        Me.clbOpcionesAuditorias.Location = New System.Drawing.Point(6, 19)
        Me.clbOpcionesAuditorias.Name = "clbOpcionesAuditorias"
        Me.clbOpcionesAuditorias.Size = New System.Drawing.Size(331, 99)
        Me.clbOpcionesAuditorias.TabIndex = 0
        '
        'grbDevoluciones
        '
        Me.grbDevoluciones.BackColor = System.Drawing.Color.Cyan
        Me.grbDevoluciones.Controls.Add(Me.clbOpcionesDev)
        Me.grbDevoluciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDevoluciones.Location = New System.Drawing.Point(687, 12)
        Me.grbDevoluciones.Name = "grbDevoluciones"
        Me.grbDevoluciones.Size = New System.Drawing.Size(263, 128)
        Me.grbDevoluciones.TabIndex = 8
        Me.grbDevoluciones.TabStop = False
        Me.grbDevoluciones.Text = "Opciones De Devolucion"
        '
        'clbOpcionesDev
        '
        Me.clbOpcionesDev.FormattingEnabled = True
        Me.clbOpcionesDev.Items.AddRange(New Object() {"Monto Devuelto Por Mes", "Devoluciones Por Categoria", "Devoluciones Por Fecha"})
        Me.clbOpcionesDev.Location = New System.Drawing.Point(6, 19)
        Me.clbOpcionesDev.Name = "clbOpcionesDev"
        Me.clbOpcionesDev.Size = New System.Drawing.Size(241, 80)
        Me.clbOpcionesDev.TabIndex = 0
        '
        'btnCrearInforme
        '
        Me.btnCrearInforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCrearInforme.Location = New System.Drawing.Point(478, 679)
        Me.btnCrearInforme.Name = "btnCrearInforme"
        Me.btnCrearInforme.Size = New System.Drawing.Size(200, 31)
        Me.btnCrearInforme.TabIndex = 9
        Me.btnCrearInforme.Text = "Crear Informe"
        Me.btnCrearInforme.UseVisualStyleBackColor = True
        '
        'frmGestionarInformesAdm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1128, 722)
        Me.Controls.Add(Me.btnCrearInforme)
        Me.Controls.Add(Me.grbDevoluciones)
        Me.Controls.Add(Me.grbAuditorias)
        Me.Controls.Add(Me.grbInformesVentas)
        Me.Controls.Add(Me.grbInformesProductos)
        Me.Controls.Add(Me.rtvInformes)
        Me.Name = "frmGestionarInformesAdm"
        Me.Text = "Informes"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.grbInformesProductos.ResumeLayout(False)
        Me.grbInformesVentas.ResumeLayout(False)
        Me.grbAuditorias.ResumeLayout(False)
        Me.grbDevoluciones.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rtvInformes As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents grbInformesProductos As System.Windows.Forms.GroupBox
    Friend WithEvents clbOpcionesProductos As System.Windows.Forms.CheckedListBox
    Friend WithEvents grbInformesVentas As System.Windows.Forms.GroupBox
    Friend WithEvents clbOpcionesVentas As System.Windows.Forms.CheckedListBox
    Friend WithEvents grbAuditorias As System.Windows.Forms.GroupBox
    Friend WithEvents clbOpcionesAuditorias As System.Windows.Forms.CheckedListBox
    Friend WithEvents grbDevoluciones As System.Windows.Forms.GroupBox
    Friend WithEvents clbOpcionesDev As System.Windows.Forms.CheckedListBox
    Friend WithEvents btnCrearInforme As System.Windows.Forms.Button
End Class
