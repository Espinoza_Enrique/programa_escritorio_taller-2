﻿Public Class frmGestionarInformesAdm

    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al intentar abrir la ventana", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    '-------------------------------------------------------------------------------
    'método de prueba que puede servir más adelante para negar acceso a ciertos
    'checkbox
    '-------------------------------------------------------------------------------
    Private Function controlarSeleccion() As Boolean
        If Me.clbOpcionesVentas.GetItemChecked(0) Then

            Return True
        Else
            Return False
        End If
    End Function
    Private Sub frmGestionarInformesAdm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al intentar cerrar la ventana", vbOKOnly + vbCritical, "Error de cierre de ventana")
        End Try
    End Sub
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub frmGestionarInformesAdm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.cargarFondoPantalla("cobaltoplata.jpg")
            If Sesion.getNombrePerfil() <> "Administrador" Then
                grbAuditorias.Enabled = False
            End If
        Catch ex As Exception
            MsgBox("Error al intentar cargar la ventana", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub



    Private Sub clbOpcionesVentas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles clbOpcionesVentas.SelectedIndexChanged
        'If Me.controlarSeleccion() Then
        'MsgBox("Debido a tu perfil, no puedes seleccionar esta opción", vbOKOnly)
        'clbOpcionesVentas.SetItemChecked(clbOpcionesVentas.SelectedIndex, False)

        'End If
    End Sub
End Class
