﻿Imports Entities
Imports BusinessLayer

Public Class frmGestionarUsuariosAdm
    'controla que el usuario ingresado no sea administrador
    Private noAdministrador As Boolean

    '--------------------------------------------------------------------------------------------------------
    'Se encargar de abrir la ventana
    '--------------------------------------------------------------------------------------------------------
    Public Sub abrirVentana()
        Me.ShowDialog()
    End Sub
    '-----------------------------------------------------------------------------------------------
    'Determina si la persona que accede a la ventana es o no un administrador
    'Parámetros:
    'valor-tipo: booleano-Si es true, significa que el usuario que ingresó no es un administrador
    '----------------------------------------------------------------------------------------------
    Private Sub setNoAdministrador(ByVal valor As Boolean)
        Try
            Me.noAdministrador = valor
        Catch ex As Exception
            MsgBox("Error al establecer al usuario como no administrador:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Asignar Rol")
        End Try
    End Sub
    '-----------------------------------------------------------------------------------------------
    'Permite saber si la persona que ingresó es o no un adminitrador
    'Retorno: retorna true en caso de que no sea un administrador
    '----------------------------------------------------------------------------------------------
    Private Function getNoAdministrador() As Boolean
        Try
            Return Me.noAdministrador
        Catch ex As Exception
            MsgBox("Error al consultar el rol del empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error Consulta")
            Return False
        End Try
    End Function
    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Sub seleccionarTabControl(ByVal elemento As Byte)
        Try
            If elemento >= 1 And elemento <= Me.tbcUsuarios.TabCount Then
                Me.tbcUsuarios.SelectedIndex = elemento - 1
                Me.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox("Error al seleccionar una opción en las opciones para el usuario:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Seleccionar Una Opción De Ventana")
        End Try
    End Sub

    Private Sub frmGestionarUsuariosAdm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al cerrar la ventana" + vbNewLine + ex.Message, vbOKOnly, "Error de cierre")
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------------
    'Permite deshabilitar ciertas opciones de usuarios sólo permitidas a un usuario de perfil administrador
    'Además, permite mostrar los datos de los combobox y datagridview como ser: usuarios, empleados, perfiles, etc.
    '---------------------------------------------------------------------------------------------------------------

    Private Sub frmGestionarUsuariosAdm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            ' Me.cargarFondoPantalla("cobaltoplata.jpg")
            Me.prepararFormulario()
            Me.listaEmpleados(dgvEmpleados, False)
            Me.listaEmpleados(dgvEmpNuevoAdm, True)
            Me.cargarDatosEmpleadoSesion()
            Me.cargarDatosUsuarioSesion(True)

            If Sesion.getNombrePerfil() <> "Administrador" Then
                'Me.habilitarParaNoAdm()

                Me.setNoAdministrador(True)
            Else
                Me.setNoAdministrador(False)
            End If
        Catch ex As Exception
            MsgBox("Error al cargar la ventana de gestión de usuarios:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Cargar Ventana")
        End Try
    End Sub
    '--------------------------------------------------------------------------------------------------------
    'Permite configurar los botones. En este caso, sólo cambia el sentido de la imagen.
    '--------------------------------------------------------------------------------------------------------
    Private Sub establecerImagenBtn()
        Try
            Me.btnAntEmp.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.btnAnteNuevoUsu.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.btnAntModUsuAdm.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
        Catch ex As Exception
            MsgBox("Error modificar las imagenes a los botones:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Modificar Atributos")
        End Try
    End Sub
    '---------------------------------------------------------------------------------------------------------------
    'Permite configurar los elementos del formulario. Si se quiere dar algún estilo o comportamiento a los elementos
    'del formulario, se debería colocar dicha instrucción aquí
    '---------------------------------------------------------------------------------------------------------------
    Private Sub prepararFormulario()
        Try
            Me.cmbTipoModDatosUsu.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbTiposUsuario.DropDownStyle = ComboBoxStyle.DropDownList
            Me.establecerImagenBtn()
            Me.rellenarTipoUsuario(cmbTiposUsuario)
            Me.rellenarTipoUsuario(cmbTipoModUsu)
            Me.grbDatosPersonalesMod.Enabled = False
            Me.grbNuevaContraEmpPropio.Enabled = False
        Catch ex As Exception
            MsgBox("Error al formatear la ventana:" + vbNewLine + ex.Message, vbOKOnly, "Error De Formato")
        End Try

    End Sub

    '-----------------------------------------------------------------------------------------------
    'Evento que, al cambiar la selección de alguna de las pestañas, habilita o deshabilita componentes
    'y refresca los combobox y datagridview
    '----------------------------------------------------------------------------------------------
    Private Sub tbcUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tbcUsuarios.SelectedIndexChanged
        Dim pestaniaDatosPersonales As Byte
        Dim pestaniaModUsuario As Byte
        Dim pestaniaNuevoEmp As Byte
        Dim pestaniaNuevoUsu As Byte
        Try
            pestaniaNuevoEmp = 0
            pestaniaNuevoUsu = 1
            pestaniaDatosPersonales = 3
            pestaniaModUsuario = 2
            If tbcUsuarios.SelectedIndex = pestaniaDatosPersonales Then
                Me.cargarDatosEmpleadoSesion()
                Me.cargarDatosUsuarioSesion(False)
            ElseIf tbcUsuarios.SelectedIndex = pestaniaModUsuario Then
                Tienda.limpiarGrilla(dgvUsuariosActivosAdm)
                Me.listaUsuarios(dgvUsuariosActivosAdm)
            ElseIf tbcUsuarios.SelectedIndex = pestaniaNuevoEmp Then
                Tienda.limpiarGrilla(dgvEmpleados)
                Me.listaEmpleados(dgvEmpleados, False)
            ElseIf tbcUsuarios.SelectedIndex = pestaniaNuevoUsu Then
                Tienda.limpiarGrilla(dgvEmpNuevoAdm)
                Me.listaEmpleados(dgvEmpNuevoAdm, True)
            End If
        Catch ex As Exception
            MsgBox("Error al seleccionar una opción en las opciones para el usuario:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Seleccionar Una Opción De Ventana")
        End Try
    End Sub
    '---------------------------------------------------------------------------------------------------
    'Evento que, al intentar cambiar de pestaña, evita que un usuario no permitido acceda a la pestaña
    'en cuestión.
    '---------------------------------------------------------------------------------------------------
    Private Sub tbcUsuarios_Selecting(sender As Object, e As TabControlCancelEventArgs) Handles tbcUsuarios.Selecting
        Try
            If Me.getNoAdministrador() Then
                e.Cancel = True
                MsgBox("Debido a su perfil de usuario sólo puede modificar sus propios datos personales", vbOKOnly + vbExclamation, "Error De Acceso")
            End If
        Catch ex As Exception
            MsgBox("Error al negar el acceso a un usuario:" + vbNewLine + ex.Message, vbOKOnly, "Error de Acceso")
            e.Cancel = False
        End Try
    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite controlar los datos de los empleados y devolver los mensajes de errores pertinentes
    'Parámetros:
    'ape-tipo:String-Representa el apellido del empleado
    'nom:tipo: String-Representa el nombre del empleado
    'dni-tipo: Integer-Representa el dni del empleado
    'telefono-tipo: String-Representa el teléfono del empleado, puede quedar en blanco
    'mail-tipo:String-Representa el correo electrónico del empleado
    'noModificación-tipo:Boolean-Representa una variable usada para reciclar el método. Si es falso, indica que se trata de un
    'empleado nuevo y debe verificarse que no haya dni repetidos. En caso contrario, se trata de una modificación de datos existentes.

    'Retorno
    'Retorna un String que contendrá el mensaje de los errores producidos o será en blanco en caso de no presentarse
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Function controlarEmpleado(ByVal ape As String, ByVal nom As String, ByVal dni As String, ByVal telefono As String, ByVal mail As String, ByVal modDni As Boolean) As String
        Dim mensaje As String
        Dim mensajeDni As String
        Dim mensajeTel As String
        Dim mensajeMail As String
        Try
            mensaje = ""
            mensajeDni = General.controlarDni(dni)
            mensajeTel = General.controlarTelefono(telefono)
            mensajeMail = General.validarEmail(mail)

            If mensajeDni = "" And modDni Then
                mensaje = mensaje + BusEmpleados.controlarDniRepetido(CInt(dni))
            End If
            If General.controlarCajaEnBlanco(ape) Or General.controlarCajaEnBlanco(nom) Then
                mensaje = mensaje + "Por favor, no deje el apellido y nombre en blanco" + vbNewLine
            End If

            If Not General.controlarLetras(ape) Or Not General.controlarLetras(nom) Then
                mensaje = mensaje + "Por favor, solo coloque letras en los nombres y apellidos" + vbNewLine
            End If

            Return Trim(mensaje + mensajeDni + mensajeMail + mensajeTel)
        Catch ex As Exception
            MsgBox("Error al controlar datos del empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error De control")
            Return ""
        End Try
    End Function
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite controlar los datos de los usuarios y devolver los mensajes de errores pertinentes
    'Parámetros:
    'nombreUsu-tipo:String-Representa el nombre de usuario
    'contrasenia:tipo: String-Representa la contraseña del usuario
    'repcontra-tipo: String-Representa la repetición de la contraseña del usuario
    'tipo-tipo: String-Representa el tipo de usuario que es, lo cual indicará su nivel de acceso
    'codEmp-tipo:Integer-Representa el código de empleado asociado al usuario
    'noModificación-tipo:Boolean-Representa una variable usada para reciclar el método. Si es falso, indica que se trata de un

    'Retorno
    'Retorna un String que contendrá el mensaje de los errores producidos o será en blanco en caso de no presentarse
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Function controlarUsuario(ByVal nombreUsu As String, ByVal contrasenia As String, ByVal repContra As String, ByVal idPerfil As Integer, ByVal tipo As String, ByVal codEmp As Integer) As String
        Dim mensaje As String
        Try
            mensaje = ""
            mensaje = BusUsuarios.controlarUsuarioRepetido(nombreUsu, codEmp, idPerfil)
            If General.controlarCajaEnBlanco(nombreUsu) Then
                mensaje = mensaje + "Por favor, no deje el nombre de usuario en blanco" + vbNewLine
            End If

            If General.controlarCajaEnBlanco(contrasenia) Then
                mensaje = mensaje + "Por favor, no deje la contrasenia en blanco" + vbNewLine
            End If

            If contrasenia <> repContra Then
                mensaje = mensaje + "La contrasenia y su repetición no coinciden" + vbNewLine
            End If

            If tipo = "-" Or String.IsNullOrEmpty(tipo) Then
                mensaje = mensaje + "Por favor, elija un tipo de usuario" + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar datos del usuario:" + vbNewLine + ex.Message, vbOKOnly, "Error De Control De Datos")
            Return ""
        End Try
    End Function
    Private Sub btnAgregarEmpleado_Click(sender As Object, e As EventArgs) Handles btnAgregarEmpleado.Click
        Dim mensaje As String
        Dim nombre As String
        Dim apellido As String
        Dim dni As String
        Dim tel As String
        Dim mail As String
        Try
            apellido = Me.txbApeNuevoEmp.Text
            nombre = Me.txbNomNuevoEmp.Text
            dni = Me.txbDniNuevoEmp.Text
            tel = Me.txbTelNuevoEmp.Text
            mail = Me.txbMailNuevoEmp.Text

            mensaje = Me.controlarEmpleado(apellido, nombre, dni, tel, mail, True)

            If String.IsNullOrWhiteSpace(mensaje) Then
                MsgBox("Datos del empleado correctos", vbOKOnly, "Ventana Éxito")
                ' Dim empleado As New Empleados(Tienda.getSigIdEmpleado, nombre, apellido, CInt(dni), mail, tel, True)
                Me.agregarEmpleados(apellido, nombre, dni, tel, mail)
                Me.listaEmpleados(Me.dgvEmpleados, False)
            Else
                MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Éxito")
            End If
        Catch ex As Exception
            MsgBox("Error al ingresar un nuevo empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error De Ingreso De Datos")
        End Try
    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite cargar un combobox con los tipos de usuarios habilitados
    'Parámetros:
    'tipoUsu-Tipo:Combobox-Representa el contenedor para los tipos de usuarios
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub rellenarTipoUsuario(ByVal tipoUsu As ComboBox)
        Try
            Dim perfiles As New List(Of Entities.Perfiles)
            tipoUsu.DataSource = Nothing
            perfiles = BusPerfiles.getPerfiles()
            tipoUsu.DataSource = perfiles
            tipoUsu.DisplayMember = "descripcionPerfil"
            tipoUsu.ValueMember = "idPerfil"
            'tipoUsu.Sorted = True
        Catch ex As Exception
            MsgBox("Error listar tipos de usuarios:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Seleccionar Una Opción De Ventana")
        End Try
    End Sub

    Private Sub btnCrearUsuario_Click(sender As Object, e As EventArgs) Handles btnCrearUsuario.Click
        Dim mensaje As String
        Dim nombreUsu As String
        Dim contrasenia As String
        Dim repContra As String
        Dim tipo As String
        Dim idTipoUsu As Integer
        Dim codEmp As String

        Try
            mensaje = ""
            nombreUsu = Me.txbNomUsu.Text
            contrasenia = Trim(Me.txbContraseniaUsu.Text)
            repContra = Trim(Me.txbContraRepeUsu.Text)
            tipo = Me.cmbTiposUsuario.Text
            idTipoUsu = Me.cmbTiposUsuario.SelectedValue
            codEmp = Me.txbCodEmpNuevoUsu.Text
            mensaje = Me.controlarUsuario(nombreUsu, contrasenia, repContra, idTipoUsu, tipo, codEmp)
            If String.IsNullOrWhiteSpace(mensaje) Then
                Me.crearNuevoUsuario(nombreUsu, contrasenia, codEmp, idTipoUsu)
                Me.limpiarDatosUsuarioNuevo()
                MsgBox("Los datos del nuevo usuario son correctos", vbOKOnly, "Ventana Éxito")

            Else
                MsgBox("Se presentaron los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly + vbCritical, "Error ingreso usuario")
        End Try
    End Sub



    Private Sub btnModDatosPropios_Click(sender As Object, e As EventArgs) Handles btnModDatosPropios.Click
        Dim mensaje As String
        Dim nombre As String
        Dim apellido As String
        Dim dni As String
        Dim telefono As String
        Dim mail As String
        Dim modExito As Byte
        Dim modDni As Boolean
        Try
            nombre = Me.txbNomModEmpProp.Text
            apellido = Me.txbApeModEmp.Text
            dni = Me.txbDniModEmpPropio.Text
            telefono = Me.txbTelModEmpPropio.Text
            mail = Me.txbMailModEmpPropio.Text
            modDni = Me.ckbConfirmarModDni.Checked
            mensaje = Me.controlarEmpleado(apellido, nombre, dni, telefono, mail, modDni)
            If String.IsNullOrWhiteSpace(mensaje) Then
                modExito = MsgBox("¿Desea Modificar su datos por los indicados anteriormente?", vbYesNo + vbDefaultButton2, "Ventana De Confirmación")

                If modExito = vbYes Then
                    If modDni Then
                        BusEmpleados.modificarDatosEmpleado(Sesion.getidEmpleado(), apellido, nombre, CInt(dni), telefono, mail)
                    Else
                        BusEmpleados.modificarEmpleadoSinDni(Sesion.getidEmpleado(), apellido, nombre, telefono, mail)
                    End If
                    Me.grbDatosPersonalesMod.Enabled = False
                    Me.cargarDatosEmpleadoSesion()
                    Sesion.modificarDatosEmpleado(nombre, apellido, CInt(dni))
                    MsgBox("Datos del empleado correctos", vbOKOnly, "Ventana Éxito")
                End If

                Else
                    MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Éxito")
                End If
        Catch ex As Exception
            MsgBox("Error al modificar datos propios:" + vbNewLine + ex.Message, vbOKOnly, "Error al modificar datos")
        End Try
    End Sub

    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite mover los datos de usuarios desde la grilla de datos hacia los contenedores que permitirán la modiciación de los datos
    'Parámetros:
    'grilla-tipo: DataGridView-El contenedor con los datos de todos los usuarios y desde dónde se elige cuál se desea cambiar de datos
    'contenedor-tipo:Groupbox-El contenedor con las cajas para los datos. Se habilitará para permitir la modificación de los mismos
    'perfiles-tipo: Combobox-El contenedor que almacenará el dato del perfil del usuario.
    '----------------------------------------------------------------------------------------------------------------------------------------

    Private Sub moverDatosModUsu(ByRef grilla As DataGridView, ByRef contenedor As GroupBox, ByRef perfiles As ComboBox)
        Try
            Me.rellenarTipoUsuario(perfiles)
            Me.txbNumUsuMod.Text = grilla.SelectedRows.Item(0).Cells(0).Value
            Me.txbNomModUsu.Text = grilla.SelectedRows.Item(0).Cells(1).Value
            Me.cmbTipoModUsu.SelectedItem = grilla.SelectedRows.Item(0).Cells(4).Value
            contenedor.Enabled = True
        Catch ex As Exception
            MsgBox("Error al obtener datos del usuario:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Obtener datos")
        End Try

    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite cargar en los contenedores pertinentes los datos del empleado cuya sesión está iniciada
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub cargarDatosEmpleadoSesion()
        Dim empleado As Entities.Empleados
        Try
            empleado = BusEmpleados.getEmpleadoPorId(Sesion.getidEmpleado())
            Me.txbApeModEmp.Text = empleado.apellidoEmpleado
            Me.txbNomModEmpProp.Text = empleado.nombreEmpleado
            Me.txbDniModEmpPropio.Text = empleado.dni
            Me.txbTelModEmpPropio.Text = empleado.telefono
            Me.txbMailModEmpPropio.Text = empleado.mail
        Catch ex As Exception
            MsgBox("Error al obtener los datos del empleado en sesión:" + vbNewLine + ex.Message, vbOKOnly, "Error De Obtención De Datos")
        End Try
    End Sub

    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite cargar los datos del usuario cuya sesión está activa en los contenedores correspondientes
    'Parámetros:
    'estado-tipo: boolean-Permite controlar el estado del usuario
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub cargarDatosUsuarioSesion(ByVal estado As Boolean)
        Try
            Me.txbNuevaContra.Enabled = estado
            Me.txbNuevaRepContra.Enabled = estado
            If estado Then
                Me.cmbTipoModDatosUsu.Items.Add(Tienda.getNombrePerfilPorId(Sesion.getPerfilUsuario()))
            Else
                Me.cmbTipoModDatosUsu.Items.Add("-")
                Me.txbNuevaContra.Text = ""
                Me.txbNuevaRepContra.Text = ""
            End If
            Me.cmbTipoModDatosUsu.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al cargar los datos del usuario en sesión:" + vbNewLine + ex.Message, vbOKOnly, "Error De Obtención De Datos")
        End Try
    End Sub


    Private Sub btnControlarUsuario_Click(sender As Object, e As EventArgs) Handles btnControlarUsuario.Click
        Dim nombreUsu As String
        Dim password As String
        Dim usuarioActivo As New Entities.Usuarios
        Try
            nombreUsu = Trim(Me.txbNomModUsuPropio.Text)
            password = Trim(Me.txbContraModUsuPropio.Text)

            If Not General.controlarCajaEnBlanco(nombreUsu) Or Not General.controlarCajaEnBlanco(password) Then
                usuarioActivo = BusUsuarios.controlarAcceso(nombreUsu, password)
                If Not IsNothing(usuarioActivo) And usuarioActivo.idUsuario = Sesion.getIdUsuario() Then
                    Me.grbNuevaContraEmpPropio.Enabled = True
                    Me.txbNuevaContra.Enabled = True
                    Me.txbNuevaRepContra.Enabled = True
                    Me.grbDatosPersonalesMod.Enabled = True
                    Me.txbDniModEmpPropio.Enabled = False
                    Me.txbNomModUsuPropio.Text = ""
                    Me.txbContraModUsuPropio.Text = ""
                    Me.cmbTipoModDatosUsu.Text = Sesion.getNombrePerfil()
                    Me.cargarDatosUsuarioSesion(True)
                Else
                    MsgBox("La contrasenia o el nombre de usuario no te corresponden", vbOKOnly + vbExclamation, "Ventana Error")
                End If
            Else
                MsgBox("Por favor, no deje el nombre de usuario y contraseña en blanco", vbOKOnly + vbExclamation, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error en el control de datos del usuario:" + vbNewLine + ex.Message, vbOKOnly, "Error De control de datos")
        End Try
    End Sub

    Private Sub btnModContraPropio_Click(sender As Object, e As EventArgs)
        Dim nuevaContra As String
        Dim confirmacion As String
        Dim usuario As New Entities.Usuarios

        Try
            nuevaContra = Trim(Me.txbNuevaContra.Text)
            confirmacion = Trim(Me.txbNuevaRepContra.Text)

            If Not General.controlarCajaEnBlanco(nuevaContra) Or Not General.controlarCajaEnBlanco(confirmacion) Then
                If nuevaContra = confirmacion Then
                    BusUsuarios.modificarContrasenia(nuevaContra, Sesion.getIdUsuario())
                    Me.cargarDatosUsuarioSesion(False)
                    MsgBox("Contrasenia cambiada exitosamente", vbOKOnly + vbExclamation, "Ventana Éxito")
                Else
                    MsgBox("La contraseña y su repetición no se corresponden", vbOKOnly + vbExclamation, "Ventana Error")
                End If
            Else
                MsgBox("No deje cajas en blanco por favor", vbOKOnly + vbExclamation, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error al controlar contraseña actual para cambio de la misma:" + vbNewLine + ex.Message, vbOKOnly, "Error  De Validación De Usuario")
        End Try
    End Sub



    Private Sub dgvEmpNuevoAdm_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvEmpNuevoAdm.CellClick
        Me.moverCrearNuevoUsu(Me.dgvEmpNuevoAdm)
    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite mover los datos de un empleado desde la grilla hacia las cajas que contendrán esos datos para usarlos en la creación de 
    'un nuevo usuario.
    'Parámetros:
    'grilla-tipo:DataGridView-Representa el contenedor desde donde se obtendrán los datos del empleado
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub moverCrearNuevoUsu(ByRef grilla As DataGridView)
        Try
            Me.txbCodEmpNuevoUsu.Text = grilla.SelectedRows.Item(0).Cells(0).Value
            Me.txbApeEmpUsu.Text = grilla.SelectedRows.Item(0).Cells(2).Value
            Me.txbNomEmpUsu.Text = grilla.SelectedRows.Item(0).Cells(1).Value
            Me.txbDniEmpUsu.Text = grilla.SelectedRows.Item(0).Cells(3).Value
            Me.txbTelEmpUsu.Text = grilla.SelectedRows.Item(0).Cells(4).Value
            Me.txbMailEmpUsu.Text = grilla.SelectedRows.Item(0).Cells(6).Value
        Catch ex As Exception
            MsgBox("Error al obtener datos del empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error De Obtención De Datos")
        End Try
    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite insertar un nuevo empleado a la base de datos (en el caso estático, a una lista previamente creada en tiempo de ejecución).
    'Parámetros:
    'nombre-tipo:String-Representa el nombre del usuario
    'contraseña-tipo:String-Representa la contraseña del usuario
    'idEmpleado-tipo:integer-Representa el id del empleado
    'nombrePerfil-tipo: String-Representa el nombre del perfil que tendrá el usuario al crearse.

    'Retorno
    'Retorna un String que contendrá el mensaje de los errores producidos o será en blanco en caso de no presentarse
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub crearNuevoUsuario(ByVal nombre As String, ByVal contrasenia As String, ByVal idEmpleado As String, ByVal perfil As Integer)
       
        Try
            BusUsuarios.crearNuevoUsuario(nombre, contrasenia, idEmpleado, perfil)
        Catch e As Exception
            MsgBox("Error al querer ingresar un nuevo usuario", vbOKOnly + vbCritical, "Error ingreso nuevo usuario")
        End Try

    End Sub

    Private Sub dgvUsuariosActivosAdm_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvUsuariosActivosAdm.CellClick
        Dim valorPrueba As String
        valorPrueba = dgvUsuariosActivosAdm.CurrentRow.Cells(0).Value
        If Not IsNothing(valorPrueba) And Not dgvUsuariosActivosAdm.CurrentCell.GetType.ToString Like "*Button*" Then
            Me.moverDatosModUsu(Me.dgvUsuariosActivosAdm, Me.grbModUsuAdm, Me.cmbTipoModUsu)
        Else

        End If
    End Sub

    Private Sub dgvEmpleados_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvEmpleados.CellContentClick
        Me.bajaEmpleado(dgvEmpleados, False)

    End Sub

    Private Sub dgvUsuariosActivosAdm_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvUsuariosActivosAdm.CellContentClick
        Dim idUsuario As String
        Dim estado As String
        idUsuario = Me.dgvUsuariosActivosAdm.CurrentRow.Cells(0).Value
        estado = Me.dgvUsuariosActivosAdm.CurrentRow.Cells(5).Value
        If Not General.controlarCajaEnBlanco(idUsuario) Then
            If estado = "Activo" Then
                BusUsuarios.modificarEstadoUsuario(CInt(idUsuario), 0)
            Else
                BusUsuarios.modificarEstadoUsuario(CInt(idUsuario), 1)
            End If
        End If
        Me.listaUsuarios(Me.dgvUsuariosActivosAdm)
    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite limpiar los datos de los contenedores después de crear un nuevo usuario.
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub limpiarDatosUsuarioNuevo()
        Try
            Me.txbCodEmpNuevoUsu.Text = ""
            Me.txbApeEmpUsu.Text = ""
            Me.txbNomEmpUsu.Text = ""
            Me.txbDniEmpUsu.Text = ""
            Me.txbTelEmpUsu.Text = ""
            Me.txbMailEmpUsu.Text = ""
            Me.txbNomUsu.Text = ""
            Me.txbContraseniaUsu.Text = ""
            Me.txbContraRepeUsu.Text = ""
            Me.cmbTiposUsuario.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al agregar usuario nuevo. Por favor, revise si la operación tuvo éxito", vbOKOnly + vbCritical, "Error de datos")
        End Try

    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite limpiar los contenedores una vez que se ha modificado la contraseña o tipo de un usuario
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub limpiarModContraseniaTipo()
        Try
            Me.txbNumUsuMod.Text = ""
            Me.txbContraModUsu.Text = ""
            Me.cmbTipoModUsu.Items.Clear()
            Me.txbNomModUsu.Text = ""
            Me.txbRepContraUsu.Text = ""
            Me.grbModUsuAdm.Enabled = False
        Catch ex As Exception
            MsgBox("Error al modificar contraseña o tipo. Por favor, revise si la operación tuvo éxito", vbOKOnly + vbCritical, "Error de datos")
        End Try
    End Sub

    Private Sub btnModContrasenia_Click(sender As Object, e As EventArgs) Handles btnModContrasenia.Click
        Dim mensaje As String
        Dim noBlancos As Boolean
        Try
            mensaje = ""
            If General.controlarCajaEnBlanco(Me.txbContraModUsu.Text) Or General.controlarCajaEnBlanco(Me.txbRepContraUsu.Text) Then
                mensaje = mensaje + "No deje las contraseñas en blanco" + vbNewLine
                noBlancos = False
            Else
                noBlancos = True
            End If

            If noBlancos And Me.txbContraModUsu.Text <> Me.txbRepContraUsu.Text Then
                mensaje = mensaje + "Las contraseñas no coinciden" + vbNewLine
            End If

            If String.IsNullOrEmpty(Me.cmbTipoModUsu.Text) Then
                mensaje = mensaje + "Por favor, elija un tipo de usuario" + vbNewLine
            End If

            If mensaje = "" Then
                BusUsuarios.modificarContrasenia(CInt(Me.txbNumUsuMod.Text), Me.txbRepContraUsu.Text)
                MsgBox("Modificación de contraseña correcto", vbOKOnly, "Ventana Éxito")
                Me.limpiarModContraseniaTipo()
                Me.listaUsuarios(Me.dgvUsuariosActivosAdm)
            Else
                MsgBox("Se produjeron los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error al modificar la contraseña o tipo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de cambio")
        End Try

    End Sub
    '-----------------------------------------------------------------------------------------------------------------------------------
    'Permite cargar el fondo de pantalla para la ventana
    'Parámetros:
    'nombreImagen-tipo:String-Representa el nombre de la imagen. Debe contener si es jpg,png, etc.
    '----------------------------------------------------------------------------------------------------------------------------------------
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub

    Private Sub listaUsuarios(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codUsu As New DataGridViewTextBoxColumn()
        Dim nombreUsu As New DataGridViewTextBoxColumn()
        Dim nombreEmp As New DataGridViewTextBoxColumn()
        Dim dni As New DataGridViewTextBoxColumn()
        Dim perfil As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim perfilSesion As Byte
        Dim estado As String

        Try
            perfilSesion = Sesion.getPerfilUsuario()
            'grilla.Rows.Clear()
            grilla.Columns().Clear()
            grilla.Columns.Add(codUsu)
            grilla.Columns.Add(nombreUsu)
            grilla.Columns.Add(nombreEmp)
            grilla.Columns.Add(dni)
            grilla.Columns.Add(perfil)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            codUsu.HeaderText = "Número Usuario"
            nombreUsu.HeaderText = "Nombre Usuario"
            nombreEmp.HeaderText = "Empleado"
            dni.HeaderText = "Dni"
            perfil.HeaderText = "Perfil"
            colEstado.HeaderText = "Estado"
            nombreEmp.Name = "nombreEmp"
            dni.Name = "dniEmp"
            colEstado.Name = "cambioEstado"
            codUsu.Name = "codUsu"

            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
            For Each usu In BusUsuarios.getUsuarios()
                If usu.estado Then
                    estado = "Activo"
                Else
                    estado = "Inactivo"
                End If


                grilla.Rows.Add(usu.idUsuario, usu.nombreUsuario, usu.Empleados.nombreEmpleado + " " + usu.Empleados.apellidoEmpleado, usu.Empleados.dni, usu.Perfiles.descripcionPerfil, estado)

            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Sub listaEmpleados(ByRef grilla As DataGridView, ByVal modificar As Boolean)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codEmpleado As New DataGridViewTextBoxColumn()
        Dim nombre As New DataGridViewTextBoxColumn()
        Dim apellido As New DataGridViewTextBoxColumn()
        Dim dni As New DataGridViewTextBoxColumn()
        Dim telefono As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim mail As New DataGridViewTextBoxColumn()
        Dim cantidadFilas As Integer
        Dim estado As String

        'grilla.ColumnCount = 8
        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codEmpleado)
            grilla.Columns.Add(nombre)
            grilla.Columns.Add(apellido)
            grilla.Columns.Add(dni)
            grilla.Columns.Add(telefono)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(mail)
            If Not modificar Then
                grilla.Columns.Add(boton)
            End If
            boton.HeaderText = "Alta/Baja"
            colEstado.Name = "cambioEstado"
            codEmpleado.Name = "codEmpleado"
            dni.Name = "dni"
            telefono.Name = "telefono"
            mail.Name = "mail"
            nombre.Name = "Nombre"
            apellido.Name = "Apellido"
            codEmpleado.HeaderText = "Número Empleado"
            nombre.HeaderText = "Nombre"
            apellido.HeaderText = "Apellido"
            dni.HeaderText = "Dni"
            telefono.HeaderText = "Telefono"
            colEstado.HeaderText = "Estado"
            mail.HeaderText = "Mail"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
            cantidadFilas = 0



            For Each empleado In BusEmpleados.getEmpleados()

                If empleado.estado Then
                    estado = "Activo"

                Else

                    estado = "Inactivo"
                End If
                grilla.Rows.Add(empleado.legajo, empleado.nombreEmpleado, empleado.apellidoEmpleado, empleado.dni, empleado.telefono, estado, empleado.mail)
                cantidadFilas = cantidadFilas + 1

            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub

    Private Sub agregarEmpleados(ByVal apellido As String, ByVal nombre As String, ByVal dni As Integer, ByVal telefono As String, ByVal mail As String)
        Try
            Dim mensajeError As String
            mensajeError = BusEmpleados.controlarDniRepetido(dni)
            If mensajeError = "" Then
                BusEmpleados.agregarNuevoEmpleado(apellido, nombre, dni, telefono, mail)
            Else
                MsgBox(mensajeError, vbOKOnly + vbExclamation, "Error de datos")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try

    End Sub

    Public Sub bajaEmpleado(ByRef grilla As DataGridView, ByVal modificar As Boolean)
        Dim valorPrueba As String
        Dim idEmpleado As Integer
        Dim respuesta As Integer
        Dim estado As String
        Dim nombre As String
        Dim apellido As String
        Dim dni As String
        Try
            valorPrueba = grilla.CurrentRow.Cells(0).Value
            If Not IsNothing(valorPrueba) And grilla.CurrentCell.GetType.ToString Like "*Button*" Then
                idEmpleado = grilla.CurrentRow.Cells("codEmpleado").Value
                nombre = grilla.CurrentRow.Cells("Nombre").Value
                apellido = grilla.CurrentRow.Cells("Apellido").Value
                dni = grilla.CurrentRow.Cells("Dni").Value
                estado = grilla.CurrentRow.Cells("cambioEstado").Value
                respuesta = MsgBox("¿Desea modificar el estado del empleado?: " + vbNewLine + nombre + " " + apellido + vbNewLine + "Dni: " + dni + vbNewLine + "Estado: " + estado, vbYesNo + vbDefaultButton2, "Consulta De Modificación")
                If respuesta = vbYes Then
                    BusEmpleados.cambiarEstadoEmpleado(CInt(idEmpleado), estado)
                    Me.listaEmpleados(grilla, modificar)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Sub ckbConfirmarModDni_CheckedChanged(sender As Object, e As EventArgs) Handles ckbConfirmarModDni.CheckedChanged
        Me.txbDniModEmpPropio.Enabled = Not Me.txbDniModEmpPropio.Enabled
    End Sub
End Class