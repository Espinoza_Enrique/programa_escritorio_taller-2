﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGestionarVentasAdm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGestionarVentasAdm))
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Me.tbcVentasAdm = New System.Windows.Forms.TabControl()
        Me.tpgListaVentasAdm = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lblPrecioNetoList = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.lblDescuentoList = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblPrecioBrutoList = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblUnidadVenList = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.lblClasificacionList = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lblNomProdList = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblMontoDescList = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblMontoVentaList = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblDatosEmpVentaAdm = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblFechaVentaAdm = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnApeListVentasFil = New System.Windows.Forms.Button()
        Me.txbApeListVentasFil = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnFiltroTotalList = New System.Windows.Forms.Button()
        Me.btnDniListVentasFil = New System.Windows.Forms.Button()
        Me.txbDniListVentasFil = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnFechaListVentasFil = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtpFiltroFechaFinVentasAdm = New System.Windows.Forms.DateTimePicker()
        Me.dtpFiltroFechaIniVentasAdm = New System.Windows.Forms.DateTimePicker()
        Me.grbDetallesVentaAdm = New System.Windows.Forms.GroupBox()
        Me.btnAnteriorDetallesAdm = New System.Windows.Forms.Button()
        Me.btnSiguienteDetallesAdm = New System.Windows.Forms.Button()
        Me.dgvDetallesListaVenta = New System.Windows.Forms.DataGridView()
        Me.grbListaVentasAdm = New System.Windows.Forms.GroupBox()
        Me.btnAnteriorVentasAdm = New System.Windows.Forms.Button()
        Me.btnSiguienteVentasAdm = New System.Windows.Forms.Button()
        Me.dgvListaVentas = New System.Windows.Forms.DataGridView()
        Me.tpgEstadisticasAdm = New System.Windows.Forms.TabPage()
        Me.btnVentasCatSubAdm = New System.Windows.Forms.Button()
        Me.btnVentasSubAdm = New System.Windows.Forms.Button()
        Me.btnVentasCatAdm = New System.Windows.Forms.Button()
        Me.btnVentasEmpAdm = New System.Windows.Forms.Button()
        Me.btnMasVendidoAdm = New System.Windows.Forms.Button()
        Me.chtVentasAdm = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.tpgDescuentos = New System.Windows.Forms.TabPage()
        Me.Filtro = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnFiltrarPorAmbosDesc = New System.Windows.Forms.Button()
        Me.btnFiltrarDniDesc = New System.Windows.Forms.Button()
        Me.txbDatosDniDesc = New System.Windows.Forms.TextBox()
        Me.lblTítuloDniDesc = New System.Windows.Forms.Label()
        Me.btnFiltrarPorFechaDesc = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpFechaFinDev = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaIniDev = New System.Windows.Forms.DateTimePicker()
        Me.grbDatosDevolucion = New System.Windows.Forms.GroupBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnAntDetDescVentas = New System.Windows.Forms.Button()
        Me.btnSigDetDescVentas = New System.Windows.Forms.Button()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAntDevVentas = New System.Windows.Forms.Button()
        Me.btnSigDevVentas = New System.Windows.Forms.Button()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.tbcVentasAdm.SuspendLayout()
        Me.tpgListaVentasAdm.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.grbDetallesVentaAdm.SuspendLayout()
        CType(Me.dgvDetallesListaVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbListaVentasAdm.SuspendLayout()
        CType(Me.dgvListaVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgEstadisticasAdm.SuspendLayout()
        CType(Me.chtVentasAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgDescuentos.SuspendLayout()
        Me.Filtro.SuspendLayout()
        Me.grbDatosDevolucion.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbcVentasAdm
        '
        Me.tbcVentasAdm.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbcVentasAdm.Controls.Add(Me.tpgListaVentasAdm)
        Me.tbcVentasAdm.Controls.Add(Me.tpgEstadisticasAdm)
        Me.tbcVentasAdm.Controls.Add(Me.tpgDescuentos)
        Me.tbcVentasAdm.Location = New System.Drawing.Point(12, 12)
        Me.tbcVentasAdm.Name = "tbcVentasAdm"
        Me.tbcVentasAdm.SelectedIndex = 0
        Me.tbcVentasAdm.Size = New System.Drawing.Size(1260, 710)
        Me.tbcVentasAdm.TabIndex = 0
        '
        'tpgListaVentasAdm
        '
        Me.tpgListaVentasAdm.BackColor = System.Drawing.Color.Silver
        Me.tpgListaVentasAdm.Controls.Add(Me.GroupBox5)
        Me.tpgListaVentasAdm.Controls.Add(Me.GroupBox4)
        Me.tpgListaVentasAdm.Controls.Add(Me.GroupBox3)
        Me.tpgListaVentasAdm.Controls.Add(Me.grbDetallesVentaAdm)
        Me.tpgListaVentasAdm.Controls.Add(Me.grbListaVentasAdm)
        Me.tpgListaVentasAdm.Location = New System.Drawing.Point(4, 22)
        Me.tpgListaVentasAdm.Name = "tpgListaVentasAdm"
        Me.tpgListaVentasAdm.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgListaVentasAdm.Size = New System.Drawing.Size(1252, 684)
        Me.tpgListaVentasAdm.TabIndex = 0
        Me.tpgListaVentasAdm.Text = "Lista De Ventas"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lblPrecioNetoList)
        Me.GroupBox5.Controls.Add(Me.Label28)
        Me.GroupBox5.Controls.Add(Me.lblDescuentoList)
        Me.GroupBox5.Controls.Add(Me.Label26)
        Me.GroupBox5.Controls.Add(Me.lblPrecioBrutoList)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Controls.Add(Me.lblUnidadVenList)
        Me.GroupBox5.Controls.Add(Me.Label21)
        Me.GroupBox5.Controls.Add(Me.lblClasificacionList)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.lblNomProdList)
        Me.GroupBox5.Controls.Add(Me.Label25)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(758, 342)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(488, 326)
        Me.GroupBox5.TabIndex = 25
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Datos Del Detalle"
        '
        'lblPrecioNetoList
        '
        Me.lblPrecioNetoList.AutoSize = True
        Me.lblPrecioNetoList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPrecioNetoList.Location = New System.Drawing.Point(317, 285)
        Me.lblPrecioNetoList.Name = "lblPrecioNetoList"
        Me.lblPrecioNetoList.Size = New System.Drawing.Size(165, 20)
        Me.lblPrecioNetoList.TabIndex = 11
        Me.lblPrecioNetoList.Text = "Precio bruto-descuento"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(7, 285)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(87, 18)
        Me.Label28.TabIndex = 10
        Me.Label28.Text = "Precio Neto"
        '
        'lblDescuentoList
        '
        Me.lblDescuentoList.AutoSize = True
        Me.lblDescuentoList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDescuentoList.Location = New System.Drawing.Point(282, 245)
        Me.lblDescuentoList.Name = "lblDescuentoList"
        Me.lblDescuentoList.Size = New System.Drawing.Size(200, 20)
        Me.lblDescuentoList.TabIndex = 9
        Me.lblDescuentoList.Text = "aqui va el monto descontado"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(7, 245)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(80, 18)
        Me.Label26.TabIndex = 8
        Me.Label26.Text = "Descuento"
        '
        'lblPrecioBrutoList
        '
        Me.lblPrecioBrutoList.AutoSize = True
        Me.lblPrecioBrutoList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPrecioBrutoList.Location = New System.Drawing.Point(270, 194)
        Me.lblPrecioBrutoList.Name = "lblPrecioBrutoList"
        Me.lblPrecioBrutoList.Size = New System.Drawing.Size(212, 20)
        Me.lblPrecioBrutoList.TabIndex = 7
        Me.lblPrecioBrutoList.Text = "aqui va el precio sin descuento"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(7, 194)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(91, 18)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "Precio Bruto"
        '
        'lblUnidadVenList
        '
        Me.lblUnidadVenList.AutoSize = True
        Me.lblUnidadVenList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblUnidadVenList.Location = New System.Drawing.Point(269, 142)
        Me.lblUnidadVenList.Name = "lblUnidadVenList"
        Me.lblUnidadVenList.Size = New System.Drawing.Size(213, 20)
        Me.lblUnidadVenList.TabIndex = 5
        Me.lblUnidadVenList.Text = "Aquí van las unidades vendidas"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(7, 142)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(70, 18)
        Me.Label21.TabIndex = 4
        Me.Label21.Text = "Unidades"
        '
        'lblClasificacionList
        '
        Me.lblClasificacionList.AutoSize = True
        Me.lblClasificacionList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblClasificacionList.Location = New System.Drawing.Point(256, 88)
        Me.lblClasificacionList.Name = "lblClasificacionList"
        Me.lblClasificacionList.Size = New System.Drawing.Size(226, 20)
        Me.lblClasificacionList.TabIndex = 3
        Me.lblClasificacionList.Text = "Aquí va la categoría/subcategoria"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(6, 88)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(92, 18)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Clasificación"
        '
        'lblNomProdList
        '
        Me.lblNomProdList.AutoSize = True
        Me.lblNomProdList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNomProdList.Location = New System.Drawing.Point(270, 44)
        Me.lblNomProdList.Name = "lblNomProdList"
        Me.lblNomProdList.Size = New System.Drawing.Size(212, 20)
        Me.lblNomProdList.TabIndex = 1
        Me.lblNomProdList.Text = "aquí va el nombre del producto"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(6, 44)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(69, 18)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "Producto"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblMontoDescList)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.lblMontoVentaList)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.lblDatosEmpVentaAdm)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.lblFechaVentaAdm)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(758, 111)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(488, 225)
        Me.GroupBox4.TabIndex = 24
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Datos De La Venta"
        '
        'lblMontoDescList
        '
        Me.lblMontoDescList.AutoSize = True
        Me.lblMontoDescList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMontoDescList.Location = New System.Drawing.Point(223, 190)
        Me.lblMontoDescList.Name = "lblMontoDescList"
        Me.lblMontoDescList.Size = New System.Drawing.Size(259, 20)
        Me.lblMontoDescList.TabIndex = 7
        Me.lblMontoDescList.Text = "aquí mostraría la cantidad descontada"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(7, 190)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(149, 18)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Monto En Descuento"
        '
        'lblMontoVentaList
        '
        Me.lblMontoVentaList.AutoSize = True
        Me.lblMontoVentaList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMontoVentaList.Location = New System.Drawing.Point(241, 136)
        Me.lblMontoVentaList.Name = "lblMontoVentaList"
        Me.lblMontoVentaList.Size = New System.Drawing.Size(241, 20)
        Me.lblMontoVentaList.TabIndex = 5
        Me.lblMontoVentaList.Text = "Aquí va el monto total de la compra"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(7, 136)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(92, 18)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Monto Venta"
        '
        'lblDatosEmpVentaAdm
        '
        Me.lblDatosEmpVentaAdm.AutoSize = True
        Me.lblDatosEmpVentaAdm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDatosEmpVentaAdm.Location = New System.Drawing.Point(263, 85)
        Me.lblDatosEmpVentaAdm.Name = "lblDatosEmpVentaAdm"
        Me.lblDatosEmpVentaAdm.Size = New System.Drawing.Size(219, 20)
        Me.lblDatosEmpVentaAdm.TabIndex = 3
        Me.lblDatosEmpVentaAdm.Text = "Aquí va el nombre del empleado"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(7, 85)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(75, 18)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Empleado"
        '
        'lblFechaVentaAdm
        '
        Me.lblFechaVentaAdm.AutoSize = True
        Me.lblFechaVentaAdm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblFechaVentaAdm.Location = New System.Drawing.Point(297, 32)
        Me.lblFechaVentaAdm.Name = "lblFechaVentaAdm"
        Me.lblFechaVentaAdm.Size = New System.Drawing.Size(185, 20)
        Me.lblFechaVentaAdm.TabIndex = 1
        Me.lblFechaVentaAdm.Text = "aquí va la fecha de la venta"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 32)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 18)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Fecha Venta"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.SpringGreen
        Me.GroupBox3.Controls.Add(Me.btnApeListVentasFil)
        Me.GroupBox3.Controls.Add(Me.txbApeListVentasFil)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.btnFiltroTotalList)
        Me.GroupBox3.Controls.Add(Me.btnDniListVentasFil)
        Me.GroupBox3.Controls.Add(Me.txbDniListVentasFil)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.btnFechaListVentasFil)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.dtpFiltroFechaFinVentasAdm)
        Me.GroupBox3.Controls.Add(Me.dtpFiltroFechaIniVentasAdm)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1240, 99)
        Me.GroupBox3.TabIndex = 23
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Filtro"
        '
        'btnApeListVentasFil
        '
        Me.btnApeListVentasFil.Location = New System.Drawing.Point(890, 21)
        Me.btnApeListVentasFil.Name = "btnApeListVentasFil"
        Me.btnApeListVentasFil.Size = New System.Drawing.Size(118, 65)
        Me.btnApeListVentasFil.TabIndex = 39
        Me.btnApeListVentasFil.Text = "Filtrar Por Apellido"
        Me.btnApeListVentasFil.UseVisualStyleBackColor = True
        '
        'txbApeListVentasFil
        '
        Me.txbApeListVentasFil.Location = New System.Drawing.Point(520, 62)
        Me.txbApeListVentasFil.Name = "txbApeListVentasFil"
        Me.txbApeListVentasFil.Size = New System.Drawing.Size(214, 24)
        Me.txbApeListVentasFil.TabIndex = 38
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(426, 65)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 18)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "Apellido "
        '
        'btnFiltroTotalList
        '
        Me.btnFiltroTotalList.Location = New System.Drawing.Point(1127, 21)
        Me.btnFiltroTotalList.Name = "btnFiltroTotalList"
        Me.btnFiltroTotalList.Size = New System.Drawing.Size(107, 65)
        Me.btnFiltroTotalList.TabIndex = 36
        Me.btnFiltroTotalList.Text = "Filtrar Por Ambos"
        Me.btnFiltroTotalList.UseVisualStyleBackColor = True
        '
        'btnDniListVentasFil
        '
        Me.btnDniListVentasFil.Location = New System.Drawing.Point(766, 21)
        Me.btnDniListVentasFil.Name = "btnDniListVentasFil"
        Me.btnDniListVentasFil.Size = New System.Drawing.Size(118, 65)
        Me.btnDniListVentasFil.TabIndex = 35
        Me.btnDniListVentasFil.Text = "Filtrar Por Dni"
        Me.btnDniListVentasFil.UseVisualStyleBackColor = True
        '
        'txbDniListVentasFil
        '
        Me.txbDniListVentasFil.Location = New System.Drawing.Point(520, 21)
        Me.txbDniListVentasFil.Name = "txbDniListVentasFil"
        Me.txbDniListVentasFil.Size = New System.Drawing.Size(214, 24)
        Me.txbDniListVentasFil.TabIndex = 34
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(413, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(101, 18)
        Me.Label9.TabIndex = 33
        Me.Label9.Text = "Dni Empleado"
        '
        'btnFechaListVentasFil
        '
        Me.btnFechaListVentasFil.Location = New System.Drawing.Point(1014, 21)
        Me.btnFechaListVentasFil.Name = "btnFechaListVentasFil"
        Me.btnFechaListVentasFil.Size = New System.Drawing.Size(107, 65)
        Me.btnFechaListVentasFil.TabIndex = 4
        Me.btnFechaListVentasFil.Text = "Filtrar Por Fecha"
        Me.btnFechaListVentasFil.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 65)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(73, 18)
        Me.Label10.TabIndex = 32
        Me.Label10.Text = "Fecha Fin"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(3, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(87, 18)
        Me.Label11.TabIndex = 31
        Me.Label11.Text = "Fecha Inicio"
        '
        'dtpFiltroFechaFinVentasAdm
        '
        Me.dtpFiltroFechaFinVentasAdm.Location = New System.Drawing.Point(128, 60)
        Me.dtpFiltroFechaFinVentasAdm.Name = "dtpFiltroFechaFinVentasAdm"
        Me.dtpFiltroFechaFinVentasAdm.Size = New System.Drawing.Size(264, 24)
        Me.dtpFiltroFechaFinVentasAdm.TabIndex = 30
        '
        'dtpFiltroFechaIniVentasAdm
        '
        Me.dtpFiltroFechaIniVentasAdm.Location = New System.Drawing.Point(128, 19)
        Me.dtpFiltroFechaIniVentasAdm.Name = "dtpFiltroFechaIniVentasAdm"
        Me.dtpFiltroFechaIniVentasAdm.Size = New System.Drawing.Size(264, 24)
        Me.dtpFiltroFechaIniVentasAdm.TabIndex = 29
        '
        'grbDetallesVentaAdm
        '
        Me.grbDetallesVentaAdm.BackColor = System.Drawing.Color.MediumTurquoise
        Me.grbDetallesVentaAdm.Controls.Add(Me.btnAnteriorDetallesAdm)
        Me.grbDetallesVentaAdm.Controls.Add(Me.btnSiguienteDetallesAdm)
        Me.grbDetallesVentaAdm.Controls.Add(Me.dgvDetallesListaVenta)
        Me.grbDetallesVentaAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDetallesVentaAdm.Location = New System.Drawing.Point(6, 370)
        Me.grbDetallesVentaAdm.Name = "grbDetallesVentaAdm"
        Me.grbDetallesVentaAdm.Size = New System.Drawing.Size(742, 298)
        Me.grbDetallesVentaAdm.TabIndex = 3
        Me.grbDetallesVentaAdm.TabStop = False
        Me.grbDetallesVentaAdm.Text = "Detalles Venta Seleccionada"
        '
        'btnAnteriorDetallesAdm
        '
        Me.btnAnteriorDetallesAdm.Image = CType(resources.GetObject("btnAnteriorDetallesAdm.Image"), System.Drawing.Image)
        Me.btnAnteriorDetallesAdm.Location = New System.Drawing.Point(662, 163)
        Me.btnAnteriorDetallesAdm.Name = "btnAnteriorDetallesAdm"
        Me.btnAnteriorDetallesAdm.Size = New System.Drawing.Size(72, 72)
        Me.btnAnteriorDetallesAdm.TabIndex = 5
        Me.btnAnteriorDetallesAdm.UseVisualStyleBackColor = True
        '
        'btnSiguienteDetallesAdm
        '
        Me.btnSiguienteDetallesAdm.Image = CType(resources.GetObject("btnSiguienteDetallesAdm.Image"), System.Drawing.Image)
        Me.btnSiguienteDetallesAdm.Location = New System.Drawing.Point(662, 87)
        Me.btnSiguienteDetallesAdm.Name = "btnSiguienteDetallesAdm"
        Me.btnSiguienteDetallesAdm.Size = New System.Drawing.Size(72, 72)
        Me.btnSiguienteDetallesAdm.TabIndex = 4
        Me.btnSiguienteDetallesAdm.UseVisualStyleBackColor = True
        '
        'dgvDetallesListaVenta
        '
        Me.dgvDetallesListaVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetallesListaVenta.Location = New System.Drawing.Point(6, 31)
        Me.dgvDetallesListaVenta.Name = "dgvDetallesListaVenta"
        Me.dgvDetallesListaVenta.Size = New System.Drawing.Size(640, 261)
        Me.dgvDetallesListaVenta.TabIndex = 1
        '
        'grbListaVentasAdm
        '
        Me.grbListaVentasAdm.BackColor = System.Drawing.Color.MediumTurquoise
        Me.grbListaVentasAdm.Controls.Add(Me.btnAnteriorVentasAdm)
        Me.grbListaVentasAdm.Controls.Add(Me.btnSiguienteVentasAdm)
        Me.grbListaVentasAdm.Controls.Add(Me.dgvListaVentas)
        Me.grbListaVentasAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbListaVentasAdm.Location = New System.Drawing.Point(6, 111)
        Me.grbListaVentasAdm.Name = "grbListaVentasAdm"
        Me.grbListaVentasAdm.Size = New System.Drawing.Size(742, 253)
        Me.grbListaVentasAdm.TabIndex = 2
        Me.grbListaVentasAdm.TabStop = False
        Me.grbListaVentasAdm.Text = "Lista De Ventas"
        '
        'btnAnteriorVentasAdm
        '
        Me.btnAnteriorVentasAdm.Image = CType(resources.GetObject("btnAnteriorVentasAdm.Image"), System.Drawing.Image)
        Me.btnAnteriorVentasAdm.Location = New System.Drawing.Point(664, 136)
        Me.btnAnteriorVentasAdm.Name = "btnAnteriorVentasAdm"
        Me.btnAnteriorVentasAdm.Size = New System.Drawing.Size(72, 72)
        Me.btnAnteriorVentasAdm.TabIndex = 3
        Me.btnAnteriorVentasAdm.UseVisualStyleBackColor = True
        '
        'btnSiguienteVentasAdm
        '
        Me.btnSiguienteVentasAdm.Image = CType(resources.GetObject("btnSiguienteVentasAdm.Image"), System.Drawing.Image)
        Me.btnSiguienteVentasAdm.Location = New System.Drawing.Point(664, 58)
        Me.btnSiguienteVentasAdm.Name = "btnSiguienteVentasAdm"
        Me.btnSiguienteVentasAdm.Size = New System.Drawing.Size(72, 72)
        Me.btnSiguienteVentasAdm.TabIndex = 2
        Me.btnSiguienteVentasAdm.UseVisualStyleBackColor = True
        '
        'dgvListaVentas
        '
        Me.dgvListaVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListaVentas.Location = New System.Drawing.Point(6, 31)
        Me.dgvListaVentas.Name = "dgvListaVentas"
        Me.dgvListaVentas.Size = New System.Drawing.Size(619, 216)
        Me.dgvListaVentas.TabIndex = 1
        '
        'tpgEstadisticasAdm
        '
        Me.tpgEstadisticasAdm.BackColor = System.Drawing.Color.Silver
        Me.tpgEstadisticasAdm.Controls.Add(Me.btnVentasCatSubAdm)
        Me.tpgEstadisticasAdm.Controls.Add(Me.btnVentasSubAdm)
        Me.tpgEstadisticasAdm.Controls.Add(Me.btnVentasCatAdm)
        Me.tpgEstadisticasAdm.Controls.Add(Me.btnVentasEmpAdm)
        Me.tpgEstadisticasAdm.Controls.Add(Me.btnMasVendidoAdm)
        Me.tpgEstadisticasAdm.Controls.Add(Me.chtVentasAdm)
        Me.tpgEstadisticasAdm.Location = New System.Drawing.Point(4, 22)
        Me.tpgEstadisticasAdm.Name = "tpgEstadisticasAdm"
        Me.tpgEstadisticasAdm.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgEstadisticasAdm.Size = New System.Drawing.Size(1252, 684)
        Me.tpgEstadisticasAdm.TabIndex = 1
        Me.tpgEstadisticasAdm.Text = "Estadística Ventas"
        '
        'btnVentasCatSubAdm
        '
        Me.btnVentasCatSubAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVentasCatSubAdm.Location = New System.Drawing.Point(894, 586)
        Me.btnVentasCatSubAdm.Name = "btnVentasCatSubAdm"
        Me.btnVentasCatSubAdm.Size = New System.Drawing.Size(192, 78)
        Me.btnVentasCatSubAdm.TabIndex = 5
        Me.btnVentasCatSubAdm.Text = "Ventas Por Categoria/Subcategoría"
        Me.btnVentasCatSubAdm.UseVisualStyleBackColor = True
        '
        'btnVentasSubAdm
        '
        Me.btnVentasSubAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVentasSubAdm.Location = New System.Drawing.Point(696, 586)
        Me.btnVentasSubAdm.Name = "btnVentasSubAdm"
        Me.btnVentasSubAdm.Size = New System.Drawing.Size(192, 78)
        Me.btnVentasSubAdm.TabIndex = 4
        Me.btnVentasSubAdm.Text = "Ventas Por Subcategoría"
        Me.btnVentasSubAdm.UseVisualStyleBackColor = True
        '
        'btnVentasCatAdm
        '
        Me.btnVentasCatAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVentasCatAdm.Location = New System.Drawing.Point(498, 586)
        Me.btnVentasCatAdm.Name = "btnVentasCatAdm"
        Me.btnVentasCatAdm.Size = New System.Drawing.Size(192, 78)
        Me.btnVentasCatAdm.TabIndex = 3
        Me.btnVentasCatAdm.Text = "Ventas Por Categoría"
        Me.btnVentasCatAdm.UseVisualStyleBackColor = True
        '
        'btnVentasEmpAdm
        '
        Me.btnVentasEmpAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVentasEmpAdm.Location = New System.Drawing.Point(300, 586)
        Me.btnVentasEmpAdm.Name = "btnVentasEmpAdm"
        Me.btnVentasEmpAdm.Size = New System.Drawing.Size(192, 78)
        Me.btnVentasEmpAdm.TabIndex = 2
        Me.btnVentasEmpAdm.Text = "Ventas Por Empleado"
        Me.btnVentasEmpAdm.UseVisualStyleBackColor = True
        '
        'btnMasVendidoAdm
        '
        Me.btnMasVendidoAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMasVendidoAdm.Location = New System.Drawing.Point(102, 586)
        Me.btnMasVendidoAdm.Name = "btnMasVendidoAdm"
        Me.btnMasVendidoAdm.Size = New System.Drawing.Size(192, 78)
        Me.btnMasVendidoAdm.TabIndex = 1
        Me.btnMasVendidoAdm.Text = "Más Vendidos Por Mes"
        Me.btnMasVendidoAdm.UseVisualStyleBackColor = True
        '
        'chtVentasAdm
        '
        Me.chtVentasAdm.BackColor = System.Drawing.Color.LightGoldenrodYellow
        ChartArea1.Name = "ChartArea1"
        Me.chtVentasAdm.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.chtVentasAdm.Legends.Add(Legend1)
        Me.chtVentasAdm.Location = New System.Drawing.Point(6, 6)
        Me.chtVentasAdm.Name = "chtVentasAdm"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.chtVentasAdm.Series.Add(Series1)
        Me.chtVentasAdm.Size = New System.Drawing.Size(1240, 559)
        Me.chtVentasAdm.TabIndex = 0
        Me.chtVentasAdm.Text = "Chart1"
        '
        'tpgDescuentos
        '
        Me.tpgDescuentos.BackColor = System.Drawing.Color.Silver
        Me.tpgDescuentos.Controls.Add(Me.Filtro)
        Me.tpgDescuentos.Controls.Add(Me.grbDatosDevolucion)
        Me.tpgDescuentos.Controls.Add(Me.GroupBox2)
        Me.tpgDescuentos.Controls.Add(Me.GroupBox1)
        Me.tpgDescuentos.Location = New System.Drawing.Point(4, 22)
        Me.tpgDescuentos.Name = "tpgDescuentos"
        Me.tpgDescuentos.Size = New System.Drawing.Size(1252, 684)
        Me.tpgDescuentos.TabIndex = 2
        Me.tpgDescuentos.Text = "Descuentos Realizados"
        '
        'Filtro
        '
        Me.Filtro.BackColor = System.Drawing.Color.SpringGreen
        Me.Filtro.Controls.Add(Me.Button1)
        Me.Filtro.Controls.Add(Me.TextBox6)
        Me.Filtro.Controls.Add(Me.Label7)
        Me.Filtro.Controls.Add(Me.btnFiltrarPorAmbosDesc)
        Me.Filtro.Controls.Add(Me.btnFiltrarDniDesc)
        Me.Filtro.Controls.Add(Me.txbDatosDniDesc)
        Me.Filtro.Controls.Add(Me.lblTítuloDniDesc)
        Me.Filtro.Controls.Add(Me.btnFiltrarPorFechaDesc)
        Me.Filtro.Controls.Add(Me.Label4)
        Me.Filtro.Controls.Add(Me.Label1)
        Me.Filtro.Controls.Add(Me.dtpFechaFinDev)
        Me.Filtro.Controls.Add(Me.dtpFechaIniDev)
        Me.Filtro.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Filtro.Location = New System.Drawing.Point(3, 3)
        Me.Filtro.Name = "Filtro"
        Me.Filtro.Size = New System.Drawing.Size(1246, 99)
        Me.Filtro.TabIndex = 22
        Me.Filtro.TabStop = False
        Me.Filtro.Text = "Filtro"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(896, 21)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(118, 65)
        Me.Button1.TabIndex = 39
        Me.Button1.Text = "Filtrar Por Apellido"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(520, 62)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(214, 24)
        Me.TextBox6.TabIndex = 38
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(424, 65)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 18)
        Me.Label7.TabIndex = 37
        Me.Label7.Text = "Apellido "
        '
        'btnFiltrarPorAmbosDesc
        '
        Me.btnFiltrarPorAmbosDesc.Location = New System.Drawing.Point(1133, 21)
        Me.btnFiltrarPorAmbosDesc.Name = "btnFiltrarPorAmbosDesc"
        Me.btnFiltrarPorAmbosDesc.Size = New System.Drawing.Size(107, 65)
        Me.btnFiltrarPorAmbosDesc.TabIndex = 36
        Me.btnFiltrarPorAmbosDesc.Text = "Filtrar Por Ambos"
        Me.btnFiltrarPorAmbosDesc.UseVisualStyleBackColor = True
        '
        'btnFiltrarDniDesc
        '
        Me.btnFiltrarDniDesc.Location = New System.Drawing.Point(772, 22)
        Me.btnFiltrarDniDesc.Name = "btnFiltrarDniDesc"
        Me.btnFiltrarDniDesc.Size = New System.Drawing.Size(118, 64)
        Me.btnFiltrarDniDesc.TabIndex = 35
        Me.btnFiltrarDniDesc.Text = "Filtrar Por Dni"
        Me.btnFiltrarDniDesc.UseVisualStyleBackColor = True
        '
        'txbDatosDniDesc
        '
        Me.txbDatosDniDesc.Location = New System.Drawing.Point(520, 21)
        Me.txbDatosDniDesc.Name = "txbDatosDniDesc"
        Me.txbDatosDniDesc.Size = New System.Drawing.Size(214, 24)
        Me.txbDatosDniDesc.TabIndex = 34
        '
        'lblTítuloDniDesc
        '
        Me.lblTítuloDniDesc.AutoSize = True
        Me.lblTítuloDniDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTítuloDniDesc.Location = New System.Drawing.Point(413, 24)
        Me.lblTítuloDniDesc.Name = "lblTítuloDniDesc"
        Me.lblTítuloDniDesc.Size = New System.Drawing.Size(101, 18)
        Me.lblTítuloDniDesc.TabIndex = 33
        Me.lblTítuloDniDesc.Text = "Dni Empleado"
        '
        'btnFiltrarPorFechaDesc
        '
        Me.btnFiltrarPorFechaDesc.Location = New System.Drawing.Point(1020, 21)
        Me.btnFiltrarPorFechaDesc.Name = "btnFiltrarPorFechaDesc"
        Me.btnFiltrarPorFechaDesc.Size = New System.Drawing.Size(107, 65)
        Me.btnFiltrarPorFechaDesc.TabIndex = 4
        Me.btnFiltrarPorFechaDesc.Text = "Filtrar Por Fecha"
        Me.btnFiltrarPorFechaDesc.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 65)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 18)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Fecha Fin"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 18)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Fecha Inicio"
        '
        'dtpFechaFinDev
        '
        Me.dtpFechaFinDev.Location = New System.Drawing.Point(128, 60)
        Me.dtpFechaFinDev.Name = "dtpFechaFinDev"
        Me.dtpFechaFinDev.Size = New System.Drawing.Size(264, 24)
        Me.dtpFechaFinDev.TabIndex = 30
        '
        'dtpFechaIniDev
        '
        Me.dtpFechaIniDev.Location = New System.Drawing.Point(128, 19)
        Me.dtpFechaIniDev.Name = "dtpFechaIniDev"
        Me.dtpFechaIniDev.Size = New System.Drawing.Size(264, 24)
        Me.dtpFechaIniDev.TabIndex = 29
        '
        'grbDatosDevolucion
        '
        Me.grbDatosDevolucion.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.grbDatosDevolucion.Controls.Add(Me.TextBox5)
        Me.grbDatosDevolucion.Controls.Add(Me.Label6)
        Me.grbDatosDevolucion.Controls.Add(Me.TextBox4)
        Me.grbDatosDevolucion.Controls.Add(Me.Label5)
        Me.grbDatosDevolucion.Controls.Add(Me.TextBox1)
        Me.grbDatosDevolucion.Controls.Add(Me.TextBox3)
        Me.grbDatosDevolucion.Controls.Add(Me.Label3)
        Me.grbDatosDevolucion.Controls.Add(Me.Label2)
        Me.grbDatosDevolucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosDevolucion.Location = New System.Drawing.Point(656, 108)
        Me.grbDatosDevolucion.Name = "grbDatosDevolucion"
        Me.grbDatosDevolucion.Size = New System.Drawing.Size(593, 573)
        Me.grbDatosDevolucion.TabIndex = 21
        Me.grbDatosDevolucion.TabStop = False
        Me.grbDatosDevolucion.Text = "Detalles Descuento"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(212, 304)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(375, 24)
        Me.TextBox5.TabIndex = 30
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 390)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(137, 18)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Detalles Descuento"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(6, 47)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(581, 130)
        Me.TextBox4.TabIndex = 28
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 18)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Nombre Producto"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(6, 411)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(581, 156)
        Me.TextBox1.TabIndex = 26
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(212, 235)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(375, 24)
        Me.TextBox3.TabIndex = 24
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 307)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 18)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Motivo Descuento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 238)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 18)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Descuento"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Teal
        Me.GroupBox2.Controls.Add(Me.btnAntDetDescVentas)
        Me.GroupBox2.Controls.Add(Me.btnSigDetDescVentas)
        Me.GroupBox2.Controls.Add(Me.DataGridView4)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(3, 406)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(647, 275)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "LIsta Descuentos"
        '
        'btnAntDetDescVentas
        '
        Me.btnAntDetDescVentas.Image = CType(resources.GetObject("btnAntDetDescVentas.Image"), System.Drawing.Image)
        Me.btnAntDetDescVentas.Location = New System.Drawing.Point(569, 155)
        Me.btnAntDetDescVentas.Name = "btnAntDetDescVentas"
        Me.btnAntDetDescVentas.Size = New System.Drawing.Size(72, 72)
        Me.btnAntDetDescVentas.TabIndex = 3
        Me.btnAntDetDescVentas.UseVisualStyleBackColor = True
        '
        'btnSigDetDescVentas
        '
        Me.btnSigDetDescVentas.Image = CType(resources.GetObject("btnSigDetDescVentas.Image"), System.Drawing.Image)
        Me.btnSigDetDescVentas.Location = New System.Drawing.Point(569, 77)
        Me.btnSigDetDescVentas.Name = "btnSigDetDescVentas"
        Me.btnSigDetDescVentas.Size = New System.Drawing.Size(72, 72)
        Me.btnSigDetDescVentas.TabIndex = 2
        Me.btnSigDetDescVentas.UseVisualStyleBackColor = True
        '
        'DataGridView4
        '
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView4.Location = New System.Drawing.Point(6, 23)
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.Size = New System.Drawing.Size(557, 246)
        Me.DataGridView4.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.LightSkyBlue
        Me.GroupBox1.Controls.Add(Me.btnAntDevVentas)
        Me.GroupBox1.Controls.Add(Me.btnSigDevVentas)
        Me.GroupBox1.Controls.Add(Me.DataGridView3)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(3, 108)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(647, 292)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Lista De Ventas"
        '
        'btnAntDevVentas
        '
        Me.btnAntDevVentas.Image = CType(resources.GetObject("btnAntDevVentas.Image"), System.Drawing.Image)
        Me.btnAntDevVentas.Location = New System.Drawing.Point(569, 156)
        Me.btnAntDevVentas.Name = "btnAntDevVentas"
        Me.btnAntDevVentas.Size = New System.Drawing.Size(72, 72)
        Me.btnAntDevVentas.TabIndex = 3
        Me.btnAntDevVentas.UseVisualStyleBackColor = True
        '
        'btnSigDevVentas
        '
        Me.btnSigDevVentas.Image = CType(resources.GetObject("btnSigDevVentas.Image"), System.Drawing.Image)
        Me.btnSigDevVentas.Location = New System.Drawing.Point(569, 78)
        Me.btnSigDevVentas.Name = "btnSigDevVentas"
        Me.btnSigDevVentas.Size = New System.Drawing.Size(72, 72)
        Me.btnSigDevVentas.TabIndex = 2
        Me.btnSigDevVentas.UseVisualStyleBackColor = True
        '
        'DataGridView3
        '
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Location = New System.Drawing.Point(6, 23)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(557, 263)
        Me.DataGridView3.TabIndex = 1
        '
        'frmGestionarVentasAdm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1284, 734)
        Me.Controls.Add(Me.tbcVentasAdm)
        Me.Name = "frmGestionarVentasAdm"
        Me.Text = "Ventas Administrador"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcVentasAdm.ResumeLayout(False)
        Me.tpgListaVentasAdm.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.grbDetallesVentaAdm.ResumeLayout(False)
        CType(Me.dgvDetallesListaVenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbListaVentasAdm.ResumeLayout(False)
        CType(Me.dgvListaVentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgEstadisticasAdm.ResumeLayout(False)
        CType(Me.chtVentasAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgDescuentos.ResumeLayout(False)
        Me.Filtro.ResumeLayout(False)
        Me.Filtro.PerformLayout()
        Me.grbDatosDevolucion.ResumeLayout(False)
        Me.grbDatosDevolucion.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbcVentasAdm As System.Windows.Forms.TabControl
    Friend WithEvents tpgListaVentasAdm As System.Windows.Forms.TabPage
    Friend WithEvents grbDetallesVentaAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnAnteriorDetallesAdm As System.Windows.Forms.Button
    Friend WithEvents btnSiguienteDetallesAdm As System.Windows.Forms.Button
    Friend WithEvents dgvDetallesListaVenta As System.Windows.Forms.DataGridView
    Friend WithEvents grbListaVentasAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnAnteriorVentasAdm As System.Windows.Forms.Button
    Friend WithEvents btnSiguienteVentasAdm As System.Windows.Forms.Button
    Friend WithEvents dgvListaVentas As System.Windows.Forms.DataGridView
    Friend WithEvents tpgEstadisticasAdm As System.Windows.Forms.TabPage
    Friend WithEvents btnVentasCatSubAdm As System.Windows.Forms.Button
    Friend WithEvents btnVentasSubAdm As System.Windows.Forms.Button
    Friend WithEvents btnVentasCatAdm As System.Windows.Forms.Button
    Friend WithEvents btnVentasEmpAdm As System.Windows.Forms.Button
    Friend WithEvents btnMasVendidoAdm As System.Windows.Forms.Button
    Friend WithEvents chtVentasAdm As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents tpgDescuentos As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAntDetDescVentas As System.Windows.Forms.Button
    Friend WithEvents btnSigDetDescVentas As System.Windows.Forms.Button
    Friend WithEvents DataGridView4 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAntDevVentas As System.Windows.Forms.Button
    Friend WithEvents btnSigDevVentas As System.Windows.Forms.Button
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents Filtro As System.Windows.Forms.GroupBox
    Friend WithEvents btnFiltrarPorFechaDesc As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFinDev As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaIniDev As System.Windows.Forms.DateTimePicker
    Friend WithEvents grbDatosDevolucion As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents btnFiltrarDniDesc As System.Windows.Forms.Button
    Friend WithEvents txbDatosDniDesc As System.Windows.Forms.TextBox
    Friend WithEvents lblTítuloDniDesc As System.Windows.Forms.Label
    Friend WithEvents btnFiltrarPorAmbosDesc As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnApeListVentasFil As System.Windows.Forms.Button
    Friend WithEvents txbApeListVentasFil As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnFiltroTotalList As System.Windows.Forms.Button
    Friend WithEvents btnDniListVentasFil As System.Windows.Forms.Button
    Friend WithEvents txbDniListVentasFil As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnFechaListVentasFil As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtpFiltroFechaFinVentasAdm As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFiltroFechaIniVentasAdm As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPrecioNetoList As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents lblDescuentoList As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lblPrecioBrutoList As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblUnidadVenList As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents lblClasificacionList As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lblNomProdList As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents lblMontoDescList As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblMontoVentaList As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblDatosEmpVentaAdm As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblFechaVentaAdm As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
