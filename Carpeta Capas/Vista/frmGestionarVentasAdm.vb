﻿Public Class frmGestionarVentasAdm

    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de ventas", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'control-Tipo: TabControl-Representa el tab control cuyas tab pages se quiere seleccionar
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Sub seleccionarTabControl(ByVal elemento As Byte)
        Try
            If elemento >= 1 And elemento <= Me.tbcVentasAdm.TabCount Then
                Me.tbcVentasAdm.SelectedIndex = elemento - 1
                Me.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox("Error al seleccionar una pestaña válida", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub


    Private Sub frmGestionarVentasAdm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al cerrar la ventana", vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub

    Private Sub frmGestionarVentasAdm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.prepararFormulario()
    End Sub
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub prepararFormulario()
        Try
            Me.dtpFiltroFechaFinVentasAdm.Format = DateTimePickerFormat.Short
            Me.dtpFiltroFechaIniVentasAdm.Format = DateTimePickerFormat.Short
            Me.dtpFechaFinDev.Format = DateTimePickerFormat.Short
            Me.dtpFechaIniDev.Format = DateTimePickerFormat.Short
            Me.btnAntDevVentas.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.btnAntDetDescVentas.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
        Catch ex As Exception
            MsgBox("Error al dar formato a la ventana de ventas", vbOKOnly + vbCritical, "Error de formato")
        End Try
    End Sub

End Class