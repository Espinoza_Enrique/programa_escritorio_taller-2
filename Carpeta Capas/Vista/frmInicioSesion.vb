﻿'Formulario para el inicio de sesión de los usuarios
Imports BusinessLayer
Imports Entities
'Imports Controladores
Public Class frmInicioSesion
    'Dim negocioUsu As New UsuarioBus
    ' Dim entidadUsu As New Usuario

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        'Dim perfil As Byte
        Dim nombreUsu As String
        Dim password As String
        Dim idLogueo As Integer
        Dim ventanaInicial As New frmVentanaInicialAdm

        ' Dim idUsuario As Byte
        'perfil = 1
        Try
            nombreUsu = Trim(Me.txbNombreUsuario.Text)
            password = Trim(Me.txbPassword.Text)
            If Not General.controlarCajaEnBlanco(nombreUsu) Or Not General.controlarCajaEnBlanco(password) Then
                Dim usuario As Entities.Usuarios
                usuario = BusUsuarios.controlarAcceso(nombreUsu, password)
                If Not IsNothing(usuario) Then
                    MsgBox("Acceso concedido", vbOKOnly, "Éxito en el acceso")
                    Me.limpiarCampos()
                    Me.Visible = False
                    BusLogueos.nuevoLogueo(usuario.idUsuario)
                    idLogueo = BusLogueos.idLogueoSesionActual()
                    Sesion.establecerValores(usuario.idUsuario, usuario.Empleados.nombreEmpleado, usuario.Empleados.apellidoEmpleado, usuario.Empleados.dni, usuario.idPerfil, usuario.idEmpleado, idLogueo, usuario.Perfiles.descripcionPerfil)
                    ventanaInicial.abrirVentana(Me)
                Else
                    MsgBox("Usuario y contraseña no válidos", vbOKOnly + vbExclamation, "Error De Ingreso De Datos")
                End If
            Else
                MsgBox("Por favor, coloque el nombre de usuario y contraseña", vbOKOnly + vbExclamation, "Error De Ingreso De Datos")
            End If
        Catch ex As Exception
            MsgBox("Error al iniciar sesión", vbOKOnly + vbCritical, "Error de inicio de sesión")
        End Try
    End Sub
    Public Sub abrirVentana(ByRef ventanaInicial As Form)
        Try
            ventanaInicial.Dispose()
            Me.Show()
        Catch ex As Exception
            MsgBox("Error al intentar abrir la ventana de inicio de sesión", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub frmInicioSesion_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            If General.consultarSalida() Then
                End
            Else
                e.Cancel = True
            End If
        Catch ex As Exception
            MsgBox("Error al cerrar la ventana de inicio de sesión", vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub
    Private Sub limpiarCampos()
        Try
            Me.txbNombreUsuario.Text = ""
            Me.txbPassword.Text = ""
        Catch ex As Exception
            MsgBox("Error al limpiar campos de nombre usuario y contraseña", vbOKOnly + vbCritical, "Error de refresco")
        End Try
    End Sub
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub

    Private Sub frmInicioSesion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim administrador As New Perfiles(1, "Administrador", True)
            Dim supervisor As New Perfiles(2, "Supervisor", True)
            Dim empleado As New Perfiles(3, "Empleado", True)

            Tienda.agregarPerfil(administrador)
            Tienda.agregarPerfil(supervisor)
            Tienda.agregarPerfil(empleado)
            Dim empleado1 As New Empleados(1, "Enrique", "Espinoza", 32837262, "espinoza.enrique.87@gmail.com", "4456718", True)
            Dim empleado2 As New Empleados(2, "Mariana", "Gonzales", 30887262, "marigonza@gmail.com", "4470718", True)
            Dim empleado3 As New Empleados(3, "Felipe", "Lopez", 30871262, "fellopez@gmail.com", "4498738", True)
            Dim empleado4 As New Empleados(4, "Rafael", "Fernandez", 32771262, "rafFer@gmail.com", "4876738", True)
            Dim empleado5 As New Empleados(5, "Viviana", "Mendez", 31871262, "viviMen@gmail.com", "4428731", True)
            Dim empleado6 As New Empleados(6, "Lorena", "Echeverria", 33171262, "loreEche@gmail.com", "4438739", True)
            Dim empleado7 As New Empleados(7, "Máximo", "Meridio", 36671262, "maximusElEsparano@gmail.com", "4488718", True)
            Dim empleado8 As New Empleados(8, "Miriam", "Ledezma", 34871262, "mirilezdi@gmail.com", "4438735", True)
            Dim empleado9 As New Empleados(9, "Lorenzo", "Alvarez", 39971263, "alvarezloren@gmail.com", "4477731", True)
            Dim empleado10 As New Empleados(10, "Penélope", "Gutierrez", 39721262, "pennygu@gmail.com", "4498658", True)
            Tienda.agregarEmpleados(empleado1)
            Tienda.agregarEmpleados(empleado2)
            Tienda.agregarEmpleados(empleado3)
            Tienda.agregarEmpleados(empleado4)
            Tienda.agregarEmpleados(empleado5)
            Tienda.agregarEmpleados(empleado6)
            Tienda.agregarEmpleados(empleado7)
            Tienda.agregarEmpleados(empleado8)
            Tienda.agregarEmpleados(empleado9)
            Tienda.agregarEmpleados(empleado10)
            Dim usuario1 As New Usuarios(1, "admin", "1234", 1, 1, True)
            Dim usuario2 As New Usuarios(2, "supervisor", "1234", 2, 2, True)
            Dim usuario3 As New Usuarios(3, "empleado", "1234", 3, 3, True)
            Dim usuario4 As New Usuarios(4, "rafa_fer", "1234", 4, 1, True) '"Rafael", "Fernandez"
            Dim usuario5 As New Usuarios(5, "viviMendez87", "1234", 5, 3, True) '"Viviana Mendez"
            Dim usuario6 As New Usuarios(6, "loreEche36", "1234", 6, 2, True) ' "Lorena", "Echeverria"
            Dim usuario7 As New Usuarios(7, "maximoMeridio99", "1234", 7, 3, True) '"Máximo", "Meridio"
            Dim usuario8 As New Usuarios(8, "ledezMiriam21", "1234", 8, 3, True) '"Miriam", "Ledezma"
            Dim usuario9 As New Usuarios(9, "lorenAlva761", "1234", 9, 3, True) '"Lorenzo", "Alvarez"
            Dim usuario10 As New Usuarios(10, "PennyGuti99", "1234", 10, 3, True) '"Penélope", "Gutierrez"
            Tienda.agregarUsuarios(usuario1)
            Tienda.agregarUsuarios(usuario2)
            Tienda.agregarUsuarios(usuario3)
            Dim categoria1 As New Categorias(1, "Dados", "Juego de tablero con datos o principalmente de datos")
            Dim categoria2 As New Categorias(2, "Tablero", "Juegos donde el principal componente son tableros o juegos de mesa similares")
            Dim categoria3 As New Categorias(3, "Cartas", "Juegos donde el único o principal elemento son las cartas")
            Tienda.agregarCategorias(categoria1)
            Tienda.agregarCategorias(categoria2)
            Tienda.agregarCategorias(categoria3)
            Dim subcategoria1 As New Subcategorias(1, "Familiar", "Un juego que puede ser clasificado para todas las edades")
            Dim subcategoria2 As New Subcategorias(2, "Lógica", "Un juego que precisa del ingenio para completarlo.Por su complejidad, es recomendada para adultos.")
            Dim subcategoria3 As New Subcategorias(3, "Fantasía", "Un juego cuya temática trata sobre elementos o temas fantásticos")
            Tienda.agregarSubcategoria(subcategoria1)
            Tienda.agregarSubcategoria(subcategoria2)
            Tienda.agregarSubcategoria(subcategoria3)
            'ModGlobal.setCerrarPorX(True)
        Catch ex As Exception
            MsgBox("Error al cargar la ventana de acceso", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class
