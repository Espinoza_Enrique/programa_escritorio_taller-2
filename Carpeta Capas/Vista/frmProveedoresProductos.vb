﻿Imports Entities
Imports BusinessLayer
Public Class frmProveedoresProductos
    Public Sub abrirVentana()
        Me.listarProductosProveedores(dgvProvProd)
        Me.ShowDialog()
    End Sub
    Private Sub activarDesactivarDatos(ByRef cajaDatos As GroupBox, ByVal estado As Boolean)
        cajaDatos.Enabled = estado
    End Sub

    Private Sub limpiarDatos()
        Me.grbDatosProdProv.Enabled = False
        Me.txbNroProd.Text = ""
        Me.txbNroProv.Text = ""
        Me.txbNombreProd.Text = ""
        Me.txbNombreProv.Text = ""
        Me.txbPrecioCompra.Text = ""
        'disparo el evento
        Me.chkModPrecioProdProv.Checked = True
        Me.chkModPrecioProdProv.Checked = False
    End Sub
    Private Sub moverDatosProdProv(ByRef grilla As DataGridView)
        Me.grbDatosProdProv.Enabled = True
        Me.txbNroProd.Text = grilla.CurrentRow.Cells("codProd").Value
        Me.txbNroProv.Text = grilla.CurrentRow.Cells("codProv").Value
        Me.txbNombreProd.Text = grilla.CurrentRow.Cells("nombreProducto").Value
        Me.txbNombreProv.Text = grilla.CurrentRow.Cells("nombreProveedor").Value
        Me.txbPrecioCompra.Text = grilla.CurrentRow.Cells("precioCompra").Value
        Me.chkModPrecioProdProv.Checked = True
        Me.chkModPrecioProdProv.Checked = False
    End Sub
    Private Sub listarProductosProveedores(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codProd As New DataGridViewTextBoxColumn()
        Dim codProv As New DataGridViewTextBoxColumn()
        Dim nombreProv As New DataGridViewTextBoxColumn()
        Dim nombreProd As New DataGridViewTextBoxColumn()
        Dim precioCompra As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim fechaAlta As New DataGridViewTextBoxColumn()

        Dim estado As String

        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProd)
            grilla.Columns.Add(codProv)
            grilla.Columns.Add(nombreProv)
            grilla.Columns.Add(precioCompra)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(fechaAlta)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            codProv.Name = "codProv"
            codProv.HeaderText = "Número Proveedor"
            nombreProd.Name = "nombreProducto"
            nombreProd.HeaderText = "Producto"
            nombreProv.Name = "nombreProveedor"
            nombreProv.HeaderText = "Proveedor"
            precioCompra.Name = "precioCompra"
            precioCompra.HeaderText = "Precio Compra"
            colEstado.Name = "estado"
            colEstado.HeaderText = "Estado"
            fechaAlta.Name = "fechaAlta"
            fechaAlta.HeaderText = "Fecha Alta"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
            'grilla.DataSource = BusProductos.getProductos()

            For Each prod In BusProveedoresProductos.gerProveedoresProductos()

                If prod.estado Then
                    estado = "Activo"

                Else
                    estado = "Inactivo"
                End If
                grilla.Rows.Add(prod.idProducto, prod.Productos.nombreProducto, prod.idProveedor, prod.Proveedores.nombreComercial, prod.precioCompra, estado, prod.fechaAlta)
            Next
        Catch ex As Exception
            MsgBox("Error al listar las relaciones productos/proveedores.Es posible que aún no haya guardado datos de ninguna relación." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try
    End Sub

    Private Sub chkModPrecioProdProv_CheckedChanged(sender As Object, e As EventArgs) Handles chkModPrecioProdProv.CheckedChanged
        Me.btnModPrecioProdProv.Enabled = Me.chkModPrecioProdProv.Checked
        Me.txbPrecioCompra.ReadOnly = Not Me.chkModPrecioProdProv.Checked
    End Sub

    Private Sub dgvProvProd_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProvProd.CellContentClick
        Dim idProducto As Integer
        Dim estado As Boolean
        If Not dgvProvProd.CurrentCell.GetType.ToString Like "*Button*" Then
            Me.moverDatosProdProv(Me.dgvProvProd)
        Else
            idProducto = CInt(dgvProvProd.CurrentRow.Cells("codProd").Value)
            ' If dgvEliminarProductos.CurrentRow.Cells("estado").Value = "Activo" Then
            'estado = False
            ' Else
            'estado = True
        End If
        'BusProveedoresProductos
        'End If

    End Sub

    Private Sub btnModPrecioProdProv_Click(sender As Object, e As EventArgs) Handles btnModPrecioProdProv.Click
        Dim mensaje As String
        mensaje = ""
        If General.controlarReales(Me.txbPrecioCompra.Text) Then
            If CDbl(Me.txbPrecioCompra.Text) <= 0 Then
                mensaje = mensaje + "Coloque un valor numérico mayor a cero para el precio de compra" + vbNewLine
            End If
        Else
            mensaje = mensaje + "Coloque por favor un valor numérico al nuevo precio de compra" + vbNewLine
        End If

        If String.IsNullOrWhiteSpace(mensaje) Then
            BusProveedoresProductos.modificarDatos(CInt(Me.txbNroProd.Text), CInt(Me.txbNroProv.Text), CDbl(Me.txbPrecioCompra.Text), False)
            MsgBox("Datos actualizados correctamente")
            Me.limpiarDatos()
            Me.listarProductosProveedores(Me.dgvProvProd)
        Else
            MsgBox("Error al modificar el precio de compra. Los errores producidos fueron: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Error de modificación de datos")
        End If
    End Sub

    Private Sub frmProveedoresProductos_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Me.Dispose()
    End Sub

    Private Sub frmProveedoresProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class