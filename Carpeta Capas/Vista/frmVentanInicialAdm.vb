﻿Public Class frmVentanaInicialAdm
    'permite controlar si la ventana se cerrará definitivamente o se cerrará para ir a otra ventana
    Private salirPorX As Boolean
    Private Enum pestaniasProductos
        NuevoProducto = 1
        ModificarProducto
        EliminarProducto
        ListarProducto
        AgregarCat
        AgregarProveedor
        ProveedorProducto
        Compras
        ModificarStockPrecio
    End Enum
    Private Enum pestaniasVentasAdm
        ListaVentasTotales = 1
        EstadisticaVentasTotales
        Descuentos
    End Enum
    Private Enum pestaniasVentasEmp
        RealizarVentas = 1
        EstadisticasVentasPropias
    End Enum
    Private Enum pestaniasUsuarios
        NuevoEmpleado = 1
        NuevoUsuario
        ModDatosUsuarios
        ModDatosPropios
    End Enum

    Public Sub abrirVentana(ByRef ventanaSesion As Form)
        Try
            ventanaSesion.Visible = False
            Me.Show()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana principal", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub setSalirPorX(ByVal valor As Boolean)
        Try
            Me.salirPorX = valor
        Catch ex As Exception
            MsgBox("Error al establecer cierre por el botón 'X'", vbOKOnly + vbCritical, "Error de asignación")
        End Try
    End Sub

    Private Function getSalirPorX() As Boolean
        Try
            Return Me.salirPorX
        Catch ex As Exception
            MsgBox("Error al saber si se cierra el programa por el botón 'X'", vbOKOnly + vbCritical, "Error de datos")
            Return False
        End Try
    End Function
    '------------------------------------------------------------------------------------------------------
    'Controla la salida del programa
    '------------------------------------------------------------------------------------------------------
    Private Function controlarSalidaPrograma() As Boolean
        Try
            Dim respuesta As Byte
            respuesta = MsgBox("¿Desea salir del programa?", vbYesNo + vbDefaultButton2, "Confirmar Salida")
            If respuesta = vbYes Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al consultar salida del programa", vbOKOnly + vbCritical, "Error de obtención de respuesta")
            Return False
        End Try
    End Function
    '-------------------------------------------------------------------------------------------------------
    'Controla el cierre de sesión
    '-------------------------------------------------------------------------------------------------------
    Private Sub cerrarSesion()
        Try
            If Sesion.cerrarSesion() Then
                frmInicioSesion.abrirVentana(Me)
            End If
        Catch ex As Exception
            MsgBox("Error al cerrar sesión", vbOKOnly + vbCritical, "Error de cierre de sesión")
        End Try
    End Sub

    Private Sub salir(ByRef evento As FormClosingEventArgs)
        Try
            If Me.getSalirPorX() Then
                If Me.controlarSalidaPrograma() Then
                    End
                Else
                    evento.Cancel = True
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al querer salir del programa", vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub



    Private Sub PtbProductos_Click(sender As Object, e As EventArgs) Handles PtbProductos.Click
        Try
            Dim ventanaProductos As New frmGestionarProductosAdm
            If Sesion.getNombrePerfil() <> "Empleado" Then
                ventanaProductos.abrirVentana()
            Else
                MsgBox("Debido a su perfil, usted no tiene acceso a estas opciones del programa", vbOKOnly + vbExclamation, "Acción No Válida")
            End If
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de productos", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub PtbUsuarios_Click(sender As Object, e As EventArgs) Handles PtbUsuarios.Click
        Try
            Dim ventanaUsuario As New frmGestionarUsuariosAdm
            Dim pestaniaPermitida As Byte
            If Sesion.getNombrePerfil() <> "Administrador" Then
                'el cuatro es la pestaña de modificar datos personales
                pestaniaPermitida = 4
            Else
                pestaniaPermitida = 1
            End If
            ventanaUsuario.seleccionarTabControl(pestaniaPermitida)
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de usuarios", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub


    Private Sub PtbDevoluciones_Click(sender As Object, e As EventArgs) Handles PtbDevoluciones.Click
        Try
            Dim ventanaDevAdm As New frmGestionarDevolucionesAdm
            Dim ventanaDevEmp As New frmDevolucionesEmp
            If Sesion.getNombrePerfil() <> "Empleado" Then
                ventanaDevAdm.abrirVentana()
            Else
                ventanaDevEmp.abrirVentana()
            End If
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de devoluciones", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub




    Private Sub PtbVentas_Click(sender As Object, e As EventArgs) Handles PtbVentas.Click
        Try
            Dim ventanaVentasEmp As New frmVentanaVentasEmp
            Dim ventanaVentasAdm As New frmGestionarVentasAdm
            If Sesion.getNombrePerfil() = "Empleado" Then
                ventanaVentasEmp.abrirVentana()
            Else
                ventanaVentasAdm.abrirVentana()
            End If
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de ventas", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub subItemRealizarRespaldo_Click(sender As Object, e As EventArgs) Handles subItemRealizarRespaldo.Click
        Try
            Dim ventanaRespaldo As New frmVentanaRespaldo
            ventanaRespaldo.abrirVentana()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de respaldo", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        Try
            Me.cerrarSesion()
        Catch ex As Exception
            MsgBox("Error al intentar cerrar la sesión", vbOKOnly + vbCritical, "Error de cierre de sesión")
        End Try
    End Sub

    Private Sub frmVentanaInicialAdm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.salir(e)
        Catch ex As Exception
            MsgBox("Error al cerrar la ventana inicial", vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub

    Private Sub frmVentanaInicialAdm_GotFocus(sender As Object, e As EventArgs) Handles Me.GotFocus
        Me.mostrarDatosUsuarioSesion()
    End Sub
    Private Sub frmVentanaInicialAdm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.cargarFondoPantalla("cobaltoplata.jpg")
            Me.setSalirPorX(True)
            Me.prepararVentanaInicial()
        Catch ex As Exception
            MsgBox("Error al cargar la ventana principal", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------
    'Deshabilita las opciones de menus según el perfil de los usuarios
    '---------------------------------------------------------------------------------------------
    Private Sub deshabilitarMenus()
        Dim subItemsTotales As Byte
        Dim subItemAnalizado As Byte

        Try
            subItemsTotales = 2
            subItemAnalizado = 0
            If Sesion.getNombrePerfil() <> "Administrador" Then
                While subItemAnalizado <= subItemsTotales
                    Me.menuUsuarios.DropDownItems(subItemAnalizado).Enabled = False
                    subItemAnalizado = subItemAnalizado + 1
                End While
                Me.menuProductos.Visible = False
                Me.menuVentas.DropDownItems(4).Enabled = False
            End If
            subItemsTotales = 3
            subItemAnalizado = 2
            If Sesion.getNombrePerfil() <> "Empleado" Then
                Me.menuDevoluciones.DropDownItems(0).Enabled = False
                While subItemAnalizado <= subItemsTotales
                    Me.menuVentas.DropDownItems(subItemAnalizado).Enabled = False
                    subItemAnalizado = subItemAnalizado + 1
                End While

            Else

                Me.menuVentas.DropDownItems(0).Enabled = False
                Me.menuVentas.DropDownItems(1).Enabled = False
                Me.menuDevoluciones.DropDownItems(1).Enabled = False
                Me.menuDevoluciones.DropDownItems(1).Enabled = False
            End If

        Catch ex As Exception
            MsgBox("Error al deshabilitar menúes", vbOKOnly + vbCritical, "Error de acceso")
        End Try

    End Sub
    '------------------------------------------------------------------------------------------------------
    'Adapta al menú principal dependiendo el perfil
    '------------------------------------------------------------------------------------------------------
    Private Sub prepararVentanaInicial()
        Try
            Me.deshabilitarMenus()
            Me.Text = "Ventana Inicial " + Sesion.getNombrePerfil()
            Me.mostrarDatosUsuarioSesion()
        Catch ex As Exception
            MsgBox("Error al formatear la ventana inicial", vbOKOnly + vbCritical, "Error de formato")
        End Try
    End Sub

    Private Sub mostrarDatosUsuarioSesion()
        Me.lblDatosSesion.Text = "Usuario: " + CStr(Sesion.getIdUsuario()) + "-" + Sesion.getApellidoUsu() + " " + Sesion.getNombreUsu() + "-" + CStr(Sesion.getDniUsuario())
    End Sub

    Private Sub subItemNuevoProd_Click(sender As Object, e As EventArgs) Handles subItemNuevoProd.Click
        Try
            Dim ventanaProdAdm As New frmGestionarProductosAdm
            ventanaProdAdm.seleccionarTabControl(pestaniasProductos.NuevoProducto)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de nuevos productos", vbOKOnly + vbCritical, "Error de acceso")
        End Try

    End Sub

    Private Sub subItemModificarProd_Click(sender As Object, e As EventArgs) Handles subItemModificarProd.Click
        Try
            Dim ventanaProdAdm As New frmGestionarProductosAdm
            ventanaProdAdm.seleccionarTabControl(pestaniasProductos.ModificarProducto)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de modificar productos", vbOKOnly + vbCritical, "Error de acceso")
        End Try

    End Sub



    Private Sub subItemListarPrdo_Click(sender As Object, e As EventArgs) Handles subItemListarPrdo.Click
        Try
            Dim ventanaProdAdm As New frmGestionarProductosAdm
            ventanaProdAdm.seleccionarTabControl(pestaniasProductos.ListarProducto)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de listar productos", vbOKOnly + vbCritical, "Error de acceso")
        End Try

    End Sub

    Private Sub subItemVentasTotales_Click(sender As Object, e As EventArgs) Handles subItemVentasTotales.Click
        Try
            Dim ventanaVentasAdm As New frmGestionarVentasAdm
            ventanaVentasAdm.seleccionarTabControl(pestaniasVentasAdm.ListaVentasTotales)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de ventas totales", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub



    Private Sub subItemNuevoEmp_Click(sender As Object, e As EventArgs) Handles subItemNuevoEmp.Click
        Try
            Dim ventanaUsuario As New frmGestionarUsuariosAdm
            ventanaUsuario.seleccionarTabControl(pestaniasUsuarios.NuevoEmpleado)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de nuevos empleado", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemNuevoSub_Click(sender As Object, e As EventArgs) Handles subItemNuevoSub.Click
        Try
            Dim ventanaUsuario As New frmGestionarUsuariosAdm
            ventanaUsuario.seleccionarTabControl(pestaniasUsuarios.NuevoUsuario)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de nuevos usuario", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub
    Private Sub subItemListarDev_Click(sender As Object, e As EventArgs) Handles subItemListarDev.Click
        Try
            Dim ventanaDev As New frmGestionarDevolucionesAdm
            ventanaDev.abrirVentana()
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de listar devoluciones", vbOKOnly + vbCritical, "Error de acceso")
        End Try

    End Sub

    Private Sub subItemRealizarDevoluciones_Click(sender As Object, e As EventArgs)
        Try
            Dim ventanaDevoluciones As New frmDevolucionesEmp
            ventanaDevoluciones.abrirVentana()
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción para realizar devoluciones", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemCerrarSesion_Click(sender As Object, e As EventArgs) Handles subItemCerrarSesion.Click
        Try
            Me.cerrarSesion()
        Catch ex As Exception
            MsgBox("Error al cerrar sesión", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemSalir_Click(sender As Object, e As EventArgs) Handles subItemSalir.Click
        Try
            Me.Close()
        Catch ex As Exception
            MsgBox("Error al salir del programa", vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub

    Private Sub subItemNuevaDev_Click(sender As Object, e As EventArgs) Handles subItemNuevaDev.Click
        Try
            Dim ventanaDevEmp As New frmDevolucionesEmp
            ventanaDevEmp.abrirVentana()
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de nueva devolución", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub ptbInformes_Click(sender As Object, e As EventArgs) Handles ptbInformes.Click
        Try
            Dim ventanaInformes As New frmGestionarInformesAdm
            ventanaInformes.abrirVentana()
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de informes", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemModInfoProd_Click(sender As Object, e As EventArgs) Handles subItemModInfoProd.Click
        Try
            Dim ventanaProductos As New frmGestionarProductosAdm
            ventanaProductos.seleccionarTabControl(pestaniasProductos.ModificarProducto)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de modificación datos de producto", vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub


    Private Sub subItemModStockPrecioProd_Click(sender As Object, e As EventArgs) Handles subItemModStockPrecioProd.Click
        Try
            Dim ventanaProductos As New frmGestionarProductosAdm
            ventanaProductos.seleccionarTabControl(pestaniasProductos.ModificarStockPrecio)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de modificar precio y stock:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemEliminarProd_Click(sender As Object, e As EventArgs) Handles subItemEliminarProd.Click
        Dim ventanaProductos As New frmGestionarProductosAdm
        Dim eliminarProd As Byte
        Try
            eliminarProd = 3
            ventanaProductos.seleccionarTabControl(eliminarProd)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de eliminar un producto:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemNuevoProv_Click(sender As Object, e As EventArgs) Handles subItemNuevoProv.Click
        Try
            Dim ventanaProductos As New frmGestionarProductosAdm
            ventanaProductos.seleccionarTabControl(pestaniasProductos.AgregarProveedor)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de nuevo proveedor:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemProdProv_Click(sender As Object, e As EventArgs) Handles subItemProdProv.Click
        Try
            Dim ventanaProductos As New frmGestionarProductosAdm
            ventanaProductos.seleccionarTabControl(pestaniasProductos.ProveedorProducto)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de productos y proveedores:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub


    Private Sub subItemAsentarCompras_Click(sender As Object, e As EventArgs) Handles subItemAsentarCompras.Click
        Try
            Dim ventanaProductos As New frmGestionarProductosAdm
            ventanaProductos.seleccionarTabControl(pestaniasProductos.Compras)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de asentar compra" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemEstadisticaVentaAdm_Click(sender As Object, e As EventArgs) Handles subItemEstadisticaVentaAdm.Click
        Try
            Dim ventanaVentasAdm As New frmGestionarVentasAdm
            ventanaVentasAdm.seleccionarTabControl(pestaniasVentasAdm.EstadisticaVentasTotales)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de estadísticas de ventas:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemRealizarVenta_Click(sender As Object, e As EventArgs) Handles subItemRealizarVenta.Click
        Try
            Dim ventanaVentaEmp As New frmVentanaVentasEmp
            ventanaVentaEmp.seleccionarTabControl(pestaniasVentasEmp.RealizarVentas)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de realizar venta:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemEstadisticaVentaEmp_Click(sender As Object, e As EventArgs) Handles subItemEstadisticaVentaEmp.Click
        Try
            Dim ventanaVentasEmp As New frmVentanaVentasEmp
            ventanaVentasEmp.seleccionarTabControl(pestaniasVentasEmp.EstadisticasVentasPropias)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de estadísticas de ventas propias:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemEliminarModUsu_Click(sender As Object, e As EventArgs) Handles subItemEliminarModUsu.Click
        Try
            Dim ventanaUsuario As New frmGestionarUsuariosAdm
            ventanaUsuario.seleccionarTabControl(pestaniasUsuarios.ModDatosUsuarios)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción modificar usuario:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemModDatosPropios_Click(sender As Object, e As EventArgs) Handles subItemModDatosPropios.Click
        Try
            Dim ventanaUsuario As New frmGestionarUsuariosAdm
            ventanaUsuario.seleccionarTabControl(pestaniasUsuarios.ModDatosPropios)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de modificar datos propios:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemDescuentos_Click(sender As Object, e As EventArgs) Handles subItemDescuentos.Click
        Try
            Dim ventanaVentas As New frmGestionarVentasAdm
            ventanaVentas.seleccionarTabControl(pestaniasVentasAdm.Descuentos)
        Catch ex As Exception
            MsgBox("Error al ingresar a la opción de descuentos:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub

    Private Sub subItemAcercaDe_Click(sender As Object, e As EventArgs) Handles subItemAcercaDe.Click
        Dim ventanaAcercaDe As New frmAcercaDe
        ventanaAcercaDe.ShowDialog()
    End Sub
End Class