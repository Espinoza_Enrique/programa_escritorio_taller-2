﻿Public Class frmVentanaRespaldo

    Public Sub abrirVentana()
        Me.ShowDialog()
    End Sub
    Private Sub frmVentanaRespaldo_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Me.Dispose()
    End Sub
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub frmVentanaRespaldo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class