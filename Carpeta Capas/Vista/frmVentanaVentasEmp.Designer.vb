﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVentanaVentasEmp
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVentanaVentasEmp))
        Dim ChartArea6 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend6 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series6 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Me.tbcVentasEmpleado = New System.Windows.Forms.TabControl()
        Me.tpgVentas = New System.Windows.Forms.TabPage()
        Me.grbCarritoProdEmp = New System.Windows.Forms.GroupBox()
        Me.lblTotalPago = New System.Windows.Forms.Label()
        Me.lblTituloTotalPago = New System.Windows.Forms.Label()
        Me.btnConfirmarVenta = New System.Windows.Forms.Button()
        Me.dgvCarritoProdEmp = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.grbListaProdEmp = New System.Windows.Forms.GroupBox()
        Me.btnAntProdEmp = New System.Windows.Forms.Button()
        Me.btnSigProdEmp = New System.Windows.Forms.Button()
        Me.grbFiltradoProdEmo = New System.Windows.Forms.GroupBox()
        Me.txbIdProdEmp = New System.Windows.Forms.TextBox()
        Me.txbNombreProdEmp = New System.Windows.Forms.TextBox()
        Me.lblTituloNombreProdEmp = New System.Windows.Forms.Label()
        Me.lblTituloIdProdEmp = New System.Windows.Forms.Label()
        Me.dgvListaProdEmp = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbDatosProdEmp = New System.Windows.Forms.GroupBox()
        Me.grbDatosVentas = New System.Windows.Forms.GroupBox()
        Me.nudCantProdEmp = New System.Windows.Forms.NumericUpDown()
        Me.lblFechaVenta = New System.Windows.Forms.Label()
        Me.lblCantProdEmp = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txbDetDescVentasEmp = New System.Windows.Forms.TextBox()
        Me.cmbMotDescVentasEmp = New System.Windows.Forms.ComboBox()
        Me.txbDescVentasEmp = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblMontoFinalVenta = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnAgregarCarrito = New System.Windows.Forms.Button()
        Me.grbDatosProductosVenta = New System.Windows.Forms.GroupBox()
        Me.lblIdProductoVenta = New System.Windows.Forms.Label()
        Me.lblPrecioUniVenta = New System.Windows.Forms.Label()
        Me.lblClasificacionVenta = New System.Windows.Forms.Label()
        Me.lblNomProdVentasEmp = New System.Windows.Forms.Label()
        Me.lblPrecioProdEmp = New System.Windows.Forms.Label()
        Me.lblClasificacionProdEmp = New System.Windows.Forms.Label()
        Me.lblIdProductoEmp = New System.Windows.Forms.Label()
        Me.tpgEstadisticaVentaEmp = New System.Windows.Forms.TabPage()
        Me.grbCriteriosEstadisticas = New System.Windows.Forms.GroupBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnVentasEmp = New System.Windows.Forms.Button()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.tbcVentasEmpleado.SuspendLayout()
        Me.tpgVentas.SuspendLayout()
        Me.grbCarritoProdEmp.SuspendLayout()
        CType(Me.dgvCarritoProdEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbListaProdEmp.SuspendLayout()
        Me.grbFiltradoProdEmo.SuspendLayout()
        CType(Me.dgvListaProdEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosProdEmp.SuspendLayout()
        Me.grbDatosVentas.SuspendLayout()
        CType(Me.nudCantProdEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosProductosVenta.SuspendLayout()
        Me.tpgEstadisticaVentaEmp.SuspendLayout()
        Me.grbCriteriosEstadisticas.SuspendLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbcVentasEmpleado
        '
        Me.tbcVentasEmpleado.Alignment = System.Windows.Forms.TabAlignment.Right
        Me.tbcVentasEmpleado.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbcVentasEmpleado.Controls.Add(Me.tpgVentas)
        Me.tbcVentasEmpleado.Controls.Add(Me.tpgEstadisticaVentaEmp)
        Me.tbcVentasEmpleado.Location = New System.Drawing.Point(12, 24)
        Me.tbcVentasEmpleado.Multiline = True
        Me.tbcVentasEmpleado.Name = "tbcVentasEmpleado"
        Me.tbcVentasEmpleado.SelectedIndex = 0
        Me.tbcVentasEmpleado.Size = New System.Drawing.Size(1310, 703)
        Me.tbcVentasEmpleado.TabIndex = 40
        '
        'tpgVentas
        '
        Me.tpgVentas.BackColor = System.Drawing.Color.Silver
        Me.tpgVentas.Controls.Add(Me.grbCarritoProdEmp)
        Me.tpgVentas.Controls.Add(Me.grbListaProdEmp)
        Me.tpgVentas.Controls.Add(Me.grbDatosProdEmp)
        Me.tpgVentas.Location = New System.Drawing.Point(4, 4)
        Me.tpgVentas.Name = "tpgVentas"
        Me.tpgVentas.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgVentas.Size = New System.Drawing.Size(1283, 695)
        Me.tpgVentas.TabIndex = 0
        Me.tpgVentas.Text = "Venta"
        '
        'grbCarritoProdEmp
        '
        Me.grbCarritoProdEmp.BackColor = System.Drawing.Color.LightSeaGreen
        Me.grbCarritoProdEmp.Controls.Add(Me.lblTotalPago)
        Me.grbCarritoProdEmp.Controls.Add(Me.lblTituloTotalPago)
        Me.grbCarritoProdEmp.Controls.Add(Me.btnConfirmarVenta)
        Me.grbCarritoProdEmp.Controls.Add(Me.dgvCarritoProdEmp)
        Me.grbCarritoProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbCarritoProdEmp.Location = New System.Drawing.Point(532, 428)
        Me.grbCarritoProdEmp.Name = "grbCarritoProdEmp"
        Me.grbCarritoProdEmp.Size = New System.Drawing.Size(742, 263)
        Me.grbCarritoProdEmp.TabIndex = 41
        Me.grbCarritoProdEmp.TabStop = False
        Me.grbCarritoProdEmp.Text = "Carrito"
        '
        'lblTotalPago
        '
        Me.lblTotalPago.AutoSize = True
        Me.lblTotalPago.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPago.Location = New System.Drawing.Point(92, 237)
        Me.lblTotalPago.Name = "lblTotalPago"
        Me.lblTotalPago.Size = New System.Drawing.Size(146, 20)
        Me.lblTotalPago.TabIndex = 33
        Me.lblTotalPago.Text = "Precio Total A Pagar"
        '
        'lblTituloTotalPago
        '
        Me.lblTituloTotalPago.AutoSize = True
        Me.lblTituloTotalPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloTotalPago.Location = New System.Drawing.Point(6, 239)
        Me.lblTituloTotalPago.Name = "lblTituloTotalPago"
        Me.lblTituloTotalPago.Size = New System.Drawing.Size(80, 18)
        Me.lblTituloTotalPago.TabIndex = 32
        Me.lblTituloTotalPago.Text = "Total Pago"
        '
        'btnConfirmarVenta
        '
        Me.btnConfirmarVenta.Location = New System.Drawing.Point(597, 214)
        Me.btnConfirmarVenta.Name = "btnConfirmarVenta"
        Me.btnConfirmarVenta.Size = New System.Drawing.Size(139, 43)
        Me.btnConfirmarVenta.TabIndex = 42
        Me.btnConfirmarVenta.Text = "Confirmar Venta"
        Me.btnConfirmarVenta.UseVisualStyleBackColor = True
        '
        'dgvCarritoProdEmp
        '
        Me.dgvCarritoProdEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCarritoProdEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.Column1, Me.Column4, Me.Column5, Me.Column6, Me.Column2, Me.Column3})
        Me.dgvCarritoProdEmp.Location = New System.Drawing.Point(6, 19)
        Me.dgvCarritoProdEmp.Name = "dgvCarritoProdEmp"
        Me.dgvCarritoProdEmp.Size = New System.Drawing.Size(730, 189)
        Me.dgvCarritoProdEmp.TabIndex = 29
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nombre Producto"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'Column1
        '
        Me.Column1.HeaderText = "Cantidad"
        Me.Column1.Name = "Column1"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Monto Descuento"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Motivo Descuento"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Detalle Descuento"
        Me.Column6.Name = "Column6"
        '
        'Column2
        '
        Me.Column2.HeaderText = "Quitar 1"
        Me.Column2.Name = "Column2"
        Me.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'Column3
        '
        Me.Column3.HeaderText = "Eliminar"
        Me.Column3.Name = "Column3"
        Me.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'grbListaProdEmp
        '
        Me.grbListaProdEmp.BackColor = System.Drawing.Color.DodgerBlue
        Me.grbListaProdEmp.Controls.Add(Me.btnAntProdEmp)
        Me.grbListaProdEmp.Controls.Add(Me.btnSigProdEmp)
        Me.grbListaProdEmp.Controls.Add(Me.grbFiltradoProdEmo)
        Me.grbListaProdEmp.Controls.Add(Me.dgvListaProdEmp)
        Me.grbListaProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbListaProdEmp.ForeColor = System.Drawing.Color.Crimson
        Me.grbListaProdEmp.Location = New System.Drawing.Point(532, 6)
        Me.grbListaProdEmp.Name = "grbListaProdEmp"
        Me.grbListaProdEmp.Size = New System.Drawing.Size(764, 416)
        Me.grbListaProdEmp.TabIndex = 40
        Me.grbListaProdEmp.TabStop = False
        Me.grbListaProdEmp.Text = "Lista De Productos"
        '
        'btnAntProdEmp
        '
        Me.btnAntProdEmp.Image = CType(resources.GetObject("btnAntProdEmp.Image"), System.Drawing.Image)
        Me.btnAntProdEmp.Location = New System.Drawing.Point(686, 277)
        Me.btnAntProdEmp.Name = "btnAntProdEmp"
        Me.btnAntProdEmp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnAntProdEmp.Size = New System.Drawing.Size(72, 72)
        Me.btnAntProdEmp.TabIndex = 46
        Me.btnAntProdEmp.UseVisualStyleBackColor = True
        '
        'btnSigProdEmp
        '
        Me.btnSigProdEmp.Image = CType(resources.GetObject("btnSigProdEmp.Image"), System.Drawing.Image)
        Me.btnSigProdEmp.Location = New System.Drawing.Point(686, 199)
        Me.btnSigProdEmp.Name = "btnSigProdEmp"
        Me.btnSigProdEmp.Size = New System.Drawing.Size(72, 72)
        Me.btnSigProdEmp.TabIndex = 45
        Me.btnSigProdEmp.UseVisualStyleBackColor = True
        '
        'grbFiltradoProdEmo
        '
        Me.grbFiltradoProdEmo.BackColor = System.Drawing.Color.SteelBlue
        Me.grbFiltradoProdEmo.Controls.Add(Me.txbIdProdEmp)
        Me.grbFiltradoProdEmo.Controls.Add(Me.txbNombreProdEmp)
        Me.grbFiltradoProdEmo.Controls.Add(Me.lblTituloNombreProdEmp)
        Me.grbFiltradoProdEmo.Controls.Add(Me.lblTituloIdProdEmp)
        Me.grbFiltradoProdEmo.ForeColor = System.Drawing.Color.Black
        Me.grbFiltradoProdEmo.Location = New System.Drawing.Point(6, 23)
        Me.grbFiltradoProdEmo.Name = "grbFiltradoProdEmo"
        Me.grbFiltradoProdEmo.Size = New System.Drawing.Size(752, 86)
        Me.grbFiltradoProdEmo.TabIndex = 29
        Me.grbFiltradoProdEmo.TabStop = False
        Me.grbFiltradoProdEmo.Text = "Filtrado Productos"
        '
        'txbIdProdEmp
        '
        Me.txbIdProdEmp.Location = New System.Drawing.Point(178, 56)
        Me.txbIdProdEmp.Name = "txbIdProdEmp"
        Me.txbIdProdEmp.Size = New System.Drawing.Size(195, 24)
        Me.txbIdProdEmp.TabIndex = 3
        '
        'txbNombreProdEmp
        '
        Me.txbNombreProdEmp.Location = New System.Drawing.Point(178, 24)
        Me.txbNombreProdEmp.Name = "txbNombreProdEmp"
        Me.txbNombreProdEmp.Size = New System.Drawing.Size(568, 24)
        Me.txbNombreProdEmp.TabIndex = 2
        '
        'lblTituloNombreProdEmp
        '
        Me.lblTituloNombreProdEmp.AutoSize = True
        Me.lblTituloNombreProdEmp.Location = New System.Drawing.Point(6, 59)
        Me.lblTituloNombreProdEmp.Name = "lblTituloNombreProdEmp"
        Me.lblTituloNombreProdEmp.Size = New System.Drawing.Size(127, 18)
        Me.lblTituloNombreProdEmp.TabIndex = 1
        Me.lblTituloNombreProdEmp.Text = "Número Producto"
        '
        'lblTituloIdProdEmp
        '
        Me.lblTituloIdProdEmp.AutoSize = True
        Me.lblTituloIdProdEmp.Location = New System.Drawing.Point(6, 27)
        Me.lblTituloIdProdEmp.Name = "lblTituloIdProdEmp"
        Me.lblTituloIdProdEmp.Size = New System.Drawing.Size(127, 18)
        Me.lblTituloIdProdEmp.TabIndex = 0
        Me.lblTituloIdProdEmp.Text = "Nombre Producto"
        '
        'dgvListaProdEmp
        '
        Me.dgvListaProdEmp.AllowUserToAddRows = False
        Me.dgvListaProdEmp.AllowUserToDeleteRows = False
        Me.dgvListaProdEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListaProdEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33})
        Me.dgvListaProdEmp.Location = New System.Drawing.Point(6, 115)
        Me.dgvListaProdEmp.Name = "dgvListaProdEmp"
        Me.dgvListaProdEmp.Size = New System.Drawing.Size(674, 295)
        Me.dgvListaProdEmp.TabIndex = 28
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "Nombre Producto"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "Categoria"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "Subcategoria"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "Stock"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        '
        'grbDatosProdEmp
        '
        Me.grbDatosProdEmp.BackColor = System.Drawing.Color.DarkSlateGray
        Me.grbDatosProdEmp.Controls.Add(Me.grbDatosVentas)
        Me.grbDatosProdEmp.Controls.Add(Me.grbDatosProductosVenta)
        Me.grbDatosProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProdEmp.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.grbDatosProdEmp.Location = New System.Drawing.Point(6, 6)
        Me.grbDatosProdEmp.Name = "grbDatosProdEmp"
        Me.grbDatosProdEmp.Size = New System.Drawing.Size(520, 685)
        Me.grbDatosProdEmp.TabIndex = 39
        Me.grbDatosProdEmp.TabStop = False
        Me.grbDatosProdEmp.Text = "Datos De La Venta"
        '
        'grbDatosVentas
        '
        Me.grbDatosVentas.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.grbDatosVentas.Controls.Add(Me.nudCantProdEmp)
        Me.grbDatosVentas.Controls.Add(Me.lblFechaVenta)
        Me.grbDatosVentas.Controls.Add(Me.lblCantProdEmp)
        Me.grbDatosVentas.Controls.Add(Me.Label4)
        Me.grbDatosVentas.Controls.Add(Me.txbDetDescVentasEmp)
        Me.grbDatosVentas.Controls.Add(Me.cmbMotDescVentasEmp)
        Me.grbDatosVentas.Controls.Add(Me.txbDescVentasEmp)
        Me.grbDatosVentas.Controls.Add(Me.Label3)
        Me.grbDatosVentas.Controls.Add(Me.Label2)
        Me.grbDatosVentas.Controls.Add(Me.lblMontoFinalVenta)
        Me.grbDatosVentas.Controls.Add(Me.Label7)
        Me.grbDatosVentas.Controls.Add(Me.btnAgregarCarrito)
        Me.grbDatosVentas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.grbDatosVentas.Location = New System.Drawing.Point(6, 255)
        Me.grbDatosVentas.Name = "grbDatosVentas"
        Me.grbDatosVentas.Size = New System.Drawing.Size(508, 424)
        Me.grbDatosVentas.TabIndex = 20
        Me.grbDatosVentas.TabStop = False
        Me.grbDatosVentas.Text = "Detalles Venta"
        '
        'nudCantProdEmp
        '
        Me.nudCantProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudCantProdEmp.Location = New System.Drawing.Point(165, 31)
        Me.nudCantProdEmp.Name = "nudCantProdEmp"
        Me.nudCantProdEmp.Size = New System.Drawing.Size(59, 24)
        Me.nudCantProdEmp.TabIndex = 29
        '
        'lblFechaVenta
        '
        Me.lblFechaVenta.AutoSize = True
        Me.lblFechaVenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblFechaVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaVenta.Location = New System.Drawing.Point(278, 33)
        Me.lblFechaVenta.Name = "lblFechaVenta"
        Me.lblFechaVenta.Size = New System.Drawing.Size(222, 20)
        Me.lblFechaVenta.TabIndex = 26
        Me.lblFechaVenta.Text = "Aquí aparecerá la fecha de venta"
        '
        'lblCantProdEmp
        '
        Me.lblCantProdEmp.AutoSize = True
        Me.lblCantProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantProdEmp.Location = New System.Drawing.Point(6, 33)
        Me.lblCantProdEmp.Name = "lblCantProdEmp"
        Me.lblCantProdEmp.Size = New System.Drawing.Size(66, 18)
        Me.lblCantProdEmp.TabIndex = 28
        Me.lblCantProdEmp.Text = "Cantidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 156)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(129, 18)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Detalle Descuento"
        '
        'txbDetDescVentasEmp
        '
        Me.txbDetDescVentasEmp.Location = New System.Drawing.Point(7, 177)
        Me.txbDetDescVentasEmp.Multiline = True
        Me.txbDetDescVentasEmp.Name = "txbDetDescVentasEmp"
        Me.txbDetDescVentasEmp.Size = New System.Drawing.Size(495, 152)
        Me.txbDetDescVentasEmp.TabIndex = 26
        '
        'cmbMotDescVentasEmp
        '
        Me.cmbMotDescVentasEmp.FormattingEnabled = True
        Me.cmbMotDescVentasEmp.Location = New System.Drawing.Point(234, 116)
        Me.cmbMotDescVentasEmp.Name = "cmbMotDescVentasEmp"
        Me.cmbMotDescVentasEmp.Size = New System.Drawing.Size(268, 26)
        Me.cmbMotDescVentasEmp.TabIndex = 25
        '
        'txbDescVentasEmp
        '
        Me.txbDescVentasEmp.Location = New System.Drawing.Point(234, 70)
        Me.txbDescVentasEmp.Name = "txbDescVentasEmp"
        Me.txbDescVentasEmp.Size = New System.Drawing.Size(268, 24)
        Me.txbDescVentasEmp.TabIndex = 24
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 119)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 18)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Motivo Descuento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 18)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Descuento"
        '
        'lblMontoFinalVenta
        '
        Me.lblMontoFinalVenta.AutoSize = True
        Me.lblMontoFinalVenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMontoFinalVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoFinalVenta.Location = New System.Drawing.Point(214, 332)
        Me.lblMontoFinalVenta.Name = "lblMontoFinalVenta"
        Me.lblMontoFinalVenta.Size = New System.Drawing.Size(288, 20)
        Me.lblMontoFinalVenta.TabIndex = 21
        Me.lblMontoFinalVenta.Text = "Resultado= (precio/u*cantidad)-descuento"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 332)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 18)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Monto Final"
        '
        'btnAgregarCarrito
        '
        Me.btnAgregarCarrito.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregarCarrito.Location = New System.Drawing.Point(195, 355)
        Me.btnAgregarCarrito.Name = "btnAgregarCarrito"
        Me.btnAgregarCarrito.Size = New System.Drawing.Size(153, 63)
        Me.btnAgregarCarrito.TabIndex = 19
        Me.btnAgregarCarrito.Text = "Agregar"
        Me.btnAgregarCarrito.UseVisualStyleBackColor = True
        '
        'grbDatosProductosVenta
        '
        Me.grbDatosProductosVenta.BackColor = System.Drawing.Color.CornflowerBlue
        Me.grbDatosProductosVenta.Controls.Add(Me.lblIdProductoVenta)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblPrecioUniVenta)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblClasificacionVenta)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblNomProdVentasEmp)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblPrecioProdEmp)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblClasificacionProdEmp)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblIdProductoEmp)
        Me.grbDatosProductosVenta.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.grbDatosProductosVenta.Location = New System.Drawing.Point(6, 23)
        Me.grbDatosProductosVenta.Name = "grbDatosProductosVenta"
        Me.grbDatosProductosVenta.Size = New System.Drawing.Size(508, 226)
        Me.grbDatosProductosVenta.TabIndex = 19
        Me.grbDatosProductosVenta.TabStop = False
        Me.grbDatosProductosVenta.Text = "Datos Producto"
        '
        'lblIdProductoVenta
        '
        Me.lblIdProductoVenta.AutoSize = True
        Me.lblIdProductoVenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblIdProductoVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdProductoVenta.Location = New System.Drawing.Point(278, 27)
        Me.lblIdProductoVenta.Name = "lblIdProductoVenta"
        Me.lblIdProductoVenta.Size = New System.Drawing.Size(224, 20)
        Me.lblIdProductoVenta.TabIndex = 27
        Me.lblIdProductoVenta.Text = "Aquí aparecerá el id del producto"
        '
        'lblPrecioUniVenta
        '
        Me.lblPrecioUniVenta.AutoSize = True
        Me.lblPrecioUniVenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPrecioUniVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioUniVenta.Location = New System.Drawing.Point(256, 176)
        Me.lblPrecioUniVenta.Name = "lblPrecioUniVenta"
        Me.lblPrecioUniVenta.Size = New System.Drawing.Size(246, 20)
        Me.lblPrecioUniVenta.TabIndex = 25
        Me.lblPrecioUniVenta.Text = "Aquí aparecerá el precio dl producto"
        '
        'lblClasificacionVenta
        '
        Me.lblClasificacionVenta.AutoSize = True
        Me.lblClasificacionVenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblClasificacionVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacionVenta.Location = New System.Drawing.Point(225, 124)
        Me.lblClasificacionVenta.Name = "lblClasificacionVenta"
        Me.lblClasificacionVenta.Size = New System.Drawing.Size(277, 20)
        Me.lblClasificacionVenta.TabIndex = 24
        Me.lblClasificacionVenta.Text = "Aquí aparecerá la categoría/subcategoría"
        '
        'lblNomProdVentasEmp
        '
        Me.lblNomProdVentasEmp.AutoSize = True
        Me.lblNomProdVentasEmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNomProdVentasEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNomProdVentasEmp.Location = New System.Drawing.Point(6, 66)
        Me.lblNomProdVentasEmp.Name = "lblNomProdVentasEmp"
        Me.lblNomProdVentasEmp.Size = New System.Drawing.Size(264, 20)
        Me.lblNomProdVentasEmp.TabIndex = 23
        Me.lblNomProdVentasEmp.Text = "Aquí aparecerá el nombre del producto"
        '
        'lblPrecioProdEmp
        '
        Me.lblPrecioProdEmp.AutoSize = True
        Me.lblPrecioProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioProdEmp.Location = New System.Drawing.Point(3, 176)
        Me.lblPrecioProdEmp.Name = "lblPrecioProdEmp"
        Me.lblPrecioProdEmp.Size = New System.Drawing.Size(106, 18)
        Me.lblPrecioProdEmp.TabIndex = 19
        Me.lblPrecioProdEmp.Text = "Precio Unitario"
        '
        'lblClasificacionProdEmp
        '
        Me.lblClasificacionProdEmp.AutoSize = True
        Me.lblClasificacionProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacionProdEmp.Location = New System.Drawing.Point(3, 124)
        Me.lblClasificacionProdEmp.Name = "lblClasificacionProdEmp"
        Me.lblClasificacionProdEmp.Size = New System.Drawing.Size(183, 18)
        Me.lblClasificacionProdEmp.TabIndex = 18
        Me.lblClasificacionProdEmp.Text = "Clasificacion Del Producto"
        '
        'lblIdProductoEmp
        '
        Me.lblIdProductoEmp.AutoSize = True
        Me.lblIdProductoEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdProductoEmp.Location = New System.Drawing.Point(6, 27)
        Me.lblIdProductoEmp.Name = "lblIdProductoEmp"
        Me.lblIdProductoEmp.Size = New System.Drawing.Size(127, 18)
        Me.lblIdProductoEmp.TabIndex = 17
        Me.lblIdProductoEmp.Text = "Número Producto"
        '
        'tpgEstadisticaVentaEmp
        '
        Me.tpgEstadisticaVentaEmp.BackColor = System.Drawing.Color.DarkGray
        Me.tpgEstadisticaVentaEmp.Controls.Add(Me.grbCriteriosEstadisticas)
        Me.tpgEstadisticaVentaEmp.Controls.Add(Me.Chart1)
        Me.tpgEstadisticaVentaEmp.Location = New System.Drawing.Point(4, 4)
        Me.tpgEstadisticaVentaEmp.Name = "tpgEstadisticaVentaEmp"
        Me.tpgEstadisticaVentaEmp.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgEstadisticaVentaEmp.Size = New System.Drawing.Size(1283, 695)
        Me.tpgEstadisticaVentaEmp.TabIndex = 1
        Me.tpgEstadisticaVentaEmp.Text = "Estadisticas"
        '
        'grbCriteriosEstadisticas
        '
        Me.grbCriteriosEstadisticas.BackColor = System.Drawing.Color.MediumAquamarine
        Me.grbCriteriosEstadisticas.Controls.Add(Me.Button4)
        Me.grbCriteriosEstadisticas.Controls.Add(Me.Button3)
        Me.grbCriteriosEstadisticas.Controls.Add(Me.Button2)
        Me.grbCriteriosEstadisticas.Controls.Add(Me.Button1)
        Me.grbCriteriosEstadisticas.Controls.Add(Me.btnVentasEmp)
        Me.grbCriteriosEstadisticas.Location = New System.Drawing.Point(6, 468)
        Me.grbCriteriosEstadisticas.Name = "grbCriteriosEstadisticas"
        Me.grbCriteriosEstadisticas.Size = New System.Drawing.Size(1271, 160)
        Me.grbCriteriosEstadisticas.TabIndex = 1
        Me.grbCriteriosEstadisticas.TabStop = False
        Me.grbCriteriosEstadisticas.Text = "Datos Estadísticos"
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(1022, 47)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(243, 68)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "10 Primeros"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(777, 47)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(243, 68)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "Ventas Por Categoria"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(259, 47)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(253, 68)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Ventas Por Mes"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(518, 47)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(253, 68)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Ventas Por Año"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnVentasEmp
        '
        Me.btnVentasEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVentasEmp.Location = New System.Drawing.Point(6, 47)
        Me.btnVentasEmp.Name = "btnVentasEmp"
        Me.btnVentasEmp.Size = New System.Drawing.Size(253, 68)
        Me.btnVentasEmp.TabIndex = 0
        Me.btnVentasEmp.Text = "Todas Las Ventas"
        Me.btnVentasEmp.UseVisualStyleBackColor = True
        '
        'Chart1
        '
        Me.Chart1.BackColor = System.Drawing.Color.Transparent
        ChartArea6.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea6)
        Legend6.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend6)
        Me.Chart1.Location = New System.Drawing.Point(6, 6)
        Me.Chart1.Name = "Chart1"
        Series6.ChartArea = "ChartArea1"
        Series6.Legend = "Legend1"
        Series6.Name = "Series1"
        Me.Chart1.Series.Add(Series6)
        Me.Chart1.Size = New System.Drawing.Size(1290, 456)
        Me.Chart1.TabIndex = 0
        Me.Chart1.Text = "Chart1"
        '
        'frmVentanaVentasEmp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1334, 749)
        Me.Controls.Add(Me.tbcVentasEmpleado)
        Me.Name = "frmVentanaVentasEmp"
        Me.Text = "Ventas"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcVentasEmpleado.ResumeLayout(False)
        Me.tpgVentas.ResumeLayout(False)
        Me.grbCarritoProdEmp.ResumeLayout(False)
        Me.grbCarritoProdEmp.PerformLayout()
        CType(Me.dgvCarritoProdEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbListaProdEmp.ResumeLayout(False)
        Me.grbFiltradoProdEmo.ResumeLayout(False)
        Me.grbFiltradoProdEmo.PerformLayout()
        CType(Me.dgvListaProdEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosProdEmp.ResumeLayout(False)
        Me.grbDatosVentas.ResumeLayout(False)
        Me.grbDatosVentas.PerformLayout()
        CType(Me.nudCantProdEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosProductosVenta.ResumeLayout(False)
        Me.grbDatosProductosVenta.PerformLayout()
        Me.tpgEstadisticaVentaEmp.ResumeLayout(False)
        Me.grbCriteriosEstadisticas.ResumeLayout(False)
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbcVentasEmpleado As System.Windows.Forms.TabControl
    Friend WithEvents tpgVentas As System.Windows.Forms.TabPage
    Friend WithEvents btnConfirmarVenta As System.Windows.Forms.Button
    Friend WithEvents grbCarritoProdEmp As System.Windows.Forms.GroupBox
    Friend WithEvents dgvCarritoProdEmp As System.Windows.Forms.DataGridView
    Friend WithEvents grbListaProdEmp As System.Windows.Forms.GroupBox
    Friend WithEvents grbFiltradoProdEmo As System.Windows.Forms.GroupBox
    Friend WithEvents txbIdProdEmp As System.Windows.Forms.TextBox
    Friend WithEvents txbNombreProdEmp As System.Windows.Forms.TextBox
    Friend WithEvents lblTituloNombreProdEmp As System.Windows.Forms.Label
    Friend WithEvents lblTituloIdProdEmp As System.Windows.Forms.Label
    Friend WithEvents dgvListaProdEmp As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grbDatosProdEmp As System.Windows.Forms.GroupBox
    Friend WithEvents tpgEstadisticaVentaEmp As System.Windows.Forms.TabPage
    Friend WithEvents grbCriteriosEstadisticas As System.Windows.Forms.GroupBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnVentasEmp As System.Windows.Forms.Button
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents lblTotalPago As System.Windows.Forms.Label
    Friend WithEvents lblTituloTotalPago As System.Windows.Forms.Label
    Friend WithEvents grbDatosVentas As System.Windows.Forms.GroupBox
    Friend WithEvents nudCantProdEmp As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblFechaVenta As System.Windows.Forms.Label
    Friend WithEvents lblCantProdEmp As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txbDetDescVentasEmp As System.Windows.Forms.TextBox
    Friend WithEvents cmbMotDescVentasEmp As System.Windows.Forms.ComboBox
    Friend WithEvents txbDescVentasEmp As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblMontoFinalVenta As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnAgregarCarrito As System.Windows.Forms.Button
    Friend WithEvents grbDatosProductosVenta As System.Windows.Forms.GroupBox
    Friend WithEvents lblIdProductoVenta As System.Windows.Forms.Label
    Friend WithEvents lblPrecioUniVenta As System.Windows.Forms.Label
    Friend WithEvents lblClasificacionVenta As System.Windows.Forms.Label
    Friend WithEvents lblNomProdVentasEmp As System.Windows.Forms.Label
    Friend WithEvents lblPrecioProdEmp As System.Windows.Forms.Label
    Friend WithEvents lblClasificacionProdEmp As System.Windows.Forms.Label
    Friend WithEvents lblIdProductoEmp As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnAntProdEmp As System.Windows.Forms.Button
    Friend WithEvents btnSigProdEmp As System.Windows.Forms.Button
End Class
