﻿Public Class frmVentanaVentasEmp
    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'control-Tipo: TabControl-Representa el tab control cuyas tab pages se quiere seleccionar
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Sub seleccionarTabControl(ByVal elemento As Byte)
        Try
            If elemento >= 1 And elemento <= Me.tbcVentasEmpleado.TabCount Then
                Me.tbcVentasEmpleado.SelectedIndex = elemento - 1
                Me.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox("Error al seleccionar una opción en las opciones para el empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Seleccionar Una Opción De Ventana")
        End Try
    End Sub

    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de ventas para el empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub


    Private Sub frmVentanaVentasEmp_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Me.cargarFondoPantalla("cobaltoplata.jpg")
            Me.btnAntProdEmp.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
        Catch ex As Exception
            MsgBox("Error al cargar la ventana del empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error De Carga De Ventana")
        End Try

    End Sub


    Private Sub btnAgregarCarrito_Click(sender As Object, e As EventArgs) Handles btnAgregarCarrito.Click
        Dim mensaje As String
        Try
            mensaje = ""
            If Me.nudCantProdEmp.Value <= 0 Then
                mensaje = mensaje + "Por favor, que la cantidad a vender sea mayor a cero" + vbNewLine
            End If

            If Me.cmbMotDescVentasEmp.Text <> "-" Or IsNothing(Me.cmbMotDescVentasEmp.Text) Then
                mensaje = mensaje + "Por favor, coloque un monto a descontar" + vbNewLine
            ElseIf Not IsNumeric(Me.txbDescVentasEmp.Text) Then
                mensaje = mensaje + "Por favor, coloque un valor numérico al descuento" + vbNewLine
            End If

            If Not IsNothing(Me.cmbMotDescVentasEmp.Text) And IsNumeric(Me.txbDescVentasEmp.Text) And Me.txbDetDescVentasEmp.Text = "" Then
                mensaje = mensaje + "Por favor, no deje el motivo del descuento en blanco" + vbNewLine
            End If

            If mensaje <> "" Then
                MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje)
            Else
                MsgBox("Datos Correctos")
            End If
        Catch ex As Exception
            MsgBox("Error al ingresar productos al carrito:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Seleccionar Una Opción De Ventana")
        End Try
    End Sub
End Class