﻿Imports Entities
Public Class DatLogueos

    Public Shared Function siguienteIdLogueo() As Integer
        Dim contexto As New CalabozoEntities
        Try
            Dim consulta = (From logueos In contexto.Logueos Select logueos).ToList().LastOrDefault().idLogueo
            Return consulta
        Catch ex As Exception
            MsgBox("Error al determinar la identificación del último logueo. Los errores presentados son: " + vbNewLine + ex.Message)
            Return 0
        End Try
    End Function

    Public Shared Function insertarNuevoLogueo(ByVal idUsuario As Integer) As Integer
        Dim contexto As New CalabozoEntities
        Dim nuevoLogueo As New Logueos
        Dim resultado As Integer
        Try
            nuevoLogueo.fechaLogueo = Now()
            nuevoLogueo.idUsuario = idUsuario
            contexto.Logueos.Add(nuevoLogueo)
            resultado = contexto.SaveChanges()
            Return resultado
        Catch ex As Exception
            MsgBox("Error al establecer los datos del logueo. Los errores presentados son: " + vbNewLine + ex.Message)
            Return 0
        End Try
    End Function

    Public Shared Sub establecerDeslogueo(ByVal idLogueo As Integer)
        Dim contexto As New CalabozoEntities
        Dim logueoUsuario As New Logueos
        Try
            Dim consulta = From log In contexto.Logueos Where log.idLogueo = idLogueo
            logueoUsuario = consulta.First()
            logueoUsuario.fechaDeslogueo = Date.UtcNow()
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al guardas datos de cierre de sesión. Los errores presentados son los siguientes: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error Cierre De Sesión")
        End Try
    End Sub
End Class
