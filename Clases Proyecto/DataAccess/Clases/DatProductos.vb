﻿Imports Entities


Public Class DatProductos
    Public Shared Function modificarPrecio(ByVal idProd As Integer, ByVal nuevoPrecio As Integer, ByVal motivo As Integer, ByVal descripcion As String, ByVal dni As Integer, ByVal empleado As String) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim modPrecio As New ModPreciosProductos
            Dim producto As New Productos
            Dim consulta = (From prod In contexto.Productos Where prod.idProducto = idProd Select prod)
            If consulta.Count() > 0 Then
                producto = consulta.Single
                modPrecio.valorAnterior = producto.precio
                modPrecio.valorActual = nuevoPrecio
                modPrecio.fechaModicacion = Date.Now()
                modPrecio.idProducto = idProd
                modPrecio.nombreEmpMod = empleado
                modPrecio.dniEmpMod = dni
                modPrecio.idMotivo = motivo
                producto.precio = nuevoPrecio
                contexto.ModPreciosProductos.Add(modPrecio)
                contexto.SaveChanges()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al modificar el precio. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de alta")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarStock(ByVal idProd As Integer, ByVal nuevoStock As Integer, ByVal motivo As Integer, ByVal descripcion As String, ByVal dni As Integer, ByVal empleado As String) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim motivoModStock As New ModStockProductos
            Dim producto As New Productos
            Dim consulta = (From prod In contexto.Productos Where prod.idProducto = idProd Select prod)
            If consulta.Count() > 0 Then
                producto = consulta.Single
                motivoModStock.valorAnterior = producto.stock
                motivoModStock.valorActual = nuevoStock
                motivoModStock.fechaModificacion = Date.Now()
                motivoModStock.idProducto = idProd
                motivoModStock.nombreEmpMod = empleado
                motivoModStock.dniEmpMod = dni
                motivoModStock.idMotivo = motivo
                producto.stock = nuevoStock
                contexto.ModStockProductos.Add(motivoModStock)
                contexto.SaveChanges()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al modificar el stock. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de alta")
            Return Nothing
        End Try
    End Function
    Public Shared Sub insertarProducto(ByVal nombre As String, ByVal idCat As Integer, ByVal idSub As Integer, ByVal precio As Double, ByVal stockMin As Integer, ByVal garantia As Integer, ByVal nombreFoto As String, ByVal descripcion As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim nuevoProd As New Productos

            nuevoProd.nombreProducto = nombre
            nuevoProd.idCategoria = idCat
            nuevoProd.idSubcategoria = idSub
            nuevoProd.precio = precio
            nuevoProd.stockMinimo = stockMin
            nuevoProd.stock = 0
            nuevoProd.garantia = garantia
            nuevoProd.fecha = Date.Now()
            nuevoProd.estado = True
            nuevoProd.nombreFoto = nombreFoto
            nuevoProd.descripcionProducto = descripcion
            contexto.Productos.Add(nuevoProd)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al almacenar el nuevo producto. Los errores producidos fueron: " + vbNewLine + ex.Message)
        End Try
    End Sub

    Public Shared Function getProductos() As List(Of Productos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From prod In contexto.Productos Select prod)
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function getHabilitados() As List(Of Productos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From prod In contexto.Productos Select prod Where prod.estado = True And prod.stock > 0)
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos habilitados. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarNombreRepetido(ByVal nombre As String) As Boolean
        Try

            Dim contexto As New CalabozoEntities
            Dim producto = (From prod In contexto.Productos Select prod Where prod.nombreProducto = nombre)
            If producto.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al conmprobar si el producto está repetido. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al buscar repeticiones")
            Return Nothing
        End Try
    End Function
    Public Shared Function getProductoPorId(ByVal idProducto) As Productos
        Try
            Dim prod As New Productos
            Dim contexto As New CalabozoEntities
            prod = (From producto In contexto.Productos Select producto Where producto.idProducto = idProducto).Single
            Return prod
        Catch ex As Exception
            MsgBox("Error al localizar el producto con el número especificado. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de búsqueda de datos")
            Return Nothing
        End Try
    End Function

    Public Shared Sub modificarEstadoProducto(ByVal idProducto As Integer, ByVal estado As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim prodMod As New Productos
            prodMod = (From prod In contexto.Productos Where prod.idProducto = idProducto Select prod)
            prodMod.estado = estado
            prodMod.fechaModProd = Date.UtcNow()
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al modificar el estado del producto. Los errores presentados fueron: " + vbNewLine + ex.Message, "Error de modificación de estado")
        End Try
    End Sub
    Public Shared Sub modificarProducto(ByVal idProd As Integer, ByVal nombre As String, ByVal idCat As Integer, ByVal idSub As Integer, ByVal stockMin As Integer, ByVal garantia As Integer, ByVal descripcion As String, ByVal nombreFoto As String, ByVal modNombre As Boolean, ByVal modCatSub As Boolean, ByVal habModCatSub As Boolean, ByVal cambiarFoto As Boolean)
        Try
            Dim prodMod As Productos
            Dim contexto As New CalabozoEntities
            prodMod = (From prod In contexto.Productos Where prod.idProducto = idProd Select prod).Single
            If Not IsNothing(prodMod) Then
                If modNombre Then
                    prodMod.nombreProducto = nombre
                End If
                If modCatSub And habModCatSub Then
                    prodMod.idCategoria = idCat
                    prodMod.idSubcategoria = idSub
                End If

                If cambiarFoto Then
                    prodMod.nombreFoto = nombreFoto
                End If

                prodMod.stockMinimo = stockMin
                prodMod.garantia = garantia
                prodMod.descripcionProducto = descripcion
                prodMod.fechaModProd = Date.UtcNow()
                contexto.SaveChanges()
            End If
        Catch ex As Exception
            MsgBox("Error al localizar el producto con el número especificado. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de moficicación de datos")
        End Try
    End Sub
End Class
