﻿Imports Entities
Public Class DatProductosProveedores
    Public Shared Function gerProdProv() As List(Of ProveedoresProductos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From prodProv In contexto.ProveedoresProductos Select prodProv
            Return consulta.ToList()
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las relacines productos/proveedores. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Sub modificarEstado(ByVal idProd As Integer, ByVal idProv As Integer, ByVal estado As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim buscado As New ProveedoresProductos
            buscado = (From elemento In contexto.ProveedoresProductos Where elemento.idProveedor = idProv And elemento.idProducto = idProd Select elemento).Single()
            buscado.estado = estado
            buscado.fechaModRelacion = Date.UtcNow()
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al modificar el estado de la relación producto/proveedor. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de control")
        End Try
    End Sub
    Public Shared Function comprobarRepetido(ByVal idProd As Integer, ByVal idProv As Integer)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From rep In contexto.ProveedoresProductos Where rep.idProducto = idProd And rep.idProveedor = idProv
            If consulta.Count() > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al comprobar si hay una relación producto/proveedor repetida. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de comprobación")
            Return Nothing
        End Try
    End Function
    Public Shared Sub modificarProductoProveedor(ByVal idProd As Integer, ByVal idProv As Integer, ByVal precioCompra As Double, ByVal modProdProv As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim prodProv As New ProveedoresProductos
            prodProv = (From buscado In contexto.ProveedoresProductos Where buscado.idProducto = idProd And buscado.idProveedor = idProv Select buscado).Single()
            If modProdProv Then
                prodProv.idProducto = idProd
                prodProv.idProveedor = idProv
            Else
                prodProv.precioCompra = precioCompra
            End If
            contexto.SaveChanges()
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Sub insertarProductoProveedor(ByVal idProd As Integer, ByVal idProv As Integer, ByVal precioCompra As Double)
        Try
            Dim contexto As New CalabozoEntities
            Dim prodProv As New ProveedoresProductos
            prodProv.idProducto = idProd
            prodProv.idProveedor = idProv
            prodProv.precioCompra = precioCompra
            prodProv.estado = True
            prodProv.fechaAlta = Date.Now()
            contexto.ProveedoresProductos.Add(prodProv)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al agregar una nueva relación producto/proveedor. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de alta")
        End Try
    End Sub
End Class
