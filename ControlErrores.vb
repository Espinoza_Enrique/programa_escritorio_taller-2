﻿Imports Microsoft.VisualBasic

Public Class ControlErrores
    Public Function controlEntero(ByVal valor As String) As Boolean
        Dim analizado As String = valor.trim()
        Dim resultado As Boolean = True


        Try
            resultado = isNumeric(CInt(analizado)) And analizado.contains(".") And analizado.contains(",")
        Catch ex As Exception
            resultado = False
        End Try
        Return resultado
    End Function

    Public Function controlReal(ByVal valor As String) As Boolean
        Dim analizado As String = valor.trim()
        Dim resultado As Boolean = True

        Try
            resultado = isNumeric(CDbl(analizado))
        Catch ex As Exception
            resultado = False
        End Try
        Return resultado

    End Function

    Public Function controlBlancos(ByVal valor As String) As Boolean
        Return String.IsNullOrWhiteSpace(valor)
    End Function
End Class
