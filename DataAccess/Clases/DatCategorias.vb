﻿Imports Entities
Public Class DatCategorias
    Public Shared Function filtrarCatPorId(ByVal idCat As Integer) As List(Of Categorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.Categorias Select cat Where cat.idCategoria = idCat And cat.estado = True
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las categorias activas registradas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function getCategoriasConSubcategorias() As List(Of Categorias)
        Try
            'Dim catSub As New List(Of CategoriasSubcategorias)
            Dim categorias As New List(Of Categorias)
            Dim contexto As New CalabozoEntities
            categorias = (From cat In contexto.Categorias Join catesub In contexto.CategoriasSubcategorias On cat.idCategoria Equals catesub.idCategoria Select cat Where cat.estado = True Order By cat.nombre).Distinct().ToList()
            'catSub = (From entidades In contexto.CategoriasSubcategorias Where entidades.Categorias.estado = True Select entidades Order By entidades.idCategoria).ToList()
            'For Each unidades In catSub
            'categorias.Add(unidades.Categorias)
            'Next
            Return categorias
        Catch ex As Exception
            MsgBox("Error al obtener las categorias con subcategorias. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
            Return Nothing
        End Try
    End Function
    Public Shared Sub modificarEstadoCategoria(ByVal idCat As Integer, ByVal estado As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim categoria As New Categorias
            Dim consulta = From cat In contexto.Categorias Select cat Where cat.idCategoria = idCat
            categoria = consulta.Single()
            categoria.estado = estado
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al ingresar al controlar si hay categorias repetidas. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
        End Try
    End Sub
    Public Shared Function controlarRepetidosCategoria(ByVal nombreCat As String) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.Categorias Select cat Where cat.nombre = nombreCat
            If consulta.Count() > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al ingresar al controlar si hay categorias repetidas. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
            Return False
        End Try
    End Function
    Public Shared Sub modificarSubcategoria(ByVal idSub As Integer, ByVal nombreSub As String, ByVal descripcion As String, ByVal soloDescripcion As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim subcategoriaNueva As New Subcategorias
            subcategoriaNueva = (From cat In contexto.Subcategorias Select cat Where cat.idSubcategoria = idSub).Single
            If soloDescripcion Then
                subcategoriaNueva.descripcionSub = descripcion
            Else
                subcategoriaNueva.descripcionSub = descripcion
                subcategoriaNueva.nombre = nombreSub
            End If
            contexto.SaveChanges()

        Catch ex As Exception
            MsgBox("Error al ingresar  al modificar los datos de la categoria. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
        End Try
    End Sub
    Public Shared Sub modificarCategoria(ByVal idCategoria As Integer, ByVal nombreCat As String, ByVal descripcion As String, ByVal soloDescripcion As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim categoriaNueva As New Categorias
            categoriaNueva = (From cat In contexto.Categorias Select cat Where cat.idCategoria = idCategoria).Single
            If soloDescripcion Then
                categoriaNueva.descripcionCat = descripcion
            Else
                categoriaNueva.descripcionCat = descripcion
                categoriaNueva.nombre = nombreCat
            End If
            contexto.SaveChanges()

        Catch ex As Exception
            MsgBox("Error al ingresar  al modificar los datos de la categoria. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
        End Try
    End Sub
    Public Shared Function controlarRepetidosSub(ByVal nombreSub As String) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.Subcategorias Select cat Where cat.nombre = nombreSub
            If consulta.Count() > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al  controlar si hay subcategorias repetidas. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
            Return False
        End Try

    End Function
    Public Shared Sub modificarEstadoCatSub(ByVal idCat As Integer, ByVal idSub As Integer, ByVal estado As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From catsub In contexto.CategoriasSubcategorias Select catsub Where catsub.idCategoria = idCat And catsub.idSubcategoria = idSub)
            If consulta.Count > 0 Then
                consulta.Single().estado = estado
                contexto.SaveChanges()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function getCategoriasActivas() As List(Of Categorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.Categorias Select cat Where cat.estado = True
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las categorias activas registradas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function

    Public Shared Function getSubActivas() As List(Of Subcategorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.Subcategorias Select cat Where cat.estado = True
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las subcategorias registradas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function
    
    Public Shared Sub modificarEstadoSub(ByVal idSub As Integer, ByVal estado As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim subcat As New Subcategorias
            Dim consulta = From cat In contexto.Subcategorias Select cat Where cat.idSubcategoria = idSub
            subcat = consulta.Single()
            subcat.estado = estado
            subcat.fechaModSub = Now()
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al ingresar al controlar si hay categorias repetidas. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
        End Try
    End Sub
    Public Shared Sub insertarSubcategoria(ByVal nombreSub As String, ByVal descripcion As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim categoriaNueva As New Subcategorias
            categoriaNueva.nombre = nombreSub
            categoriaNueva.descripcionSub = descripcion
            contexto.Subcategorias.Add(categoriaNueva)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al ingresar la nueva categoria. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
        End Try
    End Sub
    Public Shared Sub insertarCategoria(ByVal nombreCat As String, ByVal descripcion As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim categoriaNueva As New Categorias
            categoriaNueva.nombre = nombreCat
            categoriaNueva.descripcionCat = descripcion
            contexto.Categorias.Add(categoriaNueva)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al ingresar la nueva categoria. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al ingresar datos")
        End Try
    End Sub
    Public Shared Function getCategorias() As List(Of Categorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.Categorias Select cat
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las categorias registradas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function

    Public Shared Function getSubcategorias() As List(Of Subcategorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.Subcategorias Select cat
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las subcategorias registradas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function getCatSubActivas() As List(Of CategoriasSubcategorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.CategoriasSubcategorias Select cat Where cat.idCategoria = True And cat.idSubcategoria = True
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las relaciones categorías/subcategorías activas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function getCategoriasSubcategorias() As List(Of CategoriasSubcategorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.CategoriasSubcategorias Select cat
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las subcategorias registradas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function

    Public Shared Function getSubcatPorCategorias(ByVal idCategoria As Integer) As List(Of CategoriasSubcategorias)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cat In contexto.CategoriasSubcategorias Where cat.idCategoria = idCategoria And cat.Subcategorias.estado = True And cat.estado = True Select cat Order By cat.Subcategorias.nombre
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de las subcategorias registradas. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al obtener datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarCatSubRepetidos(ByVal idCat As Integer, ByVal idSub As Integer) As CategoriasSubcategorias
        Try
            Dim mensaje As String
            Dim contexto As New CalabozoEntities
            mensaje = ""
            Dim consulta = From catsub In contexto.CategoriasSubcategorias Select catsub Where catsub.idCategoria = idCat And catsub.idSubcategoria = idSub
            If consulta.Count() > 0 Then
                Return consulta.Single()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener la relación categoría/subcategoria para comparar repetidos. Los errores presentados son: " + vbNewLine + ex.Message, "Error de control")
            Return Nothing
        End Try
    End Function
    Public Shared Sub insertarNuevaCatSub(ByVal idCat As Integer, ByVal idSub As Integer, ByVal descripcion As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim nuevaCatSub As New CategoriasSubcategorias
            nuevaCatSub.idCategoria = idCat
            nuevaCatSub.idSubcategoria = idSub
            nuevaCatSub.descripcionCatSub = descripcion
            contexto.CategoriasSubcategorias.Add(nuevaCatSub)
            contexto.SaveChanges()
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub modificarCatSub(ByVal idCat As Integer, ByVal idSub As Integer, ByVal descripcion As String, ByVal modificacion As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim entidad As New CategoriasSubcategorias
            entidad = (From catesub In contexto.CategoriasSubcategorias Where catesub.idCategoria = idCat And catesub.idSubcategoria = idSub).Single
            If modificacion Then
                entidad.idCategoria = idCat
                entidad.idSubcategoria = idSub
                entidad.descripcionCatSub = descripcion
            Else
                entidad.descripcionCatSub = descripcion
            End If
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Se han producido errores al querer modificar los datos de la relación. Los errores producidos fueron: " + ex.Message)
        End Try
    End Sub
End Class
