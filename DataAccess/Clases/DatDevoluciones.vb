﻿Imports Entities
Public Class DatDevoluciones

    Public Shared Function getDevolucionesProductos() As List(Of AuxDevolucionesProd)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From mot In contexto.DevolucionesDetalles Select New AuxDevolucionesProd With {.nombreProducto = mot.DetallesVentas.Productos.nombreProducto,
                                                                                                          .unidadesDevueltas = mot.unidadesDev,
                                                                                                          .montoDevuelto = mot.montoDetalleDev,
                                                                                                          .fechaDevolucion = mot.Devoluciones.fecha,
                                                                                                          .nombreMotivoDevolucion = mot.MotivosDevoluciones.nombreMotivo,
                                                                                                          .descripcion = mot.descripcion}
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de las devoluciones. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getMotivosDev() As List(Of MotivosDevoluciones)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From mot In contexto.MotivosDevoluciones Select mot
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los motivos de devolución para los productos. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Sub insertarCabeceraDev(ByVal montoTotal As Double)
        Try
            Dim contexto As New CalabozoEntities
            Dim nuevaCab As New Devoluciones
            nuevaCab.montoTotal = montoTotal
            nuevaCab.estado = True
            nuevaCab.fecha = DateTime.Now()
            contexto.Devoluciones.Add(nuevaCab)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al ingresar los datos para la nueva devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Public Shared Function getUltimoIdCabecera() As Integer
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cab In contexto.Devoluciones Select cab
            If consulta.Count > 0 Then
                Return consulta.ToList().LastOrDefault().idDevolucion
            Else
                Return 0
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos necesarios para asentar la devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return 0
        End Try
    End Function

    Public Shared Sub insertarDetallesDev(ByRef detalles As List(Of DevolucionesDetalles))
        Try
            Dim contexto As New CalabozoEntities
            contexto.DevolucionesDetalles.AddRange(detalles)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al guardar los detalles de las devoluciones. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Public Shared Function getDevPorIdVenta(ByVal idVenta As Integer, ByVal idDetalle As Integer) As List(Of DevolucionesDetalles)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From det In contexto.DevolucionesDetalles Where det.idVenta = idVenta And det.idDetalleDev = idDetalle Select det
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los detalles de devolución para la venta especificada. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
End Class
