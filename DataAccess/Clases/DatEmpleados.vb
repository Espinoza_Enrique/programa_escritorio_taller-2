﻿Imports Entities
Public Class DatEmpleados

    Public Shared Function filtrarPorDni(ByVal dni As String) As List(Of Empleados)
        Try
            Dim contexto As New CalabozoEntities
         
            Dim consulta = From emp In contexto.Empleados Where emp.dni = dni Select emp
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los empleados. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function filtrarPorApellido(ByVal ape As String) As List(Of Empleados)
        Try
            Dim contexto As New CalabozoEntities
            Dim busqueda As String
            busqueda = ape
            Dim consulta = From emp In contexto.Empleados Where emp.apellidoEmpleado.ToLower().Contains(busqueda.ToLower().Trim()) Select emp
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los empleados. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarDni(ByVal dni As Integer) As String
        Dim mensaje As String
        Try
            Dim contexto As New CalabozoEntities
            mensaje = ""
            Dim consulta = From emp In contexto.Empleados Select emp Where emp.dni = dni
            If consulta.Count > 0 Then
                mensaje = mensaje + vbNewLine + "Ya existo un empleado con el dni introducido. Por favor, controle el dni."
            End If
            Return mensaje
        Catch ex As Exception
            mensaje = ""
            mensaje = mensaje + vbNewLine + "Error al controlar si el dni es repetido. Los errores producidos fueron: " + vbNewLine + ex.Message
            Return mensaje
        End Try
    End Function
    Public Shared Sub modificarDatosEmpleado(ByVal idEmpleado As Integer, ByVal apellido As String, ByVal nombre As String, ByVal dni As Integer, ByVal telefono As String, ByVal mail As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim empleado As New Empleados
            Dim consulta = From emp In contexto.Empleados Select emp Where emp.legajo = idEmpleado
            empleado = consulta.First()
            empleado.apellidoEmpleado = apellido
            empleado.nombreEmpleado = nombre
            empleado.mail = mail
            empleado.telefono = telefono
            empleado.dni = dni
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al modificar los datos del empleado. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error modificar empleado")
        End Try
    End Sub

    Public Shared Sub modificarDatosEmpleadoSinDni(ByVal idEmpleado As Integer, ByVal apellido As String, ByVal nombre As String, ByVal telefono As String, ByVal mail As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim empleado As New Empleados
            Dim consulta = From emp In contexto.Empleados Select emp Where emp.legajo = idEmpleado
            empleado = consulta.First()
            empleado.apellidoEmpleado = apellido
            empleado.nombreEmpleado = nombre
            empleado.mail = mail
            empleado.telefono = telefono
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al modificar los datos del empleado. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error modificar empleado")
        End Try
    End Sub
    Public Shared Sub agregarNuevoEmpleado(ByVal apellido As String, ByVal nombre As String, ByVal dni As Integer, ByVal telefono As String, ByVal mail As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim empleado As New Empleados
            empleado.apellidoEmpleado = apellido
            empleado.nombreEmpleado = nombre
            empleado.dni = dni
            empleado.telefono = telefono
            empleado.mail = mail
            empleado.estado = True
            contexto.Empleados.Add(empleado)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al intentar ingresar el nuevo empleado. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al crear nuevo empleado")
        End Try

    End Sub
    Public Shared Sub altaEmpleado(ByVal idEmpleado As Integer)
        Try
            Dim contexto As New CalabozoEntities
            Dim empleado As New Empleados
            Dim consulta = From emp In contexto.Empleados Select emp Where emp.legajo = idEmpleado
            empleado = consulta.First()
            empleado.estado = True
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al intentar modificar el estado de usuarios ligados al empleado. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al modificar estado")
        End Try
    End Sub
    Public Shared Sub bajaEmpleado(ByVal idEmpleado As Integer)
        Try
            Dim contexto As New CalabozoEntities
            Dim empleado As New Empleados
            Dim consulta = From emp In contexto.Empleados Select emp Where emp.legajo = idEmpleado
            empleado = consulta.First()
            empleado.estado = False
            contexto.SaveChanges()
            DatUsuarios.cambiarEstadoEmpleado(idEmpleado, False)
        Catch ex As Exception
            MsgBox("Error al intentar modificar el estado de usuarios ligados al empleado. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al modificar estado")
        End Try

    End Sub
    Public Shared Function getEmpleados() As List(Of Empleados)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From emp In contexto.Empleados Select emp
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar datos de los empleados registrados. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al recuperar datos de DB")
            Return Nothing
        End Try
    End Function

    Public Shared Function getEmpleadosActivos() As List(Of Empleados)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From emp In contexto.Empleados Select emp Where emp.estado = True
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar datos de los empleados registrados. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al recuperar datos de DB")
            Return Nothing
        End Try
    End Function

    Public Shared Function getEmpleadoPorId(ByVal idEmpleado As Integer) As Empleados
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From emp In contexto.Empleados Select emp Where emp.legajo = idEmpleado
            Return consulta.Single()
        Catch ex As Exception
            MsgBox("Error al obtener el empleado con id: " + CStr(idEmpleado) + ".Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function
End Class
