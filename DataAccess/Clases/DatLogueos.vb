﻿Imports Entities
Public Class DatLogueos
    Public Shared Function getInformeLogueos() As List(Of AuxLogueos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From log In contexto.Logueos Select New AuxLogueos With {.nombreEmpleado = log.Usuarios.Empleados.nombreEmpleado, .dniEmpleado = log.Usuarios.Empleados.dni,
                                                                                 .fechaLogueo = log.fechaLogueo, .fechaDeslogueo = log.fechaDeslogueo}
            Return consulta.ToList()
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function getLogueosPorDniYFecha(ByVal dni As Integer, ByVal fecha As DateTime)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From log In contexto.Logueos Where System.Data.Entity.DbFunctions.TruncateTime(log.fechaLogueo) = fecha.Date And log.Usuarios.Empleados.dni = dni And log.fechaDeslogueo IsNot Nothing Select New AuxLogueos With {.nombreEmpleado = log.Usuarios.Empleados.nombreEmpleado,
                                                                                                                                                                                                                                            .dniEmpleado = log.Usuarios.Empleados.dni,
                                                                                                                                                                                                                                            .fechaLogueo = log.fechaLogueo,
                                                                                                                                                                                                                                            .fechaDeslogueo = log.fechaDeslogueo}
            If consulta.Count >= 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getInformeLogueosPorFecha(ByVal fecha As DateTime) As List(Of AuxLogueos)
        Try
            Dim contexto As New CalabozoEntities

            Dim consulta = From log In contexto.Logueos Where System.Data.Entity.DbFunctions.TruncateTime(log.fechaLogueo) = fecha.Date Select New AuxLogueos With {.nombreEmpleado = log.Usuarios.Empleados.nombreEmpleado,
                                                                                                                                                                                               .dniEmpleado = log.Usuarios.Empleados.dni,
                                                                                                                                                                                               .fechaLogueo = log.fechaLogueo,
                                                                                                                                                                                               .fechaDeslogueo = log.fechaDeslogueo}
            If consulta.Count >= 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try

    End Function
    Public Shared Function getInformeLogueosPorDni(ByVal dni As Integer) As List(Of AuxLogueos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From log In contexto.Logueos Where log.Usuarios.Empleados.dni = dni Select New AuxLogueos With {.nombreEmpleado = log.Usuarios.Empleados.nombreEmpleado,
                                                                                                                           .dniEmpleado = log.Usuarios.Empleados.dni,
                                                                                                                           .fechaLogueo = log.fechaLogueo,
                                                                                                                       .fechaDeslogueo = log.fechaDeslogueo}
            If consulta.Count >= 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los logueos y deslogueos de usuarios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function siguienteIdLogueo() As Integer
        Dim contexto As New CalabozoEntities
        Dim consulta As Integer
        Try
            consulta = (From logueos In contexto.Logueos Select logueos).ToList().LastOrDefault().idLogueo
            Return consulta
        Catch ex As Exception
            MsgBox("Error al determinar la identificación del último logueo. Los errores presentados son: " + vbNewLine + ex.Message)
            Return 0
        End Try
    End Function

    Public Shared Function insertarNuevoLogueo(ByVal idUsuario As Integer) As Integer
        Dim contexto As New CalabozoEntities
        Dim nuevoLogueo As New Logueos
        Dim resultado As Integer
        Try
            nuevoLogueo.fechaLogueo = DateTime.Now()
            nuevoLogueo.idUsuario = idUsuario
            contexto.Logueos.Add(nuevoLogueo)
            resultado = contexto.SaveChanges()
            Return resultado
        Catch ex As Exception
            MsgBox("Error al establecer los datos del logueo. Los errores presentados son: " + vbNewLine + ex.Message)
            Return 0
        End Try
    End Function

    Public Shared Sub establecerDeslogueo(ByVal idLogueo As Integer)
        Dim contexto As New CalabozoEntities
        Dim logueoUsuario As New Logueos
        Try
            Dim consulta = From log In contexto.Logueos Where log.idLogueo = idLogueo
            logueoUsuario = consulta.First()
            logueoUsuario.fechaDeslogueo = DateTime.Now()
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al guardas datos de cierre de sesión. Los errores presentados son los siguientes: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error Cierre De Sesión")
        End Try
    End Sub
End Class
