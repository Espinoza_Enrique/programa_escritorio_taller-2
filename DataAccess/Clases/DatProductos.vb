﻿Imports Entities


Public Class DatProductos

    Public Shared Function filtrarCatSub(ByVal idCat As Integer, ByVal idSub As Integer) As List(Of Productos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From prod In contexto.Productos Where prod.idCategoria = idCat And prod.idSubcategoria = idSub Select prod
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los productos con bajo stock. Los errores cometidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function filtrarPorNombre(ByVal nombre As String) As List(Of Productos)
        Try
            Dim contexto As New CalabozoEntities
            Dim busqueda As String
            busqueda = nombre
            Dim consulta = From prod In contexto.Productos Where prod.nombreProducto.ToLower().Contains(busqueda.ToLower().Trim()) Select prod
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los productos con bajo stock. Los errores cometidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function filtrarPorCategoria(ByVal idCat As Integer) As List(Of Productos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From prod In contexto.Productos Where prod.idCategoria = idCat Select prod
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los productos con bajo stock. Los errores cometidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function

    Private Shared Function filtrarCatNom(ByVal idCat As Integer, ByVal nombre As String) As List(Of Productos)
        Try
            Dim contexto As New CalabozoEntities
            Dim busqueda As String

            busqueda = nombre
            Dim consulta = From prod In contexto.Productos Where prod.nombreProducto.ToLower().Contains(busqueda.ToLower().Trim()) And prod.idCategoria = idCat Select prod
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los productos con bajo stock. Los errores cometidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getProdStockMin() As List(Of AuxStockMin)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From prod In contexto.Productos Where prod.stock <= prod.stockMinimo Select New AuxStockMin With {.nombreProducto = prod.nombreProducto, .stockActual = prod.stock,
                                                                                                                       .stockMinimo = prod.stockMinimo, .categoria = prod.CategoriasSubcategorias.Categorias.nombre,
                                                                                                                        .subcategoria = prod.CategoriasSubcategorias.Subcategorias.nombre}
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los productos con bajo stock. Los errores cometidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getMotivosModPrecio() As List(Of MotivosModPrecio)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From mot In contexto.MotivosModPrecio Where mot.estado = True Select mot)
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los motivos de modificación de precios. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function

    Public Shared Function getMotivosModStock() As List(Of MotivosModStock)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From mot In contexto.MotivosModStock Where mot.estado = True Select mot)
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los motivos de modificación de stock. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function

    Public Shared Function modificarPrecio(ByVal idProd As Integer, ByVal nuevoPrecio As Double, ByVal motivo As Integer, ByVal descripcion As String, ByVal dni As Integer, ByVal empleado As String) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim modPrecio As New ModPreciosProductos
            Dim producto As New Productos
            Dim consulta = (From prod In contexto.Productos Where prod.idProducto = idProd Select prod)
            If consulta.Count() > 0 Then
                producto = consulta.Single
                modPrecio.valorAnterior = producto.precio
                modPrecio.valorActual = nuevoPrecio
                modPrecio.fechaModicacion = Date.Now()
                modPrecio.idProducto = idProd
                modPrecio.nombreEmpMod = empleado
                modPrecio.dniEmpMod = dni
                modPrecio.idMotivo = motivo
                modPrecio.descModPrecio = descripcion
                producto.precio = nuevoPrecio
                contexto.ModPreciosProductos.Add(modPrecio)
                contexto.SaveChanges()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al modificar el precio. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de alta")
            Return Nothing
        End Try
    End Function
    Public Shared Function modificarStock(ByVal idProd As Integer, ByVal nuevoStock As Integer, ByVal motivo As Integer, ByVal descripcion As String, ByVal dni As Integer, ByVal empleado As String) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim motivoModStock As New ModStockProductos
            Dim producto As New Productos
            Dim consulta = (From prod In contexto.Productos Where prod.idProducto = idProd Select prod)
            If consulta.Count() > 0 Then
                producto = consulta.Single()
                motivoModStock.valorAnterior = producto.stock
                motivoModStock.valorActual = nuevoStock
                motivoModStock.fechaModificacion = Date.Now()
                motivoModStock.idProducto = idProd
                motivoModStock.nombreEmpMod = empleado
                motivoModStock.dniEmpMod = dni
                motivoModStock.idMotivo = motivo
                motivoModStock.descModStock = descripcion
                producto.stock = nuevoStock
                contexto.ModStockProductos.Add(motivoModStock)
                contexto.SaveChanges()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al modificar el stock. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de alta")
            Return Nothing
        End Try
    End Function
    Public Shared Sub insertarProducto(ByVal nombre As String, ByVal idCat As Integer, ByVal idSub As Integer, ByVal precio As Double, ByVal stockMin As Integer, ByVal garantia As Integer, ByVal nombreFoto As String, ByVal descripcion As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim nuevoProd As New Productos

            nuevoProd.nombreProducto = nombre
            nuevoProd.idCategoria = idCat
            nuevoProd.idSubcategoria = idSub
            nuevoProd.precio = precio
            nuevoProd.stockMinimo = stockMin
            nuevoProd.stock = 0
            nuevoProd.garantia = garantia
            nuevoProd.fecha = DateTime.Now()
            nuevoProd.estado = True
            nuevoProd.nombreFoto = nombreFoto
            nuevoProd.descripcionProducto = descripcion
            contexto.Productos.Add(nuevoProd)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al almacenar el nuevo producto. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventan Error")
        End Try
    End Sub

    Public Shared Function getProductos() As List(Of Productos)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From prod In contexto.Productos Select prod)
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function
    Public Shared Function filtrarProdCatVentas(ByVal idCat As String) As List(Of ProductosHabilitados)
        Try
            Dim contexto As New CalabozoEntities


            Dim consulta = (From prod In contexto.Productos Where prod.estado = True And prod.stock > 0 And prod.idCategoria = idCat Select New ProductosHabilitados With {.nroProducto = prod.idProducto, .nombre = prod.nombreProducto, .precio = prod.precio,
                                                                                                                                             .categoria = prod.CategoriasSubcategorias.Categorias.nombre,
                                                                                                                                             .subcategoria = prod.CategoriasSubcategorias.Subcategorias.nombre})
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos habilitados. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function filtrarProdSubCatVentas(ByVal idSub As Integer) As List(Of ProductosHabilitados)
        Try
            Dim contexto As New CalabozoEntities


            Dim consulta = (From prod In contexto.Productos Where prod.estado = True And prod.stock > 0 And prod.idSubcategoria = idSub Select New ProductosHabilitados With {.nroProducto = prod.idProducto, .nombre = prod.nombreProducto, .precio = prod.precio,
                                                                                                                                             .categoria = prod.CategoriasSubcategorias.Categorias.nombre,
                                                                                                                                             .subcategoria = prod.CategoriasSubcategorias.Subcategorias.nombre})
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos habilitados. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function filtrarProdCat_SubcatVentas(ByVal idCat As Integer, ByVal idSub As Integer) As List(Of ProductosHabilitados)
        Try
            Dim contexto As New CalabozoEntities


            Dim consulta = (From prod In contexto.Productos Where prod.estado = True And prod.stock > 0 And prod.idCategoria = idCat And prod.idSubcategoria = idSub Select New ProductosHabilitados With {.nroProducto = prod.idProducto, .nombre = prod.nombreProducto, .precio = prod.precio,
                                                                                                                                             .categoria = prod.CategoriasSubcategorias.Categorias.nombre,
                                                                                                                                             .subcategoria = prod.CategoriasSubcategorias.Subcategorias.nombre})
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos habilitados. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function
    Public Shared Function filtrarProdNombreVentas(ByVal nombre As String) As List(Of ProductosHabilitados)
        Try
            Dim contexto As New CalabozoEntities
            Dim busqueda As String

            busqueda = nombre
            Dim consulta = (From prod In contexto.Productos Where prod.estado = True And prod.stock > 0 And prod.nombreProducto.ToLower().Contains(busqueda.ToLower().Trim()) Select New ProductosHabilitados With {.nroProducto = prod.idProducto, .nombre = prod.nombreProducto, .precio = prod.precio,
                                                                                                                                             .categoria = prod.CategoriasSubcategorias.Categorias.nombre,
                                                                                                                                             .subcategoria = prod.CategoriasSubcategorias.Subcategorias.nombre})
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos habilitados. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function getProdHabilitados() As List(Of ProductosHabilitados)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From prod In contexto.Productos Where prod.estado = True And prod.stock > 0 Select New ProductosHabilitados With {.nroProducto = prod.idProducto, .nombre = prod.nombreProducto, .precio = prod.precio,
                                                                                                                                             .categoria = prod.CategoriasSubcategorias.Categorias.nombre,
                                                                                                                                             .subcategoria = prod.CategoriasSubcategorias.Subcategorias.nombre})
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de los productos habilitados. Los errores producidos fueron: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarNombreRepetido(ByVal nombre As String) As Boolean
        Try

            Dim contexto As New CalabozoEntities
            Dim producto = (From prod In contexto.Productos Select prod Where prod.nombreProducto = nombre)
            If producto.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al conmprobar si el producto está repetido. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al buscar repeticiones")
            Return Nothing
        End Try
    End Function
    Public Shared Function getProductoPorId(ByVal idProducto) As Productos
        Try
            Dim prod As New Productos
            Dim contexto As New CalabozoEntities
            prod = (From producto In contexto.Productos Select producto Where producto.idProducto = idProducto).Single
            Return prod
        Catch ex As Exception
            MsgBox("Error al localizar el producto con el número especificado. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de búsqueda de datos")
            Return Nothing
        End Try
    End Function

    Public Shared Sub modificarEstadoProducto(ByVal idProducto As Integer, ByVal estado As Boolean)
        Try
            Dim contexto As New CalabozoEntities
            Dim prodMod As New Productos
            prodMod = (From prod In contexto.Productos Where prod.idProducto = idProducto Select prod).Single()
            prodMod.estado = estado
            prodMod.fechaModProd = Date.UtcNow()
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al modificar el estado del producto. Los errores presentados fueron: " + vbNewLine + ex.Message, "Error de modificación de estado")
        End Try
    End Sub
    Public Shared Sub modificarProducto(ByVal idProd As Integer, ByVal nombre As String, ByVal idCat As Integer, ByVal idSub As Integer, ByVal stockMin As Integer, ByVal garantia As Integer, ByVal descripcion As String, ByVal nombreFoto As String, ByVal modNombre As Boolean, ByVal modCatSub As Boolean, ByVal habModCatSub As Boolean, ByVal cambiarFoto As Boolean)
        Try
            Dim prodMod As Productos
            Dim contexto As New CalabozoEntities
            prodMod = (From prod In contexto.Productos Where prod.idProducto = idProd Select prod).Single
            If Not IsNothing(prodMod) Then
                If modNombre Then
                    prodMod.nombreProducto = nombre
                End If
                If modCatSub And habModCatSub Then
                    prodMod.idCategoria = idCat
                    prodMod.idSubcategoria = idSub
                End If

                If cambiarFoto Then
                    prodMod.nombreFoto = nombreFoto
                End If

                prodMod.stockMinimo = stockMin
                prodMod.garantia = garantia
                prodMod.descripcionProducto = descripcion
                prodMod.fechaModProd = Date.UtcNow()
                contexto.SaveChanges()
            End If
        Catch ex As Exception
            MsgBox("Error al localizar el producto con el número especificado. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de moficicación de datos")
        End Try
    End Sub

    Public Shared Function controlarStock(ByVal idProducto As Integer, ByVal stockComparado As Integer) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim resultado As Boolean

            resultado = True
            Dim consulta = From prod In contexto.Productos Where prod.idProducto = idProducto Select prod
            If consulta.Count() > 0 Then
                If consulta.Single().stock < stockComparado Then
                    resultado = False
                End If
                Return resultado
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al controlar si el stock disponible es mayor o igual a las unidades compradas. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
End Class
