﻿Imports Entities
Public Class DatUsuarios

    Public Shared Function filtrarPorApellido(ByVal ape As String) As List(Of Usuarios)
        Try
            Dim contexto As New CalabozoEntities
            Dim busqueda As String
            busqueda = ape
            Dim consulta = From emp In contexto.Usuarios Where emp.Empleados.apellidoEmpleado.ToLower().Contains(busqueda.ToLower().Trim()) Select emp
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los empleados. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function filtrarPorDniEmp(ByVal dni As Integer) As List(Of Usuarios)
        Try
            Dim contexto As New CalabozoEntities

            Dim consulta = From emp In contexto.Usuarios Where emp.Empleados.dni = dni Select emp
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los datos de los empleados. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarUsuarioRepetido(ByVal nombreUsuario As String, ByVal idEmpleado As Integer, ByVal idPerfil As Integer) As String
        Try
            Dim mensajeError As String
            mensajeError = ""
            Dim contexto As New CalabozoEntities
            Dim consulta = From usu In contexto.Usuarios Select usu Where usu.nombreUsuario = nombreUsuario Or (usu.idEmpleado = idEmpleado And usu.idPerfil = idPerfil)
            If consulta.Count() > 0 Then
                mensajeError = "Ya existe un usuario con el nombre: " + nombreUsuario + "o ya existe un usuario del perfil indicado para el empleado elegido." + vbNewLine + "Por favor, elija otro nombre de usuario y verifique que el empleado seleccionado tenga un usuario con el perfil indicado."
            End If
            Return mensajeError
        Catch ex As Exception
            MsgBox("Error al controlar si el nombre de usuario es válido. Los errores presentados son los siguientes: " + vbNewLine + ex.Message, vbOKOnly + vbExclamation, "Error al controlar datos")
            Return Nothing
        End Try
    End Function
    Public Shared Sub nuevoUsuario(ByVal nombreUsu As String, ByVal password As String, ByVal idEmpleado As Integer, ByVal idPerfil As Integer)
        Try
            Dim contexto As New CalabozoEntities
            Dim usuario As New Usuarios
            usuario.idPerfil = idPerfil
            usuario.idEmpleado = idEmpleado
            usuario.nombreUsuario = nombreUsu
            usuario.contrasenia = password
            usuario.estado = True
            contexto.Usuarios.Add(usuario)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al agregar nuevo usuario. Los errores son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de ingreso de datos")
        End Try
    End Sub
    Public Shared Sub modificarContrasenia(ByVal idUsuario As Integer, ByVal password As String)
        Try
            Dim contexto As New CalabozoEntities
            Dim usuario As New Usuarios
            Dim consulta = From usu In contexto.Usuarios Select usu Where usu.idUsuario = idUsuario
            usuario = consulta.First()
            usuario.contrasenia = password
            usuario.fechaModUsu = Now()
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al modificar la contraseña. La errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error modificación de datos")
        End Try
    End Sub
    Public Shared Sub cambiarEstadoEmpleado(ByVal idEmpleado As Integer, ByVal estado As Byte)
        Try
            Dim contexto As New CalabozoEntities
            Dim usuarios As New Usuarios
            Dim consulta = From usu In contexto.Usuarios Select usu Where usu.idEmpleado = idEmpleado
            For Each usuMod In consulta.ToList()
                usuMod.estado = estado
            Next
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al intentar modificar el estado de usuarios ligados al empleado. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al modificar estado")
        End Try

    End Sub

    Public Shared Function controlarAccesso(ByVal nombreUsuario As String, ByVal contra As String) As Usuarios
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From usu In contexto.Usuarios Select usu Where usu.nombreUsuario = nombreUsuario And usu.contrasenia = contra And usu.estado = True
            If consulta.Count > 0 Then
                Return consulta.Single()
            Else
                Return Nothing
            End If

        Catch ex As Exception
            MsgBox("Error al buscar usuario válido. Se ha presentado el siguient error: " + vbNewLine + ex.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function getUsuarios() As List(Of Usuarios)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From usu In contexto.Usuarios Select usu Where usu.Empleados.estado = True
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar datos de los usuarios registrados. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al recuperar datos")
            Return Nothing
        End Try
    End Function

    Public Shared Sub modificarEstadoUsuario(ByVal idUsuario As Integer, ByVal valorEstado As Byte)
        Try
            Dim contexto As New CalabozoEntities
            Dim usuario As New Usuarios
            Dim consulta = From usu In contexto.Usuarios Select usu Where usu.idUsuario = idUsuario
            usuario = consulta.First()
            usuario.estado = valorEstado
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al modificar el estado del usuario seleccionado. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso a datos")
        End Try
    End Sub
End Class
