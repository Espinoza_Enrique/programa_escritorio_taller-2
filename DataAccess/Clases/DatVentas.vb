﻿Imports Entities
Public Class DatVentas

    Public Shared Function getUnidadesVendidaDetalle(ByVal idVenta As Integer, ByVal idDetalle As Integer) As Integer
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From det In contexto.DetallesVentas Where det.idVenta = idVenta And det.idDetalle = idDetalle Select det
            If consulta.Count > 0 Then
                Return consulta.Single().cantidad
            Else
                Return -1
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar los datos de las unidades vendidas para el producto en cuestión. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return -1
        End Try
    End Function
    Public Shared Function getDetallesParaDevPorId(ByVal idCabecera As Integer) As List(Of DetallesVentasDevoluciones)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From det In contexto.DetallesVentas Where det.idVenta = idCabecera Select New DetallesVentasDevoluciones With {
                .codVenta = det.idVenta, .codDetalle = det.idDetalle, .nombreProducto = det.Productos.nombreProducto,
                .unidadesVendidas = det.cantidad, .precioVenta = det.precioDetalle,
                .categoria = det.Productos.CategoriasSubcategorias.Categorias.nombre,
                .subcategoria = det.Productos.CategoriasSubcategorias.Subcategorias.nombre}
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Erorr al recuperar los datos de las cabeceras de venta para las devoluciones. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function getCabecerasParaFiltro(ByVal fechaInicio As DateTime, ByVal fechafin As DateTime) As List(Of VentasDevoluciones)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta As New List(Of VentasDevoluciones)
            If fechaInicio.Date = fechafin.Date Then
                consulta = (From cab In contexto.OrdenesVentas Where System.Data.Entity.DbFunctions.TruncateTime(cab.fechaVenta) = fechaInicio.Date Select New VentasDevoluciones With {.codVenta = cab.idVenta, .fecha = cab.fechaVenta, .montoTotal = cab.montoTotal}).ToList()
            Else
                consulta = (From cab In contexto.OrdenesVentas Where System.Data.Entity.DbFunctions.TruncateTime(cab.fechaVenta) >= System.Data.Entity.DbFunctions.TruncateTime(fechaInicio) And System.Data.Entity.DbFunctions.TruncateTime(cab.fechaVenta) <= System.Data.Entity.DbFunctions.TruncateTime(fechafin) Select New VentasDevoluciones With {.codVenta = cab.idVenta, .fecha = cab.fechaVenta, .montoTotal = cab.montoTotal}).ToList()
            End If
            'System.Data.Entity.DbFunctions.TruncateTime(log.fechaLogueo) = fecha.Date

            If consulta.Count > 0 Then
                Return consulta
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Erorr al recuperar los datos de las cabeceras de venta para las devoluciones. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getCabecerasParaDev() As List(Of VentasDevoluciones)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From cab In contexto.OrdenesVentas Select New VentasDevoluciones With {.codVenta = cab.idVenta, .fecha = cab.fechaVenta, .montoTotal = cab.montoTotal}
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Erorr al recuperar los datos de las cabeceras de venta para las devoluciones. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getVentas(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime) As List(Of RepVentas)
        Try
            Dim contexto As New CalabozoEntities
            Dim ventasTotales As List(Of RepVentas)
            If fechaInicio.Date = fechaInicio.Date Then
                ventasTotales = (From ventas In contexto.DetallesVentas Where ventas.OrdenesVentas.fechaVenta = fechaInicio.Date
            Select New RepVentas With {.precio = ventas.precioDetalle, .nombreProducto = ventas.Productos.nombreProducto,
            .categoria = ventas.Productos.CategoriasSubcategorias.Categorias.nombre,
            .subcategoria = ventas.Productos.CategoriasSubcategorias.Subcategorias.nombre,
            .cantidad = ventas.cantidad}).ToList()
            Else
                ventasTotales = (From ventas In contexto.DetallesVentas Where ventas.OrdenesVentas.fechaVenta >= fechaInicio And ventas.OrdenesVentas.fechaVenta <= fechaFin
            Select New RepVentas With {.precio = ventas.precioDetalle, .nombreProducto = ventas.Productos.nombreProducto,
            .categoria = ventas.Productos.CategoriasSubcategorias.Categorias.nombre,
            .subcategoria = ventas.Productos.CategoriasSubcategorias.Subcategorias.nombre,
            .cantidad = ventas.cantidad}).ToList()
            End If
            
            If ventasTotales.Count() > 0 Then
                Return ventasTotales
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al recuperar las ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getUltimoIdCabecera() As Integer
        Try
            Dim contexto As New CalabozoEntities
            Dim id As Integer
            id = (From cab In contexto.OrdenesVentas Select cab).ToList().LastOrDefault().idVenta
            Return id

        Catch ex As Exception
            MsgBox("Error al obtener la última factura de venta. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return -1
        End Try
    End Function
    Public Shared Function getDetallesIdVenta(ByVal idVenta As Integer) As List(Of DetallesVentas)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From det In contexto.DetallesVentas Where det.idVenta = idVenta Select det
            If consulta.Count > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener los detalles de compra para el compra especificada. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function getMotivosModVentas() As List(Of MotivosModVentas)
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = From motivos In contexto.MotivosModVentas Where motivos.estado = True Order By motivos.idMotivoModVentas Descending Select motivos
            If consulta.Count() > 0 Then
                Return consulta.ToList()
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al obtener la lista de motivos de modificación para ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarStock(ByVal idProd As Integer, ByVal stock As Integer) As Boolean
        Try
            Dim contexto As New CalabozoEntities
            Dim prod = (From p In contexto.Productos Where p.idProducto = idProd Select p)
            If prod.Single().stock >= stock Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al controlar si hay stock disponible. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function

    Public Shared Sub insertarCabeceraModVenta(ByVal montoTotal As Double)
        Try
            Dim contexto As New CalabozoEntities
            Dim cabecera As New CabeceraModVentas
            cabecera.fechaDescuento = Date.Now()
            cabecera.montoDescuento = montoTotal
            cabecera.estado = True
            contexto.CabeceraModVentas.Add(cabecera)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al colocar la cabecera de la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub insertarDetallesMod(ByVal detalles As List(Of DetallesModVentas))
        Try
            Dim contexto As New CalabozoEntities
            contexto.DetallesModVentas.AddRange(detalles)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de modificación de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Function getUltimaCabeceraMod() As Integer
        Try
            Dim contexto As New CalabozoEntities
            Dim consulta = (From comp In contexto.CabeceraModVentas Select comp).ToList().LastOrDefault().idModVentas
            Return consulta
        Catch ex As Exception
            MsgBox("Error al obtener la última cabecera de modificación insertada para la venta. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return 0
        End Try
    End Function
    Public Shared Sub insertarCabeceraVenta(ByVal montoTotal As Double, ByVal idUsuario As Integer)
        Try
            Dim contexto As New CalabozoEntities
            Dim cabecera As New OrdenesVentas
            cabecera.montoTotal = montoTotal
            cabecera.idUsuario = idUsuario
            cabecera.estado = True
            cabecera.fechaVenta = Date.Now()
            contexto.OrdenesVentas.Add(cabecera)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al colocar la cabecera de la venta. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub



    Public Shared Sub insertarDetallesVentas(ByVal detalles As List(Of DetallesVentas))
        Try
            Dim contexto As New CalabozoEntities
            contexto.DetallesVentas.AddRange(detalles)
            contexto.SaveChanges()
        Catch ex As Exception
            MsgBox("Error al insertar los detalles de compras. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al insertar datos")
        End Try
    End Sub

    Public Shared Sub actualizarStockPorVenta(ByVal idProd As Integer, ByVal unidadesVendidas As Integer)
        Try
            Dim contexto As New CalabozoEntities
            Dim producto As New Productos
            Dim stockAntiguo As Integer
            Dim consulta = From prod In contexto.Productos Where prod.idProducto = idProd Select prod
            If consulta.Count > 0 Then
                producto = consulta.Single()
                stockAntiguo = producto.stock
                producto.stock = stockAntiguo - unidadesVendidas
                contexto.SaveChanges()
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class
