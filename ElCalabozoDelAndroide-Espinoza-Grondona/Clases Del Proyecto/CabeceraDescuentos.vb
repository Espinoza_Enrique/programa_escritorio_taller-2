﻿Public Class CabeceraDescuentos
    Private _idDescuento As Integer
    Private _montoDescuento As Double
    Private _estado As Boolean

    Public Sub New(ByVal _idDescuento As Integer, ByVal _montoDescuento As Double)
        Me.setIdDescuento(_idDescuento)
        Me.setMontoDescuento(_montoDescuento)
    End Sub

    'constructor sin el id de descuento, ya que es declarado como identity en la base de datos
     Public Sub New(ByVal _montoDescuento As Double, ByVal _estado As Boolean)
        Me.setMontoDescuento(_montoDescuento)
        Me.setEstado(_estado)
    End Sub


    Private Sub setIdDescuento(ByVal id As Integer)
        Me._idDescuento = id
    End Sub

    Private Sub setMontoDescuento(ByVal monto As Double)
        Me._montoDescuento = monto
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Private Function getIdDescuento() As Integer
        Return Me._idDescuento
    End Function

    Private Function getMontoDescuento() As Double
        Return Me._montoDescuento
    End Function
    Private Function getEstado() As Boolean
        Return Me._estado
    End Function
End Class
