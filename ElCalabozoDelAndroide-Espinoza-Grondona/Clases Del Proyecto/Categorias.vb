﻿Public Class Categorias
	private _idCategoria  as integer
	private _nombre as string
    Private _descripcionCat As String
	private _estado as boolean
	Private _fechaMod as Date

	Public Sub New(Byval nombre as String, Byval descripcion as String)
		Me.setNombre(nombre)
		Me.setDescripcion(descripcion)
	end Sub

    Public Sub New(ByVal id As Integer, ByVal nombre As String, ByVal descripcion As String)
        Me.setIdCategoria(id)
        Me.setNombre(nombre)
        Me.setDescripcion(descripcion)
        Me.setEstado(True)
    End Sub


    Private Sub setFechaMod(Byval fecha as Date)
    	Me._fechaMod=fecha
    End Sub
	
	private sub setIdCategoria(byval id as integer)
		Me._idCategoria =id
	end sub

	private sub setNombre(byval _nombre as string)
		Me._nombre=_nombre
	end sub

	private sub setDescripcion(byval descripcion as string)
		Me._descripcionCat=descripcion
	end sub

	private sub setEstado(byval _estado as boolean)
		Me._estado=_estado
	end sub

	public function getCategoria() as integer
		return Me._idCategoria 
	end function

	public function getNombre() as String
		return Me._nombre
	end function

	public function getDescripcion() as String
		return Me._descripcionCat
	end function

	public function getEstado() as boolean
		return Me._estado
	end function

    Private Function getFechaMod() As Date
        Return Me._fechaMod
    End Function
End Class
