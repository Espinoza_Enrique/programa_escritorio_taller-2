Public Class CategoriasSubcategorias
    Private _idCategoria As Integer
    Private _idSubcategoria As Integer
    Private _descripcionCatSub As String
    Private _estado As Boolean
    Private _fechaModCatSub As Date


    Public Sub New(ByVal _idCategoria As Integer, ByVal idSubcat As Integer, ByVal descripcion As String)
        Me.setIdCategoria(_idCategoria)
        Me.setIdSubcategoria(idSubcat)
        Me.setDescripcion(descripcion)
    End Sub



    Private Sub setIdCategoria(ByVal id As Integer)
        Me._idCategoria = id
    End Sub

    Private Sub setIdSubcategoria(ByVal id As Integer)
        Me._idSubcategoria = id
    End Sub

    Private Sub setDescripcion(ByVal descripcion As String)
        Me._descripcionCatSub = descripcion
    End Sub


    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Private Sub setFechaModCatSub(ByVal fecha As Date)
        Me._fechaModCatSub = fecha
    End Sub

    Public Function getIdCategoria() As Integer
        Return Me._idCategoria
    End Function

    Public Function getIdSubcategoria() As Integer
        Return Me._idSubcategoria
    End Function

    Public Function getDescripcion() As String
        Return Me._descripcionCatSub
    End Function



    Public Function getEstado() As Boolean
        Return Me._estado
    End Function

    Public Function get_fechaModCatSub() As Date
        Return Me._fechaModCatSub
    End Function

    Public Sub modificarFecha(ByVal fecha As Date)
        Me.setFechaModCatSub(fecha)
    End Sub

    Public Sub modificarEstado(ByVal _estado As Boolean)
        Me.setEstado(_estado)
    End Sub
End Class