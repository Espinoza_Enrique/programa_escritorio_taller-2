﻿Public Class ComprasProductos
    Private _idCompra As Integer
    Private _idUsuario as Integer
    Private _precioTotal as Double
    Private _fechaCompra As Date
    Private _estado As Boolean

    Public Sub New(Byval idUsuario as Integer, ByVal precio As Double,Byval fechaCompra as Date)
        Me.setPrecioTotal(precio)
        Me.setIdUsuario(idUsuario)
        Me.setFechaCompra(fechaCompra)
    End Sub

     Public Sub New(Byval idUsuario as Integer, ByVal precio As Double)
        Me.setPrecioTotal(precio)
        Me.setIdUsuario(idUsuario)
    End Sub

    Private Sub setIdUsuario(Byval id as Integer)
        Me._idUsuario=id
    End Sub

    Private Sub setIdCompra(ByVal id As Integer)
        Me._idCompra = id
    End Sub

    Private Sub setPrecioTotal(ByVal precio As Double)
        Me._precioTotal = precio
    End Sub

    Private Sub setFechaCompra(ByVal fecha As Date)
        Me._fechaCompra = fecha
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Public Function getIdCompra() As Integer
        Return Me._idCompra
    End Function

    Public Function getPrecio() As Double
        Return Me._precioTotal
    End Function

    Public Function getFechaCompra() As Date
        Return Me._fechaCompra
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function

    Public Function getIdUsuario() as Integer
        Return Me._idUsuario
    End Function

    Public Function getPrecioCompra() As Double
        Return Me._precioTotal
    End Function
End Class
