﻿Public Class DetallesCompras
    Private _idCompra As Integer
    Private _idDetalleCompra As Integer
    Private _idProducto As Integer
    Private _idProveedor As Integer
    Private _unidadesCompra As Integer
    Private _precioCompra As Double
    Private _estado As Boolean

    Public Sub New(ByVal _idCompra As Integer, ByVal _idDetalleCompra As Integer, ByVal _idProducto As Integer, ByVal _idProveedor As Integer, ByVal unidades As Integer, ByVal _precioCompra As Integer)
        Me.setIdCompra(_idCompra)
        Me.setIdDetalleCompra(_idDetalleCompra)
        Me.setIdProducto(_idProducto)
        Me.setIdProveedor(_idProveedor)
        Me.setUnidadesCompra(unidades)
        Me.setPrecioCompra(_precioCompra)
    End Sub

    Private Sub setPrecioCompra(ByVal precio As Double)
        Me._precioCompra = precio
    End Sub
    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub
    Private Sub setUnidadesCompra(ByVal unidades As Integer)
        Me._unidadesCompra = unidades
    End Sub
    Private Sub setIdProveedor(ByVal id As Integer)
        Me._idProveedor = id
    End Sub
    Private Sub setIdProducto(ByVal id As Integer)
        Me._idProducto = id
    End Sub
    Private Sub setIdCompra(ByVal id As Integer)
        Me._idCompra = id
    End Sub

    Private Sub setIdDetalleCompra(ByVal id As Integer)
        Me._idDetalleCompra = id
    End Sub

    Public Function getIdCompra() As Integer
        Return Me._idCompra
    End Function

    Public Function getIdDetalleCompra() As Integer
        Return Me._idDetalleCompra
    End Function

    Public Function getIdProducto() As Integer
        Return Me._idProducto
    End Function

    Public Function getIdProveedor() As Integer
        Return Me._idProveedor
    End Function

    Public Function getUnidadesCompra() As Integer
        Return Me._unidadesCompra
    End Function

    Public Function getPrecioCompra() As Double
        Return Me._precioCompra
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function
End Class
