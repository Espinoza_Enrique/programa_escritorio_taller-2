﻿Public Class DetallesDescuentos
    Private _idDescuento As Integer
    Private _idDetalleDescuento As Integer
    Private _idVenta As Integer
    Private _idDetalleVenta As Integer
    Private _descuentoDetalle As Double
    Private _estado As Boolean

    Public Sub New(ByVal _idDescuento As Integer, ByVal _idDetalleDescuento As Integer, ByVal _idVenta As Integer, ByVal _idDetalleVenta As Integer, ByVal montoDescuento As Double)
        Me.setIdDescuento(_idDescuento)
        Me.setIdDetalleDescuento(_idDetalleDescuento)
        Me.setIdVenta(_idVenta)
        Me.setIdDetalleVenta(_idDetalleVenta)
        Me.setMontoDescuento(montoDescuento)
    End Sub
    Private Sub setIdDescuento(ByVal id As Integer)
        Me._idDescuento = id
    End Sub

    Private Sub setIdDetalleDescuento(ByVal id As Integer)
        Me._idDetalleDescuento = id
    End Sub

    Private Sub setIdVenta(ByVal id As Integer)
        Me._idVenta = id
    End Sub

    Private Sub setIdDetalleVenta(ByVal id As Integer)
        Me._idDetalleVenta = id
    End Sub

    Private Sub setMontoDescuento(ByVal descuento As Double)
        Me._descuentoDetalle = descuento
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Public Function getIdDescuento() As Integer
        Return Me._idDescuento
    End Function

    Public Function getIdDetalleDescuento() As Integer
        Return Me._idDetalleDescuento
    End Function

    Public Function getIdVenta() As Integer
        Return Me._idVenta
    End Function

    Public Function getIdDetalleVenta() As Integer
        Return Me._idDetalleVenta
    End Function

    Public Function getMontoDescuento() As Double
        Return Me._descuentoDetalle
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function
End Class
