Public Class DetallesVentas
    Private _idVenta As Integer
    Private _idDetalle As Integer
    Private _idProducto As Integer
    Private _cantidad As Integer
    Private _precioDetalle As Double
    Private _fechaModDet As Date
    Private _estado As Boolean

    Public Sub New(ByVal _idVenta As Integer, ByVal _idDetalleVenta As Integer, ByVal _idProducto As Integer, ByVal _cantidad As Integer, ByVal precio As Double, ByVal estado As Boolean)
        Me.setIdVenta(_idVenta)
        Me.setIdDetalle(_idDetalleVenta)
        Me.setIdProducto(_idProducto)
        Me.setCantidad(_cantidad)
        Me.setPrecio(precio)
        Me.setEstado(_estado)
    End Sub

    Public Sub New(ByVal _idVenta As Integer, ByVal _idDetalleVenta As Integer, ByVal _idProducto As Integer, ByVal _cantidad As Integer, ByVal precio As Double)
        Me.setIdVenta(_idVenta)
        Me.setIdDetalle(_idDetalleVenta)
        Me.setIdProducto(_idProducto)
        Me.setCantidad(_cantidad)
        Me.setPrecio(precio)
    End Sub


    Private Sub setIdVenta(ByVal id As Integer)
        Me._idVenta = id
    End Sub

    Private Sub setIdDetalle(ByVal id As Integer)
        Me._idDetalle = id
    End Sub

    Private Sub setIdProducto(ByVal id As Integer)
        Me._idProducto = id
    End Sub

    Private Sub setCantidad(ByVal _cantidad As Integer)
        Me._cantidad = _cantidad
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Private Sub setPrecio(ByVal precio As Double)
        Me._precioDetalle = precio
    End Sub

    Private Sub setFechaModDetalle(ByVal fecha As Date)
        Me._fechaModDet= fecha
    End Sub

    Public Function getIdVenta() As Integer
        Return Me._idVenta
    End Function

    Public Function getIdDetalleVenta() As Integer
        Return Me._idDetalle
    End Function

    Public Function getIdProducto() As Integer
        Return Me._idProducto
    End Function

    Public Function getCantidad() As Integer
        Return Me._cantidad
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function

    Public Function getPrecio() As Double
        Return Me._precioDetalle
    End Function

    Public Function getFechaModDetalle() As Date
        Return Me._fechaModDet
    End Function

End Class
