﻿Public Class Devoluciones
    Private _idDevolucion As Integer
    Private _fecha As Date
    Private _montoTotal As Double
    Private _estado As Boolean
    Private _fechaModDev As Date

    Public Sub New(ByVal _idDevolucion As Integer, ByVal _fecha As Date, ByVal montoDevuelto As Double)
        Me.setIdDevolucion(_idDevolucion)
        Me.setFechaDev(_fecha)
        Me.setMontoDevuelto(montoDevuelto)
    End Sub

    Private Sub setFechaDev(ByVal _fecha As Date)
        Me._fecha = _fecha
    End Sub

    Private Sub setIdDevolucion(ByVal id As Integer)
        Me._idDevolucion = id
    End Sub

    Private Sub setMontoDevuelto(ByVal monto As Integer)
        Me._montoTotal = monto
    End Sub


    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    
    Private Sub setFechaModDev(ByVal _fecha As Date)
        Me._fechaModDev = _fecha
    End Sub

    Public Function getIdDevolucion() As Integer
        Return Me._idDevolucion
    End Function

    Public Function getFechaDetalle() As Date
        Return Me._fecha
    End Function
    Public Function getMontoDevuelto() As Double
        Return Me._montoTotal
    End Function



    Public Function getEstado() As Boolean
        Return Me._estado
    End Function


    Public Function getFechaModDevolucion() As Date
        Return Me._fechaModDev
    End Function
End Class
