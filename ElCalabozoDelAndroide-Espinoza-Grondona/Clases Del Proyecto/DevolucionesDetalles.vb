﻿Public Class DevolucionesDetalles
    Private _idDevolucion As Integer
    Private _idDetalleDev As Integer
    Private _montoDetalleDev As Double
    Private _idVenta As Integer
    Private _idDetalleVenta As Integer
    Private _idMotivo As Integer
    Private _descripcion As String
    Private _estado As Boolean
    Private _fechaModDevDet As Date

    Public Sub New(ByVal _idDevolucion As Integer, ByVal _idDetalleDev As Integer, ByVal _montoDetalleDev As Double, ByVal _idVenta As Integer, ByVal _idDetalleVenta As Integer, ByVal motivo As Integer, ByVal descripcion As String)
        Me.setIdDevolucion(_idDevolucion)
        Me.setIdDetalleDev(_idDetalleDev)
        Me.setMonto(_montoDetalleDev)
        Me.setIdVenta(_idVenta)
        Me.setIdDetalleVenta(_idDetalleVenta)
        Me.setIdMotivo(motivo)
        Me.setDescripcion(descripcion)
    End Sub

    Private Sub setIdVenta(ByVal id As Integer)
        Me._idVenta = id
    End Sub

    Private Sub setIdDetalleVenta(ByVal id As Integer)
        Me._idDetalleVenta = id
    End Sub

    Private Sub setIdMotivo(ByVal id As Integer)
        Me._idMotivo = id
    End Sub


    Private Sub setIdDevolucion(ByVal id As Integer)
        Me._idDevolucion = id
    End Sub

    Private Sub setIdDetalleDev(ByVal id As Integer)
        Me._idDetalleDev = id
    End Sub

    Private Sub setDescripcion(ByVal descripcion As String)
        Me._descripcion = descripcion
    End Sub

    Private Sub setMonto(ByVal monto As Double)
        Me._montoDetalleDev = monto
    End Sub


    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub


    Private Sub setFechaModDetalle(ByVal fecha As Date)
        Me._fechaModDevDet = fecha
    End Sub

    Public Function getIdVenta() As Integer
        Return Me._idVenta
    End Function

    Public Function getIdDetalleVenta() As Integer
        Return Me._idDetalleVenta
    End Function

    Public Function getIdDevolucion() As Integer
        Return Me._idDevolucion
    End Function

    Public Function getDetalleDev() As Integer
        Return Me._idDetalleDev
    End Function



    Public Function getEstado() As Boolean
        Return Me._estado
    End Function


    Public Function getFechaModDevDetalle() As Date
        Return Me._fechaModDevDet
    End Function

    Public Function getMotivo() As Integer
        Return Me._idMotivo
    End Function

    Public Function getDescripcion() As String
        Return Me.getDescripcion()
    End Function

    Public Function getMontoDetalle() As Double
        Return Me._montoDetalleDev
    End Function
End Class
