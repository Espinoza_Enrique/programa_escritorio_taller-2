﻿Public Class Empleados
    Private _idEmpleado As Integer
    Private _nombreEmpleado As String
    Private _apellidoEmpleado As String
    Private _dni As Integer
    Private _mail As String
    Private _telefono As String
    Private _estado As Boolean
    Private _fechaModEmp As Date

    Public Sub New(ByVal id As Integer, ByVal nombre As String, ByVal apellido As String, ByVal dni As Integer, ByVal mail As String, ByVal telefono As String, ByVal estado As Boolean)
        Me.setIdEmpleado(id)
        Me.setNombre(nombre)
        Me.setApellido(apellido)
        Me.setDni(dni)
        Me.setMail(mail)
        Me.setTelefono(telefono)
        Me.setEstado(estado)
        Me.setFechaModEmp(Now())
    End Sub

    Public Sub New(ByVal nombre As String, ByVal apellido As String, ByVal _dni As Integer, ByVal _mail As String, ByVal _telefono As String)
        Me.setNombre(nombre)
        Me.setApellido(apellido)
        Me.setDni(_dni)
        Me.setMail(_dni)
        Me.setTelefono(_telefono)
    End Sub
    Private Sub setIdEmpleado(ByVal id As Integer)
        Me._idEmpleado = id
    End Sub

    Private Sub setNombre(ByVal nombre As String)
        Me._nombreEmpleado = nombre
    End Sub

    Private Sub setApellido(ByVal apellido As String)
        Me._apellidoEmpleado = apellido
    End Sub

    Private Sub setDni(ByVal _dni As Integer)
        Me._dni = _dni
    End Sub

    Private Sub setMail(ByVal _mail As String)
        Me._mail = _mail
    End Sub

    Private Sub setTelefono(ByVal _telefono As String)
        Me._telefono = _telefono
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Private Sub setFechaModEmp(ByVal fecha As Date)
        Me._fechaModEmp = fecha
    End Sub

    Public Function getNombre() As String
        Return Me._nombreEmpleado
    End Function

    Public Function getApellido() As String
        Return Me._apellidoEmpleado
    End Function
    Public Function getIdEmpleado() As Integer
        Return Me._idEmpleado
    End Function

    Public Function getMail() As String
        Return Me._mail
    End Function

    Public Function getTelefono() As String
        Return Me._telefono
    End Function
    Public Function getDni() As Integer
        Return Me._dni
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function

    Public Function getFechaModEmp() As Date
        Return Me._fechaModEmp
    End Function

    Public Sub modificarFecha(ByVal fecha As Date)
        Me.setFechaModEmp(fecha)
    End Sub
    Public Sub modificarNombre(ByVal nombre As String)
        Me.setNombre(nombre)
    End Sub

    Public Sub modificarMail(ByVal mail As String)
        Me.setMail(mail)
    End Sub

    Public Sub modificarApellido(ByVal apellido As String)
        Me.setApellido(apellido)
    End Sub
    Public Sub modificarEstado(ByVal _estado As Boolean)
        Me.setEstado(_estado)
    End Sub

    Public Sub modificarDni(ByVal dni As Integer)
        Me.setDni(dni)
    End Sub

    Public Sub modificarTel(ByVal tel As String)
        Me.setTelefono(tel)
    End Sub
End Class
