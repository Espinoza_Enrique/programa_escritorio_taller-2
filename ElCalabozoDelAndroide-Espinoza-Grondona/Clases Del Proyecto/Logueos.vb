Public Class Logueos
   Private _idLogueo as Integer
   Private _idUsuario as Integer
   Private _fechaLogueo as Date
   Private _fechaDeslogueo as Date

   PUblic Sub New(Byval idLogueo as integer,Byval fechaDeslogueo as date)
   		Me.setIdLogueo(idLogueo)
   		Me.setFechaDeslogueo(fechaDeslogueo)
   End Sub

   PUblic Sub New(Byval idLogueo as integer, Byval idUsuario as Integer, Byval fechaLogueo as date, Byval fechaDeslogueo as date)
         Me.setIdLogueo(idLogueo)
         Me.setIdUsuario(idUsuario)
         Me.setFechaLogueo(fechaLogueo)
         Me.setFechaDeslogueo(fechaDeslogueo)
   End Sub

    PUblic Sub New(Byval idUsuario as Integer, Byval fechaLogueo as date, Byval fechaDeslogueo as date)
        Me.setIdUsuario(idUsuario)
         Me.setFechaLogueo(fechaLogueo)
         Me.setFechaDeslogueo(fechaDeslogueo)
   End Sub


   Private Sub setIdLogueo(Byval id as integer)
   		Me._idLogueo=id
   End Sub

   Private Sub setIdUsuario(Byval id as integer)
         Me._idUsuario=id
   End Sub

    Private Sub setFechaLogueo(Byval fecha as Date)
         Me._FechaLogueo=fecha
   End Sub

   Private Sub setFechaDeslogueo(Byval fecha as Date)
         Me._FechaDeslogueo=fecha
   End Sub
   
  
   Public Function getIdLogueo() as integer
   		return Me._idLogueo
   End Function

   Public Function getIdUsuario() as integer
         return Me._idUsuario
   End Function

   Public Function getFechaLogueo() as Date
         return Me._fechaLogueo
   End Function


   Public Function getFechaDesLogueo() as Date
         return Me._fechaDesLogueo
   End Function

End Class