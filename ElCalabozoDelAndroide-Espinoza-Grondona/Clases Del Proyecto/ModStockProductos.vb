Public Class ModStockProductos
   Private _idMotivoModStock as integer
   Private _idProducto as integer
   Private _fechaModicacion as date
   Private _motivoModificacion as String
   Private _valorAnterior as double
   Private _valorActual as double
   Private _detalleCambio as String

   Public Sub New(Byval idProducto as integer,Byval valorAnte as double, Byval valorNuevo as double,Byval detalle as string,Byval motivo as String)
        Me.setIdProducto(idProducto)
        Me.setValorAnterior(valorAnte)
        Me.setValorActual(valorNuevo)
        Me.setMotivoModificacion(motivo)
   End Sub

   Private Sub setidMotivoModStock (Byval idMotivoModStock as integer)
      Me._idMotivoModStock=idMotivoModStock
   End Sub

   Private Sub setIdProducto(Byval idProducto as integer)
      Me._idProducto=idProducto
   End Sub

   Private Sub setFechaModificacion(Byval fecha as date)
        Me._fechaModicacion = fecha
   End Sub

   Private Sub setMotivoModificacion(Byval motivo as String)
        Me._motivoModificacion = motivo
   End Sub

   Private Sub setValorAnterior(Byval valorAnte as double)
      Me._valorAnterior=valorAnte
   End Sub

   Private Sub setValorActual(Byval valorActual as double)
        Me._valorActual = valorActual
   End Sub

   Private Sub setDetalleCambio(Byval detalle as String)
      Me._detalleCambio=detalle
   End Sub


   Public Function getidMotivoModStock() as Integer
      return Me._idMotivoModStock
    End Function

   Public Function getIdProducto() as Integer
      return Me._idProducto
    End Function

   Public Function getFechaModificacion() as Date
        Return Me._fechaModicacion
    End Function

   Public Function getValorAnterior() as Double
      return Me._valorAnterior
    End Function

   Public Function getValorActual() as Double
      return Me._valorActual
    End Function

   Public Function getDetalleCambio() as String
      return Me._detalleCambio
    End Function
End Class