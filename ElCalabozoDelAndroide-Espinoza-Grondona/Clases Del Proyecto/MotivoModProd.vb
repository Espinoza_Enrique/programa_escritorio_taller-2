Public Class MotivoModProd
    Private _idMotivoModProd As Integer
    Private _campoMotivo As String
    Private _nombreDelMotivo As String
    Private _descripcionMotivo As String
    Private _estado As Boolean

    Public Sub New(ByVal campoMotivo As String, ByVal nombreMotivo As String, ByVal descripcionMotivo As String)
        Me.setCampoMotivo(campoMotivo)
        Me.setNombreMotivo(nombreMotivo)
        Me.setDescripcion(descripcionMotivo)
    End Sub

    Private Sub setIdMotivoModProd(ByVal idMotivo As Integer)
        Me._idMotivoModProd = idMotivo
    End Sub

    Private Sub setCampoMotivo(ByVal motivo As String)
        Me._campoMotivo = motivo
    End Sub

    Private Sub setNombreMotivo(ByVal nombreMotivo As String)
        Me._nombreDelMotivo = nombreMotivo
    End Sub

    Private Sub setDescripcion(ByVal descripcion As String)
        Me._descripcionMotivo = descripcion
    End Sub

    Private Sub setEstado(ByVal estado As Boolean)
        Me._estado = estado
    End Sub

    Public Function getIdMotivoModProd() As Integer
        Return Me._idMotivoModProd
    End Function

    Public Function getMotivo() As String
        Return Me._campoMotivo
    End Function

    Public Function getNombreMotivo() As String
        Return Me._nombreDelMotivo
    End Function



    Public Function getDescripcion() As String
        Return Me._descripcionMotivo
    End Function

    Public Function getCampoMotivo() As Boolean
        Return Me._campoMotivo
    End Function
End Class