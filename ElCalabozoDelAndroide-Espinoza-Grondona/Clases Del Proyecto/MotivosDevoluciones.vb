﻿Public Class MotivosDevoluciones
    Private _idMotivo As Integer
    Private _descripcion As String
    Private _estado As Boolean
    Private _fechaModMotivos As Date

    Public Sub New(ByVal _idMotivo As Integer, ByVal _descripcion As String)
        Me.setIdMotivo(_idMotivo)
        Me.setDescripcion(_descripcion)
    End Sub

    Private Sub setIdMotivo(ByVal id As Integer)
        Me._idMotivo = id
    End Sub

    Private Sub setDescripcion(ByVal _descripcion As String)
        Me._descripcion = _descripcion
    End Sub

    Private Sub setFechaModMotivo(ByVal fecha As Date)
        Me._fechaModMotivos = fecha
    End Sub

    Public Function getIdMotivo() As Integer
        Return Me._idMotivo
    End Function

    Public Function getDescripcion() As String
        Return Me._descripcion
    End Function

    Public Function getEstado() As Integer
        Return Me._estado
    End Function

    Public Function getFechaModificacion() As Date
        Return Me._fechaModMotivos
    End Function
End Class
