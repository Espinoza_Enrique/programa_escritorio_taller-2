﻿Public Class OrdenesVentas
    Private _idVenta As Integer
    Private _idUsuario As Integer
    Private _fechaVenta As Date
    Private _montoTotal As Double
    Private _estado As Boolean
    Private _fechaModOrdVenta As Date

    Public Sub New(Byval idVenta as integer, Byval idUsuario as integer, ByVal fechaVenta as date, Byval montoTotal as double, Byval estado as boolean, Byval fechaModOrdenVenta as date)
        Me.setIdVenta(idVenta)
        Me.setIdUsuario(idUsuario)
        Me.setFechaVenta(fechaVenta)
        Me.setMontoTotal(montoTotal)
        Me.setEstado(estado)
        Me.setFechaModOrdVenta(fechaModOrdenVenta)
    end sub

     Public Sub New(Byval idUsuario as integer, ByVal fechaVenta as date, Byval montoTotal as double, Byval estado as boolean, Byval fechaModOrdenVenta as date)
        Me.setIdUsuario(idUsuario)
        Me.setFechaVenta(fechaVenta)
        Me.setMontoTotal(montoTotal)
    end sub

    Private Sub setFechaModOrdVenta(ByVal fecha As Date)
        Me._fechaModOrdVenta = fecha
    End Sub

    Private Sub setIdVenta(ByVal id As Integer)
        Me._idVenta = id
    End Sub

    Private Sub setIdUsuario(ByVal id As Integer)
        Me._idUsuario = id
    End Sub

    Private Sub setFechaVenta(ByVal fecha As Date)
        Me._fechaVenta = fecha
    End Sub

    Private Sub setMontoTotal(ByVal monto As Double)
        Me._montoTotal = monto
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Public Function getIdVenta() as integer
        return Me._idVenta
    end function


    Public Function getIdUsuario() as integer
        return Me._idUsuario
    end function

    Public Function getFechaModOrdenVenta() as date
        Return Me._fechaModOrdVenta
    end function

     Public Function getFechaVenta() as date
        return Me._fechaVenta
    end function

    Public Function getMontoTotal() as double
        return Me._montoTotal
    end function

    Public Function getEstado() as boolean
        return Me._estado
    end function
End Class
