﻿Public Class Perfiles
    Private _idPerfil As Integer
    Private _descripcionPerfil As String
    Private _estado As Boolean

    Public Sub New(ByVal id As Integer, ByVal descripcion As String)
        Me.setDescripcion(descripcion)
        Me.setIdPerfil(id)
    End Sub

    Public Sub New(ByVal id As Integer, ByVal descripcion As String, Byval estado as boolean)
        Me.setDescripcion(descripcion)
        Me.setIdPerfil(id)
        Me.setEstado(estado)
    End Sub

    Public Sub New(ByVal descripcion As String)
        Me.setDescripcion(descripcion)
    End Sub

    Private Sub setIdPerfil(ByVal _idPerfil As Integer)
        Me._idPerfil = _idPerfil
    End Sub

    Private Sub setDescripcion(ByVal descripcion As String)
        Me._descripcionPerfil = descripcion
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Public Function getId() As Integer
        Return Me._idPerfil
    End Function

    Public Function getDescripcion() As String
        Return Me._descripcionPerfil
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function
End Class
