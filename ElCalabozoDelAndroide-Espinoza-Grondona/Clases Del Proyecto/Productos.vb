﻿Public Class Productos
    Private _idProducto As Integer
    Private _nombreProducto As String
    Private _precio As Double
    Private _fecha As Date
    Private _nombreFoto As String
    Private _stock As Integer
    Private _stockMinimo As Integer
    Private _estado As Boolean
    Private _descripcionProducto  As String
    Private _idCategoria As Integer
    Private _idSubcategoria As Integer
    Private _garantia As Integer
    Private _fechaModProd As Date

    Public Sub New(ByVal nombre As String, ByVal _precio As Double, ByVal _nombreFoto As String, ByVal _stock As Integer, ByVal _stockMin As Integer, ByVal descripcion As String, ByVal cat As Integer, ByVal subcat As Integer, ByVal _garantia As Integer)
        Me.setNombre(nombre)
        Me.setPrecio(_precio)
        Me.setNombre(_nombreFoto)
        Me.setStock(_stock)
        Me.setStockMin(_stockMin)
        Me.setDescripcion(descripcion)
        Me.setIdCategoria(cat)
        Me.setIdSubcategoria(subcat)
        Me.setGarantia(_garantia)
    End Sub

     Public Sub New(Byval idProd as integer,ByVal nombre As String, ByVal _precio As Double, ByVal _fechaAlta As Date, ByVal _nombreFoto As String, ByVal _stock As Integer, ByVal _stockMin As Integer, ByVal descripcion As String, ByVal cat As Integer, ByVal subcat As Integer, ByVal _garantia As Integer,Byval fechaModProd as date,Byval estado as Boolean)
        Me.setIdProducto(idProd)
        Me.setNombre(nombre)
        Me.setPrecio(_precio)
        Me.setFechaAlta(_fechaAlta)
        Me.setNombre(_nombreFoto)
        Me.setStock(_stock)
        Me.setStockMin(_stockMin)
        Me.setDescripcion(descripcion)
        Me.setIdCategoria(cat)
        Me.setIdSubcategoria(subcat)
        Me.setGarantia(_garantia)
        Me.setFechaModProd(fechaModProd)
        Me.setEstado(estado)
    End Sub
    
    Private Sub setIdProducto(ByVal id As Integer)
        Me._idProducto = id
    End Sub

    Private Sub setNombre(ByVal nombre As String)
        Me._nombreProducto = nombre
    End Sub

    Private Sub setPrecio(ByVal _precio As Double)
        Me._precio = _precio
    End Sub

    Private Sub setFechaAlta(ByVal _fecha As Date)
        Me._fecha = _fecha
    End Sub

    Private Sub setNombreFoto(ByVal nombre As String)
        Me._nombreFoto = nombre
    End Sub

    Private Sub setStock(ByVal _stock As Integer)
        Me._stock = _stock
    End Sub

    Private Sub setStockMin(ByVal _stock As Integer)
        Me._stockMinimo = _stock
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Private Sub setDescripcion(ByVal descripcion As String)
        Me._descripcionProducto  = descripcion
    End Sub

    Private Sub setIdCategoria(ByVal id As Integer)
        Me._idCategoria = id
    End Sub

    Private Sub setIdSubcategoria(ByVal id As Integer)
        Me._idSubcategoria = id
    End Sub

    Private Sub setGarantia(ByVal _garantia As Integer)
        Me._garantia = _garantia
    End Sub

    Private Sub setFechaModProd(ByVal _fecha As Date)
        Me._fechaModProd = _fecha
    End Sub

    Public Function getIdProducto() As Integer
        Return Me._idProducto
    End Function

    Public Function getNombreProducto() As String
        Return Me._nombreProducto
    End Function

    Public Function getNombreFoto() As String
        Return Me._nombreFoto
    End Function

    Public Function getDescripcion() As String
        Return Me._descripcionProducto 
    End Function

    Public Function getFechaAlta() As Date
        Return Me._fecha
    End Function

    Public Function getStock() As Integer
        Return Me._stock
    End Function

    Public Function getStockMinimo() As Integer
        Return Me._stockMinimo
    End Function

    Public Function getIdCategoria() As Integer
        Return Me._idCategoria
    End Function

    Public Function getIdSubcategoria() As Integer
        Return Me._idSubcategoria
    End Function

    Public Function getGarantia() As Integer
        Return Me._garantia
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function
End Class
