﻿Public Class Proveedores
    Private _idProveedor As Integer
    Private _nombreComercial As String
    Private _direccionProveedor As String
    Private _telefonoProv As String
    Private _celularProv As String
    Private _mailProv As String
    Private _estado As Boolean
    Private _fechaModProv As Date

    Public Sub New(ByVal _nombreComercial As String, ByVal direccion As String, ByVal telefono As String, ByVal celular As String, ByVal mail As String)
        Me.setNombreComercial(_nombreComercial)
        Me.setDireccion(direccion)
        Me.setTelefono(telefono)
        Me.setCelular(celular)
        Me.setMail(mail)
    End Sub
    Private Sub setIdProveedor(ByVal id As Integer)
        Me._idProveedor = id
    End Sub

    Private Sub setNombreComercial(ByVal nombre As String)
        Me._nombreComercial = nombre
    End Sub

    Private Sub setDireccion(ByVal direccion As String)
        Me._direccionProveedor = direccion
    End Sub

    Private Sub setTelefono(ByVal telefono As String)
        Me._telefonoProv = telefono
    End Sub

    Private Sub setCelular(ByVal celular As String)
        Me._celularProv = celular
    End Sub

    Private Sub setMail(ByVal mail As String)
        Me._mailProv = mail
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Private Sub setFechaModProv(ByVal fecha As Date)
        Me._fechaModProv = fecha
    End Sub

    Private Function getIdProveedor() As Integer
        Return Me._idProveedor
    End Function

    Private Function getNombreComercial() As String
        Return Me._nombreComercial
    End Function

    Private Function getDireccion() As String
        Return Me._direccionProveedor
    End Function

    Private Function getTelefono() As String
        Return Me._telefonoProv
    End Function

    Private Function getCelular() As String
        Return Me._celularProv
    End Function

    Private Function getMail() As String
        Return Me._mailProv
    End Function

    Private Function getEstado() As Boolean
        Return Me._estado
    End Function

    Private Function getFechaModificacion() As Date
        Return Me._fechaModProv
    End Function
End Class
