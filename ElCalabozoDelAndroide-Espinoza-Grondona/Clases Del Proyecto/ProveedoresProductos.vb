﻿Public Class ProveedoresProductos
    Private _idProveedor As Integer
    Private _idProducto As Integer
    Private _precioCompra As Double
    Private _fechaAlta As Date
    Private _estado As Boolean

    Public Sub New(ByVal _idProveedor As Integer, ByVal _idProducto As Integer, ByVal _precioCompra As Double)
        Me.setIdProveedor(_idProveedor)
        Me.setIdProducto(_idProducto)
        Me.setPrecioCompra(_precioCompra)
    End Sub

    Private Sub setIdProveedor(ByVal id As Integer)
        Me._idProveedor = id
    End Sub

    Private Sub setIdProducto(ByVal id As Integer)
        Me._idProducto = id
    End Sub

    Private Sub setPrecioCompra(ByVal precio As Double)
        Me._precioCompra = precio
    End Sub

    Private Sub setFechaAlta(ByVal fecha As Date)
        Me._fechaAlta = fecha
    End Sub

    Private Sub setEstado(ByVal _estado As Boolean)
        Me._estado = _estado
    End Sub

    Public Function getIdProveedor() As Integer
        Return Me._idProveedor
    End Function

    Public Function getIdProducto() As Integer
        Return Me._idProducto
    End Function

    Public Function getPrecioCompra() As Double
        Return Me._precioCompra
    End Function

    Public Function getFechaAlta() As Date
        Return Me._fechaAlta
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function
End Class
