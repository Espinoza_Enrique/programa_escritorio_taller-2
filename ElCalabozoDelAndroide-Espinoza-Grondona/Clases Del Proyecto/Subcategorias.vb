﻿Public Class Subcategorias
	private _idSubcategoria as integer
	private _nombre as string
	private _descripcionSub as string
	private _estado as boolean
	
    Public Sub New(ByVal idSub As Integer, ByVal nombre As String, ByVal descripcion As String)
        Me.setSubCategoria(idSub)
        Me.setNombre(nombre)
        Me.setDescripcion(descripcion)
        Me.setEstado(True)
    End Sub

	Public Sub New(Byval nombre as string, Byval descripcion as string)
		Me.setNombre(nombre)
		Me.setDescripcion(descripcion)
	End Sub


	private sub setSubCategoria(byval id as integer)
		Me._idSubcategoria=id
	end sub

	private sub setNombre(byval _nombre as string)
		Me._nombre=_nombre
	end sub

	private sub setDescripcion(byval descripcion as string)
		Me._descripcionSub=descripcion
	end sub

	private sub setEstado(byval _estado as boolean)
		Me._estado=_estado
	end sub

	public function getSubcategoria() as integer
		return Me._idSubcategoria
	end function

	public function getNombre() as String
		return Me._nombre
	end function

	public function getDescripcion() as String
		return Me._descripcionSub
	end function

	public function getEstado() as boolean
		return Me._estado
	end function
End Class
