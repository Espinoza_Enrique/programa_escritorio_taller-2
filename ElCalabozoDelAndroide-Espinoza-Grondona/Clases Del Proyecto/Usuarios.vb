﻿
Public Class Usuarios
    Private _idUsuario As Integer
    Private _nombreUsuario As String
    Private _contrasenia As String
    Private _idEmpleado As Integer
    Private _idPerfil As Integer
    Private _estado As Boolean
    Private _fechaModUsu As Date

    Public Sub New(ByVal _nombreUsuario As String, ByVal _contrasenia As String, ByVal _idEmpleado As Integer, ByVal _idPerfil As Integer)
        Me.setNombreUsuario(_nombreUsuario)
        Me.setContrasenia(_contrasenia)
        Me.setIdEmpleado(_idEmpleado)
        Me.setIdPerfil(_idPerfil)
    End Sub

    Public Sub New(ByVal idUsuario As String, ByVal nombreUsuario As String, ByVal contrasenia As String, ByVal idEmpleado As Integer, ByVal idPerfil As Integer, ByVal estado As Boolean)
        Me.setNombreUsuario(nombreUsuario)
        Me.setContrasenia(contrasenia)
        Me.setIdEmpleado(idEmpleado)
        Me.setIdPerfil(idPerfil)
        Me.setIdUsuario(idUsuario)
        Me.setEstado(estado)
        Me.setFechaModUsu(Now())

    End Sub

    Public Sub New()

    End Sub

    Private Sub setIdUsuario(ByVal _idUsuario As Integer)
        Me._idUsuario = _idUsuario
    End Sub

    Private Sub setNombreUsuario(ByRef _nombreUsuario As String)
        Me._nombreUsuario = _nombreUsuario
    End Sub

    Private Sub setContrasenia(ByRef _contrasenia As String)
        Me._contrasenia = _contrasenia
    End Sub

    Private Sub setIdEmpleado(ByRef _idEmpleado As Integer)
        Me._idEmpleado = _idEmpleado
    End Sub

    Private Sub setIdPerfil(ByRef _idPerfil As Integer)
        Me._idPerfil = _idPerfil
    End Sub

    Private Sub setEstado(ByRef _estado As Boolean)
        Me._estado = _estado
    End Sub

    Private Sub setFechaModUsu(ByRef fechaModificacion As Date)
        Me._fechaModUsu = fechaModificacion
    End Sub


    Public Function getIdUsuario() As Integer
        Return Me._idUsuario
    End Function

    Public Function getContrasenia() As String
        Return Me._contrasenia
    End Function
    Public Function getNombreUsu() As String
        Return Me._nombreUsuario
    End Function

    Public Function getIdPerfil() As Integer
        Return Me._idPerfil
    End Function

    Public Function getIdEmpleado() As Integer
        Return Me._idEmpleado
    End Function

    Public Function getEstado() As Boolean
        Return Me._estado
    End Function

    Public Function getFechaModUsu() As Date
        Return Me._fechaModUsu
    End Function

    Public Sub modificarEstado(ByVal estado As Boolean)
        Me.setEstado(estado)
    End Sub

    Public Sub modificarNombreUsu(ByVal nuevoHombre As String)
        Me._nombreUsuario = nuevoHombre
    End Sub

    Public Sub modificar_contrasenia(ByVal nueva_contrasenia As String)
        Me.setContrasenia(nueva_contrasenia)
    End Sub

    Public Sub modificarPerfil(ByVal perfil As Integer)
        Me.setIdPerfil(perfil)
    End Sub
End Class
