﻿Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Public Class General

    Public Shared Function controlarRealPositivo(ByVal valor As String) As Boolean
        Try

            Dim resultado As Boolean

            resultado = True
            If String.IsNullOrWhiteSpace(valor) Or Not IsNumeric(valor) Then
                resultado = False
            ElseIf CDbl(valor) <= 0 Then
                resultado = False
            End If
            Return resultado
        Catch ex As Exception
            MsgBox("Error al controlar si el valor " + valor + " es un número real positivo. Los errores producidos fuero: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return False
        End Try
    End Function
    Public Shared Function getCodArea(ByVal numero As String) As String
        Try
            Dim cantidadDig As Integer
            Dim digito As Integer
            Dim codArea As String

            cantidadDig = numero.Length
            digito = 0
            codArea = ""
            While digito < cantidadDig And numero(digito) <> "-"
                codArea = codArea + numero(digito)
                digito = digito + 1
            End While
            Return Trim(codArea)
        Catch ex As Exception
            MsgBox("Error al obtener el código de área. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function getNumeroCel(ByVal numero As String) As String
        Try
            Dim cantidadDig As Integer
            Dim digito As Integer
            Dim numeroCel As String
            Dim numeroCorrecto As String

            numeroCorrecto = ""
            cantidadDig = 0
            digito = numero.Length - 1
            numeroCel = ""

            While digito >= cantidadDig And numero(digito) <> "-"
                numeroCel = numeroCel + numero(digito)
                digito = digito - 1
            End While

            cantidadDig = Trim(numeroCel).Length - 1
            digito = 0
            While cantidadDig >= digito
                numeroCorrecto = numeroCorrecto + numeroCel(cantidadDig)
                cantidadDig = cantidadDig - 1
            End While
            Return Trim(numeroCorrecto)
        Catch ex As Exception
            MsgBox("Error al obtener el número. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
            Return Nothing
        End Try
    End Function
    Public Shared Function capitalizar(ByVal texto As String) As String
        Dim t As String
        t = texto ' Poner el contenido del cuadro de texto
        ' en una variable de cadena.
        If t <> "" Then
            Mid$(t, 1, 1) = UCase$(Mid$(t, 1, 1))
            For i = 1 To Len(t) - 1
                If Mid$(t, i, 2) = ChrW(13) + ChrW(10) Then
                    ' las palabras en mayúscula precedidas por un retorno de carro más
                    ' la combinación de salto de línea. Esto sólo se aplica cuando la
                    ' propiedad MultiLine del cuadro de texto se establece en True:
                    Mid$(t, i + 2, 1) = UCase$(Mid$(t, i + 2, 1))
                End If
                If Mid$(t, i, 1) = " " Then
                    ' Capitalizar palabras precedidas por un espacio:
                    Mid$(t, i + 1, 1) = UCase$(Mid$(t, i + 1, 1))
                End If
            Next
            Return t
        End If
        Return ""
    End Function

    '-------------------------------------------------------------------------------------------------------------
    'Permite moverse entre ventanas cerrando la ventana origen y mostrando la ventana destino
    'Parámetros:
    'ventanaOrigen-Tipo: Formulario-La ventana que se cerrará
    'ventanaDestino-Tipo: Formulario-La vantana que se abrirá
    '------------------------------------------------------------------------------------------------------------
    Public Shared Sub moverseEntreVentana(ByRef ventanaDestino As Form)
        Try
            ventanaDestino.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub
    '------------------------------------------------------------------------------------------------------------------
    'Permite cerrar las ventanas y volver a la ventana de inicio de sesión
    'Parámetros:
    'ventanaOrigen-Tipo: Formulario-La ventana que se cerrará, será la ventana principal de un usuario según su perfil
    'ventanaSesión-Tipo: Formulario-La ventana de inicio de sesión que se volverá visible
    '----------------------------------------------------------------------------------------------------------------
    Public Shared Sub cerrarSesion(ByRef ventanaOrigen As Form, ByRef ventanaSesion As Form)
        Try
            ventanaSesion.Visible = True
            ventanaOrigen.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub
    '-------------------------------------------------------------------------------------------------------------
    'Permite moverse desde la ventana inicial hacia cualquier otra. La diferencia con moverseEntreVentanas es que
    'este método no cierra la ventana principal, sólo la vuelve invisible
    'Parámetros:
    'pantallaInicial-Tipo: Formulario-La ventana inicial que se volverá invisible. Puede ser de cualquier perfil
    'ventanaDestino-Tipo: Formulario-La ventana destino que se mostrará 
    '-------------------------------------------------------------------------------------------------------------
    Public Shared Sub moverseDesdeVentanaInicial(ByRef ventanaInicial As Form, ByRef ventanaDestino As Form)
        Try
            ventanaInicial.Visible = False
            ventanaDestino.Show()
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub
    '-------------------------------------------------------------------------------------------------------------
    'Permite controlar los valores ingresados para un producto
    'Parámetros:
    'precio-Tipo: String-Representa el precio del producto
    'stock-Tipo: String-Representa el stock inicial del producto
    'stockMin-Tipo: String-Representa el stock mínimo del producto
    'garantia-Tipo: String-Representa la cantidad de de días de garantía del producto
    'ruta-Tipo: String-Representa la ruta donde se guardará la imagen del producto
    'descripción-Tipo: String-Representa la descripción asociada al producto
    'Retorno: Retorna True si todos los datos cumplen los criterios de aceptación y False en caso contrario
    '-------------------------------------------------------------------------------------------------------------
    Public Shared Function controlaValoresProducto(ByVal precio As String, ByVal Ruta As String, ByVal descripcion As String) As Boolean
        Dim precioCorrecto As Boolean
        Dim imagenCorrecta As Boolean
        Dim descripcionCorrecta As Boolean

        Dim respuesta As Boolean
        Try
            precioCorrecto = controlarReales(precio)

            If Ruta <> "" Then
                imagenCorrecta = True
            Else
                imagenCorrecta = False
            End If

            If descripcion <> "" Then
                descripcionCorrecta = True
            Else
                descripcionCorrecta = False
            End If

            If precioCorrecto And imagenCorrecta And descripcionCorrecta Then
                respuesta = True
            Else
                respuesta = False
            End If
            controlaValoresProducto = respuesta
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function

    Public Shared Function controlarReales(ByVal real As String) As Boolean
        Try
            If IsNumeric(real) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function
    '-------------------------------------------------------------------------------------------------------------
    'Controla que los valores ingresados sean sólo enteros positivos
    'Parámetros
    'valor-Tipo: String-es el valor que se desea controlar que sea sólo un entero positivo
    'Retorno: retorna True en caso de que sea un valor entero positivo y retorna False en caso contrario
    '------------------------------------------------------------------------------------------------------------
    Public Shared Function controlarEnterosPos(ByVal valor As String) As Boolean
        Dim resultado As Boolean
        Dim longitudValor As Integer
        Dim digito As Integer

        Try
            longitudValor = valor.Length
            digito = 0
            resultado = True
            If Trim(valor) = "" Or Not IsNumeric(valor) Then
                resultado = False
            Else
                If CInt(valor) <= 0 Then
                    resultado = False
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOK, "Error en controlarValores")
            resultado = False
        End Try

        controlarEnterosPos = resultado
    End Function
    '--------------------------------------------------------------------------------------------------------------
    'Controla que el texto sólo contenga letras
    'Parámetros:
    'texto-Tipo: String-Es el texto que se va analizar que sólo contenga letras
    'Retorno: Retorna True en caso de que el texto contenga sólo letras y retorna False en caso contrario
    '---------------------------------------------------------------------------------------------------------------
    Public Shared Function controlarLetras(ByVal texto As String) As Boolean
        Dim tamanioTexto As Integer
        Dim caracter As Integer
        Dim textoCorrecto As Boolean

        caracter = 0
        tamanioTexto = texto.Length
        textoCorrecto = True
        Try
            If texto <> "" Then
                While (caracter < tamanioTexto)
                    If (IsNumeric(texto(caracter)) Or Char.IsControl(texto(caracter)) Or Char.IsPunctuation(texto(caracter))) Then
                        textoCorrecto = False
                        caracter = tamanioTexto
                    End If
                    caracter = caracter + 1
                End While
            Else
                textoCorrecto = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
        controlarLetras = textoCorrecto
    End Function

    Public Shared Function controlarTelefono(ByVal telefono As String) As String

        Dim mensaje As String

        Try
            mensaje = ""

            If telefono <> "" Then
                If Not controlarEnterosPos(telefono) Then
                    mensaje = mensaje + "Por favor, coloque sólo valores numéricos al teléfono" + vbNewLine
                End If
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function

    Public Shared Function controlarCelular(ByVal codArea As String, ByVal numero As String) As String
        Dim numeroCompleto As String
        Dim mensaje As String
        Dim digitosMin As Integer

        Try
            digitosMin = 10
            mensaje = ""
            numeroCompleto = Trim(codArea + numero)

            If (codArea = "" And numero <> "") Or (codArea <> "" And numero = "") Then
                mensaje = mensaje + "Si no quiere agregar un celular, deje el código de área y el número en blanco. Caso contrario, agregue ambos datos" + vbNewLine
            End If

            If (Not controlarEnterosPos(codArea) Or Not controlarEnterosPos(numero)) And numeroCompleto <> "" Then
                mensaje = mensaje + "El código de área y el número de celular deben ser numéricos" + vbNewLine
            End If

            If numeroCompleto.Length < digitosMin And numeroCompleto <> "" Then
                mensaje = mensaje + "El código de área debe tener, como mínimo, 4 números y el número de celular debe tener mínimo 6" + vbNewLine
            End If


            Return mensaje
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    Public Shared Function controlarSinSimbolos(ByVal texto As String) As Boolean
        Dim tamanioTexto As Integer
        Dim caracter As Integer
        Dim textoCorrecto As Boolean
        Dim simbolosInvalidos As String
        Dim elementoInvalido As Integer
        Dim simboloControlado As Integer

        Try
            simbolosInvalidos = "$%&/@!?¡¿_-*{[(}])"

            simboloControlado = 0
            elementoInvalido = simbolosInvalidos.Length
            caracter = 0
            tamanioTexto = texto.Length
            textoCorrecto = True


            While (caracter < tamanioTexto)
                simboloControlado = 0
                While simboloControlado < elementoInvalido

                    If texto(caracter) = simbolosInvalidos(simboloControlado) Then
                        textoCorrecto = False
                        simboloControlado = elementoInvalido
                        caracter = tamanioTexto
                    End If
                    simboloControlado = simboloControlado + 1
                End While
                caracter = caracter + 1
            End While


            Return textoCorrecto
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function
    '-------------------------------------------------------------------------------------------------------------
    'Permite buscar un archivo de imagen que será la imagen ilustrativa del producto
    'Parámetros
    'dialogo-Tipo: FileDialog-Es el objeto que permite se abra la caja de diálogo para elegir la imagen
    'cajaFoto-Tipo: PicturBox-Es el picture box donde irá la foto para que sea observada
    'etiquetaRuta-Tipo: Label-La etiqueta donde se almacenará la ruta del archivo para posteriormente almacenarlo
    '--------------------------------------------------------------------------------------------------------------
    Private Shared Sub buscarFoto(ByRef dialogo As FileDialog, ByRef cajaFoto As PictureBox, ByRef etiquetaRuta As TextBox)

        Try
            dialogo.Filter = "Archivos Imagenes|*.jpg|Archivos Imagenes|*.bmp|Archivos Imagenes|*.png"
            dialogo.InitialDirectory = "C:\Users"
            If dialogo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                cajaFoto.Image = Image.FromFile(dialogo.FileName)
                etiquetaRuta.Text = dialogo.FileName


            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical + vbOKOnly, "Error Al Cargar La Imagen")
        End Try
    End Sub

    '--------------------------------------------------------------------------------------------------------------------
    'Permite obtener el nombre del archivo de imagen mediante la ruta
    'Parámetros:
    'ruta-Tipo: String-Representa el nombre de la ruta donde está guardado el archivo
    'Retorno: Retorna un string que representa el nombre del archivo imagen y, en caso de no existir la ruta, retorna
    'un string vacío
    '--------------------------------------------------------------------------------------------------------------------
    Public Shared Function obtenerNombreImagen(ByVal ruta As String) As String
        Dim caracterAnalizado As Long
        Dim nombreArchivo As String
        Dim listo As Boolean
        Dim nombreCorrecto As String

        Try
            If ruta <> "" Then
                caracterAnalizado = ruta.LongCount - 1
                nombreArchivo = ""
                listo = False
                nombreCorrecto = ""

                'obtengo todos los caracteres del nombre hasta encontrar "\"
                While caracterAnalizado >= 0 And listo = False
                    If ruta(caracterAnalizado) = "\" Then
                        listo = True
                    Else
                        nombreArchivo = nombreArchivo + ruta(caracterAnalizado)
                    End If
                    caracterAnalizado = caracterAnalizado - 1
                End While
                nombreArchivo = Trim(nombreArchivo)
                caracterAnalizado = nombreArchivo.LongCount - 1
                'invierto el string del nombre del archivo para que sea correcto
                While (caracterAnalizado >= 0)
                    nombreCorrecto = nombreCorrecto + nombreArchivo(caracterAnalizado)
                    caracterAnalizado = caracterAnalizado - 1
                End While
                obtenerNombreImagen = Trim(nombreCorrecto)
            Else
                MsgBox("Por favor, seleccione una foto", vbExclamation + vbOKOnly, "Error al determinar el nombre de la foto")
                obtenerNombreImagen = ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    '-----------------------------------------------------------------------------------------------
    'Permite eliminar un archivo de imagen en una carpeta
    'Parámetros:
    'nombreArchivo-Tipo: String-Representa el nombre del archivo que se va a eliminar
    'Nota: el texto "\Fotos\" indica la subcarpeta donde se encuentra el archivo a borrar. Modificar
    'si la subcarpeta donde se guardan las imagenes tiene otro nombre
    '-----------------------------------------------------------------------------------------------
    Public Shared Sub eliminarImagenCarpeta(ByRef nombreArchivo As String)
        Dim imagenABorrar As String
        Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)
        Try
            imagenABorrar = cadena + "\Imagenes\" + nombreArchivo
            If System.IO.File.Exists(imagenABorrar) = True Then
                System.IO.File.Delete(imagenABorrar)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Public Shared Function controlarImagenRepetida(ByRef nombreArchivo As String) As Boolean
        Dim imagenABorrar As String
        Dim respuesta As Boolean
        respuesta = True
        Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)
        Try
            imagenABorrar = cadena + "\Imagenes\" + nombreArchivo
            If System.IO.File.Exists(imagenABorrar) = False Then
                respuesta = False
            End If
            Return respuesta
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function retornarRutaFoto(ByVal nombreArchivo As String) As String

        Try
            Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)
            Return cadena + "\Imagenes\" + nombreArchivo
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    '-----------------------------------------------------------------------------------------------------
    'Permite deshabilitar objetos group box para que ciertos perfiles no accedan a determinadas funciones
    'Parámetros:
    'gruposCaja-Tipo: List of GroupBox-Representa el conjunto de group box que se desactivarán
    '-------------------------------------------------------------------------------------------------
    Public Shared Sub adaptarGroupBox(ByRef gruposCaja As List(Of GroupBox))
        Dim cantidadGrupos As Integer
        Dim grupoAnalizado As Integer

        Try
            cantidadGrupos = gruposCaja.Count
            grupoAnalizado = 0
            While grupoAnalizado < cantidadGrupos
                gruposCaja(grupoAnalizado).Enabled = False
                grupoAnalizado = grupoAnalizado + 1
            End While
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub


    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'control-Tipo: TabControl-Representa el tab control cuyas tab pages se quiere seleccionar
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Shared Sub seleccionarTabControl(ByRef control As TabControl, ByVal elemento As Byte, ByRef ventanaDestino As Form)
        Try
            If elemento >= 1 And elemento <= control.TabCount Then
                control.SelectedIndex = elemento - 1
                moverseEntreVentana(ventanaDestino)

            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub
    '-----------------------------------------------------------------------------------------------
    'Permite preguntar al usuario si desea salir del programa
    'Retorno: Retorna True en caso de una respuesta afirmativa y retorna False en caso contrario
    '-----------------------------------------------------------------------------------------------
    Public Shared Function consultarSalida() As Boolean
        Dim respuesta As Byte
        Try
            respuesta = MsgBox("¿Seguo Desea Salir De La Aplicacion?", vbYesNo + vbDefaultButton2, "Salir De La Aplicación")
            If respuesta = vbYes Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function

    Public Shared Function controlarCajaEnBlanco(ByVal textoCaja As String) As Boolean
        Try
            If String.IsNullOrWhiteSpace(textoCaja) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function
    '----------------------------------------------------------------------------------------------------------------
    'Controla que el mail ingresado sea correcto
    'Par
    '----------------------------------------------------------------------------------------------------------------
    Public Shared Function validarEmail(ByVal Email As String) As String

        Dim strTmp As String
        Dim n As Long
        Dim sEXT As String
        Dim mailCorrecto As Boolean
        Dim textoError As String
        Try
            textoError = ""
            mailCorrecto = True

            sEXT = Email

            Do While InStr(1, sEXT, ".") <> 0
                sEXT = Right(sEXT, Len(sEXT) - InStr(1, sEXT, "."))
            Loop

            If Email = "" Then
                mailCorrecto = False
                textoError = textoError + "El mail no debe ser vacío. Por favor, complete el campo con un mail válido" + vbNewLine

            ElseIf InStr(1, Email, "@") = 0 Then
                mailCorrecto = False
                textoError = textoError + "El mail no contiene un arroba." + vbNewLine
            ElseIf InStr(1, Email, "@") = 1 Then
                mailCorrecto = False
                textoError = textoError + "El mail no puede contener un arroba al principio." + vbNewLine
            ElseIf InStr(1, Email, "@") = Len(Email) Then
                mailCorrecto = False
                textoError = textoError + "El mail no puede contener un arroba al final." + vbNewLine
            ElseIf EXTisOK(sEXT) = False Then
                mailCorrecto = False
                textoError = textoError + "El mail no tiene un dominio válido como: '.com', '.or'." + vbNewLine
            ElseIf Len(Email) < 6 Then
                mailCorrecto = False
                textoError = textoError + "La dirección no puede tener menos de 6 caracteres'." + vbNewLine
            End If
            strTmp = Email
            Do While InStr(1, strTmp, "@") <> 0
                n = 1
                strTmp = Right(strTmp, Len(strTmp) - InStr(1, strTmp, "@"))
            Loop
            If n > 1 Then
                mailCorrecto = False
                textoError = textoError + "La dirección no puede tener menos de 6 caracteres'." + vbNewLine
            End If

            Dim pos As Integer

            pos = InStr(1, Email, "@")

            If Mid(Email, pos + 1, 1) = "." Then
                mailCorrecto = False
                textoError = textoError + "El punto no puede estar seguido del arroba'." + vbNewLine
            End If
            Return textoError
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    '---------------------------------------------------------------------------------------------------------------------
    'Controla que el dominio del mail sea válido
    '---------------------------------------------------------------------------------------------------------------------
    Private Shared Function EXTisOK(ByVal sEXT As String) As Boolean
        Dim EXT As String
        EXTisOK = False
        Try
            EXT = ""
            If Left(sEXT, 1) <> "." Then

                sEXT = "." & sEXT
                sEXT = UCase(sEXT) 'just to avoid errors
                EXT = EXT & ".COM.EDU.GOV.NET.BIZ.ORG.TV"
                EXT = EXT & ".AF.AL.DZ.As.AD.AO.AI.AQ.AG.AP.AR.AM.AW.AU.AT.AZ.BS.BH.BD.BB.BY"
                EXT = EXT & ".BE.BZ.BJ.BM.BT.BO.BA.BW.BV.BR.IO.BN.BG.BF.MM.BI.KH.CM.CA.CV.KY"
                EXT = EXT & ".CF.TD.CL.CN.CX.CC.CO.KM.CG.CD.CK.CR.CI.HR.CU.CY.CZ.DK.DJ.DM.DO"
                EXT = EXT & ".TP.EC.EG.SV.GQ.ER.EE.ET.FK.FO.FJ.FI.CS.SU.FR.FX.GF.PF.TF.GA.GM.GE.DE"
                EXT = EXT & ".GH.GI.GB.GR.GL.GD.GP.GU.GT.GN.GW.GY.HT.HM.HN.HK.HU.IS.IN.ID.IR.IQ"
                EXT = EXT & ".IE.IL.IT.JM.JP.JO.KZ.KE.KI.KW.KG.LA.LV.LB.LS.LR.LY.LI.LT.LU.MO.MK.MG"
                EXT = EXT & ".MW.MY.MV.ML.MT.MH.MQ.MR.MU.YT.MX.FM.MD.MC.MN.MS.MA.MZ.NA"
                EXT = EXT & ".NR.NP.NL.AN.NT.NC.NZ.NI.NE.NG.NU.NF.KP.MP.NO.OM.PK.PW.PA.PG.PY"
                EXT = EXT & ".PE.PH.PN.PL.PT.PR.QA.RE.RO.RU.RW.GS.SH.KN.LC.PM.ST.VC.SM.SA.SN.SC"
                EXT = EXT & ".SL.SG.SK.SI.SB.SO.ZA.KR.ES.LK.SD.SR.SJ.SZ.SE.CH.SY.TJ.TW.TZ.TH.TG.TK"
                EXT = EXT & ".TO.TT.TN.TR.TM.TC.TV.UG.UA.AE.UK.US.UY.UM.UZ.VU.VA.VE.VN.VG.VI"
                EXT = EXT & ".WF.WS.EH.YE.YU.ZR.ZM.ZW"
                EXT = Trim(UCase(EXT)) 'just to avoid errors
                If InStr(1, EXT, sEXT, 0) <> 0 Then
                    EXTisOK = True
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function

    Public Shared Function controlarDni(ByVal dni As String) As String
        Dim mensaje As String

        Try
            mensaje = ""

            If controlarCajaEnBlanco(dni) Then
                mensaje = mensaje + "Por favor, no deje el dni en blanco" + vbNewLine
            End If

            If Not controlarEnterosPos(dni) Then
                mensaje = mensaje + "Por favor, sólo coloque valores numéricos en el dni" + vbNewLine
            End If

            If dni.Length < 8 Then
                mensaje = mensaje + "La longitud del dni debe ser como mínimo 8" + vbNewLine
            End If

            Return Trim(mensaje)
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    '-------------------------------------------------------------------------------------------------
    'Permite copiar la imagen seleccionada a la carpeta destino
    'Parámetros:
    'dialogo-Tipo: FileDialog-Es la ventana que permite seleccionar el archivo de imagen y contiene la
    'información necesaria para guardar la imagen
    'nombreArchivo-Tipo: String-Es el nombre del archivo con su extensión
    '-------------------------------------------------------------------------------------------------
    Public Shared Sub copiarImagenACarpeta(ByRef dialogo As FileDialog, ByRef nombreArchivo As String)
        'nombre de la ruta donde se está ejecutando mi programa
        Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)
        System.IO.File.Copy(dialogo.FileName, cadena + "\Imagenes\" + nombreArchivo)

    End Sub
    '-------------------------------------------------------------------------------------------------------------------------
    'Función que permite cifrar y descifrar la clave de acceso
    'Parámetros:
    'Modo-Tipo Byte- 1.Encripta-2.Decifra
    'Algoritmo-Tipo: Byte-Representa los algoritmos. Al final usaremos sólo el algoritmo 2
    'Cadena-Tipo: String-Representa la cadena a encriptar
    'Key-Tipo: String-El valor clave
    'VecI-Tipo: String-El vector inicial
    'Ejemplo de uso: Usuario: Empleado-Contraseña:1234567890123456 'Debe tener entre 16 y 24 dígitos
    '--------------------------------------------------------------------------------------------------------------------------
    Public Function Cifrado(ByVal modo As Byte, ByVal Algoritmo As Byte, ByVal cadena As String, ByVal key As String, ByVal VecI As String) As String

        Dim plaintext() As Byte

        plaintext = Nothing
        If modo = 1 Then

            plaintext = Encoding.ASCII.GetBytes(cadena)

        ElseIf modo = 2 Then

            plaintext = Convert.FromBase64String(cadena)

        End If

        Dim keys() As Byte = Encoding.ASCII.GetBytes(key)

        Dim memdata As New MemoryStream

        Dim transforma As ICryptoTransform

        transforma = Nothing

        Select Case Algoritmo

            Case 1

                Dim des As New DESCryptoServiceProvider ' DES

                des.Mode = CipherMode.CBC
                If modo = 1 Then

                    transforma = des.CreateEncryptor(keys, Encoding.ASCII.GetBytes(VecI))

                ElseIf modo = 2 Then

                    transforma = des.CreateDecryptor(keys, Encoding.ASCII.GetBytes(VecI))

                End If

            Case 2

                Dim des3 As New TripleDESCryptoServiceProvider 'TripleDES

                des3.Mode = CipherMode.CBC
                If modo = 1 Then

                    transforma = des3.CreateEncryptor(keys, Encoding.ASCII.GetBytes(VecI))

                ElseIf modo = 2 Then

                    transforma = des3.CreateDecryptor(keys, Encoding.ASCII.GetBytes(VecI))

                End If

            Case 3

                Dim rc2 As New RC2CryptoServiceProvider 'RC2

                rc2.Mode = CipherMode.CBC
                If modo = 1 Then

                    transforma = rc2.CreateEncryptor(keys, Encoding.ASCII.GetBytes(VecI))

                ElseIf modo = 2 Then

                    transforma = rc2.CreateDecryptor(keys, Encoding.ASCII.GetBytes(VecI))

                End If

            Case 4

                Dim rj As New RijndaelManaged 'Rijndael

                rj.Mode = CipherMode.CBC
                If modo = 1 Then

                    transforma = rj.CreateEncryptor(keys, Encoding.ASCII.GetBytes(VecI))

                ElseIf modo = 2 Then

                    transforma = rj.CreateDecryptor(keys, Encoding.ASCII.GetBytes(VecI))

                End If

        End Select

        Dim encstream As New CryptoStream(memdata, transforma, CryptoStreamMode.Write)

        encstream.Write(plaintext, 0, plaintext.Length)

        encstream.FlushFinalBlock()

        encstream.Close()

        If modo = 1 Then

            cadena = Convert.ToBase64String(memdata.ToArray)

        ElseIf modo = 2 Then

            cadena = Encoding.ASCII.GetString(memdata.ToArray)

        End If

        Return cadena 'Aquí Devuelve los Datos Cifrados

    End Function

End Class
