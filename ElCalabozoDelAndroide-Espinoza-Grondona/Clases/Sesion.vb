﻿'Una clase estática usa para "simular" una sesión. Se emplea para guardar, sin realizar ninguna instancia, los datos del
'usuario cuya sesión está iniciada. Esto es sólo un parche, ya que la idea real sería crear algún tipo de conexión
'cliente-servidor que permita mantener los datos del usuario cuya sesión está iniciada similar a la que se hizo en taller 1
'cuando se trabajó con codeigniter.
'Además, se dan algunos métodos necesarios para la recuperación de los datos de la "sesión" iniciada.
Public Class Sesion
    Private Shared idUsuario As Integer
    Private Shared nombreUsu As String
    Private Shared apellidoUsu As String
    Private Shared dni As Integer
    Private Shared perfil As Byte
    Private Shared idEmpleado As Integer
    Private Shared idLogueo As Integer
    Private Shared nombrePerfil




    Public Shared Function getIdLogueo() As Integer
        Return idLogueo
    End Function

    Public Shared Function getIdUsuario() As Integer
        Try
            Return idUsuario
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return -1
        End Try
    End Function

    Public Shared Function getNombreUsu() As String
        Try
            Return nombreUsu
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function

    Public Shared Function getApellidoUsu() As String
        Try
            Return apellidoUsu
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function

    Public Shared Function getDniUsuario() As Integer
        Try
            Return dni
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return -1
        End Try
    End Function

    Public Shared Function getPerfilUsuario() As Byte
        Try
            Return perfil
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return 0
        End Try
    End Function

    Public Shared Function getidEmpleado() As Integer
        Try
            Return idEmpleado
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return -1
        End Try
    End Function
    Private Shared Sub setNombrePerfil(ByVal nombre As String)
        nombrePerfil = nombre
    End Sub
    Private Shared Sub setIdEmpleado(ByVal id As Integer)
        Try
            idEmpleado = id
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Shared Sub setIdLogueo(ByVal id As Integer)
        Try
            idLogueo = id
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub
    Private Shared Sub setIdUsuario(ByVal id As Integer)
        Try
            idUsuario = id
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Shared Sub setNombreUsuario(ByVal nombre As String)
        Try
            nombreUsu = nombre
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Shared Sub setApellidoUsuario(ByVal apellido As String)
        Try
            apellidoUsu = apellido
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Shared Sub setDniUsuario(ByVal dniUsu As Integer)
        Try
            dni = dniUsu
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Shared Sub setPerfilUsuario(ByVal perfilUsu As Byte)
        Try
            perfil = perfilUsu
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Public Shared Sub establecerValores(ByVal id As Integer, ByVal nombre As String, ByVal apellido As String, ByVal dni As Integer, ByVal perfil As Byte, ByVal idEmpleado As Integer, ByVal idLogueo As Integer, ByVal nombrePerfil As String)
        Try
            setIdUsuario(id)
            setNombreUsuario(nombre)
            setApellidoUsuario(apellido)
            setDniUsuario(dni)
            setPerfilUsuario(perfil)
            setIdEmpleado(idEmpleado)
            setIdLogueo(idLogueo)
            setNombrePerfil(nombrePerfil)
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Public Shared Function getNombrePerfil() As String
        Try
            Return nombrePerfil
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function

    Public Shared Function cerrarSesion() As Boolean
        Dim respuesta As Byte
        Try
            respuesta = MsgBox("¿Seguro quiere cerrar su sesión?", vbYesNo, "Confirmación Cierre Sesión")
            If respuesta = vbYes Then

                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function

    Public Shared Sub modificarDatosEmpleado(ByVal nombre As String, ByVal apellido As String, ByVal dni As Integer)
        Try
            setNombreUsuario(nombre)
            setApellidoUsuario(apellido)
            setDniUsuario(dni)
        Catch ex As Exception
            MsgBox("Error al establecer los nuevos datos del empleado en sesión. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbExclamation, "Error al modificar datos")
        End Try
    End Sub
End Class
