﻿Public Class Tienda
    Private Shared perfiles As New List(Of Perfiles)
    Private Shared empleados As New List(Of Empleados)
    Private Shared usuarios As New List(Of Usuarios)
    Private Shared categorias As New List(Of Categorias)
    Private Shared subcategorias As New List(Of Subcategorias)
    Private Shared catsub As New List(Of CategoriasSubcategorias)

    Public Shared Function obtenerCategorias() As List(Of Categorias)
        Return categorias
    End Function

    Public Shared Function obtenerSubcategorias() As List(Of Subcategorias)
        Return subcategorias
    End Function

    Public Shared Function obtenerCatSub() As List(Of CategoriasSubcategorias)
        Return catsub
    End Function

    Public Shared Sub agregarCategorias(ByVal categoria As Categorias)
        categorias.Add(categoria)
    End Sub

    Public Shared Sub agregarSubcategoria(ByVal subcat As Subcategorias)
        subcategorias.Add(subcat)
    End Sub

    Public Shared Sub agregarCatSub(ByVal catesub As CategoriasSubcategorias)
        catsub.Add(catesub)
    End Sub
    Public Shared Function getSigIdEmpleado() As Integer
        Try
            Return empleados.Count + 1
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return -1
        End Try
    End Function

    Public Shared Function getSigIdUsuario() As Integer
        Try
            Return usuarios.Count + 1
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return -1
        End Try
    End Function
    Public Shared Sub agregarPerfil(ByRef perfil As Perfiles)
        Try
            perfiles.Add(perfil)
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Public Shared Sub agregarEmpleados(ByRef empleado As Empleados)
        Try
            empleados.Add(empleado)
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try

    End Sub
    Public Shared Function controlarUsuariosRepetidos(ByVal idEmp As Integer, ByVal idPerfil As Integer, ByVal nombreUsu As String) As String
        Dim mensaje As String
        Try
            mensaje = ""
            For Each usu In usuarios
                If usu.getIdPerfil = idPerfil And usu.getIdEmpleado = idEmp Then
                    mensaje = mensaje + "Ya existe dicho empleado con el perfil establecido" + vbNewLine
                    Return Trim(mensaje)
                ElseIf usu.getNombreUsu() = nombreUsu Then
                    mensaje = mensaje + "Ya existe el nombre de usuario que se desea utilizar" + vbNewLine
                    Return Trim(mensaje)
                End If
            Next
            Return mensaje
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    Public Shared Sub agregarUsuarios(ByRef usuario As Usuarios)
        Try
            usuarios.Add(usuario)
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub
    Public Shared Function obtenerPerfiles() As List(Of Perfiles)
        Try
            Return perfiles
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function obtenerUsuarios() As List(Of Usuarios)
        Try
            Return usuarios
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return Nothing
        End Try
    End Function

    Public Shared Function obtenerEmpleados() As List(Of Empleados)
        Try
            Return empleados
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function buscarEmpIdUsu(ByVal idEmp As Integer) As Empleados
        Dim empleadoEncontrado As Empleados
        Try
            empleadoEncontrado = Nothing
            For Each emp In empleados
                If emp.getIdEmpleado() = idEmp Then
                    Return emp
                End If
            Next

            Return empleadoEncontrado
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function controlarAcceso(ByVal nombreUsu As String, ByVal contrasenia As String) As Boolean
        Dim datosUsuario As New Dictionary(Of Integer, String)
        Dim empleado As Empleados
        Dim exitoLogueo As Boolean
        Try
            exitoLogueo = False
            For Each usu In usuarios
                If usu.getNombreUsu() = nombreUsu And usu.getContrasenia() = contrasenia And usu.getEstado() Then
                    empleado = buscarEmpIdUsu(usu.getIdEmpleado())
                    Sesion.establecerValores(usu.getIdUsuario(), empleado.getNombre(), empleado.getApellido(), empleado.getDni(), usu.getIdPerfil(), empleado.getIdEmpleado(), 0, "")
                    Return True
                End If
            Next
            Return exitoLogueo
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function

    Public Shared Function getIdPerfilPorNombre(ByVal nombre As String) As Integer
        Try
            For Each perfil In perfiles
                If perfil.getDescripcion() = nombre Then
                    Return perfil.getId()
                End If
            Next
            Return -1
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return -1
        End Try
    End Function

    Public Shared Function getUsuarioPorId(ByVal id As Integer) As Usuarios
        Try
            For Each usu In usuarios
                If usu.getIdUsuario() = id Then
                    Return usu
                End If
            Next
            Return Nothing
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return Nothing
        End Try
    End Function
    Public Shared Function controlaCambioContrasenia(ByVal nombreUsu As String, ByVal contrasenia As String)
        Try
            For Each usu In usuarios
                If usu.getNombreUsu() = nombreUsu And usu.getContrasenia() = contrasenia Then
                    If usu.getIdUsuario() = Sesion.getIdUsuario() Then
                        Return True
                    End If
                End If
            Next
            Return False
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function
    Public Shared Function getNombrePerfilPorId(ByVal id As Byte) As String
        Try
            For Each perfil In perfiles
                If perfil.getId() = id Then
                    Return perfil.getDescripcion()
                End If
            Next
            Return ""
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    Public Shared Function modificarDatosEmpleado(ByVal id As Integer, ByVal ape As String, ByVal nombre As String, ByVal dni As Integer, ByVal tel As String, ByVal mail As String) As Boolean
        Try
            For Each empleado In empleados
                If empleado.getIdEmpleado() = id Then
                    empleado.modificarApellido(ape)
                    empleado.modificarNombre(nombre)
                    empleado.modificarDni(dni)
                    empleado.modificarMail(mail)
                    empleado.modificarTel(tel)
                    empleado.modificarFecha(Now())
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return False
        End Try
    End Function
    Public Shared Sub listaEmpleados(ByRef grilla As DataGridView, ByVal modificar As Boolean)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codEmpleado As New DataGridViewTextBoxColumn()
        Dim nombre As New DataGridViewTextBoxColumn()
        Dim apellido As New DataGridViewTextBoxColumn()
        Dim dni As New DataGridViewTextBoxColumn()
        Dim telefono As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim mail As New DataGridViewTextBoxColumn()
        Dim cantidadFilas As Integer
        Dim estado As String

        'grilla.ColumnCount = 8
        Try
            grilla.Columns.Add(codEmpleado)
            grilla.Columns.Add(nombre)
            grilla.Columns.Add(apellido)
            grilla.Columns.Add(dni)
            grilla.Columns.Add(telefono)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(mail)
            If Not modificar Then
                grilla.Columns.Add(boton)
            End If
            boton.HeaderText = "Alta/Baja"
            colEstado.Name = "cambioEstado"
            codEmpleado.Name = "codEmpleado"
            codEmpleado.HeaderText = "Número Empleado"
            nombre.HeaderText = "Nombre"
            apellido.HeaderText = "Apellido"
            dni.HeaderText = "Dni"
            telefono.HeaderText = "Telefono"
            colEstado.HeaderText = "Estado"
            mail.HeaderText = "Mail"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
            cantidadFilas = 0



            For Each empleado In empleados

                If empleado.getEstado() Then
                    estado = "Activo"

                Else

                    estado = "Inactivo"
                End If
                grilla.Rows.Add(empleado.getIdEmpleado(), empleado.getNombre(), empleado.getApellido(), empleado.getDni(), empleado.getTelefono(), estado, empleado.getMail())
                cantidadFilas = cantidadFilas + 1

            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub
    Public Shared Sub bajaEmpleado(ByRef grilla As DataGridView, ByVal modificar As Boolean)
        Dim valorPrueba As String
        Dim idEmpleado As Integer
        Dim respuesta As Integer
        Dim estado As String
        Dim nombre As String
        Dim apellido As String
        Dim dni As String
        Try
            valorPrueba = grilla.CurrentRow.Cells(0).Value
            If Not IsNothing(valorPrueba) And grilla.CurrentCell.GetType.ToString Like "*Button*" Then
                idEmpleado = grilla.CurrentRow.Cells("codEmpleado").Value
                For Each empleado In empleados
                    If empleado.getIdEmpleado() = idEmpleado Then
                        nombre = empleado.getNombre()
                        apellido = empleado.getApellido()
                        dni = CStr(empleado.getDni())
                        If empleado.getEstado() Then
                            estado = "Activo"
                        Else
                            estado = "Inactivo"
                        End If
                        respuesta = MsgBox("¿Desea modificar el estado del empleado?: " + vbNewLine + nombre + " " + empleado.getApellido() + vbNewLine + "Dni: " + dni + vbNewLine + "Estado: " + estado, vbYesNo + vbDefaultButton2, "Consulta De Modificación")
                        If respuesta = vbYes Then
                            empleado.modificarEstado(Not empleado.getEstado())
                            limpiarGrilla(grilla)
                            listaEmpleados(grilla, modificar)
                            bajaUsuarioPorBajaEmpleado(empleado.getIdEmpleado())
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Public Shared Function modificarContraseniaTipoUsu(ByRef codUsu As Integer, ByRef tipo As String, ByRef contra As String) As String
        Dim mensaje As String

        mensaje = ""
        If Not IsNumeric(codUsu) Or String.IsNullOrEmpty(tipo) Or String.IsNullOrWhiteSpace(tipo) Or String.IsNullOrWhiteSpace(contra) Then
            mensaje = mensaje + "Por favor, no deje espacios en blanco y seleccione un usuario para modificar la contraseña o el tipo" + vbNewLine
        End If

        If mensaje = "" Then
            For Each usu In usuarios
                If usu.getIdUsuario() = codUsu Then
                    usu.modificar_contrasenia(contra)
                    If usu.getIdPerfil() <> getIdPerfilPorNombre(tipo) Then
                        usu.modificarPerfil(getIdPerfilPorNombre(tipo))
                    End If
                End If
            Next
        End If
        Return mensaje
    End Function
    Private Shared Sub bajaUsuarioPorBajaEmpleado(ByVal idEmpleado As Integer)
        Try
            For Each usu In usuarios
                If usu.getIdEmpleado() = idEmpleado Then
                    usu.modificarEstado(False)
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub
    Public Shared Sub bajaUsuario(ByRef grilla As DataGridView)
        Dim valorPrueba As String
        Dim idUsu As Integer
        Dim respuesta As Integer
        Dim estado As String
        Dim nombre As String
        Dim empleado As String
        Dim dni As String
        Try
            valorPrueba = grilla.CurrentRow.Cells(0).Value
            If Not IsNothing(valorPrueba) And grilla.CurrentCell.GetType.ToString Like "*Button*" Then
                idUsu = grilla.CurrentRow.Cells("codUsu").Value
                For Each usu In usuarios
                    If usu.getIdUsuario() = idUsu Then
                        nombre = usu.getNombreUsu()
                        empleado = grilla.CurrentRow.Cells("nombreEmp").Value
                        dni = grilla.CurrentRow.Cells("dniEmp").Value
                        If usu.getEstado() Then
                            estado = "Activo"
                        Else
                            estado = "Inactivo"
                        End If
                        respuesta = MsgBox("¿Desea modificar el estado del usuario: " + vbNewLine + nombre + vbNewLine + "Empleado: " + empleado + vbNewLine + vbNewLine + "Dni: " + dni + vbNewLine + "Estado: " + estado, vbYesNo + vbDefaultButton2, "Consulta De Baja")
                        If respuesta = vbYes Then
                            usu.modificarEstado(Not usu.getEstado())
                            limpiarGrilla(grilla)
                            listaUsuarios(grilla)
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub
    Public Shared Sub listaUsuarios(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codUsu As New DataGridViewTextBoxColumn()
        Dim nombreUsu As New DataGridViewTextBoxColumn()
        Dim nombreEmp As New DataGridViewTextBoxColumn()
        Dim dni As New DataGridViewTextBoxColumn()
        Dim perfil As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim perfilSesion As Byte

        Dim cantidadFilas As Integer
        Dim estado As String
        Try
            perfilSesion = Sesion.getPerfilUsuario()
            grilla.Rows.Clear()
            grilla.Columns.Add(codUsu)
            grilla.Columns.Add(nombreUsu)
            grilla.Columns.Add(nombreEmp)
            grilla.Columns.Add(dni)
            grilla.Columns.Add(perfil)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            codUsu.HeaderText = "Número Usuario"
            nombreUsu.HeaderText = "Nombre Usuario"
            nombreEmp.HeaderText = "Empleado"
            dni.HeaderText = "Dni"
            perfil.HeaderText = "Perfil"
            colEstado.HeaderText = "Estado"
            nombreEmp.Name = "nombreEmp"
            dni.Name = "dniEmp"
            colEstado.Name = "cambioEstado"
            codUsu.Name = "codUsu"

            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
            cantidadFilas = 0
            For Each usu In usuarios
                If usu.getEstado() Then
                    estado = "Activo"
                Else
                    estado = "Inactivo"
                End If

                For Each empleado In empleados
                    If empleado.getIdEmpleado() = usu.getIdEmpleado() Then
                        If usu.getIdPerfil() = 1 And perfilSesion <> 1 Then
                            Continue For
                        ElseIf (usu.getIdPerfil() = 1 Or usu.getIdPerfil() = 2) And perfilSesion = 3 Then
                            Continue For
                        Else
                            grilla.Rows.Add(usu.getIdUsuario(), usu.getNombreUsu(), empleado.getApellido() + " " + empleado.getNombre(), empleado.getDni(), getNombrePerfilPorId(usu.getIdPerfil()), estado)
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Public Shared Function controlarDniRepetido(ByVal dni As String) As String
        Dim mensaje As String
        Try
            mensaje = ""
            For Each emp In empleados
                If emp.getDni() = dni Then
                    mensaje = mensaje + "Ya existe un empleado con el mismo dni que se desea ingresar" + vbNewLine
                    Return Trim(mensaje)
                End If
            Next
            Return Trim(mensaje)
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
            Return ""
        End Try
    End Function
    Public Shared Sub limpiarGrilla(ByRef grilla As DataGridView)
        Try
            grilla.Columns.Clear()
            grilla.DataSource = Nothing
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub
End Class
