﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAcercaDe
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTituloNombreProyecto = New System.Windows.Forms.Label()
        Me.lblNombreProyecto = New System.Windows.Forms.Label()
        Me.lblTituloIntegrantes = New System.Windows.Forms.Label()
        Me.lblDatosKike = New System.Windows.Forms.Label()
        Me.lblDatosMati = New System.Windows.Forms.Label()
        Me.lblDatosCicloLectivo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblTituloNombreProyecto
        '
        Me.lblTituloNombreProyecto.AutoSize = True
        Me.lblTituloNombreProyecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloNombreProyecto.Location = New System.Drawing.Point(82, 9)
        Me.lblTituloNombreProyecto.Name = "lblTituloNombreProyecto"
        Me.lblTituloNombreProyecto.Size = New System.Drawing.Size(191, 24)
        Me.lblTituloNombreProyecto.TabIndex = 0
        Me.lblTituloNombreProyecto.Text = "Nombre Del Proyecto"
        '
        'lblNombreProyecto
        '
        Me.lblNombreProyecto.AutoSize = True
        Me.lblNombreProyecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreProyecto.Location = New System.Drawing.Point(60, 49)
        Me.lblNombreProyecto.Name = "lblNombreProyecto"
        Me.lblNombreProyecto.Size = New System.Drawing.Size(227, 24)
        Me.lblNombreProyecto.TabIndex = 1
        Me.lblNombreProyecto.Text = "El Calabozo Del Androide"
        '
        'lblTituloIntegrantes
        '
        Me.lblTituloIntegrantes.AutoSize = True
        Me.lblTituloIntegrantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloIntegrantes.Location = New System.Drawing.Point(12, 100)
        Me.lblTituloIntegrantes.Name = "lblTituloIntegrantes"
        Me.lblTituloIntegrantes.Size = New System.Drawing.Size(175, 24)
        Me.lblTituloIntegrantes.TabIndex = 2
        Me.lblTituloIntegrantes.Text = "Integrantes Grupo 7"
        '
        'lblDatosKike
        '
        Me.lblDatosKike.AutoSize = True
        Me.lblDatosKike.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatosKike.Location = New System.Drawing.Point(12, 137)
        Me.lblDatosKike.Name = "lblDatosKike"
        Me.lblDatosKike.Size = New System.Drawing.Size(246, 24)
        Me.lblDatosKike.TabIndex = 3
        Me.lblDatosKike.Text = "Espinoza Enrique-32837262"
        '
        'lblDatosMati
        '
        Me.lblDatosMati.AutoSize = True
        Me.lblDatosMati.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatosMati.Location = New System.Drawing.Point(12, 172)
        Me.lblDatosMati.Name = "lblDatosMati"
        Me.lblDatosMati.Size = New System.Drawing.Size(159, 24)
        Me.lblDatosMati.TabIndex = 4
        Me.lblDatosMati.Text = "Grondona Matías-"
        '
        'lblDatosCicloLectivo
        '
        Me.lblDatosCicloLectivo.AutoSize = True
        Me.lblDatosCicloLectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatosCicloLectivo.Location = New System.Drawing.Point(12, 205)
        Me.lblDatosCicloLectivo.Name = "lblDatosCicloLectivo"
        Me.lblDatosCicloLectivo.Size = New System.Drawing.Size(304, 48)
        Me.lblDatosCicloLectivo.TabIndex = 5
        Me.lblDatosCicloLectivo.Text = "Cátedra: Taller De Programación 2." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "UNNE-Año 2020" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'frmAcercaDe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 262)
        Me.Controls.Add(Me.lblDatosCicloLectivo)
        Me.Controls.Add(Me.lblDatosMati)
        Me.Controls.Add(Me.lblDatosKike)
        Me.Controls.Add(Me.lblTituloIntegrantes)
        Me.Controls.Add(Me.lblNombreProyecto)
        Me.Controls.Add(Me.lblTituloNombreProyecto)
        Me.Name = "frmAcercaDe"
        Me.Text = "Acerca De"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTituloNombreProyecto As System.Windows.Forms.Label
    Friend WithEvents lblNombreProyecto As System.Windows.Forms.Label
    Friend WithEvents lblTituloIntegrantes As System.Windows.Forms.Label
    Friend WithEvents lblDatosKike As System.Windows.Forms.Label
    Friend WithEvents lblDatosMati As System.Windows.Forms.Label
    Friend WithEvents lblDatosCicloLectivo As System.Windows.Forms.Label
End Class
