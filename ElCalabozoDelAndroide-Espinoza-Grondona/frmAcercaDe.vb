﻿Public Class frmAcercaDe

    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Erorr al abrir la ventana 'Acerca De': " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub frmAcercaDe_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Erorr al cerrar la ventana 'Acerca De': " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub frmAcercaDe_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.cargarFondoPantalla("carmesinegro.jpg")
    End Sub
End Class