﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDevolucionesEmp
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grbDevolucionesEmp = New System.Windows.Forms.GroupBox()
        Me.grbListaDevEmp = New System.Windows.Forms.GroupBox()
        Me.txbMontoFinalDev = New System.Windows.Forms.TextBox()
        Me.btnRealizarDevEmp = New System.Windows.Forms.Button()
        Me.dgvCarritoDev = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbDatosDev = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txbDescripcion = New System.Windows.Forms.TextBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.txbNroDetalle = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txbNroVenta = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txbMontoVenta = New System.Windows.Forms.TextBox()
        Me.txbNomProdDev = New System.Windows.Forms.TextBox()
        Me.cmbMotivoDevEmp = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnDevolverDev = New System.Windows.Forms.Button()
        Me.lblMontoDev = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nudCantidadDev = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grbFiltrarDev = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnFiltrarFechaDev = New System.Windows.Forms.Button()
        Me.lblFechaFinDev = New System.Windows.Forms.Label()
        Me.lblFechaIniDev = New System.Windows.Forms.Label()
        Me.dtpFechaFinDevFil = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaIniDevFil = New System.Windows.Forms.DateTimePicker()
        Me.grbDetallesVentaAdm = New System.Windows.Forms.GroupBox()
        Me.dgvDetallesVentas = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbListaVentasAdm = New System.Windows.Forms.GroupBox()
        Me.dgvVentasDev = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grbDevolucionesEmp.SuspendLayout()
        Me.grbListaDevEmp.SuspendLayout()
        CType(Me.dgvCarritoDev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosDev.SuspendLayout()
        CType(Me.nudCantidadDev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbFiltrarDev.SuspendLayout()
        Me.grbDetallesVentaAdm.SuspendLayout()
        CType(Me.dgvDetallesVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbListaVentasAdm.SuspendLayout()
        CType(Me.dgvVentasDev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grbDevolucionesEmp
        '
        Me.grbDevolucionesEmp.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grbDevolucionesEmp.AutoSize = True
        Me.grbDevolucionesEmp.BackColor = System.Drawing.Color.DarkGray
        Me.grbDevolucionesEmp.Controls.Add(Me.grbListaDevEmp)
        Me.grbDevolucionesEmp.Controls.Add(Me.grbDatosDev)
        Me.grbDevolucionesEmp.Controls.Add(Me.grbFiltrarDev)
        Me.grbDevolucionesEmp.Controls.Add(Me.grbDetallesVentaAdm)
        Me.grbDevolucionesEmp.Controls.Add(Me.grbListaVentasAdm)
        Me.grbDevolucionesEmp.Location = New System.Drawing.Point(0, 0)
        Me.grbDevolucionesEmp.Name = "grbDevolucionesEmp"
        Me.grbDevolucionesEmp.Size = New System.Drawing.Size(1086, 678)
        Me.grbDevolucionesEmp.TabIndex = 0
        Me.grbDevolucionesEmp.TabStop = False
        Me.grbDevolucionesEmp.Text = "Devolución De Ventas"
        '
        'grbListaDevEmp
        '
        Me.grbListaDevEmp.BackColor = System.Drawing.Color.DodgerBlue
        Me.grbListaDevEmp.Controls.Add(Me.txbMontoFinalDev)
        Me.grbListaDevEmp.Controls.Add(Me.Label1)
        Me.grbListaDevEmp.Controls.Add(Me.btnRealizarDevEmp)
        Me.grbListaDevEmp.Controls.Add(Me.dgvCarritoDev)
        Me.grbListaDevEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbListaDevEmp.Location = New System.Drawing.Point(6, 437)
        Me.grbListaDevEmp.Name = "grbListaDevEmp"
        Me.grbListaDevEmp.Size = New System.Drawing.Size(647, 209)
        Me.grbListaDevEmp.TabIndex = 8
        Me.grbListaDevEmp.TabStop = False
        Me.grbListaDevEmp.Text = "Lista Devoluciones Actuales"
        '
        'txbMontoFinalDev
        '
        Me.txbMontoFinalDev.Location = New System.Drawing.Point(531, 57)
        Me.txbMontoFinalDev.Name = "txbMontoFinalDev"
        Me.txbMontoFinalDev.ReadOnly = True
        Me.txbMontoFinalDev.Size = New System.Drawing.Size(110, 24)
        Me.txbMontoFinalDev.TabIndex = 6
        Me.txbMontoFinalDev.Text = "0"
        '
        'btnRealizarDevEmp
        '
        Me.btnRealizarDevEmp.Location = New System.Drawing.Point(531, 130)
        Me.btnRealizarDevEmp.Name = "btnRealizarDevEmp"
        Me.btnRealizarDevEmp.Size = New System.Drawing.Size(110, 72)
        Me.btnRealizarDevEmp.TabIndex = 4
        Me.btnRealizarDevEmp.Text = "Realizar Devoluciones"
        Me.btnRealizarDevEmp.UseVisualStyleBackColor = True
        '
        'dgvCarritoDev
        '
        Me.dgvCarritoDev.AllowUserToAddRows = False
        Me.dgvCarritoDev.AllowUserToDeleteRows = False
        Me.dgvCarritoDev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCarritoDev.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5})
        Me.dgvCarritoDev.Location = New System.Drawing.Point(21, 23)
        Me.dgvCarritoDev.Name = "dgvCarritoDev"
        Me.dgvCarritoDev.ReadOnly = True
        Me.dgvCarritoDev.Size = New System.Drawing.Size(504, 179)
        Me.dgvCarritoDev.TabIndex = 1
        '
        'Column1
        '
        Me.Column1.HeaderText = "Producto"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "Unidades Devueltas"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Monto devuelto"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Código Compra"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Código Detalle"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'grbDatosDev
        '
        Me.grbDatosDev.BackColor = System.Drawing.Color.SkyBlue
        Me.grbDatosDev.Controls.Add(Me.Label9)
        Me.grbDatosDev.Controls.Add(Me.txbDescripcion)
        Me.grbDatosDev.Controls.Add(Me.btnCancelar)
        Me.grbDatosDev.Controls.Add(Me.btnModificar)
        Me.grbDatosDev.Controls.Add(Me.txbNroDetalle)
        Me.grbDatosDev.Controls.Add(Me.Label6)
        Me.grbDatosDev.Controls.Add(Me.txbNroVenta)
        Me.grbDatosDev.Controls.Add(Me.Label5)
        Me.grbDatosDev.Controls.Add(Me.txbMontoVenta)
        Me.grbDatosDev.Controls.Add(Me.txbNomProdDev)
        Me.grbDatosDev.Controls.Add(Me.cmbMotivoDevEmp)
        Me.grbDatosDev.Controls.Add(Me.Label8)
        Me.grbDatosDev.Controls.Add(Me.Label7)
        Me.grbDatosDev.Controls.Add(Me.btnDevolverDev)
        Me.grbDatosDev.Controls.Add(Me.lblMontoDev)
        Me.grbDatosDev.Controls.Add(Me.Label4)
        Me.grbDatosDev.Controls.Add(Me.nudCantidadDev)
        Me.grbDatosDev.Controls.Add(Me.Label3)
        Me.grbDatosDev.Controls.Add(Me.Button1)
        Me.grbDatosDev.Controls.Add(Me.Label2)
        Me.grbDatosDev.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosDev.Location = New System.Drawing.Point(659, 206)
        Me.grbDatosDev.Name = "grbDatosDev"
        Me.grbDatosDev.Size = New System.Drawing.Size(421, 453)
        Me.grbDatosDev.TabIndex = 7
        Me.grbDatosDev.TabStop = False
        Me.grbDatosDev.Text = "Datos Devolución"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(120, 299)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(142, 20)
        Me.Label9.TabIndex = 32
        Me.Label9.Text = "Descripción Motivo"
        '
        'txbDescripcion
        '
        Me.txbDescripcion.Location = New System.Drawing.Point(6, 322)
        Me.txbDescripcion.Multiline = True
        Me.txbDescripcion.Name = "txbDescripcion"
        Me.txbDescripcion.Size = New System.Drawing.Size(407, 57)
        Me.txbDescripcion.TabIndex = 31
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(268, 385)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(115, 55)
        Me.btnCancelar.TabIndex = 30
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(145, 385)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(115, 55)
        Me.btnModificar.TabIndex = 29
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'txbNroDetalle
        '
        Me.txbNroDetalle.Location = New System.Drawing.Point(325, 29)
        Me.txbNroDetalle.Name = "txbNroDetalle"
        Me.txbNroDetalle.ReadOnly = True
        Me.txbNroDetalle.Size = New System.Drawing.Size(70, 26)
        Me.txbNroDetalle.TabIndex = 28
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(200, 35)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 20)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Número Detalle"
        '
        'txbNroVenta
        '
        Me.txbNroVenta.Location = New System.Drawing.Point(124, 29)
        Me.txbNroVenta.Name = "txbNroVenta"
        Me.txbNroVenta.ReadOnly = True
        Me.txbNroVenta.Size = New System.Drawing.Size(70, 26)
        Me.txbNroVenta.TabIndex = 26
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 20)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Número Venta"
        '
        'txbMontoVenta
        '
        Me.txbMontoVenta.Location = New System.Drawing.Point(156, 146)
        Me.txbMontoVenta.Name = "txbMontoVenta"
        Me.txbMontoVenta.ReadOnly = True
        Me.txbMontoVenta.Size = New System.Drawing.Size(157, 26)
        Me.txbMontoVenta.TabIndex = 24
        '
        'txbNomProdDev
        '
        Me.txbNomProdDev.Location = New System.Drawing.Point(10, 90)
        Me.txbNomProdDev.Multiline = True
        Me.txbNomProdDev.Name = "txbNomProdDev"
        Me.txbNomProdDev.ReadOnly = True
        Me.txbNomProdDev.Size = New System.Drawing.Size(373, 48)
        Me.txbNomProdDev.TabIndex = 23
        '
        'cmbMotivoDevEmp
        '
        Me.cmbMotivoDevEmp.FormattingEnabled = True
        Me.cmbMotivoDevEmp.Location = New System.Drawing.Point(157, 214)
        Me.cmbMotivoDevEmp.Name = "cmbMotivoDevEmp"
        Me.cmbMotivoDevEmp.Size = New System.Drawing.Size(164, 28)
        Me.cmbMotivoDevEmp.TabIndex = 21
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 218)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(137, 20)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Motivo Devolución"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 146)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(101, 20)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Monto Venta"
        '
        'btnDevolverDev
        '
        Me.btnDevolverDev.Location = New System.Drawing.Point(14, 385)
        Me.btnDevolverDev.Name = "btnDevolverDev"
        Me.btnDevolverDev.Size = New System.Drawing.Size(115, 55)
        Me.btnDevolverDev.TabIndex = 17
        Me.btnDevolverDev.Text = "Agregar Devolución"
        Me.btnDevolverDev.UseVisualStyleBackColor = True
        '
        'lblMontoDev
        '
        Me.lblMontoDev.AutoSize = True
        Me.lblMontoDev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMontoDev.Location = New System.Drawing.Point(156, 254)
        Me.lblMontoDev.Name = "lblMontoDev"
        Me.lblMontoDev.Size = New System.Drawing.Size(20, 22)
        Me.lblMontoDev.TabIndex = 16
        Me.lblMontoDev.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 254)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(134, 20)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Monto A Devolver"
        '
        'nudCantidadDev
        '
        Me.nudCantidadDev.Location = New System.Drawing.Point(157, 182)
        Me.nudCantidadDev.Name = "nudCantidadDev"
        Me.nudCantidadDev.Size = New System.Drawing.Size(161, 26)
        Me.nudCantidadDev.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 184)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(140, 20)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Cantidad Devuelta"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(650, 29)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 45)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Sólo Categoría/Subcategoria"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(120, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 20)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nombre Producto"
        '
        'grbFiltrarDev
        '
        Me.grbFiltrarDev.BackColor = System.Drawing.Color.CadetBlue
        Me.grbFiltrarDev.Controls.Add(Me.Button3)
        Me.grbFiltrarDev.Controls.Add(Me.btnFiltrarFechaDev)
        Me.grbFiltrarDev.Controls.Add(Me.lblFechaFinDev)
        Me.grbFiltrarDev.Controls.Add(Me.lblFechaIniDev)
        Me.grbFiltrarDev.Controls.Add(Me.dtpFechaFinDevFil)
        Me.grbFiltrarDev.Controls.Add(Me.dtpFechaIniDevFil)
        Me.grbFiltrarDev.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltrarDev.Location = New System.Drawing.Point(659, 19)
        Me.grbFiltrarDev.Name = "grbFiltrarDev"
        Me.grbFiltrarDev.Size = New System.Drawing.Size(395, 181)
        Me.grbFiltrarDev.TabIndex = 6
        Me.grbFiltrarDev.TabStop = False
        Me.grbFiltrarDev.Text = "Opciones De Filtrado"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(650, 29)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(133, 45)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Sólo Categoría/Subcategoria"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnFiltrarFechaDev
        '
        Me.btnFiltrarFechaDev.Location = New System.Drawing.Point(157, 138)
        Me.btnFiltrarFechaDev.Name = "btnFiltrarFechaDev"
        Me.btnFiltrarFechaDev.Size = New System.Drawing.Size(75, 35)
        Me.btnFiltrarFechaDev.TabIndex = 6
        Me.btnFiltrarFechaDev.Text = "Filtrar"
        Me.btnFiltrarFechaDev.UseVisualStyleBackColor = True
        '
        'lblFechaFinDev
        '
        Me.lblFechaFinDev.AutoSize = True
        Me.lblFechaFinDev.Location = New System.Drawing.Point(152, 81)
        Me.lblFechaFinDev.Name = "lblFechaFinDev"
        Me.lblFechaFinDev.Size = New System.Drawing.Size(80, 20)
        Me.lblFechaFinDev.TabIndex = 4
        Me.lblFechaFinDev.Text = "Fecha Fin"
        '
        'lblFechaIniDev
        '
        Me.lblFechaIniDev.AutoSize = True
        Me.lblFechaIniDev.Location = New System.Drawing.Point(143, 22)
        Me.lblFechaIniDev.Name = "lblFechaIniDev"
        Me.lblFechaIniDev.Size = New System.Drawing.Size(95, 20)
        Me.lblFechaIniDev.TabIndex = 3
        Me.lblFechaIniDev.Text = "Fecha Inicio"
        '
        'dtpFechaFinDevFil
        '
        Me.dtpFechaFinDevFil.Location = New System.Drawing.Point(50, 104)
        Me.dtpFechaFinDevFil.Name = "dtpFechaFinDevFil"
        Me.dtpFechaFinDevFil.Size = New System.Drawing.Size(310, 26)
        Me.dtpFechaFinDevFil.TabIndex = 1
        '
        'dtpFechaIniDevFil
        '
        Me.dtpFechaIniDevFil.Location = New System.Drawing.Point(50, 45)
        Me.dtpFechaIniDevFil.Name = "dtpFechaIniDevFil"
        Me.dtpFechaIniDevFil.Size = New System.Drawing.Size(310, 26)
        Me.dtpFechaIniDevFil.TabIndex = 0
        '
        'grbDetallesVentaAdm
        '
        Me.grbDetallesVentaAdm.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.grbDetallesVentaAdm.Controls.Add(Me.dgvDetallesVentas)
        Me.grbDetallesVentaAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDetallesVentaAdm.Location = New System.Drawing.Point(6, 226)
        Me.grbDetallesVentaAdm.Name = "grbDetallesVentaAdm"
        Me.grbDetallesVentaAdm.Size = New System.Drawing.Size(647, 205)
        Me.grbDetallesVentaAdm.TabIndex = 5
        Me.grbDetallesVentaAdm.TabStop = False
        Me.grbDetallesVentaAdm.Text = "Detalles Venta Seleccionada"
        '
        'dgvDetallesVentas
        '
        Me.dgvDetallesVentas.AllowUserToAddRows = False
        Me.dgvDetallesVentas.AllowUserToDeleteRows = False
        Me.dgvDetallesVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetallesVentas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.dgvDetallesVentas.Location = New System.Drawing.Point(21, 31)
        Me.dgvDetallesVentas.Name = "dgvDetallesVentas"
        Me.dgvDetallesVentas.ReadOnly = True
        Me.dgvDetallesVentas.Size = New System.Drawing.Size(542, 167)
        Me.dgvDetallesVentas.TabIndex = 1
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Código Detalle"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Producto"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Descuento"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Precio Neto"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'grbListaVentasAdm
        '
        Me.grbListaVentasAdm.BackColor = System.Drawing.Color.Aquamarine
        Me.grbListaVentasAdm.Controls.Add(Me.dgvVentasDev)
        Me.grbListaVentasAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbListaVentasAdm.Location = New System.Drawing.Point(6, 19)
        Me.grbListaVentasAdm.Name = "grbListaVentasAdm"
        Me.grbListaVentasAdm.Size = New System.Drawing.Size(647, 201)
        Me.grbListaVentasAdm.TabIndex = 4
        Me.grbListaVentasAdm.TabStop = False
        Me.grbListaVentasAdm.Text = "Lista De Ventas"
        '
        'dgvVentasDev
        '
        Me.dgvVentasDev.AllowUserToAddRows = False
        Me.dgvVentasDev.AllowUserToDeleteRows = False
        Me.dgvVentasDev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVentasDev.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.dgvVentasDev.Location = New System.Drawing.Point(21, 31)
        Me.dgvVentasDev.Name = "dgvVentasDev"
        Me.dgvVentasDev.ReadOnly = True
        Me.dgvVentasDev.Size = New System.Drawing.Size(542, 164)
        Me.dgvVentasDev.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Código Venta"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha venta"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Vendedor"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Monto Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(531, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 18)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Monto Devuelto"
        '
        'frmDevolucionesEmp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1084, 671)
        Me.Controls.Add(Me.grbDevolucionesEmp)
        Me.Name = "frmDevolucionesEmp"
        Me.Text = "Devoluciones"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.grbDevolucionesEmp.ResumeLayout(False)
        Me.grbListaDevEmp.ResumeLayout(False)
        Me.grbListaDevEmp.PerformLayout()
        CType(Me.dgvCarritoDev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosDev.ResumeLayout(False)
        Me.grbDatosDev.PerformLayout()
        CType(Me.nudCantidadDev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbFiltrarDev.ResumeLayout(False)
        Me.grbFiltrarDev.PerformLayout()
        Me.grbDetallesVentaAdm.ResumeLayout(False)
        CType(Me.dgvDetallesVentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbListaVentasAdm.ResumeLayout(False)
        CType(Me.dgvVentasDev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grbDevolucionesEmp As System.Windows.Forms.GroupBox
    Friend WithEvents grbDetallesVentaAdm As System.Windows.Forms.GroupBox
    Friend WithEvents dgvDetallesVentas As System.Windows.Forms.DataGridView
    Friend WithEvents grbListaVentasAdm As System.Windows.Forms.GroupBox
    Friend WithEvents dgvVentasDev As System.Windows.Forms.DataGridView
    Friend WithEvents grbDatosDev As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnDevolverDev As System.Windows.Forms.Button
    Friend WithEvents lblMontoDev As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nudCantidadDev As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents grbFiltrarDev As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarFechaDev As System.Windows.Forms.Button
    Friend WithEvents lblFechaFinDev As System.Windows.Forms.Label
    Friend WithEvents lblFechaIniDev As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFinDevFil As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaIniDevFil As System.Windows.Forms.DateTimePicker
    Friend WithEvents grbListaDevEmp As System.Windows.Forms.GroupBox
    Friend WithEvents btnRealizarDevEmp As System.Windows.Forms.Button
    Friend WithEvents dgvCarritoDev As System.Windows.Forms.DataGridView
    Friend WithEvents cmbMotivoDevEmp As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txbMontoVenta As System.Windows.Forms.TextBox
    Friend WithEvents txbNomProdDev As System.Windows.Forms.TextBox
    Friend WithEvents txbMontoFinalDev As System.Windows.Forms.TextBox
    Friend WithEvents txbNroDetalle As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txbNroVenta As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txbDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
