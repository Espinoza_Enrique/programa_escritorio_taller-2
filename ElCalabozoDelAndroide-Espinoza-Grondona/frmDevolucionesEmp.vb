﻿Imports Entities
Imports BusinessLayer
Public Class frmDevolucionesEmp

    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'control-Tipo: TabControl-Representa el tab control cuyas tab pages se quiere seleccionar
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de devoluciones:" + vbNewLine + ex.Message, vbOKOnly, "Error De Carga")
        End Try
    End Sub
    Private Sub frmDevolucionesEmp_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al cerrar la ventana de devoluciones:" + vbNewLine + ex.Message, vbOKOnly, "Error De Cierre De Ventana")
        End Try
    End Sub

    Private Sub frmDevolucionesEmp_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.prepararFormulario()
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub prepararFormulario()
        Try

            'Me.btnAnteriorDetallesDevEmp.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            'Me.btnAnteriorVentasDevEmp.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.cmbMotivoDevEmp.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cargarMotivosDev(Me.cmbMotivoDevEmp)
            Me.prepararGrillaVentas(Me.dgvVentasDev)
            Me.prepararGrillaDetalles(Me.dgvDetallesVentas)
            Me.prepararGrillaDevoluciones(Me.dgvCarritoDev)
            Me.listarVentas(Me.dgvVentasDev)

        Catch ex As Exception
            MsgBox("Error al dar formato a la ventana de devoluciones:" + vbNewLine + ex.Message, vbOKOnly, "Error De Formato De Tabla")
        End Try
    End Sub


    Private Sub eliminarElementoCarrito(ByRef carrito As DataGridView)
        Try
            Dim respuesta As Integer
            respuesta = MsgBox("¿Desea eliminar el producto seleccionado? ", vbYesNo + vbDefaultButton2, "Ventana Consulta")
            If respuesta = vbYes Then
                carrito.Rows.Remove(carrito.CurrentRow)
            End If
        Catch ex As Exception
            MsgBox("Error al eliminar un elemento del carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub activarGrillas(ByRef grilla As GroupBox, ByVal estado As Boolean)
        Try
            grilla.Enabled = estado
        Catch ex As Exception
            MsgBox("Error al activar la lista de ventas, detalles o devoluciones. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana error")
        End Try
    End Sub
    Public Function controlarDatosDev(ByVal modificacion As Boolean) As String
        Dim mensaje As String
        Dim unidadesDev As Integer
        Dim idVenta As Integer
        Dim idDetalle As Integer
        Dim descripcion As String
        Try
            mensaje = ""
            unidadesDev = Me.nudCantidadDev.Value
            idVenta = CInt(Me.txbNroVenta.Text)
            idDetalle = CInt(Me.txbNroDetalle.Text)
            descripcion = Me.txbDescripcion.Text

            If General.controlarCajaEnBlanco(Me.txbNroVenta.Text) Or General.controlarCajaEnBlanco(Me.txbNroDetalle.Text) Then
                mensaje = mensaje + "Por favor, seleccione una venta y uno de los elementos vendidos." + vbNewLine
            End If

            If General.controlarCajaEnBlanco(descripcion) Then
                mensaje = mensaje + "Por favor, no deje la descripcion de la devolución en blanco." + vbNewLine
            End If

            If getIndexExistenciaProd(Me.dgvCarritoDev, Trim(Me.txbNroVenta.Text), Trim(Me.txbNroDetalle.Text)) > -1 And Not modificacion Then
                mensaje = mensaje + "Ya se existe en el carrito el elemento que desea devolver." + vbNewLine
            End If

            If Not BusDevoluciones.devPermitida(idVenta, idDetalle, unidadesDev) Then
                mensaje = mensaje + "Ya se han realizado todas las devoluciones para este producto según las unidades compradas." + vbNewLine
            End If

            If Me.nudCantidadDev.Value <= 0 Then
                mensaje = mensaje + "Por favor que la cantidad devuelta sea mayor a cero" + vbNewLine
            End If

            If Me.cmbMotivoDevEmp.Text = "-" Or IsNothing(Me.cmbMotivoDevEmp.Text) Then
                mensaje = mensaje + "Por favor seleccione un motivo para la devolución" + vbNewLine
            End If


            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar datos de devolución:" + vbNewLine + ex.Message, vbOKOnly, "Error De Control")
            Return ""
        End Try
    End Function

    Private Sub btnDevolverDev_Click(sender As Object, e As EventArgs) Handles btnDevolverDev.Click
        Dim mensaje As String
        mensaje = Me.controlarDatosDev(False)
        If mensaje = "" Then
            Me.cargarDatosDev(Me.dgvCarritoDev)
            Me.txbMontoFinalDev.Text = CDbl(Me.txbMontoFinalDev.Text) + CDbl(Me.lblMontoDev.Text)
            Me.limpiarDatosDev()
            Me.activarGrillas(Me.grbListaVentasAdm, True)
            Me.activarGrillas(Me.grbDetallesVentaAdm, False)
        Else
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly, "Datos Correctos")
        End If
    End Sub

    Private Sub cargarMotivosDev(ByRef motivos As ComboBox)
        Try
            motivos.DataSource = BusDevoluciones.getMotivosDev()
            motivos.ValueMember = "idMotivo"
            motivos.DisplayMember = "nombreMotivo"
            motivos.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al cargar motivos de devoluciones: " + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub prepararGrillaVentas(ByRef grilla As DataGridView)
        Try
            Dim codVenta As New DataGridViewTextBoxColumn()
            Dim fecha As New DataGridViewTextBoxColumn()
            Dim montoTotal As New DataGridViewTextBoxColumn()
            grilla.Columns.Clear()
            grilla.Columns.Add(codVenta)
            grilla.Columns.Add(fecha)
            grilla.Columns.Add(montoTotal)
            codVenta.Name = "codVenta"
            codVenta.HeaderText = "Número Venta"
            fecha.Name = "fechaVenta"
            fecha.HeaderText = "Fecha Venta"
            montoTotal.Name = "monto"
            montoTotal.HeaderText = "Monto Total"
        Catch ex As Exception
            MsgBox("Error al cargar las ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana De Error")
        End Try
    End Sub

    Private Sub prepararGrillaDetalles(ByRef grilla As DataGridView)
        Try
            Dim codVenta As New DataGridViewTextBoxColumn()
            Dim codDetalle As New DataGridViewTextBoxColumn()
            Dim producto As New DataGridViewTextBoxColumn()
            Dim categoria As New DataGridViewTextBoxColumn()
            Dim subcategoria As New DataGridViewTextBoxColumn()
            Dim unidades As New DataGridViewTextBoxColumn()
            Dim precioVenta As New DataGridViewTextBoxColumn()

            grilla.Columns.Clear()
            grilla.Columns.Add(codVenta)
            grilla.Columns.Add(codDetalle)
            grilla.Columns.Add(producto)
            grilla.Columns.Add(unidades)
            grilla.Columns.Add(precioVenta)
            grilla.Columns.Add(categoria)
            grilla.Columns.Add(subcategoria)
            producto.Name = "nombreProducto"
            producto.HeaderText = "Producto"
            codVenta.Name = "codVenta"
            codVenta.HeaderText = "Número Venta"
            codDetalle.Name = "codDetalle"
            codDetalle.HeaderText = "Número Detalle"
            unidades.Name = "unidadesVendidas"
            unidades.HeaderText = "Unidades Vendidas"
            precioVenta.Name = "precioVenta"
            precioVenta.HeaderText = "Precio Total"
            categoria.Name = "categoria"
            categoria.HeaderText = "Categoria"
            subcategoria.Name = "subcategoria"
            subcategoria.HeaderText = "Subcategoria"
        Catch ex As Exception
            MsgBox("Error al cargar las ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana De Error")
        End Try
    End Sub

    Private Sub prepararGrillaDevoluciones(ByRef grilla As DataGridView)
        Try
            Dim btnEliminar As New System.Windows.Forms.DataGridViewButtonColumn()
            Dim btnModificar As New System.Windows.Forms.DataGridViewButtonColumn()
            Dim codVenta As New DataGridViewTextBoxColumn()
            Dim codDetalle As New DataGridViewTextBoxColumn()
            Dim producto As New DataGridViewTextBoxColumn()
            Dim unidades As New DataGridViewTextBoxColumn()
            Dim montoDevuelto As New DataGridViewTextBoxColumn()
            Dim motivo As New DataGridViewTextBoxColumn()
            Dim descripcion As New DataGridViewTextBoxColumn()
            Dim idMotivo As New DataGridViewTextBoxColumn()
            Dim montoVentaOri As New DataGridViewTextBoxColumn()

            grilla.Columns.Clear()
            grilla.Columns.Add(codVenta)
            grilla.Columns.Add(codDetalle)
            grilla.Columns.Add(producto)
            grilla.Columns.Add(unidades)
            grilla.Columns.Add(montoDevuelto)
            grilla.Columns.Add(motivo)
            grilla.Columns.Add(descripcion)
            grilla.Columns.Add(idMotivo)
            grilla.Columns.Add(montoVentaOri)
            grilla.Columns.Add(btnModificar)
            grilla.Columns.Add(btnEliminar)
            

            descripcion.Name = "descripcion"
            descripcion.HeaderText = "Descripcion"
            codVenta.Name = "codVenta"
            codVenta.HeaderText = "Número Venta"
            codDetalle.Name = "codDetalle"
            codDetalle.HeaderText = "Número Detalle"
            producto.Name = "producto"
            producto.HeaderText = "Producto"
            unidades.Name = "unidades"
            unidades.HeaderText = "Unidades Devueltas"
            montoDevuelto.Name = "montoDevuelto"
            montoDevuelto.HeaderText = "Monto Devuelto"
            motivo.Name = "motivo"
            motivo.HeaderText = "Motivo"
            idMotivo.Name = "idMotivo"
            idMotivo.Visible = False
            montoVentaOri.Name = "montoOriginal"
            montoVentaOri.Visible = False
            btnEliminar.Text = "Eliminar"
            btnEliminar.Name = "btnEliminar"
            btnEliminar.HeaderText = "Eliminar"
            btnEliminar.UseColumnTextForButtonValue = True
            btnModificar.Text = "Modificar"
            btnModificar.Name = "btnModificar"
            btnModificar.HeaderText = "Modificar"
            btnModificar.UseColumnTextForButtonValue = True

        Catch ex As Exception
            MsgBox("Error al configurar la lista de devoluciones. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub listarVentasPorFecha(ByRef grilla As DataGridView, ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime)
        Try
            grilla.Columns(0).DataPropertyName = "codVenta"
            grilla.Columns(1).DataPropertyName = "fecha"
            grilla.Columns(2).DataPropertyName = "montoTotal"
            grilla.DataSource = BusVentas.getCabecerasPorFiltro(fechaInicio, fechaFin)


        Catch ex As Exception
            MsgBox("Error al cargar los datos de las ventas a la grilla de datos. Los errores producidos fueron: " + vbOKOnly + vbCritical, "Ventana De Error")
        End Try
    End Sub
    Private Sub listarVentas(ByRef grilla As DataGridView)
        Try
            grilla.Columns(0).DataPropertyName = "codVenta"
            grilla.Columns(1).DataPropertyName = "fecha"
            grilla.Columns(2).DataPropertyName = "montoTotal"
            grilla.DataSource = BusVentas.getCabeceraParaDev()


        Catch ex As Exception
            MsgBox("Error al cargar los datos de las ventas a la grilla de datos. Los errores producidos fueron: " + vbOKOnly + vbCritical, "Ventana De Error")
        End Try
    End Sub

    Private Sub listarDetallesVentas(ByRef grilla As DataGridView, ByRef grillaVentas As DataGridView)
        Try
            Dim idVenta As Integer
            idVenta = CInt(grillaVentas.CurrentRow.Cells("codVenta").Value)
            grilla.Columns(0).DataPropertyName = "codVenta"
            grilla.Columns(1).DataPropertyName = "codDetalle"
            grilla.Columns(2).DataPropertyName = "nombreProducto"
            grilla.Columns(3).DataPropertyName = "unidadesVendidas"
            grilla.Columns(4).DataPropertyName = "precioVenta"
            grilla.Columns(5).DataPropertyName = "categoria"
            grilla.Columns(6).DataPropertyName = "subcategoria"
            grilla.DataSource = BusVentas.getDetallesVentasDev(idVenta)


        Catch ex As Exception
            MsgBox("Error al cargar los datos de las ventas a la grilla de datos. Los errores producidos fueron: " + vbOKOnly + vbCritical, "Ventana De Error")
        End Try
    End Sub

    Private Sub moverDatosDetalle(ByVal grilla As DataGridView)
        Try
            Me.txbNroDetalle.Text = grilla.CurrentRow.Cells("codDetalle").Value
            Me.txbNomProdDev.Text = grilla.CurrentRow.Cells("nombreProducto").Value
            Me.txbMontoVenta.Text = CDbl(grilla.CurrentRow.Cells("precioVenta").Value) / CInt(grilla.CurrentRow.Cells("unidadesVendidas").Value)
        Catch ex As Exception
            MsgBox("Error al mover los datos del detalle de compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub cargarDatosDev(ByRef grilla As DataGridView)
        Try

            Dim codVenta As String
            Dim codDetalle As String
            Dim producto As String
            Dim unidades As Integer
            Dim monto As Double
            Dim motivo As String
            Dim idMotivo As Integer
            Dim montoOriginal As Double
            Dim descripcion As String

            codVenta = Me.txbNroVenta.Text
            codDetalle = Me.txbNroDetalle.Text
            producto = Me.txbNomProdDev.Text
            unidades = Me.nudCantidadDev.Value
            motivo = Me.cmbMotivoDevEmp.Text
            idMotivo = Me.cmbMotivoDevEmp.SelectedValue
            monto = CDbl(Me.lblMontoDev.Text)
            montoOriginal = CDbl(Me.txbMontoVenta.Text)
            descripcion = Me.txbDescripcion.Text
            grilla.Rows.Add(codVenta, codDetalle, producto, unidades, monto, motivo, descripcion, idMotivo, montoOriginal)
        Catch ex As Exception
            MsgBox("Error al establecer los parámetros de la devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub realizarDevolucion(ByRef grilla As DataGridView)
        Try
            Dim ultimoIdCabecera As Integer
            Dim idVentas As New List(Of Integer)
            Dim idDetallesVentas As New List(Of Integer)
            Dim unidadesDev As New List(Of Integer)
            Dim montos As New List(Of Double)
            Dim idMotivos As New List(Of Integer)
            Dim descripcion As New List(Of String)
            Dim elementosCarrito As Integer
            Dim elementoAnalizado As Integer
            Dim montoTotalDev As Double

            elementosCarrito = grilla.Rows.Count
            elementoAnalizado = 0
            montoTotalDev = 0

            While elementoAnalizado < elementosCarrito
                idVentas.Add(CInt(grilla.Rows(elementoAnalizado).Cells("codVenta").Value))
                idDetallesVentas.Add(CInt(grilla.Rows(elementoAnalizado).Cells("codDetalle").Value))
                unidadesDev.Add(CInt(grilla.Rows(elementoAnalizado).Cells("unidades").Value))
                montos.Add(CDbl(grilla.Rows(elementoAnalizado).Cells("montoDevuelto").Value))
                montoTotalDev = montoTotalDev + montos(elementoAnalizado)
                idMotivos.Add(CInt(grilla.Rows(elementoAnalizado).Cells("idMotivo").Value))
                descripcion.Add(grilla.Rows(elementoAnalizado).Cells("descripcion").Value)
                elementoAnalizado = elementoAnalizado + 1
            End While
            'Inserto la cabecera de devolución
            BusDevoluciones.insertarCabeceraDev(montoTotalDev)
            'recupero el id de la última cabecera
            ultimoIdCabecera = BusDevoluciones.getUltimoIdCabecera()
            If ultimoIdCabecera > 0 Then
                'inserto los detalles de la devolución
                BusDevoluciones.insertarDetallesDev(idVentas, idDetallesVentas, unidadesDev, montos, idMotivos, descripcion)
                MsgBox("Devoluciones realizadas correctamente")
            Else
                MsgBox("Error al recuperar el último identificador de las devoluciones", vbOKOnly + vbCritical, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error al realizar la devolución. Los errores producidos fueron: " + vbOKOnly + ex.Message, "Ventana Error")
        End Try
    End Sub
    Private Sub moverNroCabeceraVenta(ByRef grilla As DataGridView)
        Try
            Me.txbNroVenta.Text = grilla.CurrentRow.Cells("codVenta").Value
        Catch ex As Exception
            MsgBox("Error al mover los datos para la devolución. Los errores producidos fueron: " + vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub moverDatosDetalleVenta(ByRef grilla As DataGridView)
        Try
            Me.txbNroDetalle.Text = grilla.CurrentRow.Cells("codDetalle").Value
            Me.txbNomProdDev.Text = grilla.CurrentRow.Cells("producto").Value
            Me.txbMontoVenta.Text = grilla.CurrentRow.Cells("precioVenta").Value
        Catch ex As Exception

        End Try
    End Sub
    Private Sub dgvVentasDev_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVentasDev.CellClick
        Try
            Me.moverNroCabeceraVenta(Me.dgvVentasDev)
            Me.listarDetallesVentas(Me.dgvDetallesVentas, Me.dgvVentasDev)
            Me.activarGrillas(Me.grbListaVentasAdm, False)
            Me.activarGrillas(Me.grbDetallesVentaAdm, True)
        Catch ex As Exception
            MsgBox("Error al seleccionar la ventan. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub dgvVentasDev_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVentasDev.CellContentClick

    End Sub

    Private Sub nudCantidadDev_ValueChanged(sender As Object, e As EventArgs) Handles nudCantidadDev.ValueChanged
        If Me.nudCantidadDev.Value < 0 Then
            MsgBox("La cantidad devuelta no puede ser menor a 1", vbOKOnly + vbExclamation, "Ventana Error")
        Else
            Me.lblMontoDev.Text = CDbl(Me.txbMontoVenta.Text) * Me.nudCantidadDev.Value
        End If
    End Sub

    Private Sub activarDev(ByVal estado As Boolean)
        Try
            Me.grbDatosDev.Enabled = estado
        Catch ex As Exception
            MsgBox("Error al habilitar el ingreso de datos para la devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, "Ventana Error")
        End Try
    End Sub

    Private Sub limpiarGrilla(ByRef grilla As DataGridView)
        Try
            grilla.DataSource = Nothing
            grilla.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al limpiar los datos de las listas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub limpiarDatosDev()
        Try
            Me.txbNroVenta.Text = ""
            Me.txbNroDetalle.Text = ""
            Me.txbNomProdDev.Text = ""
            Me.txbMontoVenta.Text = "0"
            Me.lblMontoDev.Text = "0"
            Me.nudCantidadDev.Value = 1
            Me.txbDescripcion.Text = ""
        Catch ex As Exception

        End Try
    End Sub
    Private Sub moverDatosModDev(ByRef grilla As DataGridView)
        Try
            Me.txbNroVenta.Text = grilla.CurrentRow.Cells("codVenta").Value
            Me.txbNroDetalle.Text = grilla.CurrentRow.Cells("codDetalle").Value
            Me.txbNomProdDev.Text = grilla.CurrentRow.Cells("producto").Value
            Me.txbMontoVenta.Text = grilla.CurrentRow.Cells("montoOriginal").Value
            Me.nudCantidadDev.Value = CInt(grilla.CurrentRow.Cells("unidades").Value)
            Me.cmbMotivoDevEmp.SelectedValue = CInt(grilla.CurrentRow.Cells("idMotivo").Value)
            Me.txbDescripcion.Text = grilla.CurrentRow.Cells("descripcion").Value
        Catch ex As Exception
            MsgBox("Error al mover los datos para modificar. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Function getIndexExistenciaProd(ByVal grilla As DataGridView, ByVal idVenta As String, ByVal idDetalle As String) As Integer
        Try
            Dim elementoAnalizado As Integer
            Dim elementosGrilla As Integer
            Dim indexMod As Integer

            elementoAnalizado = 0
            elementosGrilla = grilla.Rows.Count
            indexMod = -1
            While elementoAnalizado < elementosGrilla
                If grilla.Rows(elementoAnalizado).Cells("codVenta").Value = idVenta And grilla.Rows(elementoAnalizado).Cells("codDetalle").Value = idDetalle Then
                    indexMod = elementoAnalizado
                    elementoAnalizado = elementosGrilla
                End If
                elementoAnalizado = elementoAnalizado + 1
            End While
            Return indexMod
        Catch ex As Exception
            MsgBox("Error al controlar si ya existe el elemento seleccionado en el carrito de devoluciones. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return -1
        End Try
    End Function
    Private Sub modificarDatos(ByRef grilla As DataGridView)
        Try
            Dim idVenta As String
            Dim idDetalle As String
            Dim elementosCarrito As Integer
            Dim elementoAnalizado As Integer
            Dim indexMod As Integer

            elementosCarrito = grilla.ColumnCount
            elementoAnalizado = 0
            idVenta = Me.txbNroVenta.Text
            idDetalle = Me.txbNroDetalle.Text
            indexMod = Me.getIndexExistenciaProd(grilla, idVenta, idDetalle)

            If indexMod <> -1 Then
                grilla.Rows(indexMod).Cells("codVenta").Value = Me.txbNroVenta.Text
                grilla.Rows(indexMod).Cells("codDetalle").Value = Me.txbNroDetalle.Text
                grilla.Rows(indexMod).Cells("producto").Value = Me.txbNomProdDev.Text
                grilla.Rows(indexMod).Cells("montoDevuelto").Value = Me.lblMontoDev.Text
                grilla.Rows(indexMod).Cells("unidades").Value = Me.nudCantidadDev.Value
                grilla.Rows(indexMod).Cells("idMotivo").Value = Me.cmbMotivoDevEmp.SelectedValue
                grilla.Rows(indexMod).Cells("descripcion").Value = Me.txbDescripcion.Text
            End If
        Catch ex As Exception
            MsgBox("Error al modificar los datos presentes en la devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    
    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Dim mensaje As String
        mensaje = Me.controlarDatosDev(True)
        If mensaje = "" Then
            Me.modificarDatos(Me.dgvCarritoDev)
            Me.limpiarDatosDev()
            Me.activarGrillas(Me.grbListaVentasAdm, True)
            Me.activarGrillas(Me.grbDetallesVentaAdm, False)
            Me.activarModificacion(False)
        Else
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly, "Datos Correctos")
        End If
    End Sub

    Private Sub limpiarDatos()
        Try
            Me.txbNroVenta.Text = ""
            Me.txbNroDetalle.Text = ""
            Me.txbNomProdDev.Text = ""
            Me.txbMontoVenta.Text = 0
            Me.nudCantidadDev.Value = ""
            Me.lblMontoDev.Text = 0
            Me.cmbMotivoDevEmp.SelectedValue = 1

        Catch ex As Exception
            MsgBox("Error al limpiar los datos para una nueva devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub activarModificacion(ByVal estado As Boolean)
        Try
            Me.btnDevolverDev.Enabled = Not estado
            Me.btnModificar.Enabled = estado
        Catch ex As Exception
            MsgBox("Error al permitir modificar datos o agregar nuevas devoluciones. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub dgvDetallesVentas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDetallesVentas.CellClick
        Me.activarModificacion(False)
        Me.activarDev(True)
        Me.moverDatosDetalle(Me.dgvDetallesVentas)
    End Sub

    Private Sub dgvCarritoDev_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCarritoDev.CellClick
        'Dim respuesta As MsgBoxResult
        Dim cell As DataGridViewButtonCell = TryCast(dgvCarritoDev.CurrentCell, 
        DataGridViewButtonCell)
        If cell IsNot Nothing Then 'Verifica que la celdas tengan informacion
            Dim bc As DataGridViewButtonColumn = TryCast(dgvCarritoDev.Columns(e.ColumnIndex), 
            DataGridViewButtonColumn) 'Genero una variable que contiene el boton en el datagrid
            If bc IsNot Nothing Then
                Dim s As String = Convert.ToString(cell.Value)
                Select Case bc.Name
                    Case "btnModificar"
                        Me.moverDatosModDev(dgvCarritoDev)
                        Me.activarModificacion(True)
                        Me.activarGrillas(Me.grbListaVentasAdm, False)
                        Me.activarGrillas(Me.grbDetallesVentaAdm, False)
                    Case "btnEliminar"
                        Me.eliminarElementoCarrito(Me.dgvCarritoDev)
                        Me.activarGrillas(Me.grbListaVentasAdm, True)
                        Me.activarGrillas(Me.grbDetallesVentaAdm, False)
                End Select
            End If

        End If
    End Sub

    
    Private Sub dgvCarritoDev_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCarritoDev.CellContentClick

    End Sub

    Private Sub dgvDetallesVentas_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDetallesVentas.CellContentClick

    End Sub

    Private Sub btnRealizarDevEmp_Click(sender As Object, e As EventArgs) Handles btnRealizarDevEmp.Click
        Try
            Me.realizarDevolucion(Me.dgvCarritoDev)
            Me.limpiarGrilla(Me.dgvCarritoDev)
        Catch ex As Exception
            MsgBox("Error al intentar realizar la devolución. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.activarModificacion(False)
        Me.limpiarDatosDev()
        Me.activarGrillas(Me.grbListaVentasAdm, True)
        Me.activarGrillas(Me.grbDetallesVentaAdm, False)
    End Sub

    Private Sub btnFiltrarFechaDev_Click(sender As Object, e As EventArgs) Handles btnFiltrarFechaDev.Click
        Dim fechaIni As DateTime
        Dim fechaFin As DateTime

        fechaIni = Me.dtpFechaIniDevFil.Value
        fechaFin = Me.dtpFechaFinDevFil.Value
        If fechaIni > fechaFin Then
            MsgBox("La fecha inicial debe ser menor a la fecha final", vbOKOnly + vbExclamation, "Ventana Advertencia")
        Else
            Me.listarVentasPorFecha(Me.dgvVentasDev, fechaIni, fechaFin)
        End If

    End Sub

    Private Sub txbNroDetalle_TextChanged(sender As Object, e As EventArgs) Handles txbNroDetalle.TextChanged

    End Sub
End Class