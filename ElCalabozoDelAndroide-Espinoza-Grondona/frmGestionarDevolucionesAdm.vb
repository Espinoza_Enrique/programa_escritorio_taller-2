﻿Public Class frmGestionarDevolucionesAdm

    Private Sub prepararFormulario()
        Try
            Me.cmbCatFiltradoDevAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbSubFiltradoDevAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.dtpFechaFinDevAdm.Format = DateTimePickerFormat.Short
            Me.dtpFechaInicioDevAdm.Format = DateTimePickerFormat.Short
            Me.btnAntDetallesDev.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.btnAnteriorDev.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.rellenarCategorias(cmbCatFiltradoDevAdm)
            Me.rellenarSubcategorias(cmbSubFiltradoDevAdm)
        Catch e As Exception
            MsgBox("Error al dar formato a la ventana de devoluciones", vbOKOnly + vbCritical, "Error de formato")
        End Try
    End Sub
    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al intentar abrir la ventana", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub frmGestionarDevolucionesAdm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al querer cerrr la ventana", vbOKOnly + vbCritical, "Error ingreso nuevo usuario")
        End Try
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub frmGestionarDevolucionesAdm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.prepararFormulario()
    End Sub

    Private Sub rellenarSubcategorias(ByRef subcategorias As ComboBox)
        Try
            subcategorias.Items.Add("-")
            subcategorias.Items.Add("Familiar")
            subcategorias.Items.Add("Lógico")
            subcategorias.Items.Add("Fantasía")
            subcategorias.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al obtener las subcategorías del producto", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub rellenarCategorias(ByRef categorias As ComboBox)
        Try
            categorias.Items.Add("-")
            categorias.Items.Add("Cartas")
            categorias.Items.Add("Tableros")
            categorias.Items.Add("Dados")
            categorias.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al obtener las categorías de un producto", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

End Class