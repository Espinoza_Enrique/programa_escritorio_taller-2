﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGestionarInformesAdm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource3 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource4 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.tbcInformes = New System.Windows.Forms.TabControl()
        Me.tpgVentas = New System.Windows.Forms.TabPage()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.tpgLogueos = New System.Windows.Forms.TabPage()
        Me.rpvLogueos = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.grbFiltroLogueo = New System.Windows.Forms.GroupBox()
        Me.btnFiltrarLogueos = New System.Windows.Forms.Button()
        Me.ckbFiltrarPorFecha = New System.Windows.Forms.CheckBox()
        Me.dtpFechaLogueos = New System.Windows.Forms.DateTimePicker()
        Me.chkFiltrarPorDni = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txbDniLogueos = New System.Windows.Forms.TextBox()
        Me.tpgStock = New System.Windows.Forms.TabPage()
        Me.rpvStockMinimo = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.tpgDevoluciones = New System.Windows.Forms.TabPage()
        Me.rpvDevoluciones = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.RepVentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AuxLogueosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AuxStockMinBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AuxDevolucionesProdBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.dtpFechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.dtpfechaFin = New System.Windows.Forms.DateTimePicker()
        Me.btnFiltrar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grbFiltrosVentas = New System.Windows.Forms.GroupBox()
        Me.tbcInformes.SuspendLayout()
        Me.tpgVentas.SuspendLayout()
        Me.tpgLogueos.SuspendLayout()
        Me.grbFiltroLogueo.SuspendLayout()
        Me.tpgStock.SuspendLayout()
        Me.tpgDevoluciones.SuspendLayout()
        CType(Me.RepVentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AuxLogueosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AuxStockMinBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AuxDevolucionesProdBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbFiltrosVentas.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbcInformes
        '
        Me.tbcInformes.Controls.Add(Me.tpgVentas)
        Me.tbcInformes.Controls.Add(Me.tpgLogueos)
        Me.tbcInformes.Controls.Add(Me.tpgStock)
        Me.tbcInformes.Controls.Add(Me.tpgDevoluciones)
        Me.tbcInformes.Location = New System.Drawing.Point(12, 34)
        Me.tbcInformes.Name = "tbcInformes"
        Me.tbcInformes.SelectedIndex = 0
        Me.tbcInformes.Size = New System.Drawing.Size(1058, 676)
        Me.tbcInformes.TabIndex = 4
        '
        'tpgVentas
        '
        Me.tpgVentas.Controls.Add(Me.grbFiltrosVentas)
        Me.tpgVentas.Controls.Add(Me.ReportViewer1)
        Me.tpgVentas.Location = New System.Drawing.Point(4, 22)
        Me.tpgVentas.Name = "tpgVentas"
        Me.tpgVentas.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgVentas.Size = New System.Drawing.Size(1050, 650)
        Me.tpgVentas.TabIndex = 0
        Me.tpgVentas.Text = "Ventas"
        Me.tpgVentas.UseVisualStyleBackColor = True
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "ventasReporte"
        ReportDataSource1.Value = Me.RepVentasBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ElCalabozoDelAndroide_Espinoza_Grondona.Report1.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(3, 109)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(1041, 535)
        Me.ReportViewer1.TabIndex = 3
        '
        'tpgLogueos
        '
        Me.tpgLogueos.Controls.Add(Me.rpvLogueos)
        Me.tpgLogueos.Controls.Add(Me.grbFiltroLogueo)
        Me.tpgLogueos.Location = New System.Drawing.Point(4, 22)
        Me.tpgLogueos.Name = "tpgLogueos"
        Me.tpgLogueos.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgLogueos.Size = New System.Drawing.Size(1050, 650)
        Me.tpgLogueos.TabIndex = 1
        Me.tpgLogueos.Text = "Logueos"
        Me.tpgLogueos.UseVisualStyleBackColor = True
        '
        'rpvLogueos
        '
        ReportDataSource2.Name = "Logueos"
        ReportDataSource2.Value = Me.AuxLogueosBindingSource
        Me.rpvLogueos.LocalReport.DataSources.Add(ReportDataSource2)
        Me.rpvLogueos.LocalReport.ReportEmbeddedResource = "ElCalabozoDelAndroide_Espinoza_Grondona.ReporteLogueos.rdlc"
        Me.rpvLogueos.Location = New System.Drawing.Point(6, 112)
        Me.rpvLogueos.Name = "rpvLogueos"
        Me.rpvLogueos.Size = New System.Drawing.Size(1038, 542)
        Me.rpvLogueos.TabIndex = 1
        '
        'grbFiltroLogueo
        '
        Me.grbFiltroLogueo.BackColor = System.Drawing.Color.SpringGreen
        Me.grbFiltroLogueo.Controls.Add(Me.btnFiltrarLogueos)
        Me.grbFiltroLogueo.Controls.Add(Me.ckbFiltrarPorFecha)
        Me.grbFiltroLogueo.Controls.Add(Me.dtpFechaLogueos)
        Me.grbFiltroLogueo.Controls.Add(Me.chkFiltrarPorDni)
        Me.grbFiltroLogueo.Controls.Add(Me.Label3)
        Me.grbFiltroLogueo.Controls.Add(Me.txbDniLogueos)
        Me.grbFiltroLogueo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltroLogueo.Location = New System.Drawing.Point(6, 6)
        Me.grbFiltroLogueo.Name = "grbFiltroLogueo"
        Me.grbFiltroLogueo.Size = New System.Drawing.Size(1038, 100)
        Me.grbFiltroLogueo.TabIndex = 0
        Me.grbFiltroLogueo.TabStop = False
        Me.grbFiltroLogueo.Text = "Filtros"
        '
        'btnFiltrarLogueos
        '
        Me.btnFiltrarLogueos.Location = New System.Drawing.Point(718, 57)
        Me.btnFiltrarLogueos.Name = "btnFiltrarLogueos"
        Me.btnFiltrarLogueos.Size = New System.Drawing.Size(114, 26)
        Me.btnFiltrarLogueos.TabIndex = 5
        Me.btnFiltrarLogueos.Text = "Filtrar"
        Me.btnFiltrarLogueos.UseVisualStyleBackColor = True
        '
        'ckbFiltrarPorFecha
        '
        Me.ckbFiltrarPorFecha.AutoSize = True
        Me.ckbFiltrarPorFecha.Location = New System.Drawing.Point(357, 14)
        Me.ckbFiltrarPorFecha.Name = "ckbFiltrarPorFecha"
        Me.ckbFiltrarPorFecha.Size = New System.Drawing.Size(145, 24)
        Me.ckbFiltrarPorFecha.TabIndex = 4
        Me.ckbFiltrarPorFecha.Text = "Filtrar Por Fecha"
        Me.ckbFiltrarPorFecha.UseVisualStyleBackColor = True
        '
        'dtpFechaLogueos
        '
        Me.dtpFechaLogueos.Location = New System.Drawing.Point(318, 55)
        Me.dtpFechaLogueos.Name = "dtpFechaLogueos"
        Me.dtpFechaLogueos.Size = New System.Drawing.Size(284, 26)
        Me.dtpFechaLogueos.TabIndex = 3
        '
        'chkFiltrarPorDni
        '
        Me.chkFiltrarPorDni.AutoSize = True
        Me.chkFiltrarPorDni.Location = New System.Drawing.Point(124, 14)
        Me.chkFiltrarPorDni.Name = "chkFiltrarPorDni"
        Me.chkFiltrarPorDni.Size = New System.Drawing.Size(124, 24)
        Me.chkFiltrarPorDni.TabIndex = 2
        Me.chkFiltrarPorDni.Text = "Filtrar Por Dni"
        Me.chkFiltrarPorDni.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(20, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Dni"
        '
        'txbDniLogueos
        '
        Me.txbDniLogueos.Location = New System.Drawing.Point(114, 57)
        Me.txbDniLogueos.Name = "txbDniLogueos"
        Me.txbDniLogueos.Size = New System.Drawing.Size(134, 26)
        Me.txbDniLogueos.TabIndex = 0
        '
        'tpgStock
        '
        Me.tpgStock.Controls.Add(Me.rpvStockMinimo)
        Me.tpgStock.Location = New System.Drawing.Point(4, 22)
        Me.tpgStock.Name = "tpgStock"
        Me.tpgStock.Size = New System.Drawing.Size(1050, 650)
        Me.tpgStock.TabIndex = 2
        Me.tpgStock.Text = "Stock"
        Me.tpgStock.UseVisualStyleBackColor = True
        '
        'rpvStockMinimo
        '
        Me.rpvStockMinimo.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource3.Name = "dsStockMin"
        ReportDataSource3.Value = Me.AuxStockMinBindingSource
        Me.rpvStockMinimo.LocalReport.DataSources.Add(ReportDataSource3)
        Me.rpvStockMinimo.LocalReport.ReportEmbeddedResource = "ElCalabozoDelAndroide_Espinoza_Grondona.ReporteStock.rdlc"
        Me.rpvStockMinimo.Location = New System.Drawing.Point(0, 0)
        Me.rpvStockMinimo.Name = "rpvStockMinimo"
        Me.rpvStockMinimo.Size = New System.Drawing.Size(1050, 650)
        Me.rpvStockMinimo.TabIndex = 0
        '
        'tpgDevoluciones
        '
        Me.tpgDevoluciones.Controls.Add(Me.rpvDevoluciones)
        Me.tpgDevoluciones.Location = New System.Drawing.Point(4, 22)
        Me.tpgDevoluciones.Name = "tpgDevoluciones"
        Me.tpgDevoluciones.Size = New System.Drawing.Size(1050, 650)
        Me.tpgDevoluciones.TabIndex = 3
        Me.tpgDevoluciones.Text = "Devoluciones"
        Me.tpgDevoluciones.UseVisualStyleBackColor = True
        '
        'rpvDevoluciones
        '
        Me.rpvDevoluciones.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource4.Name = "dbDevoluciones"
        ReportDataSource4.Value = Me.AuxDevolucionesProdBindingSource
        Me.rpvDevoluciones.LocalReport.DataSources.Add(ReportDataSource4)
        Me.rpvDevoluciones.LocalReport.ReportEmbeddedResource = "ElCalabozoDelAndroide_Espinoza_Grondona.ReporteDevoluciones.rdlc"
        Me.rpvDevoluciones.Location = New System.Drawing.Point(0, 0)
        Me.rpvDevoluciones.Name = "rpvDevoluciones"
        Me.rpvDevoluciones.Size = New System.Drawing.Size(1050, 650)
        Me.rpvDevoluciones.TabIndex = 0
        '
        'RepVentasBindingSource
        '
        Me.RepVentasBindingSource.DataSource = GetType(Entities.RepVentas)
        '
        'AuxLogueosBindingSource
        '
        Me.AuxLogueosBindingSource.DataSource = GetType(Entities.AuxLogueos)
        '
        'AuxStockMinBindingSource
        '
        Me.AuxStockMinBindingSource.DataSource = GetType(Entities.AuxStockMin)
        '
        'AuxDevolucionesProdBindingSource
        '
        Me.AuxDevolucionesProdBindingSource.DataSource = GetType(Entities.AuxDevolucionesProd)
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Location = New System.Drawing.Point(6, 46)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(298, 26)
        Me.dtpFechaInicio.TabIndex = 7
        '
        'dtpfechaFin
        '
        Me.dtpfechaFin.Location = New System.Drawing.Point(362, 46)
        Me.dtpfechaFin.Name = "dtpfechaFin"
        Me.dtpfechaFin.Size = New System.Drawing.Size(300, 26)
        Me.dtpfechaFin.TabIndex = 8
        '
        'btnFiltrar
        '
        Me.btnFiltrar.Location = New System.Drawing.Point(692, 46)
        Me.btnFiltrar.Name = "btnFiltrar"
        Me.btnFiltrar.Size = New System.Drawing.Size(98, 29)
        Me.btnFiltrar.TabIndex = 6
        Me.btnFiltrar.Text = "Filtrar"
        Me.btnFiltrar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(91, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 20)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Fecha Inicio"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(467, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 20)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Fecha Fin"
        '
        'grbFiltrosVentas
        '
        Me.grbFiltrosVentas.BackColor = System.Drawing.Color.Turquoise
        Me.grbFiltrosVentas.Controls.Add(Me.Label2)
        Me.grbFiltrosVentas.Controls.Add(Me.Label1)
        Me.grbFiltrosVentas.Controls.Add(Me.btnFiltrar)
        Me.grbFiltrosVentas.Controls.Add(Me.dtpfechaFin)
        Me.grbFiltrosVentas.Controls.Add(Me.dtpFechaInicio)
        Me.grbFiltrosVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltrosVentas.Location = New System.Drawing.Point(6, 6)
        Me.grbFiltrosVentas.Name = "grbFiltrosVentas"
        Me.grbFiltrosVentas.Size = New System.Drawing.Size(1038, 97)
        Me.grbFiltrosVentas.TabIndex = 4
        Me.grbFiltrosVentas.TabStop = False
        Me.grbFiltrosVentas.Text = "Filtros"
        '
        'frmGestionarInformesAdm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1128, 722)
        Me.Controls.Add(Me.tbcInformes)
        Me.Name = "frmGestionarInformesAdm"
        Me.Text = "Informes"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcInformes.ResumeLayout(False)
        Me.tpgVentas.ResumeLayout(False)
        Me.tpgLogueos.ResumeLayout(False)
        Me.grbFiltroLogueo.ResumeLayout(False)
        Me.grbFiltroLogueo.PerformLayout()
        Me.tpgStock.ResumeLayout(False)
        Me.tpgDevoluciones.ResumeLayout(False)
        CType(Me.RepVentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AuxLogueosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AuxStockMinBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AuxDevolucionesProdBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbFiltrosVentas.ResumeLayout(False)
        Me.grbFiltrosVentas.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RepVentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbcInformes As System.Windows.Forms.TabControl
    Friend WithEvents tpgVentas As System.Windows.Forms.TabPage
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents tpgLogueos As System.Windows.Forms.TabPage
    Friend WithEvents grbFiltroLogueo As System.Windows.Forms.GroupBox
    Friend WithEvents btnFiltrarLogueos As System.Windows.Forms.Button
    Friend WithEvents ckbFiltrarPorFecha As System.Windows.Forms.CheckBox
    Friend WithEvents dtpFechaLogueos As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkFiltrarPorDni As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txbDniLogueos As System.Windows.Forms.TextBox
    Friend WithEvents rpvLogueos As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents AuxLogueosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tpgStock As System.Windows.Forms.TabPage
    Friend WithEvents rpvStockMinimo As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents AuxStockMinBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tpgDevoluciones As System.Windows.Forms.TabPage
    Friend WithEvents rpvDevoluciones As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents AuxDevolucionesProdBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents grbFiltrosVentas As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnFiltrar As System.Windows.Forms.Button
    Friend WithEvents dtpfechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaInicio As System.Windows.Forms.DateTimePicker
End Class
