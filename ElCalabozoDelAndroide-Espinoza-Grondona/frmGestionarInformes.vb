﻿Imports Entities
Imports BusinessLayer
Public Class frmGestionarInformesAdm

    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al intentar abrir la ventana", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    '-------------------------------------------------------------------------------
    'método de prueba que puede servir más adelante para negar acceso a ciertos
    'checkbox
    '-------------------------------------------------------------------------------

    Private Sub frmGestionarInformesAdm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al intentar cerrar la ventana", vbOKOnly + vbCritical, "Error de cierre de ventana")
        End Try
    End Sub
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub

    Private Sub mostrarVentas()

    End Sub
    Private Sub frmGestionarInformesAdm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'dsVentas.getVentas' Puede moverla o quitarla según sea necesario.

        Try

            RepVentasBindingSource.DataSource = BusVentas.getVentas(Now, Now)
            ReportViewer1.RefreshReport()
            'Me.cargarFondoPantalla("cobaltoplata.jpg")
            If Sesion.getNombrePerfil() <> "Administrador" Then
                'grbAuditorias.Enabled = False
            End If
        Catch ex As Exception
            MsgBox("Error al intentar cargar la ventana", vbOKOnly + vbCritical, "Error de carga")
        End Try
        'table adapter creado, procedimiento fill y como parámetro el nombre del table adapter creado




        Me.ReportViewer1.RefreshReport()
        Me.rpvLogueos.RefreshReport()
        Me.rpvStockMinimo.RefreshReport()
        Me.rpvDevoluciones.RefreshReport()
    End Sub



    Private Sub clbOpcionesVentas_SelectedIndexChanged(sender As Object, e As EventArgs)
        'If Me.controlarSeleccion() Then
        'MsgBox("Debido a tu perfil, no puedes seleccionar esta opción", vbOKOnly)
        'clbOpcionesVentas.SetItemChecked(clbOpcionesVentas.SelectedIndex, False)

        'End If
    End Sub




    Private Sub btnFiltrar_Click_1(sender As Object, e As EventArgs) Handles btnFiltrar.Click
        If Me.dtpFechaInicio.Value < Me.dtpfechaFin.Value Then
            RepVentasBindingSource.DataSource = BusVentas.getVentas(Me.dtpFechaInicio.Value, Me.dtpfechaFin.Value)
            ReportViewer1.RefreshReport()
        Else
            MsgBox("La fecha de inicio debe ser menor que la fecha de fin", vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub
    Private Function controlarFiltroLogueos(ByVal dni As String) As String
        Try
            Dim mensaje As String

            mensaje = ""
            If String.IsNullOrWhiteSpace(dni) Or dni.Length < 8 Then
                mensaje = "Por favor, coloque un valor para el dni válido" + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar los datos del filtro.Los errores producidos son:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Private Sub btnFiltrarLogueos_Click(sender As Object, e As EventArgs) Handles btnFiltrarLogueos.Click
        Dim filtrarPorDni As Boolean
        Dim filtrarPorFecha As Boolean
        Dim mensaje As String

        mensaje = ""
        filtrarPorDni = Me.chkFiltrarPorDni.Checked
        filtrarPorFecha = Me.ckbFiltrarPorFecha.Checked
        mensaje = Me.controlarFiltroLogueos(Trim(Me.txbDniLogueos.Text))

        If filtrarPorDni And filtrarPorFecha And String.IsNullOrWhiteSpace(mensaje) Then
            AuxLogueosBindingSource.DataSource = BusLogueos.getInformesPorFechaYDni(CInt(Me.txbDniLogueos.Text), DateTime.Parse(Me.dtpFechaLogueos.Value.ToString()))
            rpvLogueos.RefreshReport()
        ElseIf filtrarPorDni And String.IsNullOrWhiteSpace(mensaje) Then
            AuxLogueosBindingSource.DataSource = BusLogueos.getInformesPorDni(CInt(Me.txbDniLogueos.Text))
            rpvLogueos.RefreshReport()
        ElseIf filtrarPorFecha Then
            AuxLogueosBindingSource.DataSource = BusLogueos.getInformesPorFecha(DateTime.Parse(Me.dtpFechaLogueos.Value.ToString()))
            rpvLogueos.RefreshReport()
        End If

        If Not String.IsNullOrWhiteSpace(mensaje) And filtrarPorDni Then
            MsgBox("Se han producido los siguientes errores:" + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub

    Private Sub tbcInformes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tbcInformes.SelectedIndexChanged
        If tbcInformes.SelectedIndex = 3 Then
            Me.AuxDevolucionesProdBindingSource.DataSource = BusDevoluciones.getDevolucionesProductos()
            rpvDevoluciones.RefreshReport()
        End If
    End Sub

    Private Sub tbcInformes_Selecting(sender As Object, e As TabControlCancelEventArgs) Handles tbcInformes.Selecting
        If tbcInformes.SelectedIndex = 2 Then
            Me.AuxStockMinBindingSource.DataSource = BusProductos.getProdStockMin()
            Me.rpvStockMinimo.RefreshReport()
        End If
    End Sub

  
End Class
