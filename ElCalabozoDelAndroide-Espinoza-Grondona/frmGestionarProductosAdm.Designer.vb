﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGestionarProductosAdm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGestionarProductosAdm))
        Me.tbcGestionProdAdm = New System.Windows.Forms.TabControl()
        Me.tpgAgregarProd = New System.Windows.Forms.TabPage()
        Me.grbDatosNuevosProd = New System.Windows.Forms.GroupBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.txbRutaFotoNuevoProd = New System.Windows.Forms.TextBox()
        Me.btnIngresarProdNuevo = New System.Windows.Forms.Button()
        Me.grbDatosProductoNuevo = New System.Windows.Forms.GroupBox()
        Me.nudGarantiaNuevoProd = New System.Windows.Forms.NumericUpDown()
        Me.nudStockMinNuevoProd = New System.Windows.Forms.NumericUpDown()
        Me.ptbAyDescNuevoProd = New System.Windows.Forms.PictureBox()
        Me.ptbAyudaGarantia = New System.Windows.Forms.PictureBox()
        Me.ptbAyudaStockMin = New System.Windows.Forms.PictureBox()
        Me.ptbAyudaPrecio = New System.Windows.Forms.PictureBox()
        Me.lblDescProdAdmNuevo = New System.Windows.Forms.Label()
        Me.txbDescProdAdmNuevo = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txbPrecioProdAdmNuevo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbSubCatProdAdmNuevo = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbCatProdAdmNuevo = New System.Windows.Forms.ComboBox()
        Me.txbNombreProdAdmNuevo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnNuegoAgregarLista = New System.Windows.Forms.Button()
        Me.btnBuscarFotoNuevoProd = New System.Windows.Forms.Button()
        Me.ptbFotoAltaProd = New System.Windows.Forms.PictureBox()
        Me.tgpModProd = New System.Windows.Forms.TabPage()
        Me.grbDatosProductosMod = New System.Windows.Forms.GroupBox()
        Me.dgvListaProdMod = New System.Windows.Forms.DataGridView()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chkMostrarTodos = New System.Windows.Forms.CheckBox()
        Me.chkFilProdSubCat = New System.Windows.Forms.CheckBox()
        Me.chkFilPorCat = New System.Windows.Forms.CheckBox()
        Me.chkFilPorNombre = New System.Windows.Forms.CheckBox()
        Me.ptbAyudaFiltroModProd = New System.Windows.Forms.PictureBox()
        Me.ptbAySubcatProdFil = New System.Windows.Forms.PictureBox()
        Me.ptbAyCatProdFil = New System.Windows.Forms.PictureBox()
        Me.ptbAyNombreProdFil = New System.Windows.Forms.PictureBox()
        Me.cmbFiltroSubProdAdm = New System.Windows.Forms.ComboBox()
        Me.cmbFiltroCatProdAdm = New System.Windows.Forms.ComboBox()
        Me.btnFiltrarModProd = New System.Windows.Forms.Button()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txbNomModProdFil = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.gpbModProductos = New System.Windows.Forms.GroupBox()
        Me.txbIdModProd = New System.Windows.Forms.TextBox()
        Me.lblTituloNroProd = New System.Windows.Forms.Label()
        Me.chkCambiarCatSubModProd = New System.Windows.Forms.CheckBox()
        Me.txbRutaFotoModProd = New System.Windows.Forms.TextBox()
        Me.btnCancelarModProd = New System.Windows.Forms.Button()
        Me.chkModNombreProd = New System.Windows.Forms.CheckBox()
        Me.lblRutaFotoModProd = New System.Windows.Forms.Label()
        Me.nudModGarProd = New System.Windows.Forms.NumericUpDown()
        Me.nudModStockMinProd = New System.Windows.Forms.NumericUpDown()
        Me.ptbAyudaGarModProd = New System.Windows.Forms.PictureBox()
        Me.ptbAyudaStockMinModProd = New System.Windows.Forms.PictureBox()
        Me.btnModProducto = New System.Windows.Forms.Button()
        Me.txbDescModProd = New System.Windows.Forms.TextBox()
        Me.ptbFotoModProd = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmbModSub = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbModCat = New System.Windows.Forms.ComboBox()
        Me.txbModProdNombre = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnFotoModProd = New System.Windows.Forms.Button()
        Me.tpgNuevaCat = New System.Windows.Forms.TabPage()
        Me.grbNuevaCatSub = New System.Windows.Forms.GroupBox()
        Me.ckbModCatSub = New System.Windows.Forms.CheckBox()
        Me.ptbAyNuevaCatSub = New System.Windows.Forms.PictureBox()
        Me.btnCancelarModCatSub = New System.Windows.Forms.Button()
        Me.btnModificarCatSub = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txbDescCatSub = New System.Windows.Forms.TextBox()
        Me.cmbNuevasSubCatAdm = New System.Windows.Forms.ComboBox()
        Me.cmbNuevasCatAdm = New System.Windows.Forms.ComboBox()
        Me.btnAgregarNuevaCatSub = New System.Windows.Forms.Button()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.dgvCatSubAdm = New System.Windows.Forms.DataGridView()
        Me.idCat_CatSub = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idSub_CatSub = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descripcionCatSub = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbNuevaSubcat = New System.Windows.Forms.GroupBox()
        Me.ckbModDatosSubcat = New System.Windows.Forms.CheckBox()
        Me.ptbAyNuevaSubcat = New System.Windows.Forms.PictureBox()
        Me.btnCancelarModSubcat = New System.Windows.Forms.Button()
        Me.btnModificarSubCat = New System.Windows.Forms.Button()
        Me.btnAgregarNuevaSubCat = New System.Windows.Forms.Button()
        Me.txbDescripcionNuevaSubCat = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txbNuevaSubcat = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.dgvSubAdm = New System.Windows.Forms.DataGridView()
        Me.idSubcategoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombreSubcategoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descripcionSub = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbNuevaCategoria = New System.Windows.Forms.GroupBox()
        Me.ckbModificarNombreCat = New System.Windows.Forms.CheckBox()
        Me.ptbAyNuevaCat = New System.Windows.Forms.PictureBox()
        Me.btnCancelarModCat = New System.Windows.Forms.Button()
        Me.btnModificarCat = New System.Windows.Forms.Button()
        Me.btnAgregarNuevaCat = New System.Windows.Forms.Button()
        Me.txbDescripcionNuevaCat = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txbNuevaCategoria = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.dgvCategoriasAdm = New System.Windows.Forms.DataGridView()
        Me.idCategoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombreCategoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descripcionCategoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgProveedores = New System.Windows.Forms.TabPage()
        Me.grbModDatosProv = New System.Windows.Forms.GroupBox()
        Me.txbIdModProv = New System.Windows.Forms.TextBox()
        Me.lblTituloNroModProv = New System.Windows.Forms.Label()
        Me.chkModNombreProve = New System.Windows.Forms.CheckBox()
        Me.ptbAyNomModProv = New System.Windows.Forms.PictureBox()
        Me.ptbAyDirModProv = New System.Windows.Forms.PictureBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txbCodAreaModProv = New System.Windows.Forms.TextBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.btnModProv = New System.Windows.Forms.Button()
        Me.txbMailModProv = New System.Windows.Forms.TextBox()
        Me.txbCelModProv = New System.Windows.Forms.TextBox()
        Me.txbTelModProv = New System.Windows.Forms.TextBox()
        Me.txbDirModProv = New System.Windows.Forms.TextBox()
        Me.txbNombreModProv = New System.Windows.Forms.TextBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.grbDatosProveedor = New System.Windows.Forms.GroupBox()
        Me.ptbAyNombreProv = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txbCodAreaCel = New System.Windows.Forms.TextBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.ptbAyTelProv = New System.Windows.Forms.PictureBox()
        Me.btnAgregarProv = New System.Windows.Forms.Button()
        Me.txbMailProv = New System.Windows.Forms.TextBox()
        Me.txbCelProv = New System.Windows.Forms.TextBox()
        Me.txbTelProv = New System.Windows.Forms.TextBox()
        Me.txbDireccionProv = New System.Windows.Forms.TextBox()
        Me.txbNombreProv = New System.Windows.Forms.TextBox()
        Me.lblCelularProv = New System.Windows.Forms.Label()
        Me.lblMailProveedor = New System.Windows.Forms.Label()
        Me.lblTelefonoProv = New System.Windows.Forms.Label()
        Me.lblDireccionProv = New System.Windows.Forms.Label()
        Me.lblNombreProveedor = New System.Windows.Forms.Label()
        Me.grbProveedores = New System.Windows.Forms.GroupBox()
        Me.dgvProveedores = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgProvProd = New System.Windows.Forms.TabPage()
        Me.grbDatosProdProv = New System.Windows.Forms.GroupBox()
        Me.btnCancelarRelacion = New System.Windows.Forms.Button()
        Me.btnMostrarRelacionProdProv = New System.Windows.Forms.Button()
        Me.btnPrecioCompraProd = New System.Windows.Forms.Button()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.txbPrecioVenta_ProvProd = New System.Windows.Forms.TextBox()
        Me.txbPrecioCompraProd = New System.Windows.Forms.TextBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.lblTituloPrecioVenta = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.lblNroProd = New System.Windows.Forms.Label()
        Me.lblTituloNroProv = New System.Windows.Forms.Label()
        Me.txbProd_ProvProd = New System.Windows.Forms.TextBox()
        Me.txbProv_ProdProv = New System.Windows.Forms.TextBox()
        Me.lblIdProducto = New System.Windows.Forms.Label()
        Me.lblIdProveedor = New System.Windows.Forms.Label()
        Me.lblTituloProducto = New System.Windows.Forms.Label()
        Me.lblTituloProveedor = New System.Windows.Forms.Label()
        Me.grbProvProdProv = New System.Windows.Forms.GroupBox()
        Me.btnFiltrarProveedor = New System.Windows.Forms.Button()
        Me.txbFiltrarProvProdProv = New System.Windows.Forms.TextBox()
        Me.lblFiltrarProvProdProv = New System.Windows.Forms.Label()
        Me.dgvProv_ProvProd = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbDatosProdProvProd = New System.Windows.Forms.GroupBox()
        Me.btnFiltrarProd = New System.Windows.Forms.Button()
        Me.btnSigProdProv_Prod = New System.Windows.Forms.Button()
        Me.btnAntProdProv_Prod = New System.Windows.Forms.Button()
        Me.txbFiltrarProdProv = New System.Windows.Forms.TextBox()
        Me.lblFiltradoProdProv = New System.Windows.Forms.Label()
        Me.dgvProvProd = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgCompras = New System.Windows.Forms.TabPage()
        Me.grbFiltrosCompras = New System.Windows.Forms.GroupBox()
        Me.btnFiltrarProdProv = New System.Windows.Forms.Button()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.txbProvFilCompra = New System.Windows.Forms.TextBox()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.txbProdFilCompra = New System.Windows.Forms.TextBox()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.btnCancelarModCarrito = New System.Windows.Forms.Button()
        Me.btnModCarrito = New System.Windows.Forms.Button()
        Me.txbPrecioCompra = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.txbProvCompra = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txbNroProdCompra = New System.Windows.Forms.TextBox()
        Me.txbNroProvCompra = New System.Windows.Forms.TextBox()
        Me.txbNombreProdCompra = New System.Windows.Forms.TextBox()
        Me.ckbConfModPrecio = New System.Windows.Forms.CheckBox()
        Me.grbModPrecioCompra = New System.Windows.Forms.GroupBox()
        Me.txbDescModCompra = New System.Windows.Forms.TextBox()
        Me.chkDescPrecioCompra = New System.Windows.Forms.CheckBox()
        Me.chkAuPrecioCompra = New System.Windows.Forms.CheckBox()
        Me.cmbMotivoModCompra = New System.Windows.Forms.ComboBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.txbModiPrecioCompra = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.btnAsignarCarritoCompra = New System.Windows.Forms.Button()
        Me.nudCantidadCompra = New System.Windows.Forms.NumericUpDown()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.grbCarritoCompra = New System.Windows.Forms.GroupBox()
        Me.btnComprar = New System.Windows.Forms.Button()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.dgvCarritoCompras = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.dgvProvProdCompras = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn52 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn53 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn54 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn57 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbModStockProd = New System.Windows.Forms.TabPage()
        Me.grbProdModStockPre = New System.Windows.Forms.GroupBox()
        Me.chkFilModStockPrecio = New System.Windows.Forms.CheckBox()
        Me.cmbFiltroSubcat = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbCatModStockPrecio = New System.Windows.Forms.ComboBox()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.dgvProdModstockPrecio = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn51 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn55 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn56 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn58 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn59 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn60 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn61 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbDatosProdModPrecio = New System.Windows.Forms.GroupBox()
        Me.txbPrecioActModPrecio = New System.Windows.Forms.TextBox()
        Me.txbNroProdModPrecio = New System.Windows.Forms.TextBox()
        Me.txbProdModPrecio = New System.Windows.Forms.TextBox()
        Me.btnCancelarModPrecio = New System.Windows.Forms.Button()
        Me.txbPrecioModProd = New System.Windows.Forms.TextBox()
        Me.btnModPrecioAdm = New System.Windows.Forms.Button()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.txbDetalleModPrecio = New System.Windows.Forms.TextBox()
        Me.cmbMotivoPrecioModProd = New System.Windows.Forms.ComboBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.grbDatosProdModStock = New System.Windows.Forms.GroupBox()
        Me.txbStockActModStock = New System.Windows.Forms.TextBox()
        Me.txbNroProdModStock = New System.Windows.Forms.TextBox()
        Me.txbProdModStock = New System.Windows.Forms.TextBox()
        Me.btnCancelarModStock = New System.Windows.Forms.Button()
        Me.btnModStockProd = New System.Windows.Forms.Button()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.txbDetalleModStock = New System.Windows.Forms.TextBox()
        Me.cmbMotivoModStock = New System.Windows.Forms.ComboBox()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.nudModificarStock = New System.Windows.Forms.NumericUpDown()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.tpAyudas = New System.Windows.Forms.ToolTip(Me.components)
        Me.ofdBuscarFoto = New System.Windows.Forms.OpenFileDialog()
        Me.eprProdAdm = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tbcGestionProdAdm.SuspendLayout()
        Me.tpgAgregarProd.SuspendLayout()
        Me.grbDatosNuevosProd.SuspendLayout()
        Me.grbDatosProductoNuevo.SuspendLayout()
        CType(Me.nudGarantiaNuevoProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudStockMinNuevoProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyDescNuevoProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyudaGarantia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyudaStockMin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyudaPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbFotoAltaProd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tgpModProd.SuspendLayout()
        Me.grbDatosProductosMod.SuspendLayout()
        CType(Me.dgvListaProdMod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.ptbAyudaFiltroModProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAySubcatProdFil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyCatProdFil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyNombreProdFil, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpbModProductos.SuspendLayout()
        CType(Me.nudModGarProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudModStockMinProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyudaGarModProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyudaStockMinModProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbFotoModProd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgNuevaCat.SuspendLayout()
        Me.grbNuevaCatSub.SuspendLayout()
        CType(Me.ptbAyNuevaCatSub, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCatSubAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbNuevaSubcat.SuspendLayout()
        CType(Me.ptbAyNuevaSubcat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSubAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbNuevaCategoria.SuspendLayout()
        CType(Me.ptbAyNuevaCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCategoriasAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgProveedores.SuspendLayout()
        Me.grbModDatosProv.SuspendLayout()
        CType(Me.ptbAyNomModProv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyDirModProv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosProveedor.SuspendLayout()
        CType(Me.ptbAyNombreProv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbAyTelProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbProveedores.SuspendLayout()
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgProvProd.SuspendLayout()
        Me.grbDatosProdProv.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.grbProvProdProv.SuspendLayout()
        CType(Me.dgvProv_ProvProd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosProdProvProd.SuspendLayout()
        CType(Me.dgvProvProd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgCompras.SuspendLayout()
        Me.grbFiltrosCompras.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox12.SuspendLayout()
        Me.grbModPrecioCompra.SuspendLayout()
        CType(Me.nudCantidadCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbCarritoCompra.SuspendLayout()
        CType(Me.dgvCarritoCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox11.SuspendLayout()
        CType(Me.dgvProvProdCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbModStockProd.SuspendLayout()
        Me.grbProdModStockPre.SuspendLayout()
        CType(Me.dgvProdModstockPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosProdModPrecio.SuspendLayout()
        Me.grbDatosProdModStock.SuspendLayout()
        CType(Me.nudModificarStock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.eprProdAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbcGestionProdAdm
        '
        Me.tbcGestionProdAdm.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.tbcGestionProdAdm.Controls.Add(Me.tpgAgregarProd)
        Me.tbcGestionProdAdm.Controls.Add(Me.tgpModProd)
        Me.tbcGestionProdAdm.Controls.Add(Me.tpgNuevaCat)
        Me.tbcGestionProdAdm.Controls.Add(Me.tpgProveedores)
        Me.tbcGestionProdAdm.Controls.Add(Me.tpgProvProd)
        Me.tbcGestionProdAdm.Controls.Add(Me.tpgCompras)
        Me.tbcGestionProdAdm.Controls.Add(Me.grbModStockProd)
        Me.tbcGestionProdAdm.Location = New System.Drawing.Point(-2, 58)
        Me.tbcGestionProdAdm.Name = "tbcGestionProdAdm"
        Me.tbcGestionProdAdm.SelectedIndex = 0
        Me.tbcGestionProdAdm.Size = New System.Drawing.Size(1287, 679)
        Me.tbcGestionProdAdm.TabIndex = 0
        '
        'tpgAgregarProd
        '
        Me.tpgAgregarProd.BackColor = System.Drawing.Color.Black
        Me.tpgAgregarProd.Controls.Add(Me.grbDatosNuevosProd)
        Me.tpgAgregarProd.Location = New System.Drawing.Point(4, 22)
        Me.tpgAgregarProd.Name = "tpgAgregarProd"
        Me.tpgAgregarProd.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgAgregarProd.Size = New System.Drawing.Size(1279, 653)
        Me.tpgAgregarProd.TabIndex = 0
        Me.tpgAgregarProd.Text = "Agregar Productos"
        '
        'grbDatosNuevosProd
        '
        Me.grbDatosNuevosProd.BackColor = System.Drawing.Color.Silver
        Me.grbDatosNuevosProd.Controls.Add(Me.Label57)
        Me.grbDatosNuevosProd.Controls.Add(Me.txbRutaFotoNuevoProd)
        Me.grbDatosNuevosProd.Controls.Add(Me.btnIngresarProdNuevo)
        Me.grbDatosNuevosProd.Controls.Add(Me.grbDatosProductoNuevo)
        Me.grbDatosNuevosProd.Controls.Add(Me.btnNuegoAgregarLista)
        Me.grbDatosNuevosProd.Controls.Add(Me.btnBuscarFotoNuevoProd)
        Me.grbDatosNuevosProd.Controls.Add(Me.ptbFotoAltaProd)
        Me.grbDatosNuevosProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosNuevosProd.Location = New System.Drawing.Point(1, 3)
        Me.grbDatosNuevosProd.Name = "grbDatosNuevosProd"
        Me.grbDatosNuevosProd.Size = New System.Drawing.Size(1270, 649)
        Me.grbDatosNuevosProd.TabIndex = 0
        Me.grbDatosNuevosProd.TabStop = False
        Me.grbDatosNuevosProd.Text = "Datos Del Producto Nuevo"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.BackColor = System.Drawing.Color.White
        Me.Label57.ForeColor = System.Drawing.Color.Red
        Me.Label57.Location = New System.Drawing.Point(618, 294)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(74, 18)
        Me.Label57.TabIndex = 31
        Me.Label57.Text = "Ruta Foto"
        '
        'txbRutaFotoNuevoProd
        '
        Me.txbRutaFotoNuevoProd.Location = New System.Drawing.Point(698, 291)
        Me.txbRutaFotoNuevoProd.Name = "txbRutaFotoNuevoProd"
        Me.txbRutaFotoNuevoProd.Size = New System.Drawing.Size(415, 24)
        Me.txbRutaFotoNuevoProd.TabIndex = 30
        '
        'btnIngresarProdNuevo
        '
        Me.btnIngresarProdNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIngresarProdNuevo.Location = New System.Drawing.Point(944, 333)
        Me.btnIngresarProdNuevo.Name = "btnIngresarProdNuevo"
        Me.btnIngresarProdNuevo.Size = New System.Drawing.Size(127, 49)
        Me.btnIngresarProdNuevo.TabIndex = 29
        Me.btnIngresarProdNuevo.Text = "Ingresar Producto"
        Me.btnIngresarProdNuevo.UseVisualStyleBackColor = True
        '
        'grbDatosProductoNuevo
        '
        Me.grbDatosProductoNuevo.BackColor = System.Drawing.Color.PaleTurquoise
        Me.grbDatosProductoNuevo.Controls.Add(Me.nudGarantiaNuevoProd)
        Me.grbDatosProductoNuevo.Controls.Add(Me.nudStockMinNuevoProd)
        Me.grbDatosProductoNuevo.Controls.Add(Me.ptbAyDescNuevoProd)
        Me.grbDatosProductoNuevo.Controls.Add(Me.ptbAyudaGarantia)
        Me.grbDatosProductoNuevo.Controls.Add(Me.ptbAyudaStockMin)
        Me.grbDatosProductoNuevo.Controls.Add(Me.ptbAyudaPrecio)
        Me.grbDatosProductoNuevo.Controls.Add(Me.lblDescProdAdmNuevo)
        Me.grbDatosProductoNuevo.Controls.Add(Me.txbDescProdAdmNuevo)
        Me.grbDatosProductoNuevo.Controls.Add(Me.Label7)
        Me.grbDatosProductoNuevo.Controls.Add(Me.Label6)
        Me.grbDatosProductoNuevo.Controls.Add(Me.txbPrecioProdAdmNuevo)
        Me.grbDatosProductoNuevo.Controls.Add(Me.Label4)
        Me.grbDatosProductoNuevo.Controls.Add(Me.Label3)
        Me.grbDatosProductoNuevo.Controls.Add(Me.cmbSubCatProdAdmNuevo)
        Me.grbDatosProductoNuevo.Controls.Add(Me.Label2)
        Me.grbDatosProductoNuevo.Controls.Add(Me.cmbCatProdAdmNuevo)
        Me.grbDatosProductoNuevo.Controls.Add(Me.txbNombreProdAdmNuevo)
        Me.grbDatosProductoNuevo.Controls.Add(Me.Label1)
        Me.grbDatosProductoNuevo.Location = New System.Drawing.Point(7, 23)
        Me.grbDatosProductoNuevo.Name = "grbDatosProductoNuevo"
        Me.grbDatosProductoNuevo.Size = New System.Drawing.Size(592, 619)
        Me.grbDatosProductoNuevo.TabIndex = 27
        Me.grbDatosProductoNuevo.TabStop = False
        Me.grbDatosProductoNuevo.Text = "Datos Del Producto"
        '
        'nudGarantiaNuevoProd
        '
        Me.nudGarantiaNuevoProd.Location = New System.Drawing.Point(435, 325)
        Me.nudGarantiaNuevoProd.Name = "nudGarantiaNuevoProd"
        Me.nudGarantiaNuevoProd.Size = New System.Drawing.Size(120, 24)
        Me.nudGarantiaNuevoProd.TabIndex = 39
        '
        'nudStockMinNuevoProd
        '
        Me.nudStockMinNuevoProd.Location = New System.Drawing.Point(435, 257)
        Me.nudStockMinNuevoProd.Name = "nudStockMinNuevoProd"
        Me.nudStockMinNuevoProd.Size = New System.Drawing.Size(120, 24)
        Me.nudStockMinNuevoProd.TabIndex = 38
        '
        'ptbAyDescNuevoProd
        '
        Me.ptbAyDescNuevoProd.Image = CType(resources.GetObject("ptbAyDescNuevoProd.Image"), System.Drawing.Image)
        Me.ptbAyDescNuevoProd.Location = New System.Drawing.Point(356, 421)
        Me.ptbAyDescNuevoProd.Name = "ptbAyDescNuevoProd"
        Me.ptbAyDescNuevoProd.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyDescNuevoProd.TabIndex = 37
        Me.ptbAyDescNuevoProd.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyDescNuevoProd, "Por favor, coloque una descripción para el producto de no más de 250 caracteres.")
        '
        'ptbAyudaGarantia
        '
        Me.ptbAyudaGarantia.Image = CType(resources.GetObject("ptbAyudaGarantia.Image"), System.Drawing.Image)
        Me.ptbAyudaGarantia.Location = New System.Drawing.Point(561, 325)
        Me.ptbAyudaGarantia.Name = "ptbAyudaGarantia"
        Me.ptbAyudaGarantia.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyudaGarantia.TabIndex = 36
        Me.ptbAyudaGarantia.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyudaGarantia, "Por favor, coloque la garantía en días. Un garantía de cero días indica que el " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
        "producto no tiene garantía.")
        '
        'ptbAyudaStockMin
        '
        Me.ptbAyudaStockMin.Image = CType(resources.GetObject("ptbAyudaStockMin.Image"), System.Drawing.Image)
        Me.ptbAyudaStockMin.Location = New System.Drawing.Point(561, 257)
        Me.ptbAyudaStockMin.Name = "ptbAyudaStockMin"
        Me.ptbAyudaStockMin.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyudaStockMin.TabIndex = 35
        Me.ptbAyudaStockMin.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyudaStockMin, "Coloque el stock mínimo del producto. Dicho valor no puede ser cero. Por favor," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
        "no intente colocar valores distintos a números mayores a cero")
        '
        'ptbAyudaPrecio
        '
        Me.ptbAyudaPrecio.Image = CType(resources.GetObject("ptbAyudaPrecio.Image"), System.Drawing.Image)
        Me.ptbAyudaPrecio.Location = New System.Drawing.Point(561, 196)
        Me.ptbAyudaPrecio.Name = "ptbAyudaPrecio"
        Me.ptbAyudaPrecio.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyudaPrecio.TabIndex = 34
        Me.ptbAyudaPrecio.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyudaPrecio, "Por favor, coloque un precio para el producto. Separe los valores decimales con u" & _
        "n" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "punto y no coloque otros valores que no sean numéricos.")
        '
        'lblDescProdAdmNuevo
        '
        Me.lblDescProdAdmNuevo.AutoSize = True
        Me.lblDescProdAdmNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescProdAdmNuevo.Location = New System.Drawing.Point(151, 421)
        Me.lblDescProdAdmNuevo.Name = "lblDescProdAdmNuevo"
        Me.lblDescProdAdmNuevo.Size = New System.Drawing.Size(188, 20)
        Me.lblDescProdAdmNuevo.TabIndex = 33
        Me.lblDescProdAdmNuevo.Text = "Descripción Del Producto"
        '
        'txbDescProdAdmNuevo
        '
        Me.txbDescProdAdmNuevo.Location = New System.Drawing.Point(6, 447)
        Me.txbDescProdAdmNuevo.MaxLength = 250
        Me.txbDescProdAdmNuevo.Multiline = True
        Me.txbDescProdAdmNuevo.Name = "txbDescProdAdmNuevo"
        Me.txbDescProdAdmNuevo.Size = New System.Drawing.Size(580, 165)
        Me.txbDescProdAdmNuevo.TabIndex = 32
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 329)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(136, 20)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "Garantía (en días)"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 261)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 20)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Stock Mínimo"
        '
        'txbPrecioProdAdmNuevo
        '
        Me.txbPrecioProdAdmNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbPrecioProdAdmNuevo.Location = New System.Drawing.Point(416, 193)
        Me.txbPrecioProdAdmNuevo.Name = "txbPrecioProdAdmNuevo"
        Me.txbPrecioProdAdmNuevo.Size = New System.Drawing.Size(139, 26)
        Me.txbPrecioProdAdmNuevo.TabIndex = 23
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 196)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 20)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Precio Unitario"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 133)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 20)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Subcategoría"
        '
        'cmbSubCatProdAdmNuevo
        '
        Me.cmbSubCatProdAdmNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSubCatProdAdmNuevo.FormattingEnabled = True
        Me.cmbSubCatProdAdmNuevo.Location = New System.Drawing.Point(365, 130)
        Me.cmbSubCatProdAdmNuevo.Name = "cmbSubCatProdAdmNuevo"
        Me.cmbSubCatProdAdmNuevo.Size = New System.Drawing.Size(221, 28)
        Me.cmbSubCatProdAdmNuevo.TabIndex = 20
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 20)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Categoría"
        '
        'cmbCatProdAdmNuevo
        '
        Me.cmbCatProdAdmNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCatProdAdmNuevo.FormattingEnabled = True
        Me.cmbCatProdAdmNuevo.Location = New System.Drawing.Point(365, 72)
        Me.cmbCatProdAdmNuevo.Name = "cmbCatProdAdmNuevo"
        Me.cmbCatProdAdmNuevo.Size = New System.Drawing.Size(221, 28)
        Me.cmbCatProdAdmNuevo.TabIndex = 18
        '
        'txbNombreProdAdmNuevo
        '
        Me.txbNombreProdAdmNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbNombreProdAdmNuevo.Location = New System.Drawing.Point(262, 21)
        Me.txbNombreProdAdmNuevo.Name = "txbNombreProdAdmNuevo"
        Me.txbNombreProdAdmNuevo.Size = New System.Drawing.Size(324, 26)
        Me.txbNombreProdAdmNuevo.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(133, 20)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Nombre Producto"
        '
        'btnNuegoAgregarLista
        '
        Me.btnNuegoAgregarLista.Location = New System.Drawing.Point(286, 321)
        Me.btnNuegoAgregarLista.Name = "btnNuegoAgregarLista"
        Me.btnNuegoAgregarLista.Size = New System.Drawing.Size(260, 38)
        Me.btnNuegoAgregarLista.TabIndex = 16
        Me.btnNuegoAgregarLista.Text = "Agregar Producto A La Lista"
        Me.btnNuegoAgregarLista.UseVisualStyleBackColor = True
        '
        'btnBuscarFotoNuevoProd
        '
        Me.btnBuscarFotoNuevoProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarFotoNuevoProd.Location = New System.Drawing.Point(811, 333)
        Me.btnBuscarFotoNuevoProd.Name = "btnBuscarFotoNuevoProd"
        Me.btnBuscarFotoNuevoProd.Size = New System.Drawing.Size(127, 49)
        Me.btnBuscarFotoNuevoProd.TabIndex = 1
        Me.btnBuscarFotoNuevoProd.Text = "Buscar Foto"
        Me.btnBuscarFotoNuevoProd.UseVisualStyleBackColor = True
        '
        'ptbFotoAltaProd
        '
        Me.ptbFotoAltaProd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ptbFotoAltaProd.Location = New System.Drawing.Point(811, 23)
        Me.ptbFotoAltaProd.Name = "ptbFotoAltaProd"
        Me.ptbFotoAltaProd.Size = New System.Drawing.Size(260, 250)
        Me.ptbFotoAltaProd.TabIndex = 0
        Me.ptbFotoAltaProd.TabStop = False
        '
        'tgpModProd
        '
        Me.tgpModProd.BackColor = System.Drawing.Color.Silver
        Me.tgpModProd.Controls.Add(Me.grbDatosProductosMod)
        Me.tgpModProd.Controls.Add(Me.GroupBox5)
        Me.tgpModProd.Controls.Add(Me.gpbModProductos)
        Me.tgpModProd.Location = New System.Drawing.Point(4, 22)
        Me.tgpModProd.Name = "tgpModProd"
        Me.tgpModProd.Padding = New System.Windows.Forms.Padding(3)
        Me.tgpModProd.Size = New System.Drawing.Size(1279, 653)
        Me.tgpModProd.TabIndex = 1
        Me.tgpModProd.Text = "Modificar Datos"
        '
        'grbDatosProductosMod
        '
        Me.grbDatosProductosMod.BackColor = System.Drawing.Color.Aquamarine
        Me.grbDatosProductosMod.Controls.Add(Me.dgvListaProdMod)
        Me.grbDatosProductosMod.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProductosMod.Location = New System.Drawing.Point(8, 446)
        Me.grbDatosProductosMod.Name = "grbDatosProductosMod"
        Me.grbDatosProductosMod.Size = New System.Drawing.Size(1260, 197)
        Me.grbDatosProductosMod.TabIndex = 26
        Me.grbDatosProductosMod.TabStop = False
        Me.grbDatosProductosMod.Text = "Datos Productos"
        '
        'dgvListaProdMod
        '
        Me.dgvListaProdMod.AllowUserToAddRows = False
        Me.dgvListaProdMod.AllowUserToDeleteRows = False
        Me.dgvListaProdMod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListaProdMod.Location = New System.Drawing.Point(8, 41)
        Me.dgvListaProdMod.Name = "dgvListaProdMod"
        Me.dgvListaProdMod.ReadOnly = True
        Me.dgvListaProdMod.Size = New System.Drawing.Size(1167, 150)
        Me.dgvListaProdMod.TabIndex = 29
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.LightSeaGreen
        Me.GroupBox5.Controls.Add(Me.chkMostrarTodos)
        Me.GroupBox5.Controls.Add(Me.chkFilProdSubCat)
        Me.GroupBox5.Controls.Add(Me.chkFilPorCat)
        Me.GroupBox5.Controls.Add(Me.chkFilPorNombre)
        Me.GroupBox5.Controls.Add(Me.ptbAyudaFiltroModProd)
        Me.GroupBox5.Controls.Add(Me.ptbAySubcatProdFil)
        Me.GroupBox5.Controls.Add(Me.ptbAyCatProdFil)
        Me.GroupBox5.Controls.Add(Me.ptbAyNombreProdFil)
        Me.GroupBox5.Controls.Add(Me.cmbFiltroSubProdAdm)
        Me.GroupBox5.Controls.Add(Me.cmbFiltroCatProdAdm)
        Me.GroupBox5.Controls.Add(Me.btnFiltrarModProd)
        Me.GroupBox5.Controls.Add(Me.Label53)
        Me.GroupBox5.Controls.Add(Me.Label54)
        Me.GroupBox5.Controls.Add(Me.txbNomModProdFil)
        Me.GroupBox5.Controls.Add(Me.Label55)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(919, 7)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(349, 433)
        Me.GroupBox5.TabIndex = 25
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Datos Para Filtración"
        '
        'chkMostrarTodos
        '
        Me.chkMostrarTodos.AutoSize = True
        Me.chkMostrarTodos.Location = New System.Drawing.Point(85, 337)
        Me.chkMostrarTodos.Name = "chkMostrarTodos"
        Me.chkMostrarTodos.Size = New System.Drawing.Size(228, 22)
        Me.chkMostrarTodos.TabIndex = 64
        Me.chkMostrarTodos.Text = "Mostrar Todos Los Productos"
        Me.chkMostrarTodos.UseVisualStyleBackColor = True
        '
        'chkFilProdSubCat
        '
        Me.chkFilProdSubCat.AutoSize = True
        Me.chkFilProdSubCat.Location = New System.Drawing.Point(130, 230)
        Me.chkFilProdSubCat.Name = "chkFilProdSubCat"
        Me.chkFilProdSubCat.Size = New System.Drawing.Size(183, 22)
        Me.chkFilProdSubCat.TabIndex = 63
        Me.chkFilProdSubCat.Text = "Filtrar Por Subcategoría"
        Me.chkFilProdSubCat.UseVisualStyleBackColor = True
        '
        'chkFilPorCat
        '
        Me.chkFilPorCat.AutoSize = True
        Me.chkFilPorCat.Location = New System.Drawing.Point(136, 122)
        Me.chkFilPorCat.Name = "chkFilPorCat"
        Me.chkFilPorCat.Size = New System.Drawing.Size(160, 22)
        Me.chkFilPorCat.TabIndex = 62
        Me.chkFilPorCat.Text = "Filtrar Por Categoría"
        Me.chkFilPorCat.UseVisualStyleBackColor = True
        '
        'chkFilPorNombre
        '
        Me.chkFilPorNombre.AutoSize = True
        Me.chkFilPorNombre.Location = New System.Drawing.Point(137, 23)
        Me.chkFilPorNombre.Name = "chkFilPorNombre"
        Me.chkFilPorNombre.Size = New System.Drawing.Size(150, 22)
        Me.chkFilPorNombre.TabIndex = 61
        Me.chkFilPorNombre.Text = "Filtrar Por Nombre"
        Me.chkFilPorNombre.UseVisualStyleBackColor = True
        '
        'ptbAyudaFiltroModProd
        '
        Me.ptbAyudaFiltroModProd.Image = CType(resources.GetObject("ptbAyudaFiltroModProd.Image"), System.Drawing.Image)
        Me.ptbAyudaFiltroModProd.Location = New System.Drawing.Point(243, 397)
        Me.ptbAyudaFiltroModProd.Name = "ptbAyudaFiltroModProd"
        Me.ptbAyudaFiltroModProd.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyudaFiltroModProd.TabIndex = 60
        Me.ptbAyudaFiltroModProd.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyudaFiltroModProd, resources.GetString("ptbAyudaFiltroModProd.ToolTip"))
        '
        'ptbAySubcatProdFil
        '
        Me.ptbAySubcatProdFil.Image = CType(resources.GetObject("ptbAySubcatProdFil.Image"), System.Drawing.Image)
        Me.ptbAySubcatProdFil.Location = New System.Drawing.Point(312, 284)
        Me.ptbAySubcatProdFil.Name = "ptbAySubcatProdFil"
        Me.ptbAySubcatProdFil.Size = New System.Drawing.Size(20, 20)
        Me.ptbAySubcatProdFil.TabIndex = 55
        Me.ptbAySubcatProdFil.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAySubcatProdFil, "Seleccione una subcategoria y elija la pestaña 'Filtrar Por Subcategoría'. Esto" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
        "filtrará mediante la categoría y subcategoría seleccionada.")
        '
        'ptbAyCatProdFil
        '
        Me.ptbAyCatProdFil.Image = CType(resources.GetObject("ptbAyCatProdFil.Image"), System.Drawing.Image)
        Me.ptbAyCatProdFil.Location = New System.Drawing.Point(312, 180)
        Me.ptbAyCatProdFil.Name = "ptbAyCatProdFil"
        Me.ptbAyCatProdFil.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyCatProdFil.TabIndex = 54
        Me.ptbAyCatProdFil.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyCatProdFil, "Seleccione una categoria y haga clic en la pestaña 'Filtrar Por Categoría'." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Esto" & _
        " buscará sólo por categoría")
        '
        'ptbAyNombreProdFil
        '
        Me.ptbAyNombreProdFil.Image = CType(resources.GetObject("ptbAyNombreProdFil.Image"), System.Drawing.Image)
        Me.ptbAyNombreProdFil.Location = New System.Drawing.Point(312, 67)
        Me.ptbAyNombreProdFil.Name = "ptbAyNombreProdFil"
        Me.ptbAyNombreProdFil.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyNombreProdFil.TabIndex = 52
        Me.ptbAyNombreProdFil.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyNombreProdFil, "Coloque el nombre del producto y seleccione la pestaña 'Filtrar Por Nombre'" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Sólo" & _
        " buscará por nombre.")
        '
        'cmbFiltroSubProdAdm
        '
        Me.cmbFiltroSubProdAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFiltroSubProdAdm.FormattingEnabled = True
        Me.cmbFiltroSubProdAdm.Location = New System.Drawing.Point(130, 278)
        Me.cmbFiltroSubProdAdm.Name = "cmbFiltroSubProdAdm"
        Me.cmbFiltroSubProdAdm.Size = New System.Drawing.Size(157, 26)
        Me.cmbFiltroSubProdAdm.TabIndex = 48
        '
        'cmbFiltroCatProdAdm
        '
        Me.cmbFiltroCatProdAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFiltroCatProdAdm.FormattingEnabled = True
        Me.cmbFiltroCatProdAdm.Location = New System.Drawing.Point(130, 174)
        Me.cmbFiltroCatProdAdm.Name = "cmbFiltroCatProdAdm"
        Me.cmbFiltroCatProdAdm.Size = New System.Drawing.Size(157, 26)
        Me.cmbFiltroCatProdAdm.TabIndex = 41
        '
        'btnFiltrarModProd
        '
        Me.btnFiltrarModProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFiltrarModProd.Location = New System.Drawing.Point(139, 387)
        Me.btnFiltrarModProd.Name = "btnFiltrarModProd"
        Me.btnFiltrarModProd.Size = New System.Drawing.Size(98, 40)
        Me.btnFiltrarModProd.TabIndex = 40
        Me.btnFiltrarModProd.Text = "Filtrar"
        Me.btnFiltrarModProd.UseVisualStyleBackColor = True
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(17, 286)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(95, 18)
        Me.Label53.TabIndex = 25
        Me.Label53.Text = "Subcategoría"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(6, 177)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(72, 18)
        Me.Label54.TabIndex = 23
        Me.Label54.Text = "Categoría"
        '
        'txbNomModProdFil
        '
        Me.txbNomModProdFil.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbNomModProdFil.Location = New System.Drawing.Point(139, 64)
        Me.txbNomModProdFil.Name = "txbNomModProdFil"
        Me.txbNomModProdFil.Size = New System.Drawing.Size(157, 24)
        Me.txbNomModProdFil.TabIndex = 21
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(6, 67)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(127, 18)
        Me.Label55.TabIndex = 20
        Me.Label55.Text = "Nombre Producto"
        '
        'gpbModProductos
        '
        Me.gpbModProductos.BackColor = System.Drawing.Color.DarkTurquoise
        Me.gpbModProductos.Controls.Add(Me.txbIdModProd)
        Me.gpbModProductos.Controls.Add(Me.lblTituloNroProd)
        Me.gpbModProductos.Controls.Add(Me.chkCambiarCatSubModProd)
        Me.gpbModProductos.Controls.Add(Me.txbRutaFotoModProd)
        Me.gpbModProductos.Controls.Add(Me.btnCancelarModProd)
        Me.gpbModProductos.Controls.Add(Me.chkModNombreProd)
        Me.gpbModProductos.Controls.Add(Me.lblRutaFotoModProd)
        Me.gpbModProductos.Controls.Add(Me.nudModGarProd)
        Me.gpbModProductos.Controls.Add(Me.nudModStockMinProd)
        Me.gpbModProductos.Controls.Add(Me.ptbAyudaGarModProd)
        Me.gpbModProductos.Controls.Add(Me.ptbAyudaStockMinModProd)
        Me.gpbModProductos.Controls.Add(Me.btnModProducto)
        Me.gpbModProductos.Controls.Add(Me.txbDescModProd)
        Me.gpbModProductos.Controls.Add(Me.ptbFotoModProd)
        Me.gpbModProductos.Controls.Add(Me.Label8)
        Me.gpbModProductos.Controls.Add(Me.Label9)
        Me.gpbModProductos.Controls.Add(Me.Label12)
        Me.gpbModProductos.Controls.Add(Me.cmbModSub)
        Me.gpbModProductos.Controls.Add(Me.Label13)
        Me.gpbModProductos.Controls.Add(Me.cmbModCat)
        Me.gpbModProductos.Controls.Add(Me.txbModProdNombre)
        Me.gpbModProductos.Controls.Add(Me.Label14)
        Me.gpbModProductos.Controls.Add(Me.btnFotoModProd)
        Me.gpbModProductos.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbModProductos.Location = New System.Drawing.Point(6, 6)
        Me.gpbModProductos.Name = "gpbModProductos"
        Me.gpbModProductos.Size = New System.Drawing.Size(907, 434)
        Me.gpbModProductos.TabIndex = 1
        Me.gpbModProductos.TabStop = False
        Me.gpbModProductos.Text = "Datos Del Producto Nuevo"
        '
        'txbIdModProd
        '
        Me.txbIdModProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbIdModProd.Location = New System.Drawing.Point(154, 28)
        Me.txbIdModProd.Name = "txbIdModProd"
        Me.txbIdModProd.ReadOnly = True
        Me.txbIdModProd.Size = New System.Drawing.Size(221, 26)
        Me.txbIdModProd.TabIndex = 48
        '
        'lblTituloNroProd
        '
        Me.lblTituloNroProd.AutoSize = True
        Me.lblTituloNroProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloNroProd.Location = New System.Drawing.Point(4, 31)
        Me.lblTituloNroProd.Name = "lblTituloNroProd"
        Me.lblTituloNroProd.Size = New System.Drawing.Size(133, 20)
        Me.lblTituloNroProd.TabIndex = 47
        Me.lblTituloNroProd.Text = "Número Producto"
        '
        'chkCambiarCatSubModProd
        '
        Me.chkCambiarCatSubModProd.AutoSize = True
        Me.chkCambiarCatSubModProd.Location = New System.Drawing.Point(381, 123)
        Me.chkCambiarCatSubModProd.Name = "chkCambiarCatSubModProd"
        Me.chkCambiarCatSubModProd.Size = New System.Drawing.Size(187, 22)
        Me.chkCambiarCatSubModProd.TabIndex = 46
        Me.chkCambiarCatSubModProd.Text = "¿Cambiar Clasificación?"
        Me.chkCambiarCatSubModProd.UseVisualStyleBackColor = True
        '
        'txbRutaFotoModProd
        '
        Me.txbRutaFotoModProd.Location = New System.Drawing.Point(574, 287)
        Me.txbRutaFotoModProd.Name = "txbRutaFotoModProd"
        Me.txbRutaFotoModProd.ReadOnly = True
        Me.txbRutaFotoModProd.Size = New System.Drawing.Size(317, 24)
        Me.txbRutaFotoModProd.TabIndex = 45
        '
        'btnCancelarModProd
        '
        Me.btnCancelarModProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelarModProd.Location = New System.Drawing.Point(774, 338)
        Me.btnCancelarModProd.Name = "btnCancelarModProd"
        Me.btnCancelarModProd.Size = New System.Drawing.Size(127, 49)
        Me.btnCancelarModProd.TabIndex = 44
        Me.btnCancelarModProd.Text = "Cancelar Modificación"
        Me.btnCancelarModProd.UseVisualStyleBackColor = True
        '
        'chkModNombreProd
        '
        Me.chkModNombreProd.AutoSize = True
        Me.chkModNombreProd.Location = New System.Drawing.Point(381, 63)
        Me.chkModNombreProd.Name = "chkModNombreProd"
        Me.chkModNombreProd.Size = New System.Drawing.Size(162, 22)
        Me.chkModNombreProd.TabIndex = 43
        Me.chkModNombreProd.Text = "¿Modificar Nombre?"
        Me.chkModNombreProd.UseVisualStyleBackColor = True
        '
        'lblRutaFotoModProd
        '
        Me.lblRutaFotoModProd.AutoSize = True
        Me.lblRutaFotoModProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRutaFotoModProd.ForeColor = System.Drawing.Color.Red
        Me.lblRutaFotoModProd.Location = New System.Drawing.Point(500, 290)
        Me.lblRutaFotoModProd.Name = "lblRutaFotoModProd"
        Me.lblRutaFotoModProd.Size = New System.Drawing.Size(78, 18)
        Me.lblRutaFotoModProd.TabIndex = 42
        Me.lblRutaFotoModProd.Text = "Ruta Foto "
        '
        'nudModGarProd
        '
        Me.nudModGarProd.Location = New System.Drawing.Point(187, 220)
        Me.nudModGarProd.Name = "nudModGarProd"
        Me.nudModGarProd.Size = New System.Drawing.Size(120, 24)
        Me.nudModGarProd.TabIndex = 41
        '
        'nudModStockMinProd
        '
        Me.nudModStockMinProd.Location = New System.Drawing.Point(187, 181)
        Me.nudModStockMinProd.Name = "nudModStockMinProd"
        Me.nudModStockMinProd.Size = New System.Drawing.Size(120, 24)
        Me.nudModStockMinProd.TabIndex = 40
        '
        'ptbAyudaGarModProd
        '
        Me.ptbAyudaGarModProd.Image = CType(resources.GetObject("ptbAyudaGarModProd.Image"), System.Drawing.Image)
        Me.ptbAyudaGarModProd.Location = New System.Drawing.Point(313, 220)
        Me.ptbAyudaGarModProd.Name = "ptbAyudaGarModProd"
        Me.ptbAyudaGarModProd.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyudaGarModProd.TabIndex = 39
        Me.ptbAyudaGarModProd.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyudaGarModProd, "Por favor, coloque la garantía en días. Un garantía de cero días indica que el " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
        "producto no tiene garantía.")
        '
        'ptbAyudaStockMinModProd
        '
        Me.ptbAyudaStockMinModProd.Image = CType(resources.GetObject("ptbAyudaStockMinModProd.Image"), System.Drawing.Image)
        Me.ptbAyudaStockMinModProd.Location = New System.Drawing.Point(313, 181)
        Me.ptbAyudaStockMinModProd.Name = "ptbAyudaStockMinModProd"
        Me.ptbAyudaStockMinModProd.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyudaStockMinModProd.TabIndex = 38
        Me.ptbAyudaStockMinModProd.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyudaStockMinModProd, "Coloque el stock mínimo del producto. Dicho valor no puede ser cero. Por favor," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
        "no intente colocar valores distintos a números mayores a cero.")
        '
        'btnModProducto
        '
        Me.btnModProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModProducto.Location = New System.Drawing.Point(500, 338)
        Me.btnModProducto.Name = "btnModProducto"
        Me.btnModProducto.Size = New System.Drawing.Size(127, 49)
        Me.btnModProducto.TabIndex = 23
        Me.btnModProducto.Text = "Modificar Datos"
        Me.btnModProducto.UseVisualStyleBackColor = True
        '
        'txbDescModProd
        '
        Me.txbDescModProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbDescModProd.Location = New System.Drawing.Point(4, 257)
        Me.txbDescModProd.MaxLength = 250
        Me.txbDescModProd.Multiline = True
        Me.txbDescModProd.Name = "txbDescModProd"
        Me.txbDescModProd.Size = New System.Drawing.Size(480, 171)
        Me.txbDescModProd.TabIndex = 21
        '
        'ptbFotoModProd
        '
        Me.ptbFotoModProd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ptbFotoModProd.Location = New System.Drawing.Point(584, 19)
        Me.ptbFotoModProd.Name = "ptbFotoModProd"
        Me.ptbFotoModProd.Size = New System.Drawing.Size(260, 250)
        Me.ptbFotoModProd.TabIndex = 20
        Me.ptbFotoModProd.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 220)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(136, 20)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Garantía (en días)"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 181)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 20)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "Stock Mínimo"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(6, 134)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(104, 20)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "Subcategoría"
        '
        'cmbModSub
        '
        Me.cmbModSub.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbModSub.FormattingEnabled = True
        Me.cmbModSub.Location = New System.Drawing.Point(154, 134)
        Me.cmbModSub.Name = "cmbModSub"
        Me.cmbModSub.Size = New System.Drawing.Size(221, 28)
        Me.cmbModSub.TabIndex = 6
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(2, 97)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 20)
        Me.Label13.TabIndex = 5
        Me.Label13.Text = "Categoría"
        '
        'cmbModCat
        '
        Me.cmbModCat.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbModCat.FormattingEnabled = True
        Me.cmbModCat.Location = New System.Drawing.Point(154, 97)
        Me.cmbModCat.Name = "cmbModCat"
        Me.cmbModCat.Size = New System.Drawing.Size(221, 28)
        Me.cmbModCat.TabIndex = 4
        '
        'txbModProdNombre
        '
        Me.txbModProdNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txbModProdNombre.Location = New System.Drawing.Point(154, 60)
        Me.txbModProdNombre.Name = "txbModProdNombre"
        Me.txbModProdNombre.Size = New System.Drawing.Size(221, 26)
        Me.txbModProdNombre.TabIndex = 3
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(2, 63)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(133, 20)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Nombre Producto"
        '
        'btnFotoModProd
        '
        Me.btnFotoModProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFotoModProd.Location = New System.Drawing.Point(641, 338)
        Me.btnFotoModProd.Name = "btnFotoModProd"
        Me.btnFotoModProd.Size = New System.Drawing.Size(127, 49)
        Me.btnFotoModProd.TabIndex = 1
        Me.btnFotoModProd.Text = "Buscar Foto"
        Me.btnFotoModProd.UseVisualStyleBackColor = True
        '
        'tpgNuevaCat
        '
        Me.tpgNuevaCat.BackColor = System.Drawing.Color.Silver
        Me.tpgNuevaCat.Controls.Add(Me.grbNuevaCatSub)
        Me.tpgNuevaCat.Controls.Add(Me.grbNuevaSubcat)
        Me.tpgNuevaCat.Controls.Add(Me.grbNuevaCategoria)
        Me.tpgNuevaCat.Location = New System.Drawing.Point(4, 22)
        Me.tpgNuevaCat.Name = "tpgNuevaCat"
        Me.tpgNuevaCat.Size = New System.Drawing.Size(1279, 653)
        Me.tpgNuevaCat.TabIndex = 4
        Me.tpgNuevaCat.Text = "Agregar Categorias"
        '
        'grbNuevaCatSub
        '
        Me.grbNuevaCatSub.BackColor = System.Drawing.Color.MediumAquamarine
        Me.grbNuevaCatSub.Controls.Add(Me.ckbModCatSub)
        Me.grbNuevaCatSub.Controls.Add(Me.ptbAyNuevaCatSub)
        Me.grbNuevaCatSub.Controls.Add(Me.btnCancelarModCatSub)
        Me.grbNuevaCatSub.Controls.Add(Me.btnModificarCatSub)
        Me.grbNuevaCatSub.Controls.Add(Me.Label10)
        Me.grbNuevaCatSub.Controls.Add(Me.txbDescCatSub)
        Me.grbNuevaCatSub.Controls.Add(Me.cmbNuevasSubCatAdm)
        Me.grbNuevaCatSub.Controls.Add(Me.cmbNuevasCatAdm)
        Me.grbNuevaCatSub.Controls.Add(Me.btnAgregarNuevaCatSub)
        Me.grbNuevaCatSub.Controls.Add(Me.Label46)
        Me.grbNuevaCatSub.Controls.Add(Me.Label47)
        Me.grbNuevaCatSub.Controls.Add(Me.dgvCatSubAdm)
        Me.grbNuevaCatSub.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbNuevaCatSub.Location = New System.Drawing.Point(3, 430)
        Me.grbNuevaCatSub.Name = "grbNuevaCatSub"
        Me.grbNuevaCatSub.Size = New System.Drawing.Size(1268, 220)
        Me.grbNuevaCatSub.TabIndex = 34
        Me.grbNuevaCatSub.TabStop = False
        Me.grbNuevaCatSub.Text = "Datos Categpría/Subcategoría"
        '
        'ckbModCatSub
        '
        Me.ckbModCatSub.AutoSize = True
        Me.ckbModCatSub.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbModCatSub.Location = New System.Drawing.Point(1023, 183)
        Me.ckbModCatSub.Name = "ckbModCatSub"
        Me.ckbModCatSub.Size = New System.Drawing.Size(237, 17)
        Me.ckbModCatSub.TabIndex = 70
        Me.ckbModCatSub.Text = "¿Modificar categoria o subcategoria?"
        Me.ckbModCatSub.UseVisualStyleBackColor = True
        '
        'ptbAyNuevaCatSub
        '
        Me.ptbAyNuevaCatSub.Image = CType(resources.GetObject("ptbAyNuevaCatSub.Image"), System.Drawing.Image)
        Me.ptbAyNuevaCatSub.Location = New System.Drawing.Point(715, 71)
        Me.ptbAyNuevaCatSub.Name = "ptbAyNuevaCatSub"
        Me.ptbAyNuevaCatSub.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyNuevaCatSub.TabIndex = 69
        Me.ptbAyNuevaCatSub.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyNuevaCatSub, resources.GetString("ptbAyNuevaCatSub.ToolTip"))
        '
        'btnCancelarModCatSub
        '
        Me.btnCancelarModCatSub.Location = New System.Drawing.Point(895, 172)
        Me.btnCancelarModCatSub.Name = "btnCancelarModCatSub"
        Me.btnCancelarModCatSub.Size = New System.Drawing.Size(122, 43)
        Me.btnCancelarModCatSub.TabIndex = 37
        Me.btnCancelarModCatSub.Text = "Cancelar"
        Me.btnCancelarModCatSub.UseVisualStyleBackColor = True
        '
        'btnModificarCatSub
        '
        Me.btnModificarCatSub.Location = New System.Drawing.Point(750, 172)
        Me.btnModificarCatSub.Name = "btnModificarCatSub"
        Me.btnModificarCatSub.Size = New System.Drawing.Size(122, 43)
        Me.btnModificarCatSub.TabIndex = 36
        Me.btnModificarCatSub.Text = "Modificar"
        Me.btnModificarCatSub.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(622, 71)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 18)
        Me.Label10.TabIndex = 35
        Me.Label10.Text = "Descripción"
        '
        'txbDescCatSub
        '
        Me.txbDescCatSub.Location = New System.Drawing.Point(622, 92)
        Me.txbDescCatSub.MaxLength = 100
        Me.txbDescCatSub.Multiline = True
        Me.txbDescCatSub.Name = "txbDescCatSub"
        Me.txbDescCatSub.Size = New System.Drawing.Size(640, 74)
        Me.txbDescCatSub.TabIndex = 33
        '
        'cmbNuevasSubCatAdm
        '
        Me.cmbNuevasSubCatAdm.FormattingEnabled = True
        Me.cmbNuevasSubCatAdm.Location = New System.Drawing.Point(1080, 29)
        Me.cmbNuevasSubCatAdm.Name = "cmbNuevasSubCatAdm"
        Me.cmbNuevasSubCatAdm.Size = New System.Drawing.Size(182, 26)
        Me.cmbNuevasSubCatAdm.TabIndex = 34
        '
        'cmbNuevasCatAdm
        '
        Me.cmbNuevasCatAdm.FormattingEnabled = True
        Me.cmbNuevasCatAdm.Location = New System.Drawing.Point(709, 29)
        Me.cmbNuevasCatAdm.Name = "cmbNuevasCatAdm"
        Me.cmbNuevasCatAdm.Size = New System.Drawing.Size(182, 26)
        Me.cmbNuevasCatAdm.TabIndex = 33
        '
        'btnAgregarNuevaCatSub
        '
        Me.btnAgregarNuevaCatSub.Location = New System.Drawing.Point(622, 172)
        Me.btnAgregarNuevaCatSub.Name = "btnAgregarNuevaCatSub"
        Me.btnAgregarNuevaCatSub.Size = New System.Drawing.Size(122, 43)
        Me.btnAgregarNuevaCatSub.TabIndex = 32
        Me.btnAgregarNuevaCatSub.Text = "Agregar "
        Me.btnAgregarNuevaCatSub.UseVisualStyleBackColor = True
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(971, 32)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(103, 18)
        Me.Label46.TabIndex = 30
        Me.Label46.Text = "Subcategorías"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(623, 32)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(80, 18)
        Me.Label47.TabIndex = 28
        Me.Label47.Text = "Categorías"
        '
        'dgvCatSubAdm
        '
        Me.dgvCatSubAdm.AllowUserToAddRows = False
        Me.dgvCatSubAdm.AllowUserToDeleteRows = False
        Me.dgvCatSubAdm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCatSubAdm.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idCat_CatSub, Me.idSub_CatSub, Me.descripcionCatSub})
        Me.dgvCatSubAdm.Location = New System.Drawing.Point(7, 22)
        Me.dgvCatSubAdm.MultiSelect = False
        Me.dgvCatSubAdm.Name = "dgvCatSubAdm"
        Me.dgvCatSubAdm.ReadOnly = True
        Me.dgvCatSubAdm.Size = New System.Drawing.Size(610, 192)
        Me.dgvCatSubAdm.TabIndex = 27
        '
        'idCat_CatSub
        '
        Me.idCat_CatSub.HeaderText = "Categoria"
        Me.idCat_CatSub.Name = "idCat_CatSub"
        Me.idCat_CatSub.ReadOnly = True
        '
        'idSub_CatSub
        '
        Me.idSub_CatSub.HeaderText = "Subcategoria"
        Me.idSub_CatSub.Name = "idSub_CatSub"
        Me.idSub_CatSub.ReadOnly = True
        '
        'descripcionCatSub
        '
        Me.descripcionCatSub.HeaderText = "Descripcion"
        Me.descripcionCatSub.Name = "descripcionCatSub"
        Me.descripcionCatSub.ReadOnly = True
        '
        'grbNuevaSubcat
        '
        Me.grbNuevaSubcat.BackColor = System.Drawing.Color.PaleGreen
        Me.grbNuevaSubcat.Controls.Add(Me.ckbModDatosSubcat)
        Me.grbNuevaSubcat.Controls.Add(Me.ptbAyNuevaSubcat)
        Me.grbNuevaSubcat.Controls.Add(Me.btnCancelarModSubcat)
        Me.grbNuevaSubcat.Controls.Add(Me.btnModificarSubCat)
        Me.grbNuevaSubcat.Controls.Add(Me.btnAgregarNuevaSubCat)
        Me.grbNuevaSubcat.Controls.Add(Me.txbDescripcionNuevaSubCat)
        Me.grbNuevaSubcat.Controls.Add(Me.Label44)
        Me.grbNuevaSubcat.Controls.Add(Me.txbNuevaSubcat)
        Me.grbNuevaSubcat.Controls.Add(Me.Label45)
        Me.grbNuevaSubcat.Controls.Add(Me.dgvSubAdm)
        Me.grbNuevaSubcat.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbNuevaSubcat.Location = New System.Drawing.Point(3, 217)
        Me.grbNuevaSubcat.Name = "grbNuevaSubcat"
        Me.grbNuevaSubcat.Size = New System.Drawing.Size(1268, 208)
        Me.grbNuevaSubcat.TabIndex = 33
        Me.grbNuevaSubcat.TabStop = False
        Me.grbNuevaSubcat.Text = "Datos De Las Subcategorías"
        '
        'ckbModDatosSubcat
        '
        Me.ckbModDatosSubcat.AutoSize = True
        Me.ckbModDatosSubcat.Location = New System.Drawing.Point(781, 15)
        Me.ckbModDatosSubcat.Name = "ckbModDatosSubcat"
        Me.ckbModDatosSubcat.Size = New System.Drawing.Size(162, 22)
        Me.ckbModDatosSubcat.TabIndex = 69
        Me.ckbModDatosSubcat.Text = "¿Modificar Nombre?"
        Me.ckbModDatosSubcat.UseVisualStyleBackColor = True
        '
        'ptbAyNuevaSubcat
        '
        Me.ptbAyNuevaSubcat.Image = CType(resources.GetObject("ptbAyNuevaSubcat.Image"), System.Drawing.Image)
        Me.ptbAyNuevaSubcat.Location = New System.Drawing.Point(997, 40)
        Me.ptbAyNuevaSubcat.Name = "ptbAyNuevaSubcat"
        Me.ptbAyNuevaSubcat.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyNuevaSubcat.TabIndex = 68
        Me.ptbAyNuevaSubcat.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyNuevaSubcat, resources.GetString("ptbAyNuevaSubcat.ToolTip"))
        '
        'btnCancelarModSubcat
        '
        Me.btnCancelarModSubcat.Location = New System.Drawing.Point(1049, 146)
        Me.btnCancelarModSubcat.Name = "btnCancelarModSubcat"
        Me.btnCancelarModSubcat.Size = New System.Drawing.Size(192, 61)
        Me.btnCancelarModSubcat.TabIndex = 35
        Me.btnCancelarModSubcat.Text = "Cancelar Modificación"
        Me.btnCancelarModSubcat.UseVisualStyleBackColor = True
        '
        'btnModificarSubCat
        '
        Me.btnModificarSubCat.Location = New System.Drawing.Point(1049, 79)
        Me.btnModificarSubCat.Name = "btnModificarSubCat"
        Me.btnModificarSubCat.Size = New System.Drawing.Size(192, 61)
        Me.btnModificarSubCat.TabIndex = 34
        Me.btnModificarSubCat.Text = "Modificar Subcategoria"
        Me.btnModificarSubCat.UseVisualStyleBackColor = True
        '
        'btnAgregarNuevaSubCat
        '
        Me.btnAgregarNuevaSubCat.Location = New System.Drawing.Point(1049, 12)
        Me.btnAgregarNuevaSubCat.Name = "btnAgregarNuevaSubCat"
        Me.btnAgregarNuevaSubCat.Size = New System.Drawing.Size(192, 61)
        Me.btnAgregarNuevaSubCat.TabIndex = 32
        Me.btnAgregarNuevaSubCat.Text = "Agregar Nueva Subcategoría"
        Me.btnAgregarNuevaSubCat.UseVisualStyleBackColor = True
        '
        'txbDescripcionNuevaSubCat
        '
        Me.txbDescripcionNuevaSubCat.Location = New System.Drawing.Point(622, 111)
        Me.txbDescripcionNuevaSubCat.MaxLength = 100
        Me.txbDescripcionNuevaSubCat.Multiline = True
        Me.txbDescripcionNuevaSubCat.Name = "txbDescripcionNuevaSubCat"
        Me.txbDescripcionNuevaSubCat.Size = New System.Drawing.Size(369, 89)
        Me.txbDescripcionNuevaSubCat.TabIndex = 31
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(619, 90)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(178, 18)
        Me.Label44.TabIndex = 30
        Me.Label44.Text = "Descripcion Subcategoría"
        '
        'txbNuevaSubcat
        '
        Me.txbNuevaSubcat.Location = New System.Drawing.Point(622, 40)
        Me.txbNuevaSubcat.Name = "txbNuevaSubcat"
        Me.txbNuevaSubcat.Size = New System.Drawing.Size(369, 24)
        Me.txbNuevaSubcat.TabIndex = 29
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(622, 19)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(153, 18)
        Me.Label45.TabIndex = 28
        Me.Label45.Text = "Nombre Subcategoría"
        '
        'dgvSubAdm
        '
        Me.dgvSubAdm.AllowUserToAddRows = False
        Me.dgvSubAdm.AllowUserToDeleteRows = False
        Me.dgvSubAdm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSubAdm.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idSubcategoria, Me.nombreSubcategoria, Me.descripcionSub})
        Me.dgvSubAdm.Location = New System.Drawing.Point(6, 19)
        Me.dgvSubAdm.MultiSelect = False
        Me.dgvSubAdm.Name = "dgvSubAdm"
        Me.dgvSubAdm.ReadOnly = True
        Me.dgvSubAdm.Size = New System.Drawing.Size(610, 181)
        Me.dgvSubAdm.TabIndex = 27
        '
        'idSubcategoria
        '
        Me.idSubcategoria.HeaderText = "Subcategoria"
        Me.idSubcategoria.Name = "idSubcategoria"
        Me.idSubcategoria.ReadOnly = True
        '
        'nombreSubcategoria
        '
        Me.nombreSubcategoria.HeaderText = "Nombre Subcategoria"
        Me.nombreSubcategoria.Name = "nombreSubcategoria"
        Me.nombreSubcategoria.ReadOnly = True
        '
        'descripcionSub
        '
        Me.descripcionSub.HeaderText = "Descripcion"
        Me.descripcionSub.Name = "descripcionSub"
        Me.descripcionSub.ReadOnly = True
        '
        'grbNuevaCategoria
        '
        Me.grbNuevaCategoria.BackColor = System.Drawing.Color.LightGreen
        Me.grbNuevaCategoria.Controls.Add(Me.ckbModificarNombreCat)
        Me.grbNuevaCategoria.Controls.Add(Me.ptbAyNuevaCat)
        Me.grbNuevaCategoria.Controls.Add(Me.btnCancelarModCat)
        Me.grbNuevaCategoria.Controls.Add(Me.btnModificarCat)
        Me.grbNuevaCategoria.Controls.Add(Me.btnAgregarNuevaCat)
        Me.grbNuevaCategoria.Controls.Add(Me.txbDescripcionNuevaCat)
        Me.grbNuevaCategoria.Controls.Add(Me.Label43)
        Me.grbNuevaCategoria.Controls.Add(Me.txbNuevaCategoria)
        Me.grbNuevaCategoria.Controls.Add(Me.Label42)
        Me.grbNuevaCategoria.Controls.Add(Me.dgvCategoriasAdm)
        Me.grbNuevaCategoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbNuevaCategoria.Location = New System.Drawing.Point(3, 3)
        Me.grbNuevaCategoria.Name = "grbNuevaCategoria"
        Me.grbNuevaCategoria.Size = New System.Drawing.Size(1268, 208)
        Me.grbNuevaCategoria.TabIndex = 0
        Me.grbNuevaCategoria.TabStop = False
        Me.grbNuevaCategoria.Text = "Datos De Las Categorías"
        '
        'ckbModificarNombreCat
        '
        Me.ckbModificarNombreCat.AutoSize = True
        Me.ckbModificarNombreCat.Location = New System.Drawing.Point(758, 15)
        Me.ckbModificarNombreCat.Name = "ckbModificarNombreCat"
        Me.ckbModificarNombreCat.Size = New System.Drawing.Size(162, 22)
        Me.ckbModificarNombreCat.TabIndex = 68
        Me.ckbModificarNombreCat.Text = "¿Modificar Nombre?"
        Me.ckbModificarNombreCat.UseVisualStyleBackColor = True
        '
        'ptbAyNuevaCat
        '
        Me.ptbAyNuevaCat.Image = CType(resources.GetObject("ptbAyNuevaCat.Image"), System.Drawing.Image)
        Me.ptbAyNuevaCat.Location = New System.Drawing.Point(1001, 41)
        Me.ptbAyNuevaCat.Name = "ptbAyNuevaCat"
        Me.ptbAyNuevaCat.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyNuevaCat.TabIndex = 67
        Me.ptbAyNuevaCat.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyNuevaCat, resources.GetString("ptbAyNuevaCat.ToolTip"))
        '
        'btnCancelarModCat
        '
        Me.btnCancelarModCat.Location = New System.Drawing.Point(1049, 141)
        Me.btnCancelarModCat.Name = "btnCancelarModCat"
        Me.btnCancelarModCat.Size = New System.Drawing.Size(192, 61)
        Me.btnCancelarModCat.TabIndex = 34
        Me.btnCancelarModCat.Text = "Cancelar Modificación"
        Me.btnCancelarModCat.UseVisualStyleBackColor = True
        '
        'btnModificarCat
        '
        Me.btnModificarCat.Location = New System.Drawing.Point(1049, 80)
        Me.btnModificarCat.Name = "btnModificarCat"
        Me.btnModificarCat.Size = New System.Drawing.Size(192, 61)
        Me.btnModificarCat.TabIndex = 33
        Me.btnModificarCat.Text = "Modificar Categoria"
        Me.btnModificarCat.UseVisualStyleBackColor = True
        '
        'btnAgregarNuevaCat
        '
        Me.btnAgregarNuevaCat.Location = New System.Drawing.Point(1049, 19)
        Me.btnAgregarNuevaCat.Name = "btnAgregarNuevaCat"
        Me.btnAgregarNuevaCat.Size = New System.Drawing.Size(192, 61)
        Me.btnAgregarNuevaCat.TabIndex = 32
        Me.btnAgregarNuevaCat.Text = "Agregar Nueva Categorias"
        Me.btnAgregarNuevaCat.UseVisualStyleBackColor = True
        '
        'txbDescripcionNuevaCat
        '
        Me.txbDescripcionNuevaCat.Location = New System.Drawing.Point(622, 102)
        Me.txbDescripcionNuevaCat.MaxLength = 100
        Me.txbDescripcionNuevaCat.Multiline = True
        Me.txbDescripcionNuevaCat.Name = "txbDescripcionNuevaCat"
        Me.txbDescripcionNuevaCat.Size = New System.Drawing.Size(369, 100)
        Me.txbDescripcionNuevaCat.TabIndex = 31
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(622, 78)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(155, 18)
        Me.Label43.TabIndex = 30
        Me.Label43.Text = "Descripcion Categoria"
        '
        'txbNuevaCategoria
        '
        Me.txbNuevaCategoria.Location = New System.Drawing.Point(622, 37)
        Me.txbNuevaCategoria.Name = "txbNuevaCategoria"
        Me.txbNuevaCategoria.Size = New System.Drawing.Size(369, 24)
        Me.txbNuevaCategoria.TabIndex = 29
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(622, 19)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(130, 18)
        Me.Label42.TabIndex = 28
        Me.Label42.Text = "Nombre Categoria"
        '
        'dgvCategoriasAdm
        '
        Me.dgvCategoriasAdm.AllowUserToAddRows = False
        Me.dgvCategoriasAdm.AllowUserToDeleteRows = False
        Me.dgvCategoriasAdm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCategoriasAdm.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idCategoria, Me.nombreCategoria, Me.descripcionCategoria})
        Me.dgvCategoriasAdm.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvCategoriasAdm.Location = New System.Drawing.Point(6, 19)
        Me.dgvCategoriasAdm.MultiSelect = False
        Me.dgvCategoriasAdm.Name = "dgvCategoriasAdm"
        Me.dgvCategoriasAdm.ReadOnly = True
        Me.dgvCategoriasAdm.Size = New System.Drawing.Size(610, 183)
        Me.dgvCategoriasAdm.TabIndex = 27
        '
        'idCategoria
        '
        Me.idCategoria.HeaderText = "Categoria"
        Me.idCategoria.Name = "idCategoria"
        Me.idCategoria.ReadOnly = True
        '
        'nombreCategoria
        '
        Me.nombreCategoria.HeaderText = "Nombre Categoria"
        Me.nombreCategoria.Name = "nombreCategoria"
        Me.nombreCategoria.ReadOnly = True
        '
        'descripcionCategoria
        '
        Me.descripcionCategoria.HeaderText = "Descripcion"
        Me.descripcionCategoria.Name = "descripcionCategoria"
        Me.descripcionCategoria.ReadOnly = True
        '
        'tpgProveedores
        '
        Me.tpgProveedores.BackColor = System.Drawing.Color.DarkGray
        Me.tpgProveedores.Controls.Add(Me.grbModDatosProv)
        Me.tpgProveedores.Controls.Add(Me.grbDatosProveedor)
        Me.tpgProveedores.Controls.Add(Me.grbProveedores)
        Me.tpgProveedores.Location = New System.Drawing.Point(4, 22)
        Me.tpgProveedores.Name = "tpgProveedores"
        Me.tpgProveedores.Size = New System.Drawing.Size(1279, 653)
        Me.tpgProveedores.TabIndex = 5
        Me.tpgProveedores.Text = "Proveedores"
        '
        'grbModDatosProv
        '
        Me.grbModDatosProv.BackColor = System.Drawing.Color.LightSeaGreen
        Me.grbModDatosProv.Controls.Add(Me.txbIdModProv)
        Me.grbModDatosProv.Controls.Add(Me.lblTituloNroModProv)
        Me.grbModDatosProv.Controls.Add(Me.chkModNombreProve)
        Me.grbModDatosProv.Controls.Add(Me.ptbAyNomModProv)
        Me.grbModDatosProv.Controls.Add(Me.ptbAyDirModProv)
        Me.grbModDatosProv.Controls.Add(Me.Label41)
        Me.grbModDatosProv.Controls.Add(Me.Label39)
        Me.grbModDatosProv.Controls.Add(Me.txbCodAreaModProv)
        Me.grbModDatosProv.Controls.Add(Me.PictureBox14)
        Me.grbModDatosProv.Controls.Add(Me.PictureBox15)
        Me.grbModDatosProv.Controls.Add(Me.PictureBox16)
        Me.grbModDatosProv.Controls.Add(Me.btnModProv)
        Me.grbModDatosProv.Controls.Add(Me.txbMailModProv)
        Me.grbModDatosProv.Controls.Add(Me.txbCelModProv)
        Me.grbModDatosProv.Controls.Add(Me.txbTelModProv)
        Me.grbModDatosProv.Controls.Add(Me.txbDirModProv)
        Me.grbModDatosProv.Controls.Add(Me.txbNombreModProv)
        Me.grbModDatosProv.Controls.Add(Me.Label66)
        Me.grbModDatosProv.Controls.Add(Me.Label67)
        Me.grbModDatosProv.Controls.Add(Me.Label68)
        Me.grbModDatosProv.Controls.Add(Me.Label69)
        Me.grbModDatosProv.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbModDatosProv.Location = New System.Drawing.Point(593, 3)
        Me.grbModDatosProv.Name = "grbModDatosProv"
        Me.grbModDatosProv.Size = New System.Drawing.Size(675, 299)
        Me.grbModDatosProv.TabIndex = 11
        Me.grbModDatosProv.TabStop = False
        Me.grbModDatosProv.Text = "Modificar Datos"
        '
        'txbIdModProv
        '
        Me.txbIdModProv.Location = New System.Drawing.Point(273, 72)
        Me.txbIdModProv.Name = "txbIdModProv"
        Me.txbIdModProv.ReadOnly = True
        Me.txbIdModProv.Size = New System.Drawing.Size(98, 24)
        Me.txbIdModProv.TabIndex = 68
        '
        'lblTituloNroModProv
        '
        Me.lblTituloNroModProv.AutoSize = True
        Me.lblTituloNroModProv.Location = New System.Drawing.Point(6, 72)
        Me.lblTituloNroModProv.Name = "lblTituloNroModProv"
        Me.lblTituloNroModProv.Size = New System.Drawing.Size(135, 18)
        Me.lblTituloNroModProv.TabIndex = 67
        Me.lblTituloNroModProv.Text = "Número Proveedor"
        '
        'chkModNombreProve
        '
        Me.chkModNombreProve.AutoSize = True
        Me.chkModNombreProve.Location = New System.Drawing.Point(273, 14)
        Me.chkModNombreProve.Name = "chkModNombreProve"
        Me.chkModNombreProve.Size = New System.Drawing.Size(162, 22)
        Me.chkModNombreProve.TabIndex = 66
        Me.chkModNombreProve.Text = "¿Modificar Nombre?"
        Me.chkModNombreProve.UseVisualStyleBackColor = True
        '
        'ptbAyNomModProv
        '
        Me.ptbAyNomModProv.Image = CType(resources.GetObject("ptbAyNomModProv.Image"), System.Drawing.Image)
        Me.ptbAyNomModProv.Location = New System.Drawing.Point(649, 30)
        Me.ptbAyNomModProv.Name = "ptbAyNomModProv"
        Me.ptbAyNomModProv.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyNomModProv.TabIndex = 65
        Me.ptbAyNomModProv.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyNomModProv, "Coloque el nombre usando sólo letras y espacios en blanco. No coloque simbolos" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "e" & _
        "speciales, puntos o comas.")
        '
        'ptbAyDirModProv
        '
        Me.ptbAyDirModProv.Image = CType(resources.GetObject("ptbAyDirModProv.Image"), System.Drawing.Image)
        Me.ptbAyDirModProv.Location = New System.Drawing.Point(649, 105)
        Me.ptbAyDirModProv.Name = "ptbAyDirModProv"
        Me.ptbAyDirModProv.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyDirModProv.TabIndex = 62
        Me.ptbAyDirModProv.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyDirModProv, resources.GetString("ptbAyDirModProv.ToolTip"))
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(397, 177)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(66, 36)
        Me.Label41.TabIndex = 61
        Me.Label41.Text = "Número " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Celular" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(6, 178)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(94, 33)
        Me.Label39.TabIndex = 60
        Me.Label39.Text = "Código Area " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Celular" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txbCodAreaModProv
        '
        Me.txbCodAreaModProv.Location = New System.Drawing.Point(273, 181)
        Me.txbCodAreaModProv.Name = "txbCodAreaModProv"
        Me.txbCodAreaModProv.Size = New System.Drawing.Size(118, 24)
        Me.txbCodAreaModProv.TabIndex = 59
        '
        'PictureBox14
        '
        Me.PictureBox14.Image = CType(resources.GetObject("PictureBox14.Image"), System.Drawing.Image)
        Me.PictureBox14.Location = New System.Drawing.Point(649, 223)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox14.TabIndex = 58
        Me.PictureBox14.TabStop = False
        Me.tpAyudas.SetToolTip(Me.PictureBox14, "Por favor, corrobore que el mail ingresado cumpla con el siguiente formato:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """nom" & _
        "bre@dominio.extensión""" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Los dominios pueden ser: .com, .org, etc." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Las extension" & _
        "es pueden ser: .ar, .br, etc.")
        '
        'PictureBox15
        '
        Me.PictureBox15.Image = CType(resources.GetObject("PictureBox15.Image"), System.Drawing.Image)
        Me.PictureBox15.Location = New System.Drawing.Point(649, 188)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox15.TabIndex = 57
        Me.PictureBox15.TabStop = False
        Me.tpAyudas.SetToolTip(Me.PictureBox15, "Por favor, coloque sólo valores numéricos. No separe los códigos de área con" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """-""" & _
        "u otros símbolos.")
        '
        'PictureBox16
        '
        Me.PictureBox16.Image = CType(resources.GetObject("PictureBox16.Image"), System.Drawing.Image)
        Me.PictureBox16.Location = New System.Drawing.Point(649, 141)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox16.TabIndex = 56
        Me.PictureBox16.TabStop = False
        Me.tpAyudas.SetToolTip(Me.PictureBox16, "Por favor, coloque sólo valores numéricos.")
        '
        'btnModProv
        '
        Me.btnModProv.Location = New System.Drawing.Point(112, 241)
        Me.btnModProv.Name = "btnModProv"
        Me.btnModProv.Size = New System.Drawing.Size(144, 41)
        Me.btnModProv.TabIndex = 10
        Me.btnModProv.Text = "Modificar Datos"
        Me.btnModProv.UseVisualStyleBackColor = True
        '
        'txbMailModProv
        '
        Me.txbMailModProv.Location = New System.Drawing.Point(273, 222)
        Me.txbMailModProv.Name = "txbMailModProv"
        Me.txbMailModProv.Size = New System.Drawing.Size(370, 24)
        Me.txbMailModProv.TabIndex = 9
        '
        'txbCelModProv
        '
        Me.txbCelModProv.Location = New System.Drawing.Point(469, 183)
        Me.txbCelModProv.Name = "txbCelModProv"
        Me.txbCelModProv.Size = New System.Drawing.Size(174, 24)
        Me.txbCelModProv.TabIndex = 8
        '
        'txbTelModProv
        '
        Me.txbTelModProv.Location = New System.Drawing.Point(273, 137)
        Me.txbTelModProv.Name = "txbTelModProv"
        Me.txbTelModProv.Size = New System.Drawing.Size(370, 24)
        Me.txbTelModProv.TabIndex = 7
        '
        'txbDirModProv
        '
        Me.txbDirModProv.Location = New System.Drawing.Point(273, 103)
        Me.txbDirModProv.Name = "txbDirModProv"
        Me.txbDirModProv.Size = New System.Drawing.Size(370, 24)
        Me.txbDirModProv.TabIndex = 6
        '
        'txbNombreModProv
        '
        Me.txbNombreModProv.Location = New System.Drawing.Point(273, 36)
        Me.txbNombreModProv.Name = "txbNombreModProv"
        Me.txbNombreModProv.Size = New System.Drawing.Size(370, 24)
        Me.txbNombreModProv.TabIndex = 5
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(6, 222)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(35, 18)
        Me.Label66.TabIndex = 3
        Me.Label66.Text = "Mail"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(6, 143)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(94, 18)
        Me.Label67.TabIndex = 2
        Me.Label67.Text = "Teléfono Fijo"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(6, 109)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(71, 18)
        Me.Label68.TabIndex = 1
        Me.Label68.Text = "Dirección"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(6, 35)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(134, 18)
        Me.Label69.TabIndex = 0
        Me.Label69.Text = "Nombre Comercial"
        '
        'grbDatosProveedor
        '
        Me.grbDatosProveedor.BackColor = System.Drawing.Color.LightSeaGreen
        Me.grbDatosProveedor.Controls.Add(Me.ptbAyNombreProv)
        Me.grbDatosProveedor.Controls.Add(Me.PictureBox5)
        Me.grbDatosProveedor.Controls.Add(Me.Label38)
        Me.grbDatosProveedor.Controls.Add(Me.txbCodAreaCel)
        Me.grbDatosProveedor.Controls.Add(Me.PictureBox13)
        Me.grbDatosProveedor.Controls.Add(Me.PictureBox12)
        Me.grbDatosProveedor.Controls.Add(Me.ptbAyTelProv)
        Me.grbDatosProveedor.Controls.Add(Me.btnAgregarProv)
        Me.grbDatosProveedor.Controls.Add(Me.txbMailProv)
        Me.grbDatosProveedor.Controls.Add(Me.txbCelProv)
        Me.grbDatosProveedor.Controls.Add(Me.txbTelProv)
        Me.grbDatosProveedor.Controls.Add(Me.txbDireccionProv)
        Me.grbDatosProveedor.Controls.Add(Me.txbNombreProv)
        Me.grbDatosProveedor.Controls.Add(Me.lblCelularProv)
        Me.grbDatosProveedor.Controls.Add(Me.lblMailProveedor)
        Me.grbDatosProveedor.Controls.Add(Me.lblTelefonoProv)
        Me.grbDatosProveedor.Controls.Add(Me.lblDireccionProv)
        Me.grbDatosProveedor.Controls.Add(Me.lblNombreProveedor)
        Me.grbDatosProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProveedor.Location = New System.Drawing.Point(3, 3)
        Me.grbDatosProveedor.Name = "grbDatosProveedor"
        Me.grbDatosProveedor.Size = New System.Drawing.Size(584, 299)
        Me.grbDatosProveedor.TabIndex = 1
        Me.grbDatosProveedor.TabStop = False
        Me.grbDatosProveedor.Text = "Datos Proveedor"
        '
        'ptbAyNombreProv
        '
        Me.ptbAyNombreProv.Image = CType(resources.GetObject("ptbAyNombreProv.Image"), System.Drawing.Image)
        Me.ptbAyNombreProv.Location = New System.Drawing.Point(550, 36)
        Me.ptbAyNombreProv.Name = "ptbAyNombreProv"
        Me.ptbAyNombreProv.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyNombreProv.TabIndex = 64
        Me.ptbAyNombreProv.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyNombreProv, "Coloque el nombre usando sólo letras y espacios en blanco. No coloque simbolos" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "e" & _
        "speciales, puntos o comas.")
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(550, 70)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox5.TabIndex = 63
        Me.PictureBox5.TabStop = False
        Me.tpAyudas.SetToolTip(Me.PictureBox5, resources.GetString("PictureBox5.ToolTip"))
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(315, 142)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(66, 36)
        Me.Label38.TabIndex = 57
        Me.Label38.Text = "Número " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Celular" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txbCodAreaCel
        '
        Me.txbCodAreaCel.Location = New System.Drawing.Point(188, 145)
        Me.txbCodAreaCel.Name = "txbCodAreaCel"
        Me.txbCodAreaCel.Size = New System.Drawing.Size(121, 24)
        Me.txbCodAreaCel.TabIndex = 56
        '
        'PictureBox13
        '
        Me.PictureBox13.Image = CType(resources.GetObject("PictureBox13.Image"), System.Drawing.Image)
        Me.PictureBox13.Location = New System.Drawing.Point(550, 187)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox13.TabIndex = 55
        Me.PictureBox13.TabStop = False
        Me.tpAyudas.SetToolTip(Me.PictureBox13, "Por favor, corrobore que el mail ingresado cumpla con el siguiente formato:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """nom" & _
        "bre@dominio.extensión""" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Los dominios pueden ser: .com, .org, etc." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Las extension" & _
        "es pueden ser: .ar, .br, etc.")
        '
        'PictureBox12
        '
        Me.PictureBox12.Image = CType(resources.GetObject("PictureBox12.Image"), System.Drawing.Image)
        Me.PictureBox12.Location = New System.Drawing.Point(550, 149)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox12.TabIndex = 54
        Me.PictureBox12.TabStop = False
        Me.tpAyudas.SetToolTip(Me.PictureBox12, "Por favor, coloque sólo valores numéricos. No separe los códigos de área con" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """-""" & _
        "u otros símbolos.")
        '
        'ptbAyTelProv
        '
        Me.ptbAyTelProv.Image = CType(resources.GetObject("ptbAyTelProv.Image"), System.Drawing.Image)
        Me.ptbAyTelProv.Location = New System.Drawing.Point(550, 108)
        Me.ptbAyTelProv.Name = "ptbAyTelProv"
        Me.ptbAyTelProv.Size = New System.Drawing.Size(20, 20)
        Me.ptbAyTelProv.TabIndex = 53
        Me.ptbAyTelProv.TabStop = False
        Me.tpAyudas.SetToolTip(Me.ptbAyTelProv, "Por favor, coloque sólo valores numéricos.")
        '
        'btnAgregarProv
        '
        Me.btnAgregarProv.Location = New System.Drawing.Point(188, 233)
        Me.btnAgregarProv.Name = "btnAgregarProv"
        Me.btnAgregarProv.Size = New System.Drawing.Size(154, 49)
        Me.btnAgregarProv.TabIndex = 10
        Me.btnAgregarProv.Text = "Agregar Proveedor"
        Me.btnAgregarProv.UseVisualStyleBackColor = True
        '
        'txbMailProv
        '
        Me.txbMailProv.Location = New System.Drawing.Point(188, 184)
        Me.txbMailProv.Name = "txbMailProv"
        Me.txbMailProv.Size = New System.Drawing.Size(356, 24)
        Me.txbMailProv.TabIndex = 9
        '
        'txbCelProv
        '
        Me.txbCelProv.Location = New System.Drawing.Point(387, 145)
        Me.txbCelProv.Name = "txbCelProv"
        Me.txbCelProv.Size = New System.Drawing.Size(157, 24)
        Me.txbCelProv.TabIndex = 8
        '
        'txbTelProv
        '
        Me.txbTelProv.Location = New System.Drawing.Point(188, 104)
        Me.txbTelProv.Name = "txbTelProv"
        Me.txbTelProv.Size = New System.Drawing.Size(356, 24)
        Me.txbTelProv.TabIndex = 7
        '
        'txbDireccionProv
        '
        Me.txbDireccionProv.Location = New System.Drawing.Point(188, 66)
        Me.txbDireccionProv.Name = "txbDireccionProv"
        Me.txbDireccionProv.Size = New System.Drawing.Size(356, 24)
        Me.txbDireccionProv.TabIndex = 6
        '
        'txbNombreProv
        '
        Me.txbNombreProv.Location = New System.Drawing.Point(188, 32)
        Me.txbNombreProv.Name = "txbNombreProv"
        Me.txbNombreProv.Size = New System.Drawing.Size(356, 24)
        Me.txbNombreProv.TabIndex = 5
        '
        'lblCelularProv
        '
        Me.lblCelularProv.Location = New System.Drawing.Point(6, 142)
        Me.lblCelularProv.Name = "lblCelularProv"
        Me.lblCelularProv.Size = New System.Drawing.Size(134, 33)
        Me.lblCelularProv.TabIndex = 4
        Me.lblCelularProv.Text = "Código Area " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Celular" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lblMailProveedor
        '
        Me.lblMailProveedor.AutoSize = True
        Me.lblMailProveedor.Location = New System.Drawing.Point(5, 187)
        Me.lblMailProveedor.Name = "lblMailProveedor"
        Me.lblMailProveedor.Size = New System.Drawing.Size(35, 18)
        Me.lblMailProveedor.TabIndex = 3
        Me.lblMailProveedor.Text = "Mail"
        '
        'lblTelefonoProv
        '
        Me.lblTelefonoProv.AutoSize = True
        Me.lblTelefonoProv.Location = New System.Drawing.Point(5, 107)
        Me.lblTelefonoProv.Name = "lblTelefonoProv"
        Me.lblTelefonoProv.Size = New System.Drawing.Size(94, 18)
        Me.lblTelefonoProv.TabIndex = 2
        Me.lblTelefonoProv.Text = "Teléfono Fijo"
        '
        'lblDireccionProv
        '
        Me.lblDireccionProv.AutoSize = True
        Me.lblDireccionProv.Location = New System.Drawing.Point(6, 69)
        Me.lblDireccionProv.Name = "lblDireccionProv"
        Me.lblDireccionProv.Size = New System.Drawing.Size(71, 18)
        Me.lblDireccionProv.TabIndex = 1
        Me.lblDireccionProv.Text = "Dirección"
        '
        'lblNombreProveedor
        '
        Me.lblNombreProveedor.AutoSize = True
        Me.lblNombreProveedor.Location = New System.Drawing.Point(6, 35)
        Me.lblNombreProveedor.Name = "lblNombreProveedor"
        Me.lblNombreProveedor.Size = New System.Drawing.Size(134, 18)
        Me.lblNombreProveedor.TabIndex = 0
        Me.lblNombreProveedor.Text = "Nombre Comercial"
        '
        'grbProveedores
        '
        Me.grbProveedores.BackColor = System.Drawing.Color.LightSeaGreen
        Me.grbProveedores.Controls.Add(Me.dgvProveedores)
        Me.grbProveedores.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbProveedores.Location = New System.Drawing.Point(3, 308)
        Me.grbProveedores.Name = "grbProveedores"
        Me.grbProveedores.Size = New System.Drawing.Size(1265, 342)
        Me.grbProveedores.TabIndex = 0
        Me.grbProveedores.TabStop = False
        Me.grbProveedores.Text = "Proveedores"
        '
        'dgvProveedores
        '
        Me.dgvProveedores.AllowUserToAddRows = False
        Me.dgvProveedores.AllowUserToDeleteRows = False
        Me.dgvProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProveedores.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6})
        Me.dgvProveedores.Location = New System.Drawing.Point(6, 19)
        Me.dgvProveedores.Name = "dgvProveedores"
        Me.dgvProveedores.ReadOnly = True
        Me.dgvProveedores.Size = New System.Drawing.Size(1253, 317)
        Me.dgvProveedores.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = "Número Proveedor"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "Dirección"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Teléfono"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Celular"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Mail"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "Estado"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'tpgProvProd
        '
        Me.tpgProvProd.BackColor = System.Drawing.Color.DarkGray
        Me.tpgProvProd.Controls.Add(Me.grbDatosProdProv)
        Me.tpgProvProd.Controls.Add(Me.grbProvProdProv)
        Me.tpgProvProd.Controls.Add(Me.grbDatosProdProvProd)
        Me.tpgProvProd.Location = New System.Drawing.Point(4, 22)
        Me.tpgProvProd.Name = "tpgProvProd"
        Me.tpgProvProd.Size = New System.Drawing.Size(1279, 653)
        Me.tpgProvProd.TabIndex = 6
        Me.tpgProvProd.Text = "Proveedor/Productos"
        '
        'grbDatosProdProv
        '
        Me.grbDatosProdProv.BackColor = System.Drawing.Color.MediumSpringGreen
        Me.grbDatosProdProv.Controls.Add(Me.btnCancelarRelacion)
        Me.grbDatosProdProv.Controls.Add(Me.btnMostrarRelacionProdProv)
        Me.grbDatosProdProv.Controls.Add(Me.btnPrecioCompraProd)
        Me.grbDatosProdProv.Controls.Add(Me.GroupBox9)
        Me.grbDatosProdProv.Controls.Add(Me.GroupBox8)
        Me.grbDatosProdProv.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProdProv.Location = New System.Drawing.Point(731, 8)
        Me.grbDatosProdProv.Name = "grbDatosProdProv"
        Me.grbDatosProdProv.Size = New System.Drawing.Size(537, 642)
        Me.grbDatosProdProv.TabIndex = 2
        Me.grbDatosProdProv.TabStop = False
        Me.grbDatosProdProv.Text = "Datos Producto/Proveedor"
        '
        'btnCancelarRelacion
        '
        Me.btnCancelarRelacion.Location = New System.Drawing.Point(403, 549)
        Me.btnCancelarRelacion.Name = "btnCancelarRelacion"
        Me.btnCancelarRelacion.Size = New System.Drawing.Size(122, 75)
        Me.btnCancelarRelacion.TabIndex = 14
        Me.btnCancelarRelacion.Text = "Cancelar Relación"
        Me.btnCancelarRelacion.UseVisualStyleBackColor = True
        '
        'btnMostrarRelacionProdProv
        '
        Me.btnMostrarRelacionProdProv.Location = New System.Drawing.Point(200, 549)
        Me.btnMostrarRelacionProdProv.Name = "btnMostrarRelacionProdProv"
        Me.btnMostrarRelacionProdProv.Size = New System.Drawing.Size(188, 75)
        Me.btnMostrarRelacionProdProv.TabIndex = 13
        Me.btnMostrarRelacionProdProv.Text = "Mostrar Relaciones Producto/Proveedor"
        Me.btnMostrarRelacionProdProv.UseVisualStyleBackColor = True
        '
        'btnPrecioCompraProd
        '
        Me.btnPrecioCompraProd.Location = New System.Drawing.Point(6, 549)
        Me.btnPrecioCompraProd.Name = "btnPrecioCompraProd"
        Me.btnPrecioCompraProd.Size = New System.Drawing.Size(188, 75)
        Me.btnPrecioCompraProd.TabIndex = 12
        Me.btnPrecioCompraProd.Text = "Establecer Producto/Proveedor"
        Me.btnPrecioCompraProd.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.BackColor = System.Drawing.Color.PaleTurquoise
        Me.GroupBox9.Controls.Add(Me.txbPrecioVenta_ProvProd)
        Me.GroupBox9.Controls.Add(Me.txbPrecioCompraProd)
        Me.GroupBox9.Controls.Add(Me.Label71)
        Me.GroupBox9.Controls.Add(Me.lblTituloPrecioVenta)
        Me.GroupBox9.Location = New System.Drawing.Point(6, 286)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(525, 242)
        Me.GroupBox9.TabIndex = 11
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Datos Precio"
        '
        'txbPrecioVenta_ProvProd
        '
        Me.txbPrecioVenta_ProvProd.Location = New System.Drawing.Point(348, 48)
        Me.txbPrecioVenta_ProvProd.Name = "txbPrecioVenta_ProvProd"
        Me.txbPrecioVenta_ProvProd.ReadOnly = True
        Me.txbPrecioVenta_ProvProd.Size = New System.Drawing.Size(168, 24)
        Me.txbPrecioVenta_ProvProd.TabIndex = 16
        '
        'txbPrecioCompraProd
        '
        Me.txbPrecioCompraProd.Location = New System.Drawing.Point(348, 111)
        Me.txbPrecioCompraProd.Name = "txbPrecioCompraProd"
        Me.txbPrecioCompraProd.Size = New System.Drawing.Size(171, 24)
        Me.txbPrecioCompraProd.TabIndex = 13
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(6, 114)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(132, 18)
        Me.Label71.TabIndex = 12
        Me.Label71.Text = "Precio De Compra"
        '
        'lblTituloPrecioVenta
        '
        Me.lblTituloPrecioVenta.AutoSize = True
        Me.lblTituloPrecioVenta.Location = New System.Drawing.Point(6, 54)
        Me.lblTituloPrecioVenta.Name = "lblTituloPrecioVenta"
        Me.lblTituloPrecioVenta.Size = New System.Drawing.Size(115, 18)
        Me.lblTituloPrecioVenta.TabIndex = 10
        Me.lblTituloPrecioVenta.Text = "Precio De Venta"
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox8.Controls.Add(Me.lblNroProd)
        Me.GroupBox8.Controls.Add(Me.lblTituloNroProv)
        Me.GroupBox8.Controls.Add(Me.txbProd_ProvProd)
        Me.GroupBox8.Controls.Add(Me.txbProv_ProdProv)
        Me.GroupBox8.Controls.Add(Me.lblIdProducto)
        Me.GroupBox8.Controls.Add(Me.lblIdProveedor)
        Me.GroupBox8.Controls.Add(Me.lblTituloProducto)
        Me.GroupBox8.Controls.Add(Me.lblTituloProveedor)
        Me.GroupBox8.Location = New System.Drawing.Point(6, 23)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(525, 257)
        Me.GroupBox8.TabIndex = 10
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Información Producto/Proveedor"
        '
        'lblNroProd
        '
        Me.lblNroProd.AutoSize = True
        Me.lblNroProd.Location = New System.Drawing.Point(6, 148)
        Me.lblNroProd.Name = "lblNroProd"
        Me.lblNroProd.Size = New System.Drawing.Size(127, 18)
        Me.lblNroProd.TabIndex = 13
        Me.lblNroProd.Text = "Número Producto"
        '
        'lblTituloNroProv
        '
        Me.lblTituloNroProv.AutoSize = True
        Me.lblTituloNroProv.Location = New System.Drawing.Point(6, 33)
        Me.lblTituloNroProv.Name = "lblTituloNroProv"
        Me.lblTituloNroProv.Size = New System.Drawing.Size(135, 18)
        Me.lblTituloNroProv.TabIndex = 12
        Me.lblTituloNroProv.Text = "Número Proveedor"
        '
        'txbProd_ProvProd
        '
        Me.txbProd_ProvProd.Location = New System.Drawing.Point(98, 187)
        Me.txbProd_ProvProd.Name = "txbProd_ProvProd"
        Me.txbProd_ProvProd.ReadOnly = True
        Me.txbProd_ProvProd.Size = New System.Drawing.Size(418, 24)
        Me.txbProd_ProvProd.TabIndex = 11
        '
        'txbProv_ProdProv
        '
        Me.txbProv_ProdProv.Location = New System.Drawing.Point(98, 76)
        Me.txbProv_ProdProv.Name = "txbProv_ProdProv"
        Me.txbProv_ProdProv.ReadOnly = True
        Me.txbProv_ProdProv.Size = New System.Drawing.Size(421, 24)
        Me.txbProv_ProdProv.TabIndex = 10
        '
        'lblIdProducto
        '
        Me.lblIdProducto.AutoSize = True
        Me.lblIdProducto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblIdProducto.Location = New System.Drawing.Point(155, 146)
        Me.lblIdProducto.Name = "lblIdProducto"
        Me.lblIdProducto.Size = New System.Drawing.Size(210, 20)
        Me.lblIdProducto.TabIndex = 9
        Me.lblIdProducto.Text = "Aquí va el número de producto"
        '
        'lblIdProveedor
        '
        Me.lblIdProveedor.AutoSize = True
        Me.lblIdProveedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblIdProveedor.Location = New System.Drawing.Point(147, 33)
        Me.lblIdProveedor.Name = "lblIdProveedor"
        Me.lblIdProveedor.Size = New System.Drawing.Size(218, 20)
        Me.lblIdProveedor.TabIndex = 8
        Me.lblIdProveedor.Text = "Aquí va el número de proveedor"
        '
        'lblTituloProducto
        '
        Me.lblTituloProducto.AutoSize = True
        Me.lblTituloProducto.Location = New System.Drawing.Point(11, 187)
        Me.lblTituloProducto.Name = "lblTituloProducto"
        Me.lblTituloProducto.Size = New System.Drawing.Size(69, 18)
        Me.lblTituloProducto.TabIndex = 6
        Me.lblTituloProducto.Text = "Producto"
        '
        'lblTituloProveedor
        '
        Me.lblTituloProveedor.AutoSize = True
        Me.lblTituloProveedor.Location = New System.Drawing.Point(3, 82)
        Me.lblTituloProveedor.Name = "lblTituloProveedor"
        Me.lblTituloProveedor.Size = New System.Drawing.Size(77, 18)
        Me.lblTituloProveedor.TabIndex = 4
        Me.lblTituloProveedor.Text = "Proveedor"
        '
        'grbProvProdProv
        '
        Me.grbProvProdProv.BackColor = System.Drawing.Color.DarkCyan
        Me.grbProvProdProv.Controls.Add(Me.btnFiltrarProveedor)
        Me.grbProvProdProv.Controls.Add(Me.txbFiltrarProvProdProv)
        Me.grbProvProdProv.Controls.Add(Me.lblFiltrarProvProdProv)
        Me.grbProvProdProv.Controls.Add(Me.dgvProv_ProvProd)
        Me.grbProvProdProv.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbProvProdProv.Location = New System.Drawing.Point(3, 348)
        Me.grbProvProdProv.Name = "grbProvProdProv"
        Me.grbProvProdProv.Size = New System.Drawing.Size(722, 302)
        Me.grbProvProdProv.TabIndex = 1
        Me.grbProvProdProv.TabStop = False
        Me.grbProvProdProv.Text = "Proveedores"
        '
        'btnFiltrarProveedor
        '
        Me.btnFiltrarProveedor.Location = New System.Drawing.Point(483, 24)
        Me.btnFiltrarProveedor.Name = "btnFiltrarProveedor"
        Me.btnFiltrarProveedor.Size = New System.Drawing.Size(75, 30)
        Me.btnFiltrarProveedor.TabIndex = 34
        Me.btnFiltrarProveedor.Text = "Filtrar"
        Me.btnFiltrarProveedor.UseVisualStyleBackColor = True
        '
        'txbFiltrarProvProdProv
        '
        Me.txbFiltrarProvProdProv.Location = New System.Drawing.Point(147, 30)
        Me.txbFiltrarProvProdProv.Name = "txbFiltrarProvProdProv"
        Me.txbFiltrarProvProdProv.Size = New System.Drawing.Size(288, 24)
        Me.txbFiltrarProvProdProv.TabIndex = 31
        '
        'lblFiltrarProvProdProv
        '
        Me.lblFiltrarProvProdProv.AutoSize = True
        Me.lblFiltrarProvProdProv.Location = New System.Drawing.Point(6, 33)
        Me.lblFiltrarProvProdProv.Name = "lblFiltrarProvProdProv"
        Me.lblFiltrarProvProdProv.Size = New System.Drawing.Size(135, 18)
        Me.lblFiltrarProvProdProv.TabIndex = 30
        Me.lblFiltrarProvProdProv.Text = "Nombre Proveedor"
        '
        'dgvProv_ProvProd
        '
        Me.dgvProv_ProvProd.AllowUserToAddRows = False
        Me.dgvProv_ProvProd.AllowUserToDeleteRows = False
        Me.dgvProv_ProvProd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProv_ProvProd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38, Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41, Me.DataGridViewTextBoxColumn42})
        Me.dgvProv_ProvProd.Location = New System.Drawing.Point(6, 60)
        Me.dgvProv_ProvProd.Name = "dgvProv_ProvProd"
        Me.dgvProv_ProvProd.ReadOnly = True
        Me.dgvProv_ProvProd.Size = New System.Drawing.Size(710, 224)
        Me.dgvProv_ProvProd.TabIndex = 1
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.HeaderText = "Número Proveedor"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        Me.DataGridViewTextBoxColumn37.ReadOnly = True
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.HeaderText = "Dirección"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        Me.DataGridViewTextBoxColumn38.ReadOnly = True
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.HeaderText = "Teléfono"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        Me.DataGridViewTextBoxColumn39.ReadOnly = True
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.HeaderText = "Celular"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        Me.DataGridViewTextBoxColumn40.ReadOnly = True
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.HeaderText = "Mail"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        Me.DataGridViewTextBoxColumn41.ReadOnly = True
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.HeaderText = "Estado"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        Me.DataGridViewTextBoxColumn42.ReadOnly = True
        '
        'grbDatosProdProvProd
        '
        Me.grbDatosProdProvProd.BackColor = System.Drawing.Color.LightSeaGreen
        Me.grbDatosProdProvProd.Controls.Add(Me.btnFiltrarProd)
        Me.grbDatosProdProvProd.Controls.Add(Me.btnSigProdProv_Prod)
        Me.grbDatosProdProvProd.Controls.Add(Me.btnAntProdProv_Prod)
        Me.grbDatosProdProvProd.Controls.Add(Me.txbFiltrarProdProv)
        Me.grbDatosProdProvProd.Controls.Add(Me.lblFiltradoProdProv)
        Me.grbDatosProdProvProd.Controls.Add(Me.dgvProvProd)
        Me.grbDatosProdProvProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProdProvProd.Location = New System.Drawing.Point(3, 8)
        Me.grbDatosProdProvProd.Name = "grbDatosProdProvProd"
        Me.grbDatosProdProvProd.Size = New System.Drawing.Size(722, 334)
        Me.grbDatosProdProvProd.TabIndex = 0
        Me.grbDatosProdProvProd.TabStop = False
        Me.grbDatosProdProvProd.Text = "Productos"
        '
        'btnFiltrarProd
        '
        Me.btnFiltrarProd.Location = New System.Drawing.Point(469, 24)
        Me.btnFiltrarProd.Name = "btnFiltrarProd"
        Me.btnFiltrarProd.Size = New System.Drawing.Size(75, 30)
        Me.btnFiltrarProd.TabIndex = 32
        Me.btnFiltrarProd.Text = "Filtrar"
        Me.btnFiltrarProd.UseVisualStyleBackColor = True
        '
        'btnSigProdProv_Prod
        '
        Me.btnSigProdProv_Prod.Image = CType(resources.GetObject("btnSigProdProv_Prod.Image"), System.Drawing.Image)
        Me.btnSigProdProv_Prod.Location = New System.Drawing.Point(363, 254)
        Me.btnSigProdProv_Prod.Name = "btnSigProdProv_Prod"
        Me.btnSigProdProv_Prod.Size = New System.Drawing.Size(72, 72)
        Me.btnSigProdProv_Prod.TabIndex = 31
        Me.btnSigProdProv_Prod.UseVisualStyleBackColor = True
        '
        'btnAntProdProv_Prod
        '
        Me.btnAntProdProv_Prod.Image = CType(resources.GetObject("btnAntProdProv_Prod.Image"), System.Drawing.Image)
        Me.btnAntProdProv_Prod.Location = New System.Drawing.Point(285, 254)
        Me.btnAntProdProv_Prod.Name = "btnAntProdProv_Prod"
        Me.btnAntProdProv_Prod.Size = New System.Drawing.Size(72, 72)
        Me.btnAntProdProv_Prod.TabIndex = 30
        Me.btnAntProdProv_Prod.UseVisualStyleBackColor = True
        '
        'txbFiltrarProdProv
        '
        Me.txbFiltrarProdProv.Location = New System.Drawing.Point(147, 27)
        Me.txbFiltrarProdProv.Name = "txbFiltrarProdProv"
        Me.txbFiltrarProdProv.Size = New System.Drawing.Size(288, 24)
        Me.txbFiltrarProdProv.TabIndex = 29
        '
        'lblFiltradoProdProv
        '
        Me.lblFiltradoProdProv.AutoSize = True
        Me.lblFiltradoProdProv.Location = New System.Drawing.Point(6, 30)
        Me.lblFiltradoProdProv.Name = "lblFiltradoProdProv"
        Me.lblFiltradoProdProv.Size = New System.Drawing.Size(127, 18)
        Me.lblFiltradoProdProv.TabIndex = 28
        Me.lblFiltradoProdProv.Text = "Nombre Producto"
        '
        'dgvProvProd
        '
        Me.dgvProvProd.AllowUserToAddRows = False
        Me.dgvProvProd.AllowUserToDeleteRows = False
        Me.dgvProvProd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProvProd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18})
        Me.dgvProvProd.Location = New System.Drawing.Point(6, 70)
        Me.dgvProvProd.Name = "dgvProvProd"
        Me.dgvProvProd.ReadOnly = True
        Me.dgvProvProd.Size = New System.Drawing.Size(710, 178)
        Me.dgvProvProd.TabIndex = 27
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Nombre Producto"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Categoria"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Subcategoria"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Stock"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Stock Mínimo"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Estado"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Fecha De Alta"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        '
        'tpgCompras
        '
        Me.tpgCompras.BackColor = System.Drawing.Color.Silver
        Me.tpgCompras.Controls.Add(Me.grbFiltrosCompras)
        Me.tpgCompras.Controls.Add(Me.GroupBox12)
        Me.tpgCompras.Controls.Add(Me.grbCarritoCompra)
        Me.tpgCompras.Controls.Add(Me.GroupBox11)
        Me.tpgCompras.Location = New System.Drawing.Point(4, 22)
        Me.tpgCompras.Name = "tpgCompras"
        Me.tpgCompras.Size = New System.Drawing.Size(1279, 653)
        Me.tpgCompras.TabIndex = 7
        Me.tpgCompras.Text = "Compras"
        '
        'grbFiltrosCompras
        '
        Me.grbFiltrosCompras.BackColor = System.Drawing.Color.LightGreen
        Me.grbFiltrosCompras.Controls.Add(Me.btnFiltrarProdProv)
        Me.grbFiltrosCompras.Controls.Add(Me.PictureBox17)
        Me.grbFiltrosCompras.Controls.Add(Me.txbProvFilCompra)
        Me.grbFiltrosCompras.Controls.Add(Me.Label89)
        Me.grbFiltrosCompras.Controls.Add(Me.txbProdFilCompra)
        Me.grbFiltrosCompras.Controls.Add(Me.Label88)
        Me.grbFiltrosCompras.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltrosCompras.Location = New System.Drawing.Point(3, 3)
        Me.grbFiltrosCompras.Name = "grbFiltrosCompras"
        Me.grbFiltrosCompras.Size = New System.Drawing.Size(812, 81)
        Me.grbFiltrosCompras.TabIndex = 4
        Me.grbFiltrosCompras.TabStop = False
        Me.grbFiltrosCompras.Text = "Filtrado"
        '
        'btnFiltrarProdProv
        '
        Me.btnFiltrarProdProv.Location = New System.Drawing.Point(684, 23)
        Me.btnFiltrarProdProv.Name = "btnFiltrarProdProv"
        Me.btnFiltrarProdProv.Size = New System.Drawing.Size(75, 37)
        Me.btnFiltrarProdProv.TabIndex = 55
        Me.btnFiltrarProdProv.Text = "Filtrar"
        Me.btnFiltrarProdProv.UseVisualStyleBackColor = True
        '
        'PictureBox17
        '
        Me.PictureBox17.Image = CType(resources.GetObject("PictureBox17.Image"), System.Drawing.Image)
        Me.PictureBox17.Location = New System.Drawing.Point(786, 23)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox17.TabIndex = 54
        Me.PictureBox17.TabStop = False
        Me.tpAyudas.SetToolTip(Me.PictureBox17, resources.GetString("PictureBox17.ToolTip"))
        '
        'txbProvFilCompra
        '
        Me.txbProvFilCompra.Location = New System.Drawing.Point(488, 33)
        Me.txbProvFilCompra.Name = "txbProvFilCompra"
        Me.txbProvFilCompra.Size = New System.Drawing.Size(176, 24)
        Me.txbProvFilCompra.TabIndex = 4
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(347, 36)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(135, 18)
        Me.Label89.TabIndex = 3
        Me.Label89.Text = "Nombre Proveedor"
        '
        'txbProdFilCompra
        '
        Me.txbProdFilCompra.Location = New System.Drawing.Point(138, 36)
        Me.txbProdFilCompra.Name = "txbProdFilCompra"
        Me.txbProdFilCompra.Size = New System.Drawing.Size(202, 24)
        Me.txbProdFilCompra.TabIndex = 2
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(5, 39)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(127, 18)
        Me.Label88.TabIndex = 1
        Me.Label88.Text = "Nombre Producto"
        '
        'GroupBox12
        '
        Me.GroupBox12.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.GroupBox12.Controls.Add(Me.btnCancelarModCarrito)
        Me.GroupBox12.Controls.Add(Me.btnModCarrito)
        Me.GroupBox12.Controls.Add(Me.txbPrecioCompra)
        Me.GroupBox12.Controls.Add(Me.Label56)
        Me.GroupBox12.Controls.Add(Me.txbProvCompra)
        Me.GroupBox12.Controls.Add(Me.Label15)
        Me.GroupBox12.Controls.Add(Me.txbNroProdCompra)
        Me.GroupBox12.Controls.Add(Me.txbNroProvCompra)
        Me.GroupBox12.Controls.Add(Me.txbNombreProdCompra)
        Me.GroupBox12.Controls.Add(Me.ckbConfModPrecio)
        Me.GroupBox12.Controls.Add(Me.grbModPrecioCompra)
        Me.GroupBox12.Controls.Add(Me.btnAsignarCarritoCompra)
        Me.GroupBox12.Controls.Add(Me.nudCantidadCompra)
        Me.GroupBox12.Controls.Add(Me.Label78)
        Me.GroupBox12.Controls.Add(Me.Label77)
        Me.GroupBox12.Controls.Add(Me.Label74)
        Me.GroupBox12.Controls.Add(Me.Label70)
        Me.GroupBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox12.Location = New System.Drawing.Point(821, 3)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(447, 647)
        Me.GroupBox12.TabIndex = 3
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Datos Compra"
        '
        'btnCancelarModCarrito
        '
        Me.btnCancelarModCarrito.Location = New System.Drawing.Point(280, 596)
        Me.btnCancelarModCarrito.Name = "btnCancelarModCarrito"
        Me.btnCancelarModCarrito.Size = New System.Drawing.Size(127, 45)
        Me.btnCancelarModCarrito.TabIndex = 27
        Me.btnCancelarModCarrito.Text = "Cancelar Modificación"
        Me.btnCancelarModCarrito.UseVisualStyleBackColor = True
        '
        'btnModCarrito
        '
        Me.btnModCarrito.Location = New System.Drawing.Point(147, 596)
        Me.btnModCarrito.Name = "btnModCarrito"
        Me.btnModCarrito.Size = New System.Drawing.Size(127, 45)
        Me.btnModCarrito.TabIndex = 26
        Me.btnModCarrito.Text = "Modificar"
        Me.btnModCarrito.UseVisualStyleBackColor = True
        '
        'txbPrecioCompra
        '
        Me.txbPrecioCompra.Location = New System.Drawing.Point(153, 169)
        Me.txbPrecioCompra.Name = "txbPrecioCompra"
        Me.txbPrecioCompra.ReadOnly = True
        Me.txbPrecioCompra.Size = New System.Drawing.Size(130, 24)
        Me.txbPrecioCompra.TabIndex = 25
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(6, 139)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(77, 18)
        Me.Label56.TabIndex = 24
        Me.Label56.Text = "Proveedor"
        '
        'txbProvCompra
        '
        Me.txbProvCompra.Location = New System.Drawing.Point(153, 136)
        Me.txbProvCompra.Name = "txbProvCompra"
        Me.txbProvCompra.ReadOnly = True
        Me.txbProvCompra.Size = New System.Drawing.Size(283, 24)
        Me.txbProvCompra.TabIndex = 23
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 57)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(69, 18)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "Producto"
        '
        'txbNroProdCompra
        '
        Me.txbNroProdCompra.Location = New System.Drawing.Point(153, 19)
        Me.txbNroProdCompra.Name = "txbNroProdCompra"
        Me.txbNroProdCompra.ReadOnly = True
        Me.txbNroProdCompra.Size = New System.Drawing.Size(87, 24)
        Me.txbNroProdCompra.TabIndex = 21
        '
        'txbNroProvCompra
        '
        Me.txbNroProvCompra.Location = New System.Drawing.Point(153, 100)
        Me.txbNroProvCompra.Name = "txbNroProvCompra"
        Me.txbNroProvCompra.ReadOnly = True
        Me.txbNroProvCompra.Size = New System.Drawing.Size(70, 24)
        Me.txbNroProvCompra.TabIndex = 20
        '
        'txbNombreProdCompra
        '
        Me.txbNombreProdCompra.Location = New System.Drawing.Point(153, 57)
        Me.txbNombreProdCompra.Name = "txbNombreProdCompra"
        Me.txbNombreProdCompra.ReadOnly = True
        Me.txbNombreProdCompra.Size = New System.Drawing.Size(283, 24)
        Me.txbNombreProdCompra.TabIndex = 19
        '
        'ckbConfModPrecio
        '
        Me.ckbConfModPrecio.AutoSize = True
        Me.ckbConfModPrecio.Location = New System.Drawing.Point(285, 247)
        Me.ckbConfModPrecio.Name = "ckbConfModPrecio"
        Me.ckbConfModPrecio.Size = New System.Drawing.Size(151, 22)
        Me.ckbConfModPrecio.TabIndex = 18
        Me.ckbConfModPrecio.Text = "¿Modificar Precio?"
        Me.ckbConfModPrecio.UseVisualStyleBackColor = True
        '
        'grbModPrecioCompra
        '
        Me.grbModPrecioCompra.Controls.Add(Me.txbDescModCompra)
        Me.grbModPrecioCompra.Controls.Add(Me.chkDescPrecioCompra)
        Me.grbModPrecioCompra.Controls.Add(Me.chkAuPrecioCompra)
        Me.grbModPrecioCompra.Controls.Add(Me.cmbMotivoModCompra)
        Me.grbModPrecioCompra.Controls.Add(Me.Label83)
        Me.grbModPrecioCompra.Controls.Add(Me.txbModiPrecioCompra)
        Me.grbModPrecioCompra.Controls.Add(Me.Label79)
        Me.grbModPrecioCompra.Location = New System.Drawing.Point(14, 264)
        Me.grbModPrecioCompra.Name = "grbModPrecioCompra"
        Me.grbModPrecioCompra.Size = New System.Drawing.Size(427, 254)
        Me.grbModPrecioCompra.TabIndex = 17
        Me.grbModPrecioCompra.TabStop = False
        Me.grbModPrecioCompra.Text = "Modificación Precio"
        '
        'txbDescModCompra
        '
        Me.txbDescModCompra.Location = New System.Drawing.Point(5, 142)
        Me.txbDescModCompra.Multiline = True
        Me.txbDescModCompra.Name = "txbDescModCompra"
        Me.txbDescModCompra.Size = New System.Drawing.Size(416, 106)
        Me.txbDescModCompra.TabIndex = 22
        '
        'chkDescPrecioCompra
        '
        Me.chkDescPrecioCompra.AutoSize = True
        Me.chkDescPrecioCompra.Location = New System.Drawing.Point(170, 114)
        Me.chkDescPrecioCompra.Name = "chkDescPrecioCompra"
        Me.chkDescPrecioCompra.Size = New System.Drawing.Size(99, 22)
        Me.chkDescPrecioCompra.TabIndex = 21
        Me.chkDescPrecioCompra.Text = "Descuento"
        Me.chkDescPrecioCompra.UseVisualStyleBackColor = True
        '
        'chkAuPrecioCompra
        '
        Me.chkAuPrecioCompra.AutoSize = True
        Me.chkAuPrecioCompra.Location = New System.Drawing.Point(27, 114)
        Me.chkAuPrecioCompra.Name = "chkAuPrecioCompra"
        Me.chkAuPrecioCompra.Size = New System.Drawing.Size(86, 22)
        Me.chkAuPrecioCompra.TabIndex = 20
        Me.chkAuPrecioCompra.Text = "Aumento"
        Me.chkAuPrecioCompra.UseVisualStyleBackColor = True
        '
        'cmbMotivoModCompra
        '
        Me.cmbMotivoModCompra.FormattingEnabled = True
        Me.cmbMotivoModCompra.Location = New System.Drawing.Point(203, 70)
        Me.cmbMotivoModCompra.Name = "cmbMotivoModCompra"
        Me.cmbMotivoModCompra.Size = New System.Drawing.Size(209, 26)
        Me.cmbMotivoModCompra.TabIndex = 19
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(24, 70)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(141, 18)
        Me.Label83.TabIndex = 18
        Me.Label83.Text = "Motivo Modificación"
        '
        'txbModiPrecioCompra
        '
        Me.txbModiPrecioCompra.Location = New System.Drawing.Point(203, 28)
        Me.txbModiPrecioCompra.Name = "txbModiPrecioCompra"
        Me.txbModiPrecioCompra.Size = New System.Drawing.Size(209, 24)
        Me.txbModiPrecioCompra.TabIndex = 17
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(26, 31)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(139, 18)
        Me.Label79.TabIndex = 16
        Me.Label79.Text = "Modificación Precio"
        '
        'btnAsignarCarritoCompra
        '
        Me.btnAsignarCarritoCompra.Location = New System.Drawing.Point(14, 596)
        Me.btnAsignarCarritoCompra.Name = "btnAsignarCarritoCompra"
        Me.btnAsignarCarritoCompra.Size = New System.Drawing.Size(127, 45)
        Me.btnAsignarCarritoCompra.TabIndex = 16
        Me.btnAsignarCarritoCompra.Text = "Asignar Al Carrito"
        Me.btnAsignarCarritoCompra.UseVisualStyleBackColor = True
        '
        'nudCantidadCompra
        '
        Me.nudCantidadCompra.Location = New System.Drawing.Point(153, 206)
        Me.nudCantidadCompra.Name = "nudCantidadCompra"
        Me.nudCantidadCompra.Size = New System.Drawing.Size(120, 24)
        Me.nudCantidadCompra.TabIndex = 8
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(11, 212)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(66, 18)
        Me.Label78.TabIndex = 7
        Me.Label78.Text = "Cantidad"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(9, 172)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(109, 18)
        Me.Label77.TabIndex = 6
        Me.Label77.Text = "Precio Compra"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(6, 100)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(135, 18)
        Me.Label74.TabIndex = 2
        Me.Label74.Text = "Número Proveedor"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(6, 25)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(127, 18)
        Me.Label70.TabIndex = 0
        Me.Label70.Text = "Número Producto"
        '
        'grbCarritoCompra
        '
        Me.grbCarritoCompra.BackColor = System.Drawing.Color.LightSeaGreen
        Me.grbCarritoCompra.Controls.Add(Me.btnComprar)
        Me.grbCarritoCompra.Controls.Add(Me.Label87)
        Me.grbCarritoCompra.Controls.Add(Me.Label86)
        Me.grbCarritoCompra.Controls.Add(Me.Label85)
        Me.grbCarritoCompra.Controls.Add(Me.Label84)
        Me.grbCarritoCompra.Controls.Add(Me.Button11)
        Me.grbCarritoCompra.Controls.Add(Me.dgvCarritoCompras)
        Me.grbCarritoCompra.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbCarritoCompra.Location = New System.Drawing.Point(3, 374)
        Me.grbCarritoCompra.Name = "grbCarritoCompra"
        Me.grbCarritoCompra.Size = New System.Drawing.Size(812, 276)
        Me.grbCarritoCompra.TabIndex = 2
        Me.grbCarritoCompra.TabStop = False
        Me.grbCarritoCompra.Text = "Carrito De Compras"
        '
        'btnComprar
        '
        Me.btnComprar.Location = New System.Drawing.Point(70, 210)
        Me.btnComprar.Name = "btnComprar"
        Me.btnComprar.Size = New System.Drawing.Size(121, 45)
        Me.btnComprar.TabIndex = 26
        Me.btnComprar.Text = "Comprar"
        Me.btnComprar.UseVisualStyleBackColor = True
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label87.Location = New System.Drawing.Point(421, 296)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(199, 20)
        Me.Label87.TabIndex = 21
        Me.Label87.Text = "aquí va el precio final a pagar"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label86.Location = New System.Drawing.Point(177, 298)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(130, 20)
        Me.Label86.TabIndex = 20
        Me.Label86.Text = "aquí va la cantidad"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(329, 298)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(86, 18)
        Me.Label85.TabIndex = 19
        Me.Label85.Text = "Precio Final"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(4, 298)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(167, 18)
        Me.Label84.TabIndex = 18
        Me.Label84.Text = "Elementos En El Carrito"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(637, 277)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(169, 41)
        Me.Button11.TabIndex = 17
        Me.Button11.Text = "Confirmar Compra"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'dgvCarritoCompras
        '
        Me.dgvCarritoCompras.AllowUserToAddRows = False
        Me.dgvCarritoCompras.AllowUserToDeleteRows = False
        Me.dgvCarritoCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCarritoCompras.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn43, Me.Column9, Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn46, Me.DataGridViewTextBoxColumn47, Me.Column10, Me.DataGridViewTextBoxColumn48})
        Me.dgvCarritoCompras.Location = New System.Drawing.Point(6, 19)
        Me.dgvCarritoCompras.Name = "dgvCarritoCompras"
        Me.dgvCarritoCompras.ReadOnly = True
        Me.dgvCarritoCompras.Size = New System.Drawing.Size(800, 173)
        Me.dgvCarritoCompras.TabIndex = 0
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.HeaderText = "Número Proveedor"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        Me.DataGridViewTextBoxColumn43.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.HeaderText = "Cantidad"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.HeaderText = "Nombre Proveedor"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        Me.DataGridViewTextBoxColumn44.ReadOnly = True
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        Me.DataGridViewTextBoxColumn45.ReadOnly = True
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.HeaderText = "Nombre Producto"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        Me.DataGridViewTextBoxColumn46.ReadOnly = True
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.HeaderText = "Precio Unidad"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        Me.DataGridViewTextBoxColumn47.ReadOnly = True
        '
        'Column10
        '
        Me.Column10.HeaderText = "Cantidad"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.HeaderText = "Monto Total"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        Me.DataGridViewTextBoxColumn48.ReadOnly = True
        '
        'GroupBox11
        '
        Me.GroupBox11.BackColor = System.Drawing.Color.LightSeaGreen
        Me.GroupBox11.Controls.Add(Me.dgvProvProdCompras)
        Me.GroupBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox11.Location = New System.Drawing.Point(3, 90)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(812, 278)
        Me.GroupBox11.TabIndex = 1
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Productos Por Proveedor"
        '
        'dgvProvProdCompras
        '
        Me.dgvProvProdCompras.AllowUserToAddRows = False
        Me.dgvProvProdCompras.AllowUserToDeleteRows = False
        Me.dgvProvProdCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProvProdCompras.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn52, Me.Column7, Me.DataGridViewTextBoxColumn53, Me.Column8, Me.DataGridViewTextBoxColumn54, Me.DataGridViewTextBoxColumn57})
        Me.dgvProvProdCompras.Location = New System.Drawing.Point(6, 19)
        Me.dgvProvProdCompras.Name = "dgvProvProdCompras"
        Me.dgvProvProdCompras.ReadOnly = True
        Me.dgvProvProdCompras.Size = New System.Drawing.Size(800, 246)
        Me.dgvProvProdCompras.TabIndex = 0
        '
        'DataGridViewTextBoxColumn52
        '
        Me.DataGridViewTextBoxColumn52.HeaderText = "Número Proveedor"
        Me.DataGridViewTextBoxColumn52.Name = "DataGridViewTextBoxColumn52"
        Me.DataGridViewTextBoxColumn52.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.HeaderText = "Nombre Proveedor"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn53
        '
        Me.DataGridViewTextBoxColumn53.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn53.Name = "DataGridViewTextBoxColumn53"
        Me.DataGridViewTextBoxColumn53.ReadOnly = True
        '
        'Column8
        '
        Me.Column8.HeaderText = "Nombre Producto"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn54
        '
        Me.DataGridViewTextBoxColumn54.HeaderText = "Precio Compra"
        Me.DataGridViewTextBoxColumn54.Name = "DataGridViewTextBoxColumn54"
        Me.DataGridViewTextBoxColumn54.ReadOnly = True
        '
        'DataGridViewTextBoxColumn57
        '
        Me.DataGridViewTextBoxColumn57.HeaderText = "Estado"
        Me.DataGridViewTextBoxColumn57.Name = "DataGridViewTextBoxColumn57"
        Me.DataGridViewTextBoxColumn57.ReadOnly = True
        '
        'grbModStockProd
        '
        Me.grbModStockProd.BackColor = System.Drawing.Color.Silver
        Me.grbModStockProd.Controls.Add(Me.grbProdModStockPre)
        Me.grbModStockProd.Controls.Add(Me.grbDatosProdModPrecio)
        Me.grbModStockProd.Controls.Add(Me.grbDatosProdModStock)
        Me.grbModStockProd.Location = New System.Drawing.Point(4, 22)
        Me.grbModStockProd.Name = "grbModStockProd"
        Me.grbModStockProd.Size = New System.Drawing.Size(1279, 653)
        Me.grbModStockProd.TabIndex = 8
        Me.grbModStockProd.Text = "Modificar Stock/Precio"
        '
        'grbProdModStockPre
        '
        Me.grbProdModStockPre.BackColor = System.Drawing.Color.PaleTurquoise
        Me.grbProdModStockPre.Controls.Add(Me.chkFilModStockPrecio)
        Me.grbProdModStockPre.Controls.Add(Me.cmbFiltroSubcat)
        Me.grbProdModStockPre.Controls.Add(Me.Label5)
        Me.grbProdModStockPre.Controls.Add(Me.cmbCatModStockPrecio)
        Me.grbProdModStockPre.Controls.Add(Me.Label109)
        Me.grbProdModStockPre.Controls.Add(Me.dgvProdModstockPrecio)
        Me.grbProdModStockPre.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbProdModStockPre.Location = New System.Drawing.Point(10, 382)
        Me.grbProdModStockPre.Name = "grbProdModStockPre"
        Me.grbProdModStockPre.Size = New System.Drawing.Size(1260, 268)
        Me.grbProdModStockPre.TabIndex = 30
        Me.grbProdModStockPre.TabStop = False
        Me.grbProdModStockPre.Text = "Productos"
        '
        'chkFilModStockPrecio
        '
        Me.chkFilModStockPrecio.AutoSize = True
        Me.chkFilModStockPrecio.Location = New System.Drawing.Point(896, 19)
        Me.chkFilModStockPrecio.Name = "chkFilModStockPrecio"
        Me.chkFilModStockPrecio.Size = New System.Drawing.Size(64, 22)
        Me.chkFilModStockPrecio.TabIndex = 37
        Me.chkFilModStockPrecio.Text = "Filtrar"
        Me.chkFilModStockPrecio.UseVisualStyleBackColor = True
        '
        'cmbFiltroSubcat
        '
        Me.cmbFiltroSubcat.FormattingEnabled = True
        Me.cmbFiltroSubcat.Location = New System.Drawing.Point(655, 17)
        Me.cmbFiltroSubcat.Name = "cmbFiltroSubcat"
        Me.cmbFiltroSubcat.Size = New System.Drawing.Size(164, 26)
        Me.cmbFiltroSubcat.TabIndex = 36
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(519, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(103, 18)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Subcategorias"
        '
        'cmbCatModStockPrecio
        '
        Me.cmbCatModStockPrecio.FormattingEnabled = True
        Me.cmbCatModStockPrecio.Location = New System.Drawing.Point(268, 17)
        Me.cmbCatModStockPrecio.Name = "cmbCatModStockPrecio"
        Me.cmbCatModStockPrecio.Size = New System.Drawing.Size(164, 26)
        Me.cmbCatModStockPrecio.TabIndex = 32
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Location = New System.Drawing.Point(190, 20)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(72, 18)
        Me.Label109.TabIndex = 31
        Me.Label109.Text = "Categoria"
        '
        'dgvProdModstockPrecio
        '
        Me.dgvProdModstockPrecio.AllowUserToAddRows = False
        Me.dgvProdModstockPrecio.AllowUserToDeleteRows = False
        Me.dgvProdModstockPrecio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProdModstockPrecio.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn49, Me.DataGridViewTextBoxColumn50, Me.DataGridViewTextBoxColumn51, Me.DataGridViewTextBoxColumn55, Me.DataGridViewTextBoxColumn56, Me.DataGridViewTextBoxColumn58, Me.DataGridViewTextBoxColumn59, Me.DataGridViewTextBoxColumn60, Me.DataGridViewTextBoxColumn61})
        Me.dgvProdModstockPrecio.Location = New System.Drawing.Point(6, 49)
        Me.dgvProdModstockPrecio.Name = "dgvProdModstockPrecio"
        Me.dgvProdModstockPrecio.ReadOnly = True
        Me.dgvProdModstockPrecio.Size = New System.Drawing.Size(1170, 213)
        Me.dgvProdModstockPrecio.TabIndex = 28
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        Me.DataGridViewTextBoxColumn49.ReadOnly = True
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.HeaderText = "Nombre Producto"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        Me.DataGridViewTextBoxColumn50.ReadOnly = True
        '
        'DataGridViewTextBoxColumn51
        '
        Me.DataGridViewTextBoxColumn51.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn51.Name = "DataGridViewTextBoxColumn51"
        Me.DataGridViewTextBoxColumn51.ReadOnly = True
        '
        'DataGridViewTextBoxColumn55
        '
        Me.DataGridViewTextBoxColumn55.HeaderText = "Categoria"
        Me.DataGridViewTextBoxColumn55.Name = "DataGridViewTextBoxColumn55"
        Me.DataGridViewTextBoxColumn55.ReadOnly = True
        '
        'DataGridViewTextBoxColumn56
        '
        Me.DataGridViewTextBoxColumn56.HeaderText = "Subcategoria"
        Me.DataGridViewTextBoxColumn56.Name = "DataGridViewTextBoxColumn56"
        Me.DataGridViewTextBoxColumn56.ReadOnly = True
        '
        'DataGridViewTextBoxColumn58
        '
        Me.DataGridViewTextBoxColumn58.HeaderText = "Stock"
        Me.DataGridViewTextBoxColumn58.Name = "DataGridViewTextBoxColumn58"
        Me.DataGridViewTextBoxColumn58.ReadOnly = True
        '
        'DataGridViewTextBoxColumn59
        '
        Me.DataGridViewTextBoxColumn59.HeaderText = "Stock Mínimo"
        Me.DataGridViewTextBoxColumn59.Name = "DataGridViewTextBoxColumn59"
        Me.DataGridViewTextBoxColumn59.ReadOnly = True
        '
        'DataGridViewTextBoxColumn60
        '
        Me.DataGridViewTextBoxColumn60.HeaderText = "Estado"
        Me.DataGridViewTextBoxColumn60.Name = "DataGridViewTextBoxColumn60"
        Me.DataGridViewTextBoxColumn60.ReadOnly = True
        '
        'DataGridViewTextBoxColumn61
        '
        Me.DataGridViewTextBoxColumn61.HeaderText = "Fecha De Alta"
        Me.DataGridViewTextBoxColumn61.Name = "DataGridViewTextBoxColumn61"
        Me.DataGridViewTextBoxColumn61.ReadOnly = True
        '
        'grbDatosProdModPrecio
        '
        Me.grbDatosProdModPrecio.BackColor = System.Drawing.Color.MediumTurquoise
        Me.grbDatosProdModPrecio.Controls.Add(Me.txbPrecioActModPrecio)
        Me.grbDatosProdModPrecio.Controls.Add(Me.txbNroProdModPrecio)
        Me.grbDatosProdModPrecio.Controls.Add(Me.txbProdModPrecio)
        Me.grbDatosProdModPrecio.Controls.Add(Me.btnCancelarModPrecio)
        Me.grbDatosProdModPrecio.Controls.Add(Me.txbPrecioModProd)
        Me.grbDatosProdModPrecio.Controls.Add(Me.btnModPrecioAdm)
        Me.grbDatosProdModPrecio.Controls.Add(Me.Label99)
        Me.grbDatosProdModPrecio.Controls.Add(Me.txbDetalleModPrecio)
        Me.grbDatosProdModPrecio.Controls.Add(Me.cmbMotivoPrecioModProd)
        Me.grbDatosProdModPrecio.Controls.Add(Me.Label100)
        Me.grbDatosProdModPrecio.Controls.Add(Me.Label104)
        Me.grbDatosProdModPrecio.Controls.Add(Me.Label105)
        Me.grbDatosProdModPrecio.Controls.Add(Me.Label106)
        Me.grbDatosProdModPrecio.Controls.Add(Me.Label107)
        Me.grbDatosProdModPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProdModPrecio.Location = New System.Drawing.Point(617, 12)
        Me.grbDatosProdModPrecio.Name = "grbDatosProdModPrecio"
        Me.grbDatosProdModPrecio.Size = New System.Drawing.Size(651, 364)
        Me.grbDatosProdModPrecio.TabIndex = 29
        Me.grbDatosProdModPrecio.TabStop = False
        Me.grbDatosProdModPrecio.Text = "Modificar Precio"
        '
        'txbPrecioActModPrecio
        '
        Me.txbPrecioActModPrecio.Location = New System.Drawing.Point(289, 103)
        Me.txbPrecioActModPrecio.Name = "txbPrecioActModPrecio"
        Me.txbPrecioActModPrecio.ReadOnly = True
        Me.txbPrecioActModPrecio.Size = New System.Drawing.Size(121, 24)
        Me.txbPrecioActModPrecio.TabIndex = 19
        '
        'txbNroProdModPrecio
        '
        Me.txbNroProdModPrecio.Location = New System.Drawing.Point(289, 62)
        Me.txbNroProdModPrecio.Name = "txbNroProdModPrecio"
        Me.txbNroProdModPrecio.ReadOnly = True
        Me.txbNroProdModPrecio.Size = New System.Drawing.Size(105, 24)
        Me.txbNroProdModPrecio.TabIndex = 18
        '
        'txbProdModPrecio
        '
        Me.txbProdModPrecio.Location = New System.Drawing.Point(289, 25)
        Me.txbProdModPrecio.Name = "txbProdModPrecio"
        Me.txbProdModPrecio.ReadOnly = True
        Me.txbProdModPrecio.Size = New System.Drawing.Size(310, 24)
        Me.txbProdModPrecio.TabIndex = 17
        '
        'btnCancelarModPrecio
        '
        Me.btnCancelarModPrecio.Location = New System.Drawing.Point(543, 187)
        Me.btnCancelarModPrecio.Name = "btnCancelarModPrecio"
        Me.btnCancelarModPrecio.Size = New System.Drawing.Size(90, 76)
        Me.btnCancelarModPrecio.TabIndex = 14
        Me.btnCancelarModPrecio.Text = "Cancelar"
        Me.btnCancelarModPrecio.UseVisualStyleBackColor = True
        '
        'txbPrecioModProd
        '
        Me.txbPrecioModProd.Location = New System.Drawing.Point(289, 140)
        Me.txbPrecioModProd.Name = "txbPrecioModProd"
        Me.txbPrecioModProd.Size = New System.Drawing.Size(121, 24)
        Me.txbPrecioModProd.TabIndex = 13
        '
        'btnModPrecioAdm
        '
        Me.btnModPrecioAdm.Location = New System.Drawing.Point(543, 266)
        Me.btnModPrecioAdm.Name = "btnModPrecioAdm"
        Me.btnModPrecioAdm.Size = New System.Drawing.Size(90, 76)
        Me.btnModPrecioAdm.TabIndex = 12
        Me.btnModPrecioAdm.Text = "Modificar"
        Me.btnModPrecioAdm.UseVisualStyleBackColor = True
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(6, 230)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(136, 18)
        Me.Label99.TabIndex = 11
        Me.Label99.Text = "Detalles Del Motivo"
        '
        'txbDetalleModPrecio
        '
        Me.txbDetalleModPrecio.Location = New System.Drawing.Point(6, 254)
        Me.txbDetalleModPrecio.Multiline = True
        Me.txbDetalleModPrecio.Name = "txbDetalleModPrecio"
        Me.txbDetalleModPrecio.Size = New System.Drawing.Size(515, 100)
        Me.txbDetalleModPrecio.TabIndex = 10
        '
        'cmbMotivoPrecioModProd
        '
        Me.cmbMotivoPrecioModProd.FormattingEnabled = True
        Me.cmbMotivoPrecioModProd.Location = New System.Drawing.Point(289, 184)
        Me.cmbMotivoPrecioModProd.Name = "cmbMotivoPrecioModProd"
        Me.cmbMotivoPrecioModProd.Size = New System.Drawing.Size(232, 26)
        Me.cmbMotivoPrecioModProd.TabIndex = 9
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Location = New System.Drawing.Point(6, 187)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(53, 18)
        Me.Label100.TabIndex = 8
        Me.Label100.Text = "Motivo"
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.Location = New System.Drawing.Point(7, 62)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(127, 18)
        Me.Label104.TabIndex = 3
        Me.Label104.Text = "Número Producto"
        Me.Label104.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Location = New System.Drawing.Point(3, 143)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(128, 18)
        Me.Label105.TabIndex = 2
        Me.Label105.Text = "Precio Modificado"
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Location = New System.Drawing.Point(6, 103)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(95, 18)
        Me.Label106.TabIndex = 1
        Me.Label106.Text = "Precio Actual"
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Location = New System.Drawing.Point(6, 25)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(69, 18)
        Me.Label107.TabIndex = 0
        Me.Label107.Text = "Producto"
        '
        'grbDatosProdModStock
        '
        Me.grbDatosProdModStock.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.grbDatosProdModStock.Controls.Add(Me.txbStockActModStock)
        Me.grbDatosProdModStock.Controls.Add(Me.txbNroProdModStock)
        Me.grbDatosProdModStock.Controls.Add(Me.txbProdModStock)
        Me.grbDatosProdModStock.Controls.Add(Me.btnCancelarModStock)
        Me.grbDatosProdModStock.Controls.Add(Me.btnModStockProd)
        Me.grbDatosProdModStock.Controls.Add(Me.Label98)
        Me.grbDatosProdModStock.Controls.Add(Me.txbDetalleModStock)
        Me.grbDatosProdModStock.Controls.Add(Me.cmbMotivoModStock)
        Me.grbDatosProdModStock.Controls.Add(Me.Label97)
        Me.grbDatosProdModStock.Controls.Add(Me.nudModificarStock)
        Me.grbDatosProdModStock.Controls.Add(Me.Label93)
        Me.grbDatosProdModStock.Controls.Add(Me.Label92)
        Me.grbDatosProdModStock.Controls.Add(Me.Label91)
        Me.grbDatosProdModStock.Controls.Add(Me.Label90)
        Me.grbDatosProdModStock.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProdModStock.Location = New System.Drawing.Point(8, 12)
        Me.grbDatosProdModStock.Name = "grbDatosProdModStock"
        Me.grbDatosProdModStock.Size = New System.Drawing.Size(603, 364)
        Me.grbDatosProdModStock.TabIndex = 28
        Me.grbDatosProdModStock.TabStop = False
        Me.grbDatosProdModStock.Text = "Modificar Stock"
        '
        'txbStockActModStock
        '
        Me.txbStockActModStock.Location = New System.Drawing.Point(265, 97)
        Me.txbStockActModStock.Name = "txbStockActModStock"
        Me.txbStockActModStock.ReadOnly = True
        Me.txbStockActModStock.Size = New System.Drawing.Size(105, 24)
        Me.txbStockActModStock.TabIndex = 16
        '
        'txbNroProdModStock
        '
        Me.txbNroProdModStock.Location = New System.Drawing.Point(265, 56)
        Me.txbNroProdModStock.Name = "txbNroProdModStock"
        Me.txbNroProdModStock.ReadOnly = True
        Me.txbNroProdModStock.Size = New System.Drawing.Size(105, 24)
        Me.txbNroProdModStock.TabIndex = 15
        '
        'txbProdModStock
        '
        Me.txbProdModStock.Location = New System.Drawing.Point(265, 19)
        Me.txbProdModStock.Name = "txbProdModStock"
        Me.txbProdModStock.ReadOnly = True
        Me.txbProdModStock.Size = New System.Drawing.Size(310, 24)
        Me.txbProdModStock.TabIndex = 14
        '
        'btnCancelarModStock
        '
        Me.btnCancelarModStock.Location = New System.Drawing.Point(485, 192)
        Me.btnCancelarModStock.Name = "btnCancelarModStock"
        Me.btnCancelarModStock.Size = New System.Drawing.Size(90, 76)
        Me.btnCancelarModStock.TabIndex = 13
        Me.btnCancelarModStock.Text = "Cancelar"
        Me.btnCancelarModStock.UseVisualStyleBackColor = True
        '
        'btnModStockProd
        '
        Me.btnModStockProd.Location = New System.Drawing.Point(485, 278)
        Me.btnModStockProd.Name = "btnModStockProd"
        Me.btnModStockProd.Size = New System.Drawing.Size(90, 76)
        Me.btnModStockProd.TabIndex = 12
        Me.btnModStockProd.Text = "Modificar"
        Me.btnModStockProd.UseVisualStyleBackColor = True
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Location = New System.Drawing.Point(6, 230)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(136, 18)
        Me.Label98.TabIndex = 11
        Me.Label98.Text = "Detalles Del Motivo"
        '
        'txbDetalleModStock
        '
        Me.txbDetalleModStock.Location = New System.Drawing.Point(6, 254)
        Me.txbDetalleModStock.Multiline = True
        Me.txbDetalleModStock.Name = "txbDetalleModStock"
        Me.txbDetalleModStock.Size = New System.Drawing.Size(463, 100)
        Me.txbDetalleModStock.TabIndex = 10
        '
        'cmbMotivoModStock
        '
        Me.cmbMotivoModStock.FormattingEnabled = True
        Me.cmbMotivoModStock.Location = New System.Drawing.Point(305, 192)
        Me.cmbMotivoModStock.Name = "cmbMotivoModStock"
        Me.cmbMotivoModStock.Size = New System.Drawing.Size(164, 26)
        Me.cmbMotivoModStock.TabIndex = 9
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Location = New System.Drawing.Point(3, 192)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(53, 18)
        Me.Label97.TabIndex = 8
        Me.Label97.Text = "Motivo"
        '
        'nudModificarStock
        '
        Me.nudModificarStock.Location = New System.Drawing.Point(349, 146)
        Me.nudModificarStock.Name = "nudModificarStock"
        Me.nudModificarStock.Size = New System.Drawing.Size(120, 24)
        Me.nudModificarStock.TabIndex = 6
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(3, 62)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(127, 18)
        Me.Label93.TabIndex = 3
        Me.Label93.Text = "Número Producto"
        Me.Label93.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(3, 148)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(82, 18)
        Me.Label92.TabIndex = 2
        Me.Label92.Text = "Stock Final"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(3, 103)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(91, 18)
        Me.Label91.TabIndex = 1
        Me.Label91.Text = "Stock Actual"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(6, 25)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(69, 18)
        Me.Label90.TabIndex = 0
        Me.Label90.Text = "Producto"
        '
        'tpAyudas
        '
        Me.tpAyudas.AutoPopDelay = 5000
        Me.tpAyudas.InitialDelay = 500
        Me.tpAyudas.ReshowDelay = 200
        '
        'eprProdAdm
        '
        Me.eprProdAdm.ContainerControl = Me
        '
        'frmGestionarProductosAdm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1297, 749)
        Me.Controls.Add(Me.tbcGestionProdAdm)
        Me.Name = "frmGestionarProductosAdm"
        Me.Text = "Gestionar Productos Administrador"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcGestionProdAdm.ResumeLayout(False)
        Me.tpgAgregarProd.ResumeLayout(False)
        Me.grbDatosNuevosProd.ResumeLayout(False)
        Me.grbDatosNuevosProd.PerformLayout()
        Me.grbDatosProductoNuevo.ResumeLayout(False)
        Me.grbDatosProductoNuevo.PerformLayout()
        CType(Me.nudGarantiaNuevoProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudStockMinNuevoProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyDescNuevoProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyudaGarantia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyudaStockMin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyudaPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbFotoAltaProd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tgpModProd.ResumeLayout(False)
        Me.grbDatosProductosMod.ResumeLayout(False)
        CType(Me.dgvListaProdMod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.ptbAyudaFiltroModProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAySubcatProdFil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyCatProdFil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyNombreProdFil, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpbModProductos.ResumeLayout(False)
        Me.gpbModProductos.PerformLayout()
        CType(Me.nudModGarProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudModStockMinProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyudaGarModProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyudaStockMinModProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbFotoModProd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgNuevaCat.ResumeLayout(False)
        Me.grbNuevaCatSub.ResumeLayout(False)
        Me.grbNuevaCatSub.PerformLayout()
        CType(Me.ptbAyNuevaCatSub, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCatSubAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbNuevaSubcat.ResumeLayout(False)
        Me.grbNuevaSubcat.PerformLayout()
        CType(Me.ptbAyNuevaSubcat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSubAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbNuevaCategoria.ResumeLayout(False)
        Me.grbNuevaCategoria.PerformLayout()
        CType(Me.ptbAyNuevaCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCategoriasAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgProveedores.ResumeLayout(False)
        Me.grbModDatosProv.ResumeLayout(False)
        Me.grbModDatosProv.PerformLayout()
        CType(Me.ptbAyNomModProv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyDirModProv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosProveedor.ResumeLayout(False)
        Me.grbDatosProveedor.PerformLayout()
        CType(Me.ptbAyNombreProv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbAyTelProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbProveedores.ResumeLayout(False)
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgProvProd.ResumeLayout(False)
        Me.grbDatosProdProv.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.grbProvProdProv.ResumeLayout(False)
        Me.grbProvProdProv.PerformLayout()
        CType(Me.dgvProv_ProvProd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosProdProvProd.ResumeLayout(False)
        Me.grbDatosProdProvProd.PerformLayout()
        CType(Me.dgvProvProd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgCompras.ResumeLayout(False)
        Me.grbFiltrosCompras.ResumeLayout(False)
        Me.grbFiltrosCompras.PerformLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.grbModPrecioCompra.ResumeLayout(False)
        Me.grbModPrecioCompra.PerformLayout()
        CType(Me.nudCantidadCompra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbCarritoCompra.ResumeLayout(False)
        Me.grbCarritoCompra.PerformLayout()
        CType(Me.dgvCarritoCompras, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox11.ResumeLayout(False)
        CType(Me.dgvProvProdCompras, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbModStockProd.ResumeLayout(False)
        Me.grbProdModStockPre.ResumeLayout(False)
        Me.grbProdModStockPre.PerformLayout()
        CType(Me.dgvProdModstockPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosProdModPrecio.ResumeLayout(False)
        Me.grbDatosProdModPrecio.PerformLayout()
        Me.grbDatosProdModStock.ResumeLayout(False)
        Me.grbDatosProdModStock.PerformLayout()
        CType(Me.nudModificarStock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.eprProdAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbcGestionProdAdm As System.Windows.Forms.TabControl
    Friend WithEvents tpgAgregarProd As System.Windows.Forms.TabPage
    Friend WithEvents grbDatosNuevosProd As System.Windows.Forms.GroupBox
    Friend WithEvents btnBuscarFotoNuevoProd As System.Windows.Forms.Button
    Friend WithEvents ptbFotoAltaProd As System.Windows.Forms.PictureBox
    Friend WithEvents tgpModProd As System.Windows.Forms.TabPage
    Friend WithEvents btnNuegoAgregarLista As System.Windows.Forms.Button
    Friend WithEvents gpbModProductos As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbModSub As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmbModCat As System.Windows.Forms.ComboBox
    Friend WithEvents txbModProdNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents btnFotoModProd As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbFiltroSubProdAdm As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFiltroCatProdAdm As System.Windows.Forms.ComboBox
    Friend WithEvents btnFiltrarModProd As System.Windows.Forms.Button
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txbNomModProdFil As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents grbDatosProductoNuevo As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txbPrecioProdAdmNuevo As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbSubCatProdAdmNuevo As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbCatProdAdmNuevo As System.Windows.Forms.ComboBox
    Friend WithEvents txbNombreProdAdmNuevo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblDescProdAdmNuevo As System.Windows.Forms.Label
    Friend WithEvents txbDescProdAdmNuevo As System.Windows.Forms.TextBox
    Friend WithEvents btnIngresarProdNuevo As System.Windows.Forms.Button
    Friend WithEvents ptbFotoModProd As System.Windows.Forms.PictureBox
    Friend WithEvents txbDescModProd As System.Windows.Forms.TextBox
    Friend WithEvents btnModProducto As System.Windows.Forms.Button
    Friend WithEvents tpgNuevaCat As System.Windows.Forms.TabPage
    Friend WithEvents grbNuevaCatSub As System.Windows.Forms.GroupBox
    Friend WithEvents cmbNuevasSubCatAdm As System.Windows.Forms.ComboBox
    Friend WithEvents cmbNuevasCatAdm As System.Windows.Forms.ComboBox
    Friend WithEvents btnAgregarNuevaCatSub As System.Windows.Forms.Button
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents dgvCatSubAdm As System.Windows.Forms.DataGridView
    Friend WithEvents grbNuevaSubcat As System.Windows.Forms.GroupBox
    Friend WithEvents btnAgregarNuevaSubCat As System.Windows.Forms.Button
    Friend WithEvents txbDescripcionNuevaSubCat As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txbNuevaSubcat As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents dgvSubAdm As System.Windows.Forms.DataGridView
    Friend WithEvents grbNuevaCategoria As System.Windows.Forms.GroupBox
    Friend WithEvents btnAgregarNuevaCat As System.Windows.Forms.Button
    Friend WithEvents txbDescripcionNuevaCat As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents txbNuevaCategoria As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents dgvCategoriasAdm As System.Windows.Forms.DataGridView
    Friend WithEvents tpgProveedores As System.Windows.Forms.TabPage
    Friend WithEvents grbDatosProveedor As System.Windows.Forms.GroupBox
    Friend WithEvents btnAgregarProv As System.Windows.Forms.Button
    Friend WithEvents txbMailProv As System.Windows.Forms.TextBox
    Friend WithEvents txbCelProv As System.Windows.Forms.TextBox
    Friend WithEvents txbTelProv As System.Windows.Forms.TextBox
    Friend WithEvents txbDireccionProv As System.Windows.Forms.TextBox
    Friend WithEvents txbNombreProv As System.Windows.Forms.TextBox
    Friend WithEvents lblCelularProv As System.Windows.Forms.Label
    Friend WithEvents lblMailProveedor As System.Windows.Forms.Label
    Friend WithEvents lblTelefonoProv As System.Windows.Forms.Label
    Friend WithEvents lblDireccionProv As System.Windows.Forms.Label
    Friend WithEvents lblNombreProveedor As System.Windows.Forms.Label
    Friend WithEvents grbProveedores As System.Windows.Forms.GroupBox
    Friend WithEvents dgvProveedores As System.Windows.Forms.DataGridView
    Friend WithEvents grbModDatosProv As System.Windows.Forms.GroupBox
    Friend WithEvents btnModProv As System.Windows.Forms.Button
    Friend WithEvents txbMailModProv As System.Windows.Forms.TextBox
    Friend WithEvents txbCelModProv As System.Windows.Forms.TextBox
    Friend WithEvents txbTelModProv As System.Windows.Forms.TextBox
    Friend WithEvents txbDirModProv As System.Windows.Forms.TextBox
    Friend WithEvents txbNombreModProv As System.Windows.Forms.TextBox
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents tpgProvProd As System.Windows.Forms.TabPage
    Friend WithEvents grbDatosProdProvProd As System.Windows.Forms.GroupBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvProvProd As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grbProvProdProv As System.Windows.Forms.GroupBox
    Friend WithEvents dgvProv_ProvProd As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grbDatosProdProv As System.Windows.Forms.GroupBox
    Friend WithEvents btnPrecioCompraProd As System.Windows.Forms.Button
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents txbPrecioCompraProd As System.Windows.Forms.TextBox
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents lblTituloPrecioVenta As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents lblIdProducto As System.Windows.Forms.Label
    Friend WithEvents lblIdProveedor As System.Windows.Forms.Label
    Friend WithEvents lblTituloProducto As System.Windows.Forms.Label
    Friend WithEvents lblTituloProveedor As System.Windows.Forms.Label
    Friend WithEvents txbFiltrarProvProdProv As System.Windows.Forms.TextBox
    Friend WithEvents lblFiltrarProvProdProv As System.Windows.Forms.Label
    Friend WithEvents btnSigProdProv_Prod As System.Windows.Forms.Button
    Friend WithEvents btnAntProdProv_Prod As System.Windows.Forms.Button
    Friend WithEvents txbFiltrarProdProv As System.Windows.Forms.TextBox
    Friend WithEvents lblFiltradoProdProv As System.Windows.Forms.Label
    Friend WithEvents tpgCompras As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvProvProdCompras As System.Windows.Forms.DataGridView
    Friend WithEvents grbCarritoCompra As System.Windows.Forms.GroupBox
    Friend WithEvents dgvCarritoCompras As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn52 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn53 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn54 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn57 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAsignarCarritoCompra As System.Windows.Forms.Button
    Friend WithEvents nudCantidadCompra As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents grbFiltrosCompras As System.Windows.Forms.GroupBox
    Friend WithEvents txbProvFilCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents txbProdFilCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents grbModStockProd As System.Windows.Forms.TabPage
    Friend WithEvents grbDatosProdModStock As System.Windows.Forms.GroupBox
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents grbDatosProductosMod As System.Windows.Forms.GroupBox
    Friend WithEvents dgvListaProdMod As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txbDescCatSub As System.Windows.Forms.TextBox
    Friend WithEvents idCat_CatSub As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idSub_CatSub As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descripcionCatSub As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idSubcategoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombreSubcategoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descripcionSub As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idCategoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombreCategoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descripcionCategoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grbProdModStockPre As System.Windows.Forms.GroupBox
    Friend WithEvents cmbCatModStockPrecio As System.Windows.Forms.ComboBox
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents dgvProdModstockPrecio As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn49 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn51 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn55 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn56 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn58 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn59 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn60 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn61 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grbDatosProdModPrecio As System.Windows.Forms.GroupBox
    Friend WithEvents txbPrecioModProd As System.Windows.Forms.TextBox
    Friend WithEvents btnModPrecioAdm As System.Windows.Forms.Button
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents txbDetalleModPrecio As System.Windows.Forms.TextBox
    Friend WithEvents cmbMotivoPrecioModProd As System.Windows.Forms.ComboBox
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents btnModStockProd As System.Windows.Forms.Button
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents txbDetalleModStock As System.Windows.Forms.TextBox
    Friend WithEvents cmbMotivoModStock As System.Windows.Forms.ComboBox
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents nudModificarStock As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnModificarCatSub As System.Windows.Forms.Button
    Friend WithEvents btnModificarSubCat As System.Windows.Forms.Button
    Friend WithEvents btnModificarCat As System.Windows.Forms.Button
    Friend WithEvents nudGarantiaNuevoProd As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudStockMinNuevoProd As System.Windows.Forms.NumericUpDown
    Friend WithEvents ptbAyDescNuevoProd As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyudaGarantia As System.Windows.Forms.PictureBox
    Friend WithEvents tpAyudas As System.Windows.Forms.ToolTip
    Friend WithEvents ptbAyudaStockMin As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyudaPrecio As System.Windows.Forms.PictureBox
    Friend WithEvents nudModGarProd As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudModStockMinProd As System.Windows.Forms.NumericUpDown
    Friend WithEvents ptbAyudaGarModProd As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyudaStockMinModProd As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyudaFiltroModProd As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyNombreProdFil As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyTelProv As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents lblRutaFotoModProd As System.Windows.Forms.Label
    Friend WithEvents ofdBuscarFoto As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txbCodAreaModProv As System.Windows.Forms.TextBox
    Friend WithEvents txbCodAreaCel As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents ptbAyDirModProv As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyNombreProv As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyNomModProv As System.Windows.Forms.PictureBox
    Friend WithEvents btnCancelarModCatSub As System.Windows.Forms.Button
    Friend WithEvents btnCancelarModSubcat As System.Windows.Forms.Button
    Friend WithEvents btnCancelarModCat As System.Windows.Forms.Button
    Friend WithEvents ptbAyNuevaCatSub As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyNuevaSubcat As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyNuevaCat As System.Windows.Forms.PictureBox
    Friend WithEvents ckbModificarNombreCat As System.Windows.Forms.CheckBox
    Friend WithEvents ckbModDatosSubcat As System.Windows.Forms.CheckBox
    Friend WithEvents chkModNombreProd As System.Windows.Forms.CheckBox
    Friend WithEvents chkModNombreProve As System.Windows.Forms.CheckBox
    Friend WithEvents ckbModCatSub As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelarModProd As System.Windows.Forms.Button
    Friend WithEvents txbRutaFotoModProd As System.Windows.Forms.TextBox
    Friend WithEvents txbRutaFotoNuevoProd As System.Windows.Forms.TextBox
    Friend WithEvents chkCambiarCatSubModProd As System.Windows.Forms.CheckBox
    Friend WithEvents txbIdModProd As System.Windows.Forms.TextBox
    Friend WithEvents lblTituloNroProd As System.Windows.Forms.Label
    Friend WithEvents txbIdModProv As System.Windows.Forms.TextBox
    Friend WithEvents lblTituloNroModProv As System.Windows.Forms.Label
    Friend WithEvents txbPrecioVenta_ProvProd As System.Windows.Forms.TextBox
    Friend WithEvents txbProd_ProvProd As System.Windows.Forms.TextBox
    Friend WithEvents txbProv_ProdProv As System.Windows.Forms.TextBox
    Friend WithEvents txbNombreProdCompra As System.Windows.Forms.TextBox
    Friend WithEvents ckbConfModPrecio As System.Windows.Forms.CheckBox
    Friend WithEvents grbModPrecioCompra As System.Windows.Forms.GroupBox
    Friend WithEvents cmbMotivoModCompra As System.Windows.Forms.ComboBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents txbModiPrecioCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents txbNroProvCompra As System.Windows.Forms.TextBox
    Friend WithEvents chkDescPrecioCompra As System.Windows.Forms.CheckBox
    Friend WithEvents chkAuPrecioCompra As System.Windows.Forms.CheckBox
    Friend WithEvents btnMostrarRelacionProdProv As System.Windows.Forms.Button
    Friend WithEvents btnCancelarRelacion As System.Windows.Forms.Button
    Friend WithEvents txbPrecioActModPrecio As System.Windows.Forms.TextBox
    Friend WithEvents txbNroProdModPrecio As System.Windows.Forms.TextBox
    Friend WithEvents txbProdModPrecio As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelarModPrecio As System.Windows.Forms.Button
    Friend WithEvents txbStockActModStock As System.Windows.Forms.TextBox
    Friend WithEvents txbNroProdModStock As System.Windows.Forms.TextBox
    Friend WithEvents txbProdModStock As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelarModStock As System.Windows.Forms.Button
    Friend WithEvents lblNroProd As System.Windows.Forms.Label
    Friend WithEvents lblTituloNroProv As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txbNroProdCompra As System.Windows.Forms.TextBox
    Friend WithEvents txbPrecioCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents txbProvCompra As System.Windows.Forms.TextBox
    Friend WithEvents txbDescModCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents btnComprar As System.Windows.Forms.Button
    Friend WithEvents btnCancelarModCarrito As System.Windows.Forms.Button
    Friend WithEvents btnModCarrito As System.Windows.Forms.Button
    Friend WithEvents chkFilProdSubCat As System.Windows.Forms.CheckBox
    Friend WithEvents chkFilPorCat As System.Windows.Forms.CheckBox
    Friend WithEvents chkFilPorNombre As System.Windows.Forms.CheckBox
    Friend WithEvents ptbAySubcatProdFil As System.Windows.Forms.PictureBox
    Friend WithEvents ptbAyCatProdFil As System.Windows.Forms.PictureBox
    Friend WithEvents btnFiltrarProveedor As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarProd As System.Windows.Forms.Button
    Friend WithEvents btnFiltrarProdProv As System.Windows.Forms.Button
    Friend WithEvents cmbFiltroSubcat As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkFilModStockPrecio As System.Windows.Forms.CheckBox
    Friend WithEvents chkMostrarTodos As System.Windows.Forms.CheckBox
    Friend WithEvents eprProdAdm As System.Windows.Forms.ErrorProvider
End Class
