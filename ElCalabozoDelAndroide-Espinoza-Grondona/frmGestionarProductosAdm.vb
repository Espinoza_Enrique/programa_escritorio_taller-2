﻿Imports BusinessLayer
Imports Entities
Imports System.IO
Imports System.Drawing

Public Class frmGestionarProductosAdm
    Private Enum opcionesProductos
        NuevoProducto
        ModDatosProducto
        AgregarCat
        AgregarProveedor
        ProveedorProducto
        Compras
        ModificarStockPrecio
    End Enum

    '------------------Métodos para la pestaña Agregar Producto----------------------'
    Private Function getProvedorError() As ErrorProvider
        Return Me.eprProdAdm
    End Function

    'Retorna al objeto textbox donde se coloca el nombre del producto para agregar un nuevo producto
    Private Function getTxbNombreProductoNuevo() As TextBox
        Return Me.txbNombreProdAdmNuevo
    End Function

    'Retorna al objeto combobox que lista las categorías del producto para agregar un nuevo producto
    Private Function getListaCategoriaNuevo() As ComboBox
        Return Me.cmbCatProdAdmNuevo
    End Function

    'Retorna al objeto combobox que lista las subcategorias del producto para agregar un nuevo producto
    Private Function getListaSubcategoriasNuevo() As ComboBox
        Return Me.cmbSubCatProdAdmNuevo
    End Function

    'Retorna al objeto textbox donde estará el precio del producto nuevo
    Private Function getTxbPrecioProdNuevo() As TextBox
        Return Me.txbPrecioProdAdmNuevo
    End Function

    'Retorna al objeto numeric up-down donde se colocará el stock mínimo para el nuevo producto
    Private Function getStockMinNuevo() As NumericUpDown
        Return Me.nudStockMinNuevoProd
    End Function

    'Retorna al objeto numeric up-down donde se colocará la garantía del nuevo producto
    Private Function getGarantiaNuevo() As NumericUpDown
        Return Me.nudGarantiaNuevoProd
    End Function

    'Retorna al objeto textbox donde se colocará la descripción del nuevo producto
    Private Function getTxtDescripcionNuevo() As TextBox
        Return Me.txbDescProdAdmNuevo
    End Function

    'Retorna el objeto picture box donde se colocará la imagen para el nuevo producto
    Private Function getPtbImagenNuevo() As PictureBox
        Return Me.ptbFotoAltaProd
    End Function

    'Retorna el objeto Button que sirve para agregar la nueva foto
    Private Function getBtnBuscarImagenNuevo() As Button
        Return Me.btnBuscarFotoNuevoProd
    End Function
    'Retorna el objeto Button que sirve para agregar el nuevo producto
    Private Function getBtnAgregarProdNuevo() As Button
        Return Me.btnIngresarProdNuevo
    End Function
    'Retorna el objeto Textbox que sirve para guardar la ruta de la imagen del nuevo producto
    Private Function getTxbRutaImagenNuevo() As TextBox
        Return Me.txbRutaFotoNuevoProd
    End Function

    '------------------Finalizan los métodos para la pestaña Agregar Producto----------------------'

    '------------------Métodos para la pestaña Modificar Datos----------------------'

    '------------------Finalizan los métodos para la pestaña Modificar Datos----------------------'

    Private Sub cargarCriteriosFiltros(ByRef filtros As ComboBox)
        Try
            filtros.Items.Clear()
            filtros.Items.Add("-")
            filtros.Items.Add(">")
            filtros.Items.Add("<")
            filtros.Items.Add(">=")
            filtros.Items.Add("<=")
            filtros.Items.Add("=")
            filtros.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al cargar los criterios de filtro", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub limpiarDatosProducto()
        Try
            Me.txbNombreProdAdmNuevo.Text = ""
            Me.txbPrecioProdAdmNuevo.Text = ""
            Me.nudStockMinNuevoProd.Value = 1
            Me.nudGarantiaNuevoProd.Value = 0
            'Me.lblRutaProdAdmNuevo.Text = ""
            Me.txbDescProdAdmNuevo.Text = ""
            Me.cmbCatProdAdmNuevo.SelectedItem = 0
            Me.cmbSubCatProdAdmNuevo.ValueMember = 0
            Me.ptbFotoAltaProd.Image = Nothing
        Catch ex As Exception
            MsgBox("Error limpiar los datos después del ingreso de productos", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana.Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub frmGestionarProductosAdm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try

            Me.Dispose()
        Catch ex As Exception
            MsgBox("Error al intentar cerrar la ventana", vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub
    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'control-Tipo: TabControl-Representa el tab control cuyas tab pages se quiere seleccionar
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Sub seleccionarTabControl(ByVal elemento As Byte)
        Try
            If elemento >= 1 And elemento <= Me.tbcGestionProdAdm.TabCount Then
                Me.tbcGestionProdAdm.SelectedIndex = elemento - 1
                Me.abrirVentana()
            End If
        Catch ex As Exception
            MsgBox("Error al intentar seleccionar una pestaña válida", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub frmGestionarProductosAdm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.prepararFormularioProductosAdm()
        ' Me.cargarFondoPantalla("cobaltoplata.jpg")
    End Sub
    '--------------------------------------------------------------------------------------------------------------
    'Activa o desactiva los botones y opciones según si deseamos modificar las categorías, subcategorías o la
    'relación categorías-subcategorías.
    '--------------------------------------------------------------------------------------------------------------
    Private Sub habilitarODeshabilitarModCatSub(ByVal cualModificar As Byte, ByVal estado As Boolean)
        If cualModificar = 1 Then
            Me.btnAgregarNuevaCat.Enabled = Not estado
            Me.btnModificarCat.Enabled = estado
            Me.btnCancelarModCat.Enabled = estado
            Me.ckbModificarNombreCat.Enabled = estado
        ElseIf cualModificar = 2 Then
            Me.btnAgregarNuevaSubCat.Enabled = Not estado
            Me.ckbModDatosSubcat.Enabled = estado
            Me.btnModificarSubCat.Enabled = estado
            Me.btnCancelarModSubcat.Enabled = estado
        Else
            Me.btnAgregarNuevaCatSub.Enabled = Not estado
            Me.btnModificarCatSub.Enabled = estado
            Me.btnCancelarModCatSub.Enabled = estado
            Me.ckbModCatSub.Enabled = estado
            Me.ckbModCatSub.Checked = estado
            'Me.cmbNuevasCatAdm.Enabled = estado
            'Me.cmbNuevasSubCatAdm.Enabled = estado
        End If
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub



    Private Sub cmbCatProdAdmNuevo_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbCatProdAdmNuevo.SelectedValueChanged
        If IsNumeric(Me.cmbCatProdAdmNuevo.SelectedValue) Then
            Me.rellenarSubcategorias(Me.cmbSubCatProdAdmNuevo, Me.cmbCatProdAdmNuevo.SelectedValue)
        End If
    End Sub
    'evento para cuando quiero que ocurra algo ante el cambio del combo box


    'Permite preparar las condiciones iniciales del formulario
    Private Sub prepararFormularioProductosAdm()
        Try
            'evita que se modifique la lista desplegable
            Me.cmbCatProdAdmNuevo.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbFiltroCatProdAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbFiltroCatProdAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbFiltroSubProdAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbModCat.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbModSub.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbMotivoModStock.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbMotivoPrecioModProd.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbNuevasCatAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbNuevasSubCatAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbSubCatProdAdmNuevo.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbCatModStockPrecio.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbFiltroSubcat.DropDownStyle = ComboBoxStyle.DropDownList
            Me.cmbFiltroSubProdAdm.DropDownStyle = ComboBoxStyle.DropDownList
            Me.nudGarantiaNuevoProd.Value = 1
            Me.nudStockMinNuevoProd.Value = 1


            Me.establecerCategoriaSubcategoria(Me.cmbCatProdAdmNuevo.Text, Me.cmbSubCatProdAdmNuevo.Text)

            Me.btnAntProdProv_Prod.Image.RotateFlip(RotateFlipType.Rotate180FlipY)

            Me.rellenarCategorias(Me.cmbFiltroCatProdAdm)
            Me.rellenarCategorias(Me.cmbCatProdAdmNuevo)
            Me.rellenarCategorias(Me.cmbModCat)
            Me.rellenarCategorias(Me.cmbNuevasCatAdm)
            Me.rellenarCategorias(Me.cmbCatModStockPrecio)

            Me.rellenarTodasCatActivas(Me.cmbNuevasCatAdm)
            Me.rellenarTodasSubcatActivas(Me.cmbNuevasSubCatAdm)
            'Me.rellenarSubcategorias(Me.cmbSubCatProdAdmNuevo)
            'Me.rellenarSubcategorias(Me.cmbModSub)
            'Me.rellenarSubcategorias(Me.cmbNuevasSubCatAdm)
            'Me.rellenarSubcategorias(Me.cmbSubEliProdFil)
            'Me.rellenarSubcategorias(Me.cmbSubProdListFil)
            Me.rellenarMotivosDisPrecio(Me.cmbMotivoModCompra)
            Me.rellenarMotivosCambioStock(cmbMotivoModStock)
            Me.rellenarMotivosCambioPrecio(cmbMotivoPrecioModProd)
            Me.habilitarODeshabilitarModCatSub(0, False)
            Me.habilitarODeshabilitarModCatSub(1, False)
            Me.habilitarODeshabilitarModCatSub(2, False)
            'Me.prepararGrillaProductos(Me.dgvListaProdMod, True, True)
        Catch ex As Exception
            MsgBox("Error al dar formato a la venatana", vbOKOnly + vbCritical, "Error de formato")
        End Try
    End Sub
    'Muestra en una etiqueta la categoría y la subcategoría que tendrá el producto
    'Parámetros:
    'categoria-Tipo: String-El nombre de la categoría seleccionada
    'subcategoria-Tipo: String-El nombre de la subcategoria asociada
    Private Sub establecerCategoriaSubcategoria(ByVal categoria As String, ByVal subcategoria As String)
        Try

        Catch ex As Exception
            MsgBox("Error al clasificar al producto", vbOKOnly + vbCritical, "Error de cargar de información")
        End Try
    End Sub



    Private Sub nudStockMinNuevoProd_ValueChanged(sender As Object, e As EventArgs) Handles nudStockMinNuevoProd.ValueChanged
        Try
            If Me.nudStockMinNuevoProd.Value <= 0 Then
                MsgBox("El stock minimo no puede ser cero o menor", vbOKOnly, "Error de stock mínimo")
                Me.nudStockMinNuevoProd.Value = 1
            End If
        Catch ex As Exception
            MsgBox("Error al modificar stock minimo", vbOKOnly + vbCritical, "Error de datos")
        End Try
    End Sub

    Private Sub cmbFiltroPrecioProdAdm_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbFiltroStockProdAdm_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbFiltroStockMinProdAdm_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbFiltroGarProdAdm_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbPreEliProdFil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbStockEliProdFil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbStockMinEliProdFil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbGarEliProdFil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbPrecioProdListfil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbStockProdListFil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbStockMinProdListFil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Private Sub cmbGarProdListFil_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            MsgBox("Error al seleccionar opciones de filtro", vbOKOnly + vbCritical, "Error de selección")
        End Try
    End Sub

    Public Function controlarFiltro(ByVal nombre As String, ByVal precio As String) As Boolean
        Return General.controlarCajaEnBlanco(nombre) And Not General.controlarReales(precio)
    End Function


    Public Sub filtrar(ByVal nombre As String, ByVal cat As String, ByVal subcat As String, ByVal precio As String, ByVal critPre As String, ByVal stock As Integer, ByVal critStock As String, ByVal stockMin As Integer, ByVal critMin As String, ByVal garantia As Integer, ByVal critGar As String, ByVal estado As String)
        Dim mensajeFiltrado As String
        Dim precioValido As Boolean
        Dim nombreValido As Boolean
        Try
            precioValido = True
            nombreValido = True
            mensajeFiltrado = ""
            If critPre <> "-" Then
                If IsNumeric(precio) Then
                    mensajeFiltrado = mensajeFiltrado + "Se filtra por precio: precio" + critPre + precio + vbNewLine
                Else
                    mensajeFiltrado = mensajeFiltrado + "Coloque un valor numérico" + vbNewLine
                    precioValido = False
                End If

            Else
                mensajeFiltrado = mensajeFiltrado + "No Se filtra por precio:" + vbNewLine
            End If


            If General.controlarCajaEnBlanco(nombre) Then
                mensajeFiltrado = mensajeFiltrado + "No se filtra por nombre" + vbNewLine
            Else
                mensajeFiltrado = mensajeFiltrado + "Se filtra por nombre: " + nombre + vbNewLine
            End If


            If cat = "-" Then
                mensajeFiltrado = mensajeFiltrado + "No se filtra por categoria" + vbNewLine
            Else
                mensajeFiltrado = mensajeFiltrado + "Se filtra por categoria: " + cat + vbNewLine
            End If

            If subcat = "-" Then
                mensajeFiltrado = mensajeFiltrado + "No se filtra por subcategoria" + vbNewLine
            Else
                mensajeFiltrado = mensajeFiltrado + "Se filtra por subcategoria: " + subcat + vbNewLine
            End If

            If critStock = "-" Then
                mensajeFiltrado = mensajeFiltrado + "No se filtra por stock" + vbNewLine
            Else
                mensajeFiltrado = mensajeFiltrado + "Se filtra por stock: stock " + critStock + CStr(stock) + vbNewLine
            End If

            If critMin = "-" Then
                mensajeFiltrado = mensajeFiltrado + "No se filtra por stock minimo" + vbNewLine
            Else
                mensajeFiltrado = mensajeFiltrado + "Se filtra por stock minimo: stockMinimo" + critMin + CStr(stockMin) + vbNewLine
            End If

            If critGar = "-" Then
                mensajeFiltrado = mensajeFiltrado + "No se filtra por garantia" + vbNewLine
            Else
                mensajeFiltrado = mensajeFiltrado + "Se filtra por garantia: garantia" + critGar + CStr(garantia) + vbNewLine
            End If

            If estado = "-" Then
                mensajeFiltrado = mensajeFiltrado + "No se filtra por estado" + vbNewLine
            Else
                mensajeFiltrado = mensajeFiltrado + "Se filtra por estado: " + estado + vbNewLine
            End If

            MsgBox(mensajeFiltrado, vbOKOnly, "Criterios Filtrado")
        Catch ex As Exception
            MsgBox("Error al intentar filtrar productos", vbOKOnly + vbCritical, "Error de filtrado")
        End Try
    End Sub
    Private Sub rellenarEstado(ByRef estados As ComboBox)
        Try
            estados.Items.Add("-")
            estados.Items.Add("Activo")
            estados.Items.Add("Inactivo")
            estados.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al intentar obtener las opciones de 'Estado' del producto", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub rellenarSubcategoriasPuras(ByRef subcategorias As ComboBox)
        Try
            subcategorias.DataSource = Nothing
            subcategorias.DataSource = BusCategorias.getSubActivas()
            subcategorias.DisplayMember = "nombre"
            subcategorias.ValueMember = "idSubcategoria"
        Catch ex As Exception
            MsgBox("Error al obtener las categorías de un producto.Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub rellenarCategoriasPuras(ByRef categorias As ComboBox)
        Try
            categorias.DataSource = Nothing
            categorias.DataSource = BusCategorias.getCategoriasActivas()
            categorias.DisplayMember = "nombre"
            categorias.ValueMember = "idCategoria"
        Catch ex As Exception
            MsgBox("Error al obtener las categorías de un producto.Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub rellenarCategorias(ByRef categorias As ComboBox)
        Try
            categorias.DataSource = Nothing
            categorias.DataSource = BusCategorias.getCategoriasConSubcategorias()
            categorias.DisplayMember = "nombre"
            categorias.ValueMember = "idCategoria"
        Catch ex As Exception
            MsgBox("Error al obtener las categorías de un producto.Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub rellenarSubcategorias(ByRef subcategorias As ComboBox, ByVal idCat As Integer)
        Try
            Dim subcat As New List(Of Entities.Subcategorias)
            subcat = BusCategorias.getSubcatPorCategorias(idCat)
            If Not IsNothing(subcat) Then
                subcategorias.DataSource = Nothing
                subcategorias.DataSource = BusCategorias.getSubcatPorCategorias(idCat)
                subcategorias.DisplayMember = "nombre"
                subcategorias.ValueMember = "idSubcategoria"
            Else
                subcategorias.DataSource = Nothing
                MsgBox("La categoria seleccionada no tiene aún una subcategoria asignada. Tenga en cuenta que no podrá seleccionar dicha categoría para el nuevo producto. Por favor, asigne una subcategoría para esta categoría, sino seleccione otra.")
            End If
        Catch ex As Exception
            MsgBox("Error al obtener las subcategorías del producto.Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub btnFiltrarModProd_Click(sender As Object, e As EventArgs) Handles btnFiltrarModProd.Click
        Dim filtrarNombre As Boolean
        Dim filtrarPorCat As Boolean
        Dim filtrarPorSub As Boolean


        Dim idCat As Integer
        Dim idSub As Integer
        'Dim estado As Boolean
        idCat = Me.cmbFiltroCatProdAdm.SelectedValue
        idSub = Me.cmbFiltroSubProdAdm.SelectedValue
        filtrarNombre = Me.chkFilPorNombre.Checked
        filtrarPorCat = Me.chkFilPorCat.Checked
        filtrarPorSub = Me.chkFilProdSubCat.Checked

        Me.prepararGrillaProductos(Me.dgvListaProdMod, True, True)
        If filtrarNombre Then
            Me.listarProductosFiltroNombre(Me.dgvListaProdMod, Me.txbNomModProdFil.Text, True, True)
        ElseIf filtrarPorCat Then
            Me.filtrarProdCat(Me.dgvListaProdMod, idCat, True, True)
        ElseIf filtrarPorSub Then
            Me.filtrarProdCatSub(Me.dgvListaProdMod, idCat, idSub, True, True)
        End If
        'Me.filtrar(nombreProd, categoria, subcat, precio, critPre, stock, critStock, stockMin, critMin, garantia, estado)
    End Sub

    Private Sub btnEliProdFil_Click(sender As Object, e As EventArgs)


        'Me.filtrar(nombreProd, categoria, subcat, precio, critPre, stock, critStock, stockMin, critMin, garantia, critGar, estado)
    End Sub


    Private Sub btnProdListFil_Click(sender As Object, e As EventArgs)



        'Me.filtrar(nombreProd, categoria, subcat, precio, critPre, stock, critStock, stockMin, critMin, garantia, critGar, estado)
    End Sub

    Private Function verificarProducto(ByRef nombre As String, ByRef categoria As String, ByRef subcat As String, ByRef precio As String, ByRef stockMin As Integer, ByRef descripcion As String, ByVal foto As Image, ByVal habModCatSub As Boolean) As String
        Dim nombreOk As Boolean
        Dim mensaje As String
        Try


            mensaje = ""
            nombreOk = Not String.IsNullOrWhiteSpace(nombre)

            If IsNothing(foto) Then
                mensaje = mensaje + "Por favor, seleccione una imagen para el producto" + vbNewLine
            End If

            If stockMin <= 0 Then
                mensaje = mensaje + "El stock minimo debe ser al menos de valor 1" + vbNewLine
            End If

            If Not nombreOk Then
                mensaje = mensaje + "Por favor, coloque un nombre al producto" + vbNewLine
            End If

            If String.IsNullOrWhiteSpace(categoria) And habModCatSub Then
                mensaje = mensaje + "Por favor seleccione una categoría" + vbNewLine
            End If


            If String.IsNullOrWhiteSpace(subcat) And habModCatSub Then
                mensaje = mensaje + "Por favor seleccione una subcategoría" + vbNewLine
            End If

            If Not IsNumeric(precio) Then
                mensaje = mensaje + "Por favor coloque un valor numérico al precio" + vbNewLine
            ElseIf CDbl(precio) <= 0 Then
                mensaje = mensaje + "Por favor coloque un precio mayor a cero" + vbNewLine
            End If

            If String.IsNullOrWhiteSpace(descripcion) Then
                mensaje = mensaje + "Por favor coloque una descripción al producto" + vbNewLine
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al verificar los datos del producto", vbOKOnly + vbCritical, "Error de verificación")
            Return ""
        End Try
    End Function

    Private Sub btnIngresarProdNuevo_Click(sender As Object, e As EventArgs) Handles btnIngresarProdNuevo.Click
        Dim mensaje As String
        Dim nombreProducto As String
        Dim idCat As Integer
        Dim idSub As Integer
        Dim precio As String
        Dim stockMin As Integer
        Dim descripcion As String
        Dim garantia As Integer
        Dim nombreFoto As String
        Dim respuesta As Integer


        descripcion = Me.txbDescProdAdmNuevo.Text
        nombreProducto = Me.txbNombreProdAdmNuevo.Text
        precio = Me.txbPrecioProdAdmNuevo.Text
        idCat = Me.cmbCatProdAdmNuevo.SelectedValue
        idSub = Me.cmbSubCatProdAdmNuevo.SelectedValue
        stockMin = Me.nudStockMinNuevoProd.Value
        garantia = Me.nudGarantiaNuevoProd.Value
        nombreFoto = General.obtenerNombreImagen(Me.txbRutaFotoNuevoProd.Text)
        mensaje = Me.verificarProducto(nombreProducto, Me.cmbCatProdAdmNuevo.Text, Me.cmbSubCatProdAdmNuevo.Text, precio, stockMin, descripcion, Me.ptbFotoAltaProd.Image, True)
        If General.controlarImagenRepetida(nombreFoto) Then
            mensaje = mensaje + "Ya existe una imagen con el nombre indicado"
        End If
        If String.IsNullOrWhiteSpace(mensaje) Then
            respuesta = MsgBox("¿Desea ingresar el siguiente producto?" + vbNewLine + "Nombre: " + nombreProducto + vbNewLine + "Categoria: " + vbNewLine + Me.cmbCatProdAdmNuevo.Text + vbNewLine + "Subcategoria: " + Me.cmbSubCatProdAdmNuevo.Text, vbYesNo + vbDefaultButton2, "Consulta De Ingreso")
            If respuesta = vbYes Then
                mensaje = mensaje + BusProductos.insertarProducto(nombreProducto, idCat, idSub, CDbl(precio), stockMin, garantia, nombreFoto, descripcion)
                If String.IsNullOrWhiteSpace(mensaje) Then
                    'General.copiarImagenACarpeta(Me.ofdBuscarFoto, nombreFoto)
                    Me.guardarImagen(Me.ofdBuscarFoto, Me.txbRutaFotoNuevoProd.Text, nombreFoto, Me.ptbFotoAltaProd)
                    'Me.redimensionarImagen(Me.ofdBuscarFoto, Me.txbRutaFotoNuevoProd.Text, nombreFoto, Me.ptbFotoAltaProd)
                    Me.limpiarNuevoProd()
                End If
            End If
        End If

        If Not String.IsNullOrWhiteSpace(mensaje) Then
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
        Else
            Me.limpiarNuevoProd()
        End If
    End Sub



    '-------------------------------------------------------------------------------------------------------------
    'Permite buscar un archivo de imagen que será la imagen ilustrativa del producto
    'Parámetros
    'dialogo-Tipo: FileDialog-Es el objeto que permite se abra la caja de diálogo para elegir la imagen
    'cajaFoto-Tipo: PicturBox-Es el picture box donde irá la foto para que sea observada
    'etiquetaRuta-Tipo: Label-La etiqueta donde se almacenará la ruta del archivo para posteriormente almacenarlo
    '--------------------------------------------------------------------------------------------------------------
    Private Sub buscarFoto(ByRef dialogo As FileDialog, ByRef cajaFoto As PictureBox, ByRef etiquetaRuta As TextBox)

        Try
            dialogo.Filter = "Archivos Imagenes|*.jpg|Archivos Imagenes|*.bmp|Archivos Imagenes|*.png"
            dialogo.InitialDirectory = "C:\Users"
            If dialogo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

                'cajaFoto.Image = Image.FromFile(dialogo.FileName)
                etiquetaRuta.Text = dialogo.FileName
                Me.redimensionarImagen(dialogo, etiquetaRuta.Text, cajaFoto)



            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical + vbOKOnly, "Error Al Cargar La Imagen")
        End Try
    End Sub

    Private Sub btnFotoModProd_Click(sender As Object, e As EventArgs) Handles btnFotoModProd.Click
        Me.buscarFoto(Me.ofdBuscarFoto, Me.ptbFotoModProd, Me.txbRutaFotoModProd)

    End Sub

    Private Sub btnBuscarFotoNuevoProd_Click(sender As Object, e As EventArgs) Handles btnBuscarFotoNuevoProd.Click
        Me.buscarFoto(Me.ofdBuscarFoto, Me.ptbFotoAltaProd, Me.txbRutaFotoNuevoProd)
    End Sub

    Private Sub limpiarNuevoProd()
        Try

            Me.txbNombreProdAdmNuevo.Text = ""
            Me.cmbCatProdAdmNuevo.SelectedIndex = 0
            Me.cmbSubCatProdAdmNuevo.SelectedIndex = 0
            Me.txbPrecioProdAdmNuevo.Text = ""
            Me.nudStockMinNuevoProd.Value = 1
            Me.nudModGarProd.Value = 1
            Me.txbDescProdAdmNuevo.Text = ""
            Me.ptbFotoAltaProd.Image = Nothing
            Me.txbRutaFotoNuevoProd.Text = ""
            Me.txbDescProdAdmNuevo.Enabled = True
        Catch ex As Exception
            MsgBox("Error al limpiar los datos al agregar un nuevo producto", vbOKOnly + vbCritical, "Error de datos")
        End Try
    End Sub

    Private Sub limpiarModProd()
        Try
            Me.txbIdModProd.Text = ""
            Me.txbModProdNombre.Text = ""
            Me.cmbModCat.SelectedIndex = 0
            Me.cmbModSub.SelectedIndex = 0
            Me.nudModStockMinProd.Value = 1
            Me.nudModGarProd.Value = 0
            Me.txbDescModProd.Text = ""
            Me.ptbFotoModProd.Image = Nothing
            Me.txbRutaFotoModProd.Text = ""
        Catch ex As Exception
            MsgBox("Error al limpiar los datos una vez modificado un producto", vbOKOnly + vbCritical, "Error de datos")
        End Try
    End Sub

    Private Function controlarCat(ByVal nombreCat As String, ByVal descripcion As String) As String
        Dim mensaje As String
        Try
            mensaje = ""
            If Not General.controlarLetras(nombreCat) Then
                mensaje = mensaje + "Por favor, coloque un nombre que contenga sólo letras" + vbNewLine
            End If

            If String.IsNullOrWhiteSpace(nombreCat) Then
                mensaje = mensaje + "Por favor, coloque un nombre a la categoria" + vbNewLine
            End If
            If String.IsNullOrWhiteSpace(descripcion) Then
                mensaje = mensaje + "Por favor, coloque un descripcion a la categoria" + vbNewLine
            End If
            Return Trim(mensaje)
        Catch ex As Exception
            MsgBox("Error al controlar la categoría de un producto", vbOKOnly + vbCritical, "Error de control")
            Return ""
        End Try
    End Function

    Private Function controlarSub(ByVal nombreSub As String, ByVal descripcion As String) As String
        Dim mensaje As String
        mensaje = ""
        Try
            If Not General.controlarLetras(nombreSub) Then
                mensaje = mensaje + "Por favor, coloque un nombre que contenga sólo letras" + vbNewLine
            End If

            If String.IsNullOrWhiteSpace(nombreSub) Then
                mensaje = mensaje + "Por favor, coloque un nombre a la subcategoria" + vbNewLine
            End If
            If String.IsNullOrWhiteSpace(descripcion) Then
                mensaje = mensaje + "Por favor, coloque un descripcion a la subcategoria" + vbNewLine
            End If
            Return Trim(mensaje)
        Catch ex As Exception
            MsgBox("Error al controlar las subcategorías", vbOKOnly + vbCritical, "Error de control")
            Return ""
        End Try
    End Function

    Private Function controlarCatSub(ByVal categoria As String, ByVal subcategoria As String, ByVal descripcion As String) As String
        Dim mensaje As String
        mensaje = ""
        Try
            If categoria = "-" Or String.IsNullOrEmpty(categoria) Then
                mensaje = mensaje + "Por favor, elija una categoria" + vbNewLine
            End If

            If subcategoria = "-" Or String.IsNullOrEmpty(subcategoria) Then
                mensaje = mensaje + "Por favor, elija una subcategoria" + vbNewLine
            End If

            If String.IsNullOrWhiteSpace(descripcion) Then
                mensaje = mensaje + "Por favor, establezca una descripción para la relación categoría-subcategoría" + vbNewLine
            End If
            Return Trim(mensaje)
        Catch ex As Exception
            MsgBox("Error al controlar categorías-subcategorías", vbOKOnly + vbCritical, "Error de control")
            Return ""
        End Try
    End Function

    Private Sub btnAgregarNuevaCat_Click(sender As Object, e As EventArgs) Handles btnAgregarNuevaCat.Click
        Dim mensaje As String
        mensaje = Me.controlarCat(Me.txbNuevaCategoria.Text, Me.txbDescripcionNuevaCat.Text)
        If mensaje = "" Then
            mensaje = mensaje + BusCategorias.insertarNuevaCategoria(General.capitalizar(Me.txbNuevaCategoria.Text), Me.txbDescripcionNuevaCat.Text)
        End If

        If mensaje <> "" Then
            MsgBox("Error al ingresar una nueva categoría. Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbCritical, "Error en ingreso de datos")
        Else
            Me.limpiarDatos(Me.txbNuevaCategoria, Me.txbDescripcionNuevaCat, Me.ckbModificarNombreCat, 1)
            Me.listarCategorias(dgvCategoriasAdm)
        End If
    End Sub

    Private Sub btnAgregarNuevaSubCat_Click(sender As Object, e As EventArgs) Handles btnAgregarNuevaSubCat.Click
        Dim mensaje As String
        mensaje = Me.controlarSub(Me.txbNuevaSubcat.Text, Me.txbDescripcionNuevaSubCat.Text)
        If mensaje = "" Then
            mensaje = mensaje + BusCategorias.insertarNuevaSubcategoria(Me.txbNuevaSubcat.Text, Me.txbDescripcionNuevaSubCat.Text)
        End If

        If mensaje = "" Then
            MsgBox("Subcategoría agregada correctamente", vbOKOnly, "Ventana Éxito")
            Me.listarSubcategorias(Me.dgvSubAdm)
            Me.limpiarDatos(Me.txbNuevaSubcat, Me.txbDescripcionNuevaSubCat, Me.ckbModDatosSubcat, 2)
        Else
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub

    Private Sub btnAgregarNuevaCatSub_Click(sender As Object, e As EventArgs) Handles btnAgregarNuevaCatSub.Click
        Dim mensaje As String
        mensaje = BusCategorias.insertarNuevaCatSub(Me.cmbNuevasCatAdm.SelectedValue, Me.cmbNuevasSubCatAdm.SelectedValue, Me.txbDescCatSub.Text)
        If mensaje = "" Then
            MsgBox("Nueva relación agregada exitosamente", vbOKOnly, "Ventana Éxito")
        Else
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub

    Private Function controlarProveedor(ByVal nombre As String, ByVal direccion As String, ByVal tel As String, ByVal codArea As String, ByVal cel As String, ByVal mail As String) As String
        Dim mensaje As String
        Dim errorMail As String
        Dim mensajeCel As String
        Dim mensajeTel As String
        Try
            mensaje = ""
            mensajeCel = Trim(General.controlarCelular(codArea, cel))
            mensajeTel = Trim(General.controlarTelefono(tel))

            If General.controlarCajaEnBlanco(nombre) Then
                mensaje = mensaje + "Por favor, no deje el nombre en blanco" + vbNewLine
            End If

            If Not General.controlarLetras(nombre) Then
                mensaje = mensaje + "Por favor, escriba el nombre del proveedor sólo con letras" + vbNewLine
            End If

            If General.controlarCajaEnBlanco(direccion) Then
                mensaje = mensaje + "Por favor, no deje la dirección en blanco" + vbNewLine
            End If


            If Not General.controlarSinSimbolos(nombre) Then
                mensaje = mensaje + "Por favor, no coloque símbolos especiales al nombre como ser: @, $, etc" + vbNewLine
            End If

            If Not General.controlarSinSimbolos(direccion) Then
                mensaje = mensaje + "Por favor, no coloque símbolos especiales a la dirección como ser: @, $, etc" + vbNewLine
            End If

            errorMail = General.validarEmail(mail)
            If errorMail <> "" Then
                mensaje = mensaje + errorMail + vbNewLine
            End If

            Return Trim(mensaje + vbNewLine + mensajeCel + vbNewLine + mensajeTel)
        Catch ex As Exception
            MsgBox("Error al controlar datos del proveedor", vbOKOnly + vbCritical, "Error de control")
            Return ""
        End Try
    End Function

    Private Sub btnAgregarProv_Click(sender As Object, e As EventArgs) Handles btnAgregarProv.Click
        Dim mensaje As String
        Dim nombreProv As String
        Dim direccion As String
        Dim telefono As String
        Dim codArea As String
        Dim celular As String
        Dim mail As String

        nombreProv = Trim(Me.txbNombreProv.Text)
        direccion = Trim(Me.txbDireccionProv.Text)
        telefono = Trim(Me.txbTelProv.Text)
        codArea = Trim(Me.txbCodAreaCel.Text)
        celular = Trim(Me.txbCelProv.Text)
        mail = Trim(Me.txbMailProv.Text)

        mensaje = Me.controlarProveedor(nombreProv, direccion, telefono, codArea, celular, mail)
        If String.IsNullOrWhiteSpace(mensaje) Then
            mensaje = mensaje + BusProveedores.insertarProveedor(nombreProv, direccion, telefono, codArea, celular, mail)
            If String.IsNullOrWhiteSpace(mensaje) Then
                MsgBox("Datos de proveedores correcto", vbOKOnly, "Ventana Éxito")
                Me.limpiarProveedores(Me.txbNombreProv, Me.txbDireccionProv, Me.txbTelProv, Me.txbCodAreaCel, Me.txbCelProv, Me.txbMailProv, False)
                Me.prepararGrillaProveedor(Me.dgvProveedores, True)
                Me.listarProveedores(Me.dgvProveedores, True)
            End If
        End If
        If Not String.IsNullOrWhiteSpace(mensaje) Then
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub



    Private Sub btnModProv_Click(sender As Object, e As EventArgs) Handles btnModProv.Click
        Dim mensaje As String
        Dim nombreProv As String
        Dim direccion As String
        Dim telefono As String
        Dim codArea As String
        Dim celular As String
        Dim mail As String
        Dim modNombre As Boolean
        Dim idProv As Integer

        idProv = CInt(Me.txbIdModProv.Text)
        nombreProv = Trim(Me.txbNombreModProv.Text)
        direccion = Trim(Me.txbDirModProv.Text)
        telefono = Trim(Me.txbTelModProv.Text)
        codArea = Trim(Me.txbCodAreaModProv.Text)
        celular = Trim(Me.txbCelModProv.Text)
        mail = Trim(Me.txbMailModProv.Text)
        modNombre = Me.chkModNombreProve.Checked
        mensaje = Me.controlarProveedor(nombreProv, direccion, telefono, codArea, celular, mail)
        If String.IsNullOrWhiteSpace(mensaje) Then
            mensaje = mensaje + BusProveedores.modificarProveedor(idProv, nombreProv, direccion, telefono, codArea, celular, mail, modNombre)
            If String.IsNullOrWhiteSpace(mensaje) Then
                MsgBox("Datos de proveedores correcto", vbOKOnly, "Ventana Éxito")
                Me.limpiarProveedores(Me.txbNombreModProv, Me.txbDirModProv, Me.txbTelModProv, Me.txbCodAreaModProv, Me.txbCelModProv, txbMailModProv, True)
                Me.listarProveedores(Me.dgvProveedores, True)
            End If
        End If

        If String.IsNullOrWhiteSpace(mensaje) Then
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub

    Private Sub grbProveedores_Enter(sender As Object, e As EventArgs) Handles grbProveedores.Enter
        Try
            Me.grbModDatosProv.Enabled = True
        Catch ex As Exception
            MsgBox("Error al habilitar los datos del proveedor", vbOKOnly + vbCritical, "Error de habilitación")
        End Try
    End Sub

    Private Sub limpiarProveedores(ByRef nombre As TextBox, ByRef direccion As TextBox, ByRef tel As TextBox, ByRef codArea As TextBox, ByRef cel As TextBox, ByRef mail As TextBox, ByVal modificacion As Boolean)
        Try
            nombre.Text = ""
            tel.Text = ""
            direccion.Text = ""
            codArea.Text = ""
            cel.Text = ""
            mail.Text = ""
            If modificacion Then
                Me.grbModDatosProv.Enabled = False
            End If
        Catch ex As Exception
            MsgBox("Error al limpiar datos del proveedor", vbOKOnly + vbCritical, "Error de refresco")
        End Try
    End Sub

    Private Sub listarProveedoresFiltradoNombre(ByRef grilla As DataGridView, ByVal nombre As String)

        'grilla.ColumnCount = 8
        Try

            Dim estado As String


            Dim proveedores = BusProveedores.getProveedorPorNombre(nombre)
            If Not IsNothing(proveedores) Then
                For Each provedor In proveedores

                    If provedor.estado Then
                        estado = "Activo"

                    Else
                        estado = "Inactivo"
                    End If
                    grilla.Rows.Add(provedor.idProveedor, provedor.nombreComercial, provedor.direccionProveedor, provedor.telefonoProv, provedor.celularProv, provedor.mailProv, estado)
                Next
            Else
                MsgBox("No hay proveedores que cumplan los criterios de búsqueda", vbOKOnly + vbExclamation, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error al listar las subcategorias.Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly, "Error")

        End Try
    End Sub


    Private Sub tbcGestionProdAdm_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tbcGestionProdAdm.SelectedIndexChanged
        Dim indiceElegido As Byte

        Try
            indiceElegido = tbcGestionProdAdm.SelectedIndex

            Select Case indiceElegido
                Case opcionesProductos.AgregarProveedor
                    Me.prepararGrillaProveedor(Me.dgvProveedores, True)
                    Me.listarProveedores(Me.dgvProveedores, True)
                Case opcionesProductos.NuevoProducto
                    Me.rellenarCategorias(Me.cmbCatProdAdmNuevo)
                    Me.limpiarNuevoProd()
                Case opcionesProductos.ModDatosProducto
                    Me.habilitarModProd(False)
                    Me.prepararGrillaProductos(Me.dgvListaProdMod, True, True)
                    Me.listarProductos(Me.dgvListaProdMod, True, True)
                    Me.rellenarCategoriasPuras(Me.cmbFiltroCatProdAdm)
                    Me.rellenarSubcategoriasPuras(Me.cmbFiltroCatProdAdm)
                Case opcionesProductos.AgregarCat
                    Me.listarCategorias(Me.dgvCategoriasAdm)
                    Me.listarSubcategorias(Me.dgvSubAdm)
                    Me.listarCatSub(Me.dgvCatSubAdm)
                    'Case opcionesProductos.EliminarProducto
                    'Me.listarProductos(Me.dgvEliminarProductos, False, True)
                Case opcionesProductos.ProveedorProducto
                    Me.dgvProveedores.Columns.Clear()
                    Me.prepararGrillaProductos(Me.dgvProvProd, False, False)
                    Me.listarProductos(Me.dgvProvProd, False, False)
                    Me.prepararGrillaProveedor(Me.dgvProv_ProvProd, False)
                    Me.listarProveedores(Me.dgvProv_ProvProd, False)
                Case opcionesProductos.ModificarStockPrecio
                    Me.prepararGrillaProductos(Me.dgvProdModstockPrecio, False, False)
                    Me.listarProductos(Me.dgvProdModstockPrecio, False, False)
                    Me.rellenarMotivosCambioPrecio(Me.cmbMotivoPrecioModProd)
                    Me.rellenarMotivosCambioStock(Me.cmbMotivoModStock)
                    Me.activarDesactivarModPrecioStock(False)
                    Me.rellenarCategoriasPuras(Me.cmbCatModStockPrecio)
                    Me.rellenarSubcategoriasPuras(Me.cmbFiltroSubcat)
                Case opcionesProductos.AgregarProveedor
                    Me.grbModDatosProv.Enabled = False
                Case opcionesProductos.Compras
                    Me.grbModDatosProv.Enabled = False
                    Me.prepararCarritoCompra(Me.dgvCarritoCompras)
                    Me.listarProductosProveedores(Me.dgvProvProdCompras)
                    Me.rellenarMotivosModCompra(Me.cmbMotivoModCompra)
                    Me.rellenarCategorias(Me.cmbCatModStockPrecio)
                    Me.rellenarSubCatPuras(Me.cmbFiltroSubcat)
            End Select

        Catch ex As Exception
            MsgBox("Error al seleccionar una pestaña válida para el usuario. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de acceso")
        End Try
    End Sub


    Private Sub grbDatosProveedor_Enter(sender As Object, e As EventArgs) Handles grbDatosProveedor.Enter
        Me.grbModDatosProv.Enabled = False
    End Sub


    Private Sub btnPrecioCompraProd_Click(sender As Object, e As EventArgs) Handles btnPrecioCompraProd.Click
        Dim mensaje As String
        Dim respuesta As Integer

        respuesta = MsgBox("¿Desea agregar la relación establecida?", vbOKOnly + vbYesNo, "Ventana Consulta")
        If respuesta = vbYes Then
            mensaje = Me.controlarProdProv()
            If String.IsNullOrWhiteSpace(mensaje) And Not IsNothing(mensaje) Then
                mensaje = mensaje + BusProveedoresProductos.insertarProductoProveedor(CInt(Me.lblIdProducto.Text), CInt(Me.lblIdProveedor.Text), CDbl(Me.txbPrecioCompraProd.Text))
            End If
            If Not String.IsNullOrWhiteSpace(mensaje) Then
                MsgBox("Error al ingresar una nueva relación producto/proveedor. Los errores producidos fueron: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Error de alta")
            Else
                MsgBox("Relación correctamente establecida", vbOKOnly, "Ventana Éxito")
                Me.limpiarDatosProdProv()
            End If
        End If
    End Sub



    Private Sub btnAsignarCarritoCompra_Click(sender As Object, e As EventArgs) Handles btnAsignarCarritoCompra.Click
        Dim mensaje As String
        Try
            mensaje = ""

            'controla que haya sido seleccionado un producto
            If Not General.controlarReales(Me.txbPrecioCompra.Text) Then
                mensaje = mensaje + "Por favor, coloque una cantidad numérica al precio de compra" + vbNewLine
            ElseIf CDbl(Me.txbPrecioCompra.Text) <= 0 Then

                mensaje = mensaje + "Por favor, coloque una modificación de precio mayor a cero" + vbNewLine
            End If

            'controlo si es que hay modificación de producto y, si las hay, verifico que el precio y la
            'descripción del motivo de la modificación sean colocadas
            If ckbConfModPrecio.Checked Then
                If Not General.controlarReales(Me.txbModiPrecioCompra.Text) Then
                    mensaje = mensaje + "Por favor, coloque una cantidad numérica al precio de compra" + vbNewLine
                ElseIf (CDbl(Me.txbModiPrecioCompra.Text) <= 0) Then
                    mensaje = mensaje + "Por favor, coloque una cantidad numérica al precio de compra mayor a cero" + vbNewLine
                End If
                If General.controlarCajaEnBlanco(Me.txbDescModCompra.Text) Then
                    mensaje = mensaje + "Por favor, si realizará aumentos o descuentos en el precio final, coloque la descripción del motivo." + vbNewLine
                End If
            End If

            'si el mensaje llegó aquí en blanco, entonces ingreso los datos al carrito
            'caso contrario, muesto el error.
            If String.IsNullOrWhiteSpace(mensaje) Then
                Me.cargarDatoCarritoCompra(Me.dgvCarritoCompras)
                MsgBox("Datos cargados al carrito", vbOKOnly, "Ventana Éxito")
                Me.limpiarCompra()
            Else
                MsgBox("Se han producido los siguientes erroes: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error al asignar un producto al carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga producto")
        End Try
    End Sub
    Private Sub rellenarMotivosModCompra(ByVal motModCompras As ComboBox)

        Try
            motModCompras.DataSource = Nothing
            motModCompras.DataSource = BusCompras.getMotivosModCompras()
            motModCompras.ValueMember = "idMotivoModCompras"
            motModCompras.DisplayMember = "nombreMotModCom"
        Catch ex As Exception
            MsgBox("Error al cargar los motivos de modificación de compras. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperción de datos")
        End Try
    End Sub
    Private Sub rellenarMotivosDisPrecio(ByVal motivosDisminucion As ComboBox)
        Try
            motivosDisminucion.Items.Clear()
            motivosDisminucion.Items.Add("-")
            motivosDisminucion.Items.Add("Descuento Por Promoción")
            motivosDisminucion.Items.Add("Descuento Por Fallos")
            motivosDisminucion.Items.Add("Descuento Por Estación")
            motivosDisminucion.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al cargar los motivos de descuento", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub rellenarMotivosCambioPrecio(ByVal motivosVarPrecio As ComboBox)
        Try
            motivosVarPrecio.DataSource = BusProductos.getMotivosModPrecio()
            motivosVarPrecio.ValueMember = "idMotivoModProd"
            motivosVarPrecio.DisplayMember = "nombreMotivo"
        Catch ex As Exception
            MsgBox("Error al cargar motivos de cambio de precio", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub rellenarMotivosCambioStock(ByVal motivosVarStock As ComboBox)
        Try
            motivosVarStock.DataSource = BusProductos.getMotivosModStock()
            motivosVarStock.ValueMember = "idMotivoModStock"
            motivosVarStock.DisplayMember = "nombreMotivo"
        Catch ex As Exception
            MsgBox("Error al cargar motivos cambio de stock", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub modificarElementoCarrito(ByRef carrito As DataGridView)
        Try
            Dim modValor As Boolean
            Dim precio As Double
            Dim cantidad As Integer
            Dim aumento As Boolean
            Dim precioAumento As Double
            Dim mensaje As String

            mensaje = ""

            modValor = Me.ckbConfModPrecio.Checked
            precio = CDbl(Me.txbPrecioCompra.Text)
            cantidad = CInt(Me.nudCantidadCompra.Value)
            aumento = Me.chkAuPrecioCompra.Checked
            If modValor Then
                If Not General.controlarCajaEnBlanco(Me.txbModiPrecioCompra.Text) Or General.controlarReales(Me.txbModiPrecioCompra.Text) Then
                    mensaje = mensaje + "Por favor, coloque un valor numérico a la modificación de precio." + vbNewLine
                ElseIf CDbl(Me.txbModiPrecioCompra.Text) <= 0 Then
                    mensaje = mensaje + "Por favor, coloque un valor numérico mayor a cero." + vbNewLine
                Else
                    precioAumento = CDbl(Me.txbModiPrecioCompra.Text)
                End If

                If Not General.controlarCajaEnBlanco(Me.txbDescModCompra.Text) Then
                    mensaje = mensaje + "Por favor, si desea realizar una modificación al precio final, coloque una descripción del por qué se realiza." + vbNewLine
                End If
            Else
                carrito.CurrentRow.Cells("cantidad").Value = cantidad
                carrito.CurrentRow.Cells("precio").Value = cantidad * precio
            End If



        Catch ex As Exception

        End Try
    End Sub
    Private Sub limpiarDatosProdProv()
        Try
            Me.lblIdProveedor.Text = "0"
            Me.txbProv_ProdProv.Text = ""
            Me.lblIdProducto.Text = "0"
            Me.txbProd_ProvProd.Text = ""
            Me.txbPrecioVenta_ProvProd.Text = ""
            Me.txbPrecioCompraProd.Text = ""
        Catch ex As Exception
            MsgBox("Error al limpiar los datos una vez establecida la relación. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbExclamation, "Ventana Error")
        End Try
    End Sub
    Private Sub limpiarCarrito(ByRef carrito As DataGridView)
        Try
            carrito.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al intentar vaciar el carrito. Los errores producidos fueron: " + vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub limpiarCompra()
        Try
            Me.txbNroProdCompra.Text = ""
            Me.txbNombreProdCompra.Text = ""
            Me.txbNroProvCompra.Text = ""
            Me.txbProvCompra.Text = ""
            Me.txbPrecioCompra.Text = 0
            Me.nudCantidadCompra.Value = 1
            Me.ckbConfModPrecio.Checked = False
            Me.grbCarritoCompra.Enabled = True
            Me.txbModiPrecioCompra.Text = ""
            Me.cmbMotivoModCompra.SelectedValue = 0
            Me.txbDescModCompra.Text = ""
            Me.btnAsignarCarritoCompra.Enabled = True
            Me.btnModCarrito.Enabled = False
            Me.btnCancelarModCarrito.Enabled = False
        Catch ex As Exception
            MsgBox("Error al intentar limpiar los datos del producto comprado o modificado", vbOKOnly + vbCritical, "Error de refresco")
        End Try
    End Sub
    Private Sub cmbMotivoModCompra_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If cmbMotivoModCompra.SelectedItem = "-" Then
                Me.txbModiPrecioCompra.Enabled = False
                Me.txbModiPrecioCompra.Text = ""
            Else
                Me.txbModiPrecioCompra.Enabled = True
            End If
        Catch ex As Exception
            MsgBox("Error al controlar los motivos de compra", vbOKOnly + vbCritical, "Error de control")
        End Try
    End Sub

    Private Function controlarModProdStock(ByVal stock As Integer, ByVal motivo As String, ByVal detalleMotivo As String) As String
        Dim mensaje As String
        Try
            mensaje = ""

            If motivo = "-" Then
                mensaje = mensaje + "Por favor, seleccione un motivo" + vbNewLine
            End If

            If General.controlarCajaEnBlanco(detalleMotivo) Then
                mensaje = mensaje + "Por favor, no deje el detalle del cambio en blanco" + vbNewLine
            End If

            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar la modificación de stock", vbOKOnly + vbCritical, "Error de control")
            Return ""
        End Try
    End Function

    Private Function controlarModProdPre(ByVal precio As String, ByVal motivo As String, ByVal detalleMotivo As String) As String
        Dim mensaje As String
        Try
            mensaje = ""
            If Not General.controlarReales(precio) Then
                mensaje = mensaje + "Por favor, coloque un valor numérico para el precio" + vbNewLine
            ElseIf CDbl(precio) <= 0 Then
                mensaje = mensaje + "Por favor, coloque un precio mayor a cero" + vbNewLine
            End If

            If motivo = "-" Then
                mensaje = mensaje + "Por favor, seleccione un motivo" + vbNewLine
            End If

            If General.controlarCajaEnBlanco(detalleMotivo) Then
                mensaje = mensaje + "Por favor, no deje el detalle del cambio en blanco" + vbNewLine
            End If

            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar cambio de precio producto", vbOKOnly + vbCritical, "Error de control")
            Return ""
        End Try
    End Function

    Private Sub btnModStockProd_Click(sender As Object, e As EventArgs) Handles btnModStockProd.Click
        Dim mensaje As String
        Dim idProd As Integer
        Dim idMotivo As Integer
        Dim descripcion As String
        Dim dniEmpleado As String
        Dim nombreEmp As String
        Dim stockNuevo As Integer

        idProd = CInt(Me.txbNroProdModStock.Text)
        stockNuevo = Me.nudModificarStock.Value
        idMotivo = Me.cmbMotivoModStock.SelectedValue
        descripcion = Me.txbDetalleModStock.Text
        dniEmpleado = Sesion.getDniUsuario()
        nombreEmp = Sesion.getNombreUsu() + Sesion.getApellidoUsu()
        mensaje = Me.controlarModProdStock(Me.nudModificarStock.Value, Me.cmbMotivoModStock.Text, Me.txbDetalleModStock.Text)
        If String.IsNullOrWhiteSpace(mensaje) Then
            BusProductos.modificarStock(idProd, stockNuevo, idMotivo, descripcion, dniEmpleado, nombreEmp)
            MsgBox("Datos de la modificación de stock correctos", vbOKOnly, "Ventana Éxito")
            Me.limpiarDatosModStockPrecio()
            Me.prepararGrillaProductos(Me.dgvProdModstockPrecio, False, False)
            Me.listarProductos(Me.dgvProdModstockPrecio, False, False)
        Else
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Éxito")
        End If
    End Sub

    Private Sub btnModPrecioAdm_Click(sender As Object, e As EventArgs) Handles btnModPrecioAdm.Click
        Dim mensaje As String
        Dim idProd As Integer
        Dim idMotivo As Integer
        Dim descripcion As String
        Dim dniEmpleado As String
        Dim nombreEmp As String
        Dim precio As Double

        idProd = CInt(Me.txbNroProdModPrecio.Text)
        idMotivo = Me.cmbMotivoPrecioModProd.SelectedValue
        descripcion = Me.txbDetalleModPrecio.Text
        dniEmpleado = Sesion.getDniUsuario()
        nombreEmp = Sesion.getNombreUsu() + Sesion.getApellidoUsu()
        mensaje = Me.controlarModProdPre(Me.txbPrecioModProd.Text, Me.cmbMotivoPrecioModProd.Text, Me.txbDetalleModPrecio.Text)
        If String.IsNullOrWhiteSpace(mensaje) Then
            precio = CDbl(Me.txbPrecioModProd.Text)
            mensaje = mensaje + BusProductos.modificarPrecio(idProd, precio, idMotivo, descripcion, dniEmpleado, nombreEmp) + vbNewLine
        End If

        If String.IsNullOrWhiteSpace(mensaje) Then
            MsgBox("Se han producido los siguientes errores: " + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
            Me.prepararGrillaProductos(Me.dgvProdModstockPrecio, False, False)
            Me.listarProductos(Me.dgvProdModstockPrecio, False, False)
            Me.limpiarDatosModStockPrecio()

        Else
            MsgBox("Modificación de Precio Correcto", vbOKOnly, "Ventana Error")
        End If
    End Sub

    Private Sub dgvCategoriasAdm_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCategoriasAdm.CellClick
        If Not dgvCategoriasAdm.CurrentCell.GetType.ToString Like "*Button*" Then
            Me.habilitarODeshabilitarModCatSub(1, True)
            Me.habilitarODeshabilitarModCatSub(2, False)
            Me.habilitarODeshabilitarModCatSub(3, False)
            Me.limpiarDatos(Me.txbNuevaSubcat, Me.txbDescripcionNuevaSubCat, Me.ckbModDatosSubcat, 2)
            Me.moverDatosParaModificar(Me.txbNuevaCategoria, Me.txbDescripcionNuevaCat, Me.ckbModificarNombreCat, Me.dgvCategoriasAdm)
        Else
            Me.modificarEstadoCategoria(Me.dgvCategoriasAdm)
        End If
    End Sub
    '----------------------------------------------------------------------------------------------------------
    'Mueve los datos de la categoría desde la tabla a las cajas para su modificación
    '----------------------------------------------------------------------------------------------------------
    Private Sub moverDatosCategoria()
        Me.txbNuevaCategoria.Text = Me.dgvCategoriasAdm.CurrentRow.Cells(1).Value
        Me.txbDescripcionNuevaCat.Text = Me.dgvCategoriasAdm.CurrentRow.Cells(3).Value
        Me.ckbModificarNombreCat.Enabled = True
        Me.ckbModificarNombreCat.Checked = True
        Me.ckbModificarNombreCat.Checked = False
    End Sub
    Private Sub activarDesactivarModCatSu()

    End Sub
    Private Sub moverDatosCatSub(ByRef cat As ComboBox, ByRef subcat As ComboBox, ByRef grilla As DataGridView, ByRef cajaDescripcion As TextBox)
        cat.SelectedValue = CInt(grilla.CurrentRow.Cells("codCat").Value)
        subcat.SelectedValue = CInt(grilla.CurrentRow.Cells("codSub").Value)
        cajaDescripcion.Text = grilla.CurrentRow.Cells("descripcion").Value
    End Sub
    Private Sub moverDatosParaModificar(ByRef cajaNombre As TextBox, ByRef cajaDescripcion As TextBox, ByRef modCheckbox As CheckBox, ByRef grillaDatos As DataGridView)
        cajaNombre.Text = grillaDatos.CurrentRow.Cells(1).Value
        cajaDescripcion.Text = grillaDatos.CurrentRow.Cells(3).Value
        modCheckbox.Enabled = True
        modCheckbox.Checked = True
        modCheckbox.Checked = False

    End Sub
    '----------------------------------------------------------------------------------------------------------
    'Mueve los datos de las subcategorias a las cajas para la modificación de los mismos
    '---------------------------------------------------------------------------------------------------------
    Private Sub moverDatosSubcategoria()
        Me.txbNuevaSubcat.Text = Me.dgvSubAdm.CurrentRow.Cells(1).Value
        Me.txbDescripcionNuevaSubCat.Text = Me.dgvSubAdm.CurrentRow.Cells(3).Value
        Me.ckbModDatosSubcat.Enabled = True
        Me.ckbModDatosSubcat.Checked = True
        Me.ckbModDatosSubcat.Checked = False
    End Sub
    Private Sub dgvCategoriasAdm_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCategoriasAdm.CellContentClick

    End Sub

    Private Sub cargarEstadosProductos(ByRef estados As ComboBox)
        Try
            estados.Items.Clear()
            estados.Items.Add("-")
            estados.Items.Add("Activo")
            estados.Items.Add("Inactivo")
            estados.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al cargar los estados de los productos: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub limpiarDatos(ByRef cajaNombre As TextBox, ByRef cajaDescripcion As TextBox, ByRef modCheckbox As CheckBox, ByVal opcionALimpiar As Byte)
        Try
            Me.habilitarODeshabilitarModCatSub(opcionALimpiar, False)
            cajaNombre.Text = ""
            cajaNombre.Enabled = True
            cajaDescripcion.Enabled = True
            cajaDescripcion.Text = ""
            modCheckbox.Enabled = False
        Catch ex As Exception
            MsgBox("Error al deshabilitar la categoría.Los errores presentes son: " + ex.Message, vbOKOnly + vbExclamation, "Error al agregar datos")
        End Try
    End Sub
    Private Sub limpiarCategorias()
        Try
            Me.habilitarODeshabilitarModCatSub(1, False)
            Me.txbNuevaCategoria.Text = ""
            Me.txbNuevaCategoria.Enabled = True
            Me.txbDescripcionNuevaCat.Enabled = True
            Me.txbDescripcionNuevaCat.Text = ""
            Me.ckbModificarNombreCat.Enabled = False
        Catch ex As Exception
            MsgBox("Error al deshabilitar la categoría.Los errores presentes son: " + ex.Message, vbOKOnly + vbExclamation, "Error al agregar datos")
        End Try
    End Sub
    Private Sub btnCancelarModCat_Click(sender As Object, e As EventArgs) Handles btnCancelarModCat.Click
        Try
            Me.limpiarCategorias()
        Catch ex As Exception
            MsgBox("Error al querer cancelar la modificación de una categoría: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de modificación")
        End Try
    End Sub

    Private Sub btnCancelarModSubcat_Click(sender As Object, e As EventArgs) Handles btnCancelarModSubcat.Click
        Try
            Me.habilitarODeshabilitarModCatSub(2, False)
            Me.ckbModDatosSubcat.Enabled = False
            Me.txbNuevaSubcat.Text = ""
            Me.txbDescripcionNuevaSubCat.Text = ""
        Catch ex As Exception
            MsgBox("Error al querer cancelar la modificación de una subcategoría: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de modificación")
        End Try
    End Sub

    Private Sub btnCancelarModCatSub_Click(sender As Object, e As EventArgs) Handles btnCancelarModCatSub.Click
        Try
            Me.habilitarODeshabilitarModCatSub(0, False)
            Me.txbDescCatSub.Text = ""
        Catch ex As Exception
            MsgBox("Error al querer cancelar la modificación de una relación categoría-subcategoría: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de modificación")
        End Try
    End Sub

    Private Sub btnModificarCat_Click(sender As Object, e As EventArgs) Handles btnModificarCat.Click
        Try
            Dim mensaje As String
            mensaje = ""
            If Not General.controlarLetras(Me.txbNuevaCategoria.Text) Or General.controlarCajaEnBlanco(Me.txbNuevaCategoria.Text) Then
                mensaje = mensaje + "Por favor, que el nombre de la categoría sólo contenga letras y que no esté en blanco"
            End If

            If General.controlarCajaEnBlanco(Me.txbDescripcionNuevaCat.Text) Then
                mensaje = mensaje + "Por favor, no deje la descripción de la categoría en blanco"
            End If

            If mensaje = "" Then
                mensaje = BusCategorias.modificarDatosCategoria(CInt(Me.dgvCategoriasAdm.CurrentRow.Cells(0).Value), Me.txbNuevaCategoria.Text, Me.txbDescripcionNuevaCat.Text, Not Me.ckbModificarNombreCat.Checked)
            End If

            If mensaje = "" Then
                MsgBox("Categoria modificada correctamente", vbOKOnly, "Ventana Éxito")
                Me.listarCategorias(dgvCategoriasAdm)
                Me.limpiarDatos(Me.txbNuevaCategoria, Me.txbDescripcionNuevaCat, Me.ckbModificarNombreCat, 1)
            Else
                MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Error al modificar la categoría")
            End If
        Catch ex As Exception
            MsgBox("Error al crear la nueva categoría: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Éxito")
        End Try
    End Sub
    Private Sub listarSubcategorias(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codSub As New DataGridViewTextBoxColumn()
        Dim nombre As New DataGridViewTextBoxColumn()
        Dim descripcion As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim estado As String

        'grilla.ColumnCount = 8
        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codSub)
            grilla.Columns.Add(nombre)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(descripcion)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            colEstado.Name = "cambioEstado"
            codSub.Name = "codSub"
            nombre.Name = "nombre"
            descripcion.Name = "descripcion"
            codSub.HeaderText = "Número Subcategoria"
            nombre.HeaderText = "Nombre"
            descripcion.HeaderText = "Descripción"
            colEstado.HeaderText = "Estado"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True

            For Each subcategorias In BusCategorias.getSubcategorias()

                If subcategorias.estado Then
                    estado = "Activo"

                Else
                    estado = "Inactivo"
                End If
                grilla.Rows.Add(subcategorias.idSubcategoria, subcategorias.nombre, estado, subcategorias.descripcionSub)
            Next
        Catch ex As Exception
            MsgBox("Error al listar las subcategorias.Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly, "Error")

        End Try
    End Sub

    Private Sub listarCategorias(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codCat As New DataGridViewTextBoxColumn()
        Dim nombre As New DataGridViewTextBoxColumn()
        Dim descripcion As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim estado As String

        'grilla.ColumnCount = 8
        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codCat)
            grilla.Columns.Add(nombre)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(descripcion)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            colEstado.Name = "cambioEstado"
            codCat.Name = "codCat"
            nombre.Name = "nombre"
            descripcion.Name = "descripcion"
            codCat.HeaderText = "Número Categoría"
            nombre.HeaderText = "Nombre"
            descripcion.HeaderText = "Descripción"
            colEstado.HeaderText = "Estado"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True

            For Each categorias In BusCategorias.getCategorias()

                If categorias.estado Then
                    estado = "Activo"

                Else

                    estado = "Inactivo"
                End If
                grilla.Rows.Add(categorias.idCategoria, categorias.nombre, estado, categorias.descripcionCat)
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub

    Private Sub listarCatSub(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codCat As New DataGridViewTextBoxColumn()
        Dim codSub As New DataGridViewTextBoxColumn()
        Dim nombreCat As New DataGridViewTextBoxColumn()
        Dim nombreSub As New DataGridViewTextBoxColumn()
        Dim descripcion As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim estado As String
        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codCat)
            grilla.Columns.Add(codSub)
            grilla.Columns.Add(nombreCat)
            grilla.Columns.Add(nombreSub)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(descripcion)
            grilla.Columns.Add(boton)


            boton.HeaderText = "Alta/Baja"
            colEstado.Name = "cambioEstado"
            codCat.Name = "codCat"
            codSub.Name = "codSub"
            nombreCat.Name = "nombreCat"
            nombreSub.Name = "nombreSub"
            descripcion.Name = "descripcion"
            codCat.HeaderText = "Número Categoría"
            codSub.HeaderText = "Número Subcategoría"
            nombreCat.HeaderText = "Categoría"
            nombreSub.HeaderText = "Subcategoría"
            descripcion.HeaderText = "Descripción"
            colEstado.HeaderText = "Estado"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True

            For Each catsub In BusCategorias.getCategoriasSubcategorias()

                If catsub.estado Then
                    estado = "Activo"

                Else

                    estado = "Inactivo"
                End If
                grilla.Rows.Add(catsub.Categorias.idCategoria, catsub.Subcategorias.idSubcategoria, catsub.Categorias.nombre, catsub.Subcategorias.nombre, estado, catsub.descripcionCatSub)
            Next
        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub


    '------------------------------------------------------------------------------------------------------------
    'Controla que, si no se elige la opción de modificar el nombre de la categoría, se vuelva al valor que tenía
    'anteriormente.
    '------------------------------------------------------------------------------------------------------------
    Private Sub ckbModificarNombreCat_CheckedChanged(sender As Object, e As EventArgs) Handles ckbModificarNombreCat.CheckedChanged
        ' If Not ckbModificarNombreCat.Checked Then
        'Me.txbNuevaCategoria.Enabled = False
        'If Not IsNothing(Me.dgvCategoriasAdm.CurrentRow.Cells(1).Value) Then
        'Me.txbNuevaCategoria.Text = Me.dgvCategoriasAdm.CurrentRow.Cells(1).Value
        'Else
        'Me.txbNuevaCategoria.Text = ""
        'End If

        'Else
        'Me.txbNuevaCategoria.Enabled = True
        'End If
        Me.limpiarOpcionesMod(Me.ckbModificarNombreCat, Me.txbNuevaCategoria, Me.dgvCategoriasAdm)
    End Sub

    Private Sub limpiarOpcionesMod(ByRef opcionCheckbox As CheckBox, ByRef cajaALimpiar As TextBox, ByRef grillaDatos As DataGridView)
        If Not opcionCheckbox.Checked Then
            cajaALimpiar.Enabled = False
            If Not IsNothing(grillaDatos.CurrentRow.Cells(0).Value) Then
                cajaALimpiar.Text = grillaDatos.CurrentRow.Cells(1).Value
            Else
                cajaALimpiar.Text = ""
            End If

        Else
            cajaALimpiar.Enabled = True
        End If
    End Sub

    Private Sub dgvCatSubAdm_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCatSubAdm.CellClick
        If dgvCatSubAdm.CurrentCell.GetType.ToString Like "*Button*" Then
            Me.modificarEstadoCatSub(Me.dgvCatSubAdm)
        Else
            Me.habilitarODeshabilitarModCatSub(3, True)
            Me.limpiarDatos(Me.txbNuevaCategoria, Me.txbDescripcionNuevaCat, Me.ckbModificarNombreCat, 1)
            Me.limpiarDatos(Me.txbNuevaSubcat, Me.txbDescripcionNuevaSubCat, Me.ckbModDatosSubcat, 2)
            Me.moverDatosCatSub(Me.cmbNuevasCatAdm, Me.cmbNuevasSubCatAdm, Me.dgvCatSubAdm, Me.txbDescCatSub)
        End If
    End Sub
    Private Sub modificarEstadoCatSub(ByRef grilla As DataGridView)
        Dim valorPrueba As String
        Dim idCategoria As Integer
        Dim idSubcategoria As Integer
        Dim nombreCat As String
        Dim nombreSub As String
        Dim estado As Boolean
        Dim nombreEstado As String
        Dim respuesta As Integer

        Try
            valorPrueba = grilla.CurrentRow.Cells(0).Value
            If Not IsNothing(valorPrueba) And grilla.CurrentCell.GetType.ToString Like "*Button*" Then
                idCategoria = CInt(grilla.CurrentRow.Cells("codCat").Value)
                idSubcategoria = CInt(grilla.CurrentRow.Cells("codSub").Value)
                nombreCat = grilla.CurrentRow.Cells("nombreCat").Value
                nombreSub = grilla.CurrentRow.Cells("nombreSub").Value
                nombreEstado = grilla.CurrentRow.Cells("cambioEstado").Value
                If nombreEstado = "Activo" Then
                    estado = False
                Else
                    estado = True
                End If
                respuesta = MsgBox("¿Desea modificar el estado de la relación siguiente?" + vbNewLine + "Categoria: " + nombreCat + vbNewLine + "Subcategoria: " + nombreSub + vbNewLine + "Estado Actual: " + nombreEstado, vbYesNo + vbDefaultButton2, "Consulta De Modificación")
                If respuesta = vbYes Then
                    BusCategorias.modificarEstadoCatSub(idCategoria, idSubcategoria, estado)
                    Me.listarCatSub(grilla)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Sub dgvSubAdm_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSubAdm.CellClick
        If Not dgvSubAdm.CurrentCell.GetType.ToString Like "*Button*" Then
            Me.habilitarODeshabilitarModCatSub(1, False)
            Me.habilitarODeshabilitarModCatSub(2, True)
            Me.habilitarODeshabilitarModCatSub(3, False)
            Me.limpiarCategorias()
            Me.moverDatosParaModificar(Me.txbNuevaSubcat, Me.txbDescripcionNuevaSubCat, Me.ckbModDatosSubcat, Me.dgvSubAdm)
        Else
            Me.modificarEstadoSubcategoria(Me.dgvSubAdm)
        End If
    End Sub

    Private Sub dgvSubAdm_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSubAdm.CellContentClick

    End Sub

    Private Sub ckbModDatosSubcat_CheckedChanged(sender As Object, e As EventArgs) Handles ckbModDatosSubcat.CheckedChanged
        Me.limpiarOpcionesMod(Me.ckbModDatosSubcat, Me.txbNuevaSubcat, Me.dgvSubAdm)
    End Sub

    Private Sub btnModificarSubCat_Click(sender As Object, e As EventArgs) Handles btnModificarSubCat.Click
        Try
            Dim mensaje As String
            mensaje = ""
            If Not General.controlarLetras(Me.txbNuevaSubcat.Text) Or General.controlarCajaEnBlanco(Me.txbNuevaSubcat.Text) Then
                mensaje = mensaje + "Por favor, que el nombre de la categoría sólo contenga letras y que no esté en blanco"
            End If

            If General.controlarCajaEnBlanco(Me.txbDescripcionNuevaSubCat.Text) Then
                mensaje = mensaje + "Por favor, no deje la descripción de la categoría en blanco"
            End If

            If mensaje = "" Then
                mensaje = BusCategorias.modificarDatosSubcategoria(CInt(Me.dgvSubAdm.CurrentRow.Cells(0).Value), Me.txbNuevaSubcat.Text, Me.txbDescripcionNuevaSubCat.Text, Not Me.ckbModDatosSubcat.Checked)
            End If

            If mensaje = "" Then
                MsgBox("Subcategoria modificada correctamente", vbOKOnly, "Ventana Éxito")
                Me.listarSubcategorias(Me.dgvSubAdm)
                Me.limpiarDatos(Me.txbNuevaSubcat, Me.txbDescripcionNuevaSubCat, Me.ckbModDatosSubcat, 2)
            Else
                MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Error al modificar la categoría")
            End If
        Catch ex As Exception
            MsgBox("Error al crear la nueva categoría: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Éxito")
        End Try
    End Sub
    Private Sub modificarEstadoSubcategoria(ByRef grilla As DataGridView)
        Dim valorPrueba As String
        Dim idSub As Integer
        Dim nombre As String
        Dim estado As Boolean
        Dim nombreEstado As String
        Dim respuesta As Integer

        Try
            valorPrueba = grilla.CurrentRow.Cells(0).Value
            If Not IsNothing(valorPrueba) And grilla.CurrentCell.GetType.ToString Like "*Button*" Then
                idSub = CInt(grilla.CurrentRow.Cells("codSub").Value)
                nombre = grilla.CurrentRow.Cells("Nombre").Value
                nombreEstado = grilla.CurrentRow.Cells("cambioEstado").Value
                If nombreEstado = "Activo" Then
                    estado = False
                Else
                    estado = True
                End If
                respuesta = MsgBox("¿Desea modificar el estado de la subcategoria?" + vbNewLine + "Categoria: " + nombre + vbNewLine + "Estado Actual: " + nombreEstado, vbYesNo + vbDefaultButton2, "Consulta De Modificación")
                If respuesta = vbYes Then
                    BusCategorias.modificarEstadoSubcategoria(idSub, estado)
                    Me.listarSubcategorias(grilla)
                    Me.rellenarTodasSubcatActivas(Me.cmbNuevasSubCatAdm)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try

    End Sub
    Private Sub modificarEstadoCategoria(ByRef grilla As DataGridView)
        Dim valorPrueba As String
        Dim idCategoria As Integer
        Dim nombre As String
        Dim estado As Boolean
        Dim nombreEstado As String
        Dim respuesta As Integer

        Try
            valorPrueba = grilla.CurrentRow.Cells(0).Value
            If Not IsNothing(valorPrueba) And grilla.CurrentCell.GetType.ToString Like "*Button*" Then
                idCategoria = CInt(grilla.CurrentRow.Cells("codCat").Value)
                nombre = grilla.CurrentRow.Cells("Nombre").Value
                nombreEstado = grilla.CurrentRow.Cells("cambioEstado").Value
                If nombreEstado = "Activo" Then
                    estado = False
                Else
                    estado = True
                End If
                respuesta = MsgBox("¿Desea modificar el estado de la categoria?" + vbNewLine + "Categoria: " + nombre + vbNewLine + "Estado Actual: " + nombreEstado, vbYesNo + vbDefaultButton2, "Consulta De Modificación")
                If respuesta = vbYes Then
                    BusCategorias.modificarEstadoCategoria(idCategoria, estado)
                    Me.listarCategorias(grilla)
                    Me.rellenarTodasCatActivas(Me.cmbNuevasCatAdm)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")
        End Try

    End Sub

    Private Sub dgvCatSubAdm_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCatSubAdm.CellContentClick

    End Sub

    Private Sub rellenarTodasCatActivas(ByVal contenedor As ComboBox)
        contenedor.DataSource = BusCategorias.getCategoriasActivas()
        contenedor.DisplayMember = "nombre"
        contenedor.ValueMember = "idCategoria"
    End Sub

    Private Sub rellenarTodasSubcatActivas(ByVal contenedor As ComboBox)
        contenedor.DataSource = BusCategorias.getSubCatActivas()
        contenedor.DisplayMember = "nombre"
        contenedor.ValueMember = "idSubcategoria"
    End Sub


    Private Sub grbNuevaCatSub_Enter(sender As Object, e As EventArgs) Handles grbNuevaCatSub.Enter

    End Sub

    Private Sub ckbModCatSub_CheckedChanged(sender As Object, e As EventArgs) Handles ckbModCatSub.CheckedChanged
        Me.cmbNuevasCatAdm.Enabled = ckbModCatSub.Checked
        Me.cmbNuevasSubCatAdm.Enabled = ckbModCatSub.Checked

    End Sub


    Private Sub btnModificarCatSub_Click(sender As Object, e As EventArgs) Handles btnModificarCatSub.Click
        Dim mensaje As String
        Dim modificarCatOSub As Boolean
        Dim idCat As Integer
        Dim idsub As Integer
        mensaje = ""
        modificarCatOSub = Me.ckbModCatSub.Checked
        If IsNothing(Me.cmbNuevasCatAdm.SelectedValue) Or IsNothing(Me.cmbNuevasSubCatAdm.SelectedValue) Then
            mensaje = mensaje + "Por favor, seleccione una categoría y una subcategoría. Controle que la categoría o subcategoría que desea seleccionar esté activa." + vbNewLine
        Else
            idCat = Me.cmbNuevasCatAdm.SelectedValue
            idsub = Me.cmbNuevasSubCatAdm.SelectedValue
        End If

        If mensaje = "" Then
            mensaje = mensaje + BusCategorias.modificarCatSub(idCat, idsub, Me.txbDescCatSub.Text, modificarCatOSub)
        End If

        If mensaje = "" Then
            MsgBox("Se han modificado los datos de la relación de manera exitosa", vbOKOnly, "Ventana Éxito")
        Else
            MsgBox("Error al modificar los datos de la relación. Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly, "Ventana Éxito")
        End If
    End Sub

    Private Sub prepararGrillaProductos(ByRef grilla As DataGridView, ByVal conDescripcion As Boolean, ByVal baja As Boolean)
        Try
            Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
            Dim codProd As New DataGridViewTextBoxColumn()
            Dim cat As New DataGridViewTextBoxColumn()
            Dim subcat As New DataGridViewTextBoxColumn()
            Dim nombre As New DataGridViewTextBoxColumn()
            Dim descripcion As New DataGridViewTextBoxColumn()
            Dim garantia As New DataGridViewTextBoxColumn()
            Dim precio As New DataGridViewTextBoxColumn()
            Dim stock As New DataGridViewTextBoxColumn()
            Dim stockMin As New DataGridViewTextBoxColumn()
            Dim nombreFoto As New DataGridViewTextBoxColumn()
            Dim colEstado As New DataGridViewTextBoxColumn()
            Dim fechaAlta As New DataGridViewTextBoxColumn()

            ' Dim estado As String


            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombre)
            If conDescripcion Then
                grilla.Columns.Add(descripcion)
                grilla.Columns.Add(nombreFoto)
            End If
            grilla.Columns.Add(precio)
            grilla.Columns.Add(cat)
            grilla.Columns.Add(subcat)
            grilla.Columns.Add(stock)
            grilla.Columns.Add(stockMin)
            grilla.Columns.Add(garantia)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(fechaAlta)
            If baja Then
                grilla.Columns.Add(boton)
            End If
            boton.HeaderText = "Alta/Baja"
            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            nombre.Name = "nombre"
            nombre.HeaderText = "Nombre"
            descripcion.Name = "descripcion"
            descripcion.HeaderText = "Descripción"
            nombreFoto.Name = "nombreFoto"
            nombreFoto.HeaderText = "Nombre Foto"
            precio.Name = "precio"
            precio.HeaderText = "Precio Unitario"
            cat.Name = "categoria"
            cat.HeaderText = "Categoria"
            subcat.Name = "subcategoria"
            subcat.HeaderText = "Subcategoria"
            stock.Name = "stock"
            stock.HeaderText = "Stock"
            stockMin.Name = "stockMin"
            stockMin.HeaderText = "Stock Mínimo"
            garantia.Name = "garantia"
            garantia.HeaderText = "Garantia"
            colEstado.Name = "estado"
            colEstado.HeaderText = "Estado"
            fechaAlta.Name = "fechaAlta"
            fechaAlta.HeaderText = "Fecha Alta"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
        Catch ex As Exception
            MsgBox("Error al formatear la grilla de productos", vbOKOnly + vbExclamation, "Ventana Error")
        End Try
    End Sub

    Private Sub listarProductosFiltroNombre(ByRef grilla As DataGridView, ByVal nombre As String, ByVal conDescripcion As Boolean, ByVal baja As Boolean)
        Try
            Dim estado As String


            Dim productos = BusProductos.filtrarPorNombre(nombre)
            If Not IsNothing(productos) Then
                For Each prod In BusProductos.filtrarPorNombre(nombre)

                    If prod.estado Then
                        estado = "Activo"

                    Else
                        estado = "Inactivo"
                    End If
                    If conDescripcion Then
                        grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.descripcionProducto, prod.nombreFoto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                    Else
                        grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                    End If

                    If estado = "Inactivo" Then
                        grilla.Rows.Item(grilla.Rows.Count - 1).DefaultCellStyle.BackColor = Color.Red
                    End If
                Next
            Else
                MsgBox("No existen productos con los criterios señalados", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If
        Catch ex As Exception
            MsgBox("Error al listar los productos.Es posible que aún no haya guardado datos de ningún producto." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try
    End Sub
    Private Sub listarProductos(ByRef grilla As DataGridView, ByVal conDescripcion As Boolean, ByVal baja As Boolean)
        Try
            Dim estado As String

            For Each prod In BusProductos.getProductos()

                If prod.estado Then
                    estado = "Activo"

                Else
                    estado = "Inactivo"
                End If
                If conDescripcion Then
                    grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.descripcionProducto, prod.nombreFoto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                Else
                    grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                End If

                If estado = "Inactivo" Then
                    grilla.Rows.Item(grilla.Rows.Count - 1).DefaultCellStyle.BackColor = Color.Red
                End If
            Next

        Catch ex As Exception
            MsgBox("Error al listar los productos.Es posible que aún no haya guardado datos de ningún producto." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try
    End Sub
    Private Sub habilitarModProd(ByVal activar As Boolean)
        Try

            Me.cmbModCat.Enabled = activar
            Me.cmbModSub.Enabled = activar
            Me.nudModStockMinProd.Enabled = activar
            Me.nudModGarProd.Enabled = activar
            Me.txbDescProdAdmNuevo.Enabled = activar
            Me.txbDescModProd.Enabled = activar
            Me.ptbFotoModProd.Enabled = activar
            Me.chkModNombreProd.Enabled = activar
            'disparo los eventos de los checkbox
            Me.chkModNombreProd.Checked = True
            Me.chkModNombreProd.Checked = False
            Me.chkCambiarCatSubModProd.Checked = True
            Me.chkCambiarCatSubModProd.Checked = False
            Me.btnModProducto.Enabled = activar
            Me.btnFotoModProd.Enabled = activar
            Me.btnCancelarModProd.Enabled = activar
            'Me.lblRutaProdAdmNuevo.Text = "Por favor, seleccione un producto de la lista para modificar sus datos"
        Catch ex As Exception
            MsgBox("Error al habilitar/deshabilitar las opciones de modificación. Los errores presentados fueron: " + vbNewLine + ex.Message)
        End Try
    End Sub
    Private Sub moverDatosModPrecioStock(ByVal grilla As DataGridView)
        Me.txbProdModStock.Text = grilla.CurrentRow.Cells("nombre").Value
        Me.txbNroProdModStock.Text = grilla.CurrentRow.Cells("codProd").Value
        Me.txbStockActModStock.Text = grilla.CurrentRow.Cells("stock").Value
        Me.txbProdModPrecio.Text = grilla.CurrentRow.Cells("nombre").Value
        Me.txbNroProdModPrecio.Text = grilla.CurrentRow.Cells("codProd").Value
        Me.txbPrecioActModPrecio.Text = grilla.CurrentRow.Cells("precio").Value
    End Sub
    Private Sub moverDatosModProd(ByRef grilla As DataGridView)
        Try
            Dim idCat As Integer
            Me.txbIdModProd.Text = grilla.CurrentRow.Cells("codProd").Value
            Me.txbModProdNombre.Text = grilla.CurrentRow.Cells("nombre").Value
            idCat = Me.cmbModCat.FindString(grilla.CurrentRow.Cells("categoria").Value)
            Me.cmbModCat.SelectedIndex = idCat

            If idCat = -1 Then
                Me.chkCambiarCatSubModProd.Enabled = False
                MsgBox("El producto seleccionado tiene su categoría o subcategoría desactivada." + vbNewLine + "Sólo se permitirá la modificación de datos que no sean la categoría o subcategoría", vbOKOnly + vbCritical, "Categorías Desactivadas")
            End If

            Me.nudModStockMinProd.Value = CInt(grilla.CurrentRow.Cells("stockMin").Value)
            Me.nudModGarProd.Value = CInt(grilla.CurrentRow.Cells("garantia").Value)
            Me.txbDescModProd.Text = grilla.CurrentRow.Cells("descripcion").Value
            Me.ptbFotoModProd.Image = Image.FromFile(General.retornarRutaFoto(grilla.CurrentRow.Cells("nombreFoto").Value))

        Catch ex As Exception
            MsgBox("Error al mover los datos del producto para modificarlos. Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error al mover datos")
        End Try
    End Sub


    Private Sub cmbModCat_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbModCat.SelectedValueChanged
        Try
            Dim controlSubActivo As Integer
            If IsNumeric(Me.cmbModCat.SelectedValue) Then
                Me.rellenarSubcategorias(Me.cmbModSub, Me.cmbModCat.SelectedValue)
                controlSubActivo = Me.controlarSubcatActivos(Me.dgvListaProdMod)
                If controlSubActivo < 0 And Not IsNothing(controlSubActivo) Then
                    Me.chkCambiarCatSubModProd.Enabled = False
                    MsgBox("La subcategoría de este producto se encuentra desactivada. No podrá modificar ni la categoría o subcategoría hasta ser activada.", vbOKOnly + vbCritical, "Subcategoria Inactiva")
                End If
                Me.cmbModSub.SelectedIndex = controlSubActivo
            End If
        Catch ex As Exception
            MsgBox("Error al cargar las categorías/subcategorías para cambiar los datos del productos. Los errores producidos son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub dgvListaProdMod_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvListaProdMod.CellClick
        Dim idProducto As Integer
        Dim estado As Boolean
        Dim litEstado As String
        Dim respuesta As Integer
        Dim producto As String
        Dim mensaje As String

        If dgvListaProdMod.CurrentCell.GetType.ToString Like "*Button*" Then
            idProducto = CInt(dgvListaProdMod.CurrentRow.Cells("codProd").Value)
            producto = dgvListaProdMod.CurrentRow.Cells("nombre").Value
            litEstado = dgvListaProdMod.CurrentRow.Cells("estado").Value
            If litEstado = "Activo" Then
                estado = False
                mensaje = "¿Desea dar de baja al producto?" + vbNewLine + producto
            Else
                estado = True
                mensaje = "¿Desea dar de alta al producto?" + vbNewLine + producto
            End If
            respuesta = MsgBox(mensaje, vbYesNo + vbDefaultButton2, "Ventana Consulta")
            If respuesta = vbYes Then
                BusProductos.modificarEstadoProducto(idProducto, estado)
                Me.prepararGrillaProductos(Me.dgvListaProdMod, True, True)
                Me.listarProductos(Me.dgvListaProdMod, True, True)
            End If
        Else
            Me.habilitarModProd(True)
            Me.moverDatosModProd(dgvListaProdMod)
        End If
    End Sub


    Private Sub dgvListaProdMod_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvListaProdMod.CellContentClick

    End Sub

    Private Sub btnIngresarProdNuevo_ContextMenuChanged(sender As Object, e As EventArgs) Handles btnIngresarProdNuevo.ContextMenuChanged

    End Sub

    Private Sub tpgAgregarProd_Click(sender As Object, e As EventArgs) Handles tpgAgregarProd.Click

    End Sub

    Private Sub chkModNombreProd_CheckedChanged(sender As Object, e As EventArgs) Handles chkModNombreProd.CheckedChanged
        Me.txbModProdNombre.Enabled = chkModNombreProd.Checked

    End Sub

    Private Sub btnCancelarModProd_Click(sender As Object, e As EventArgs) Handles btnCancelarModProd.Click
        Me.habilitarModProd(False)
        Me.limpiarModProd()
    End Sub

    Private Sub chkbCambiarCatSubModProd_CheckedChanged(sender As Object, e As EventArgs) Handles chkCambiarCatSubModProd.CheckedChanged
        Me.cmbModCat.Enabled = Me.chkCambiarCatSubModProd.Checked
        Me.cmbModSub.Enabled = Me.chkCambiarCatSubModProd.Checked
    End Sub
    Private Sub modificarDatosProducto()
        Dim mensaje As String
        Dim mensajeRep As String
        Dim nombre As String
        Dim descripcion As String
        Dim idProd As Integer
        Dim idCat As Integer
        Dim idSub As Integer
        Dim stockMin As Integer
        Dim garantia As Integer
        Dim modFoto As Boolean
        Dim modNombre As Boolean
        Dim habModCatSub As Boolean
        Dim nombreFotoNueva As String
        Dim nombreFotoAntigua As String
        Dim modCat As Boolean

        modFoto = False
        habModCatSub = Me.chkCambiarCatSubModProd.Enabled
        nombreFotoAntigua = Me.dgvListaProdMod.CurrentRow.Cells("nombreFoto").Value
        idCat = Me.cmbModCat.SelectedValue
        idSub = Me.cmbModSub.SelectedValue
        stockMin = Me.nudModStockMinProd.Value
        garantia = Me.nudModGarProd.Value
        nombre = Me.txbModProdNombre.Text
        idProd = CInt(Me.txbIdModProd.Text)
        descripcion = Me.txbDescModProd.Text
        modNombre = Me.chkModNombreProd.Checked
        modCat = Me.chkCambiarCatSubModProd.Checked
        nombreFotoNueva = ""
        mensaje = ""
        'controlo que sí se desea modificar la imagen del producto
        If Me.txbRutaFotoModProd.Text <> "" Then
            modFoto = True
            nombreFotoNueva = General.obtenerNombreImagen(Me.txbRutaFotoModProd.Text)
            'reviso que no haya unos foto repetida
            If General.controlarImagenRepetida(nombreFotoNueva) Then
                mensaje = "Ya existe una imagen con el nombre indicado." + vbNewLine + "Si desea que el producto tenga la imagen seleccionada, por favor, cambie el nombre de la imagen." + vbNewLine
            End If
        End If
        'verifica que los datos del producto sean correctos
        mensaje = mensaje + Me.verificarProducto(Me.txbModProdNombre.Text, Me.cmbModCat.Text, Me.cmbModSub.Text, "1", Me.nudStockMinNuevoProd.Value, Me.txbDescModProd.Text, Me.ptbFotoModProd.Image, habModCatSub)
        If String.IsNullOrWhiteSpace(mensaje) Then
            'verifica que el producto no tenga un nombre repetido con un producto existente
            mensajeRep = BusProductos.modificarProducto(idProd, nombre, idCat, idSub, stockMin, garantia, descripcion, nombreFotoNueva, modNombre, modCat, habModCatSub, modFoto)
            mensaje = Trim(mensaje + mensajeRep)
            If modFoto And mensaje = "" Then
                'copio la nuevo imagen a la carpeta
                Me.limpiarModProd()
                Me.limpiarTodasLasGrillas()
                General.eliminarImagenCarpeta(nombreFotoAntigua)
                General.copiarImagenACarpeta(ofdBuscarFoto, nombreFotoNueva)
            End If
        End If

        If mensaje = "" Then
            MsgBox("Datos Modificados Correctamente", vbOKOnly, "Ventana Éxito")
            Me.limpiarModProd()
            Me.prepararGrillaProductos(Me.dgvListaProdMod, True, False)
            Me.listarProductos(Me.dgvListaProdMod, True, False)
        Else
            MsgBox("Se han producido los siguientes errores: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
            Exit Sub
        End If
    End Sub
    Private Sub limpiarTodasLasGrillas()
        Try
            Me.dgvListaProdMod.DataSource = Nothing
            Me.dgvListaProdMod.Rows.Clear()
            Me.dgvProvProd.DataSource = Nothing
            Me.dgvProvProd.Rows.Clear()
            Me.dgvProvProdCompras.DataSource = Nothing
            Me.dgvProvProdCompras.Rows.Clear()
            Me.dgvProdModstockPrecio.DataSource = Nothing
            Me.dgvProdModstockPrecio.Rows.Clear()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btnModProducto_Click(sender As Object, e As EventArgs) Handles btnModProducto.Click
        Me.modificarDatosProducto()
    End Sub

    Private Sub rellenarSubCatPuras(ByRef cajaSubcat As ComboBox)
        Dim subcat As New List(Of Entities.Subcategorias)
        Try
            If Not IsNothing(subcat) Then
                cajaSubcat.DataSource = Nothing
                cajaSubcat.DataSource = BusCategorias.getSubCatActivas()
                cajaSubcat.DisplayMember = "nombre"
                cajaSubcat.ValueMember = "idSubcategoria"
            Else
                cajaSubcat.DataSource = Nothing
                MsgBox("La categoria seleccionada no tiene aún una subcategoria asignada. Tenga en cuenta que no podrá seleccionar dicha categoría para el nuevo producto. Por favor, asigne una subcategoría para esta categoría, sino seleccione otra.")
            End If
        Catch ex As Exception
            MsgBox("Error al obtener las subcategorías del producto.Los errores presentados son: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Function controlarSubcatActivos(ByRef grilla As DataGridView) As Integer
        Dim idSub As Integer

        idSub = -1
        Me.chkCambiarCatSubModProd.Enabled = True
        If Not IsNothing(grilla.CurrentRow) Then
            idSub = Me.cmbModSub.FindString(grilla.CurrentRow.Cells("subcategoria").Value)
            If idSub < 0 Then
                Me.chkCambiarCatSubModProd.Enabled = False
            End If
            Me.cmbModSub.SelectedValue = idSub
            Return idSub
        Else
            Return Nothing
        End If


    End Function

    Private Sub dgvEliminarProductos_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)



    End Sub
#Region "Proveedores"
    Private Function controlarProdProv() As String
        Try
            Dim mensaje As String
            mensaje = ""
            If General.controlarCajaEnBlanco(Me.txbProv_ProdProv.Text) Or General.controlarCajaEnBlanco(Me.txbProd_ProvProd.Text) Then
                mensaje = mensaje + "Por favor, elija el producto y el proveedor de la lista." + vbNewLine
            End If

            If Not General.controlarCajaEnBlanco(Me.txbPrecioCompraProd.Text) Then
                If General.controlarReales(Me.txbPrecioCompraProd.Text) Then
                    If CDbl(Me.txbPrecioCompraProd.Text) <= 0 Then
                        mensaje = mensaje + "Por favor, coloque un valor numérico mayor a cero para el precio de compra del producto." + vbNewLine
                    End If
                Else
                    mensaje = mensaje + "Por favor, coloque un valor numérico para el precio de compra del producto." + vbNewLine
                End If
            Else
                mensaje = mensaje + "Por favor, coloque un valor para el precio de compra." + vbNewLine
            End If

            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar los datos para la relación producto/proveedor. Los errores producidos fueron: " + ex.Message, vbOKOnly + vbCritical, "Error al controlar datos")
            Return Nothing
        End Try
    End Function
    Private Sub moverDatosProveedor(ByVal grilla As DataGridView)
        Me.txbIdModProv.Text = grilla.CurrentRow.Cells("codProv").Value
        Me.txbNombreModProv.Text = grilla.CurrentRow.Cells("nombre").Value
        Me.txbDirModProv.Text = grilla.CurrentRow.Cells("direccion").Value
        Me.txbTelModProv.Text = grilla.CurrentRow.Cells("telefono").Value
        Me.txbCodAreaModProv.Text = General.getCodArea(grilla.CurrentRow.Cells("celular").Value)
        Me.txbCelModProv.Text = General.getNumeroCel(grilla.CurrentRow.Cells("celular").Value)
        Me.txbMailModProv.Text = grilla.CurrentRow.Cells("mail").Value
        'activo el evento del checkbox
        Me.chkModNombreProve.Checked = True
        Me.chkModNombreProve.Checked = False
    End Sub
    Private Sub prepararGrillaProveedor(ByRef grilla As DataGridView, ByVal baja As Boolean)


        'grilla.ColumnCount = 8
        Try
            Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
            Dim codProv As New DataGridViewTextBoxColumn()
            Dim nombre As New DataGridViewTextBoxColumn()
            Dim direccion As New DataGridViewTextBoxColumn()
            Dim telefono As New DataGridViewTextBoxColumn()
            Dim celular As New DataGridViewTextBoxColumn()
            Dim mail As New DataGridViewTextBoxColumn()
            Dim colEstado As New DataGridViewTextBoxColumn()

            grilla.Columns.Clear()
            grilla.Columns.Add(codProv)
            grilla.Columns.Add(nombre)
            grilla.Columns.Add(direccion)
            grilla.Columns.Add(telefono)
            grilla.Columns.Add(celular)
            grilla.Columns.Add(mail)
            grilla.Columns.Add(colEstado)
            If baja Then
                grilla.Columns.Add(boton)
            End If

            boton.HeaderText = "Alta/Baja"
            colEstado.Name = "estado"
            colEstado.HeaderText = "Estado"
            codProv.HeaderText = "Número Proveedor"
            codProv.Name = "codProv"
            nombre.HeaderText = "Nombre Proveedor"
            nombre.Name = "nombre"
            direccion.HeaderText = "Dirección"
            direccion.Name = "direccion"
            telefono.HeaderText = "Teléfono"
            telefono.Name = "telefono"
            celular.HeaderText = "Celular"
            celular.Name = "celular"
            mail.HeaderText = "Mail"
            mail.Name = "mail"

            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
        Catch ex As Exception
            MsgBox("Error al preparar la grilla de proveedores. Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub listarProveedores(ByVal grilla As DataGridView, ByVal baja As Boolean)



        'grilla.ColumnCount = 8
        Try

            Dim estado As String
            For Each provedor In BusProveedores.getProveedores()

                If provedor.estado Then
                    estado = "Activo"

                Else
                    estado = "Inactivo"
                End If
                grilla.Rows.Add(provedor.idProveedor, provedor.nombreComercial, provedor.direccionProveedor, provedor.telefonoProv, provedor.celularProv, provedor.mailProv, estado)
            Next
        Catch ex As Exception
            MsgBox("Error al listar las subcategorias.Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly, "Error")

        End Try
    End Sub
    '----------------------------------------------------------------------------------------------------
    'Permite mover los datos del producto y del proveedor para establecer la relación
    'Parámetros
    'grilla-Tipo: Datagridview-Es la lista de donde se sacará la información de productos y proveedores
    'prodOProv-Tipo: Boolean-Determina si se va a mover los datos del proveedor o del producto. True es para
    'mover los datos del proveedor y false para los datos del producto
    '--------------------------------------------------------------------------------------------------
    Private Sub moverDatosProdProv(ByRef grilla As DataGridView, ByVal proveedor As Boolean)
        Try
            If proveedor Then
                Me.lblIdProveedor.Text = grilla.CurrentRow.Cells("codProv").Value
                Me.txbProv_ProdProv.Text = grilla.CurrentRow.Cells("nombre").Value
            Else
                Me.txbPrecioVenta_ProvProd.Text = grilla.CurrentRow.Cells("precio").Value
                Me.lblIdProducto.Text = grilla.CurrentRow.Cells("codProd").Value
                Me.txbProd_ProvProd.Text = grilla.CurrentRow.Cells("nombre").Value
            End If
        Catch ex As Exception
            MsgBox("Error al mover los datos de los proveedores o productos. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de recuperación de datos")
        End Try
    End Sub

    Private Sub activarDesactivarDatosProdMov(ByVal activar As Boolean)
        Me.grbDatosProdProv.Enabled = activar
    End Sub

    Private Function controlarDatos(ByVal idProv As String, ByVal idProd As String, ByVal nombreProd As String, nombreProv As String, ByVal precioCompra As String, ByVal precioVenta As String) As String
        Try
            Dim mensaje As String
            mensaje = ""
            If Not General.controlarCajaEnBlanco(idProv) Or Not General.controlarCajaEnBlanco(idProd) Then
                mensaje = mensaje + "Por favor, seleccione un producto y un proveedor para relacionarlos" + vbNewLine
            End If

            If General.controlarCajaEnBlanco(precioCompra) Then
                mensaje = mensaje + "Por favor, no deje el precio de compra en blanco" + vbNewLine
            Else
                If (CDbl(precioCompra) <= 0) Then
                    mensaje = mensaje + "Por favor, colqoue un valor mayor a cero para el precio de compra" + vbNewLine
                End If
            End If
            Return mensaje
        Catch ex As Exception
            MsgBox("Error al controlar los datos de la relación. Los errores presentados fueron: " + vbNewLine, vbOKOnly + vbCritical, "Error de control")
            Return Nothing
        End Try
    End Function
    Private Sub limpiarDatosModStockPrecio()
        Try
            Me.txbProdModStock.Text = ""
            Me.txbNroProdModStock.Text = ""
            Me.txbStockActModStock.Text = ""
            Me.txbProdModPrecio.Text = ""
            Me.txbNroProdModPrecio.Text = ""
            Me.txbPrecioActModPrecio.Text = ""
            Me.nudModificarStock.Value = 0
            Me.txbPrecioModProd.Text = ""
        Catch ex As Exception
            MsgBox("Error al preparar para la siguiente modificación. Los errores producidos fueron: " + vbOKOnly + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub activarDesactivarModPrecioStock(ByVal estado As Boolean)
        Me.grbDatosProdModStock.Enabled = estado
        Me.grbDatosProdModPrecio.Enabled = estado
        If estado = False Then
            Me.limpiarDatosModStockPrecio()
        End If
    End Sub
#End Region
#Region "Productos"

#End Region
    Private Sub dgvProveedores_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProveedores.CellContentClick
        Dim idProv As Integer
        Dim estado As Boolean
        If Not dgvProveedores.CurrentCell.GetType.ToString Like "*Button*" Then
            Me.moverDatosProveedor(dgvProveedores)
        Else
            idProv = CInt(dgvProveedores.CurrentRow.Cells("codProv").Value)
            If dgvProveedores.CurrentRow.Cells("estado").Value = "Activo" Then
                estado = False
            Else
                estado = True
            End If
            BusProveedores.modificarEstado(idProv, estado)
            Me.prepararGrillaProveedor(Me.dgvProveedores, True)
            Me.listarProveedores(Me.dgvProveedores, True)
        End If


    End Sub

    Private Sub chkModNombreProve_CheckedChanged(sender As Object, e As EventArgs) Handles chkModNombreProve.CheckedChanged
        Me.txbNombreModProv.Enabled = Me.chkModNombreProve.Checked
    End Sub

    Private Sub ckbConfModPrecio_CheckedChanged(sender As Object, e As EventArgs) Handles ckbConfModPrecio.CheckedChanged
        'verifica qué opción debe estar señalada inicialmente para los motivos de modificación
        Dim indexSinMod As Integer
        indexSinMod = Me.cmbMotivoModCompra.FindStringExact("Sin Modificación")
        Me.grbModPrecioCompra.Enabled = Me.ckbConfModPrecio.Checked
        If Me.ckbConfModPrecio.Checked Then
            Me.cmbMotivoModCompra.SelectedIndex = 0
        Else
            Me.cmbMotivoModCompra.SelectedIndex = indexSinMod
        End If
    End Sub

    Private Sub dgvProvProd_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProvProd.CellContentClick
        Me.moverDatosProdProv(Me.dgvProvProd, False)
    End Sub

    Private Sub dgvProv_ProvProd_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProv_ProvProd.CellContentClick
        Me.moverDatosProdProv(Me.dgvProv_ProvProd, True)
    End Sub

    Private Sub btnMostrarRelacionProdProv_Click(sender As Object, e As EventArgs) Handles btnMostrarRelacionProdProv.Click
        Dim provProd As New frmProveedoresProductos
        provProd.abrirVentana()
    End Sub




    Private Sub redimensionarImagen(ByRef dialogo As FileDialog, ByVal rutaImagen As String, ByRef caja As PictureBox)
        Dim fs As FileStream = New FileStream(rutaImagen, FileMode.Open, FileAccess.Read, FileShare.Read)
        Dim LaImagen As System.Drawing.Image
        'Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)
        'cadena = cadena + "\Imagenes\" + nombreArchivo
        'System.IO.File.Copy(dialogo.FileName, cadena)
        LaImagen = System.Drawing.Image.FromStream(fs)

        Dim ancho, alto As Integer
        ancho = 260
        alto = 250
        'este calculo es para que la foto no pierda proporciones
        'alto = Math.Floor((2 * ancho / LaImagen.Width) * LaImagen.Height)

        Dim NuevoBitmap As Bitmap = New Bitmap(ancho, alto)
        Dim Graficos As Graphics = Graphics.FromImage(NuevoBitmap)
        Graficos.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
        Graficos.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        Graficos.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        Dim Rectangulo As Rectangle = New Rectangle(0, 0, ancho, alto)
        caja.Image = NuevoBitmap
        Graficos.DrawImage(LaImagen, Rectangulo)
        'NuevoBitmap.Save(cadena, NuevoBitmap.RawFormat)
        'fs.Close()
        'fs = Nothing
    End Sub

    Private Sub guardarImagen(ByRef dialogo As FileDialog, ByVal rutaImagen As String, ByVal nombreArchivo As String, ByRef caja As PictureBox)
        Try
            Dim fs As FileStream = New FileStream(rutaImagen, FileMode.Open, FileAccess.Read, FileShare.Read)
            Dim LaImagen As System.Drawing.Image
            Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)
            cadena = cadena + "\Imagenes\" + nombreArchivo
            System.IO.File.Copy(dialogo.FileName, cadena)
            LaImagen = System.Drawing.Image.FromStream(fs)

            Dim ancho, alto As Integer
            ancho = 260
            alto = 250
            'este calculo es para que la foto no pierda proporciones
            'alto = Math.Floor((2 * ancho / LaImagen.Width) * LaImagen.Height)

            Dim NuevoBitmap As Bitmap = New Bitmap(ancho, alto)
            Dim Graficos As Graphics = Graphics.FromImage(NuevoBitmap)
            Graficos.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
            Graficos.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
            Graficos.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
            Dim Rectangulo As Rectangle = New Rectangle(0, 0, ancho, alto)
            caja.Image = NuevoBitmap
            Graficos.DrawImage(LaImagen, Rectangulo)
            NuevoBitmap.Save(cadena, NuevoBitmap.RawFormat)
            fs.Close()
            fs = Nothing
        Catch ex As Exception
            MsgBox("Error al guardar la imagen a la carpeta. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub dgvProdModstockPrecio_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProdModstockPrecio.CellContentClick
        Me.moverDatosModPrecioStock(Me.dgvProdModstockPrecio)
        Me.activarDesactivarModPrecioStock(True)
    End Sub

    Private Sub btnCancelarModStock_Click(sender As Object, e As EventArgs) Handles btnCancelarModStock.Click
        Me.activarDesactivarModPrecioStock(False)
    End Sub

    Private Sub btnCancelarModPrecio_Click(sender As Object, e As EventArgs) Handles btnCancelarModPrecio.Click
        Me.activarDesactivarModPrecioStock(False)
    End Sub

    Private Sub cargarDatoCarritoCompra(ByRef grilla As DataGridView)
        Dim codProd As String
        Dim nombreProducto As String
        Dim codProv As String
        Dim nombreProveedor As String
        Dim cantidad As Integer
        Dim precioMod As Double
        Dim precioTotal As Double
        Dim motivoMod As String
        Dim descripcion As String
        Dim textoMod As String
        Dim indiceMod As Integer
        Dim idMotivo As Integer
        Dim precioUnitario As String

        precioUnitario = Me.txbPrecioCompra.Text
        codProd = Me.txbNroProdCompra.Text
        nombreProducto = Me.txbNombreProdCompra.Text
        codProv = Me.txbNroProvCompra.Text
        nombreProveedor = Me.txbProvCompra.Text
        cantidad = Me.nudCantidadCompra.Value
        textoMod = Me.cmbMotivoModCompra.Text
        indiceMod = Me.cmbMotivoModCompra.FindString(textoMod)
        If Me.ckbConfModPrecio.Checked Then
            If Me.chkAuPrecioCompra.Checked Then
                precioMod = CDbl(Me.txbModiPrecioCompra.Text)
            Else
                precioMod = CDbl(Me.txbModiPrecioCompra.Text) * (-1)
            End If
            descripcion = Me.txbDescModCompra.Text
            textoMod = Me.cmbMotivoModCompra.Text

        Else
            textoMod = "Sin Modificación"
            precioMod = 0
            descripcion = "Sin descripción de modificación"
        End If

        precioTotal = CDbl(Me.txbPrecioCompra.Text) * Me.nudCantidadCompra.Value
        motivoMod = textoMod
        idMotivo = Me.cmbMotivoModCompra.SelectedValue
        grilla.Rows.Add(codProd, nombreProducto, codProv, nombreProveedor, cantidad, precioTotal, textoMod, precioMod, descripcion, idMotivo, precioUnitario)
    End Sub
    Private Sub prepararCarritoCompra(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim btnEliminar As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codProd As New DataGridViewTextBoxColumn()
        Dim nombreProducto As New DataGridViewTextBoxColumn()
        Dim codProv As New DataGridViewTextBoxColumn()
        Dim nombreProveedor As New DataGridViewTextBoxColumn()
        Dim cantidad As New DataGridViewTextBoxColumn()
        Dim precioTotal As New DataGridViewTextBoxColumn()
        Dim motivoMod As New DataGridViewTextBoxColumn()
        Dim cantidadMod As New DataGridViewTextBoxColumn()
        Dim idMotivo As New DataGridViewTextBoxColumn()
        Dim precioUnitario As New DataGridViewTextBoxColumn()
        Dim descripcion As New DataGridViewTextBoxColumn()


        'grilla.ColumnCount = 8
        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProducto)
            grilla.Columns.Add(codProv)
            grilla.Columns.Add(nombreProveedor)
            grilla.Columns.Add(cantidad)
            grilla.Columns.Add(precioTotal)
            grilla.Columns.Add(motivoMod)
            grilla.Columns.Add(cantidadMod)
            grilla.Columns.Add(descripcion)
            grilla.Columns.Add(idMotivo)
            grilla.Columns.Add(precioUnitario)
            grilla.Columns.Add(boton)
            grilla.Columns.Add(btnEliminar)


            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            nombreProducto.Name = "nombreProducto"
            nombreProducto.HeaderText = "Producto"
            codProv.Name = "codProv"
            codProv.HeaderText = "Número Proveedor"
            nombreProveedor.Name = "nombreProveedor"
            nombreProveedor.HeaderText = "Proveedor"
            cantidad.Name = "cantidad"
            cantidad.HeaderText = "Cantidad"
            precioTotal.Name = "precio"
            precioTotal.HeaderText = "Precio"
            motivoMod.Name = "motivoMod"
            motivoMod.HeaderText = "Modificación Precio"
            cantidadMod.Name = "cantidadMod"
            cantidadMod.HeaderText = "Monto Modificado"
            descripcion.Name = "descMod"
            descripcion.HeaderText = "Descripción Modificación"
            idMotivo.Name = "idMotivo"
            idMotivo.Visible = False
            precioUnitario.Name = "precioUnitario"
            precioUnitario.Visible = False
            boton.Text = "Modificar"
            boton.Name = "btnDisminuir"
            boton.HeaderText = "Modificar Unidades"
            boton.UseColumnTextForButtonValue = True
            btnEliminar.Text = "Eliminar"
            btnEliminar.Name = "btnEliminar"
            btnEliminar.HeaderText = "Eliminar"
            btnEliminar.UseColumnTextForButtonValue = True

        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub

    Private Sub moverDatosProdProv(ByRef grilla As DataGridView)
        Try
            If grilla.CurrentRow.Cells("estado").Value = "Inactivo" Then
                MsgBox("Por favor, revise que el producto seleccionado se encuentra en estado inactivo." + vbNewLine + "Por favor tenga en cuenta que, aun comprando unidades para este prpducto, no aparecerá para la venta hasta activarl.", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If
            Me.grbModPrecioCompra.Enabled = True
            Me.txbNroProdCompra.Text = grilla.CurrentRow.Cells("codProd").Value
            Me.txbNroProvCompra.Text = grilla.CurrentRow.Cells("codProv").Value
            Me.txbNombreProdCompra.Text = grilla.CurrentRow.Cells("nombreProducto").Value
            Me.txbProvCompra.Text = grilla.CurrentRow.Cells("nombreProveedor").Value
            Me.txbPrecioCompra.Text = grilla.CurrentRow.Cells("precioCompra").Value
            'dispara el evento
            Me.ckbConfModPrecio.Checked = True
            Me.ckbConfModPrecio.Checked = False

        Catch ex As Exception
            MsgBox("Error al mover los datos para realizar la compra del producto. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub listarProductosProveedores(ByRef grilla As DataGridView)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codProd As New DataGridViewTextBoxColumn()
        Dim codProv As New DataGridViewTextBoxColumn()
        Dim nombreProv As New DataGridViewTextBoxColumn()
        Dim nombreProd As New DataGridViewTextBoxColumn()
        Dim precioCompra As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim fechaAlta As New DataGridViewTextBoxColumn()

        Dim estado As String

        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProd)
            grilla.Columns.Add(codProv)
            grilla.Columns.Add(nombreProv)
            grilla.Columns.Add(precioCompra)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(fechaAlta)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            codProv.Name = "codProv"
            codProv.HeaderText = "Número Proveedor"
            nombreProd.Name = "nombreProducto"
            nombreProd.HeaderText = "Producto"
            nombreProv.Name = "nombreProveedor"
            nombreProv.HeaderText = "Proveedor"
            precioCompra.Name = "precioCompra"
            precioCompra.HeaderText = "Precio Compra"
            colEstado.Name = "estado"
            colEstado.HeaderText = "Estado"
            fechaAlta.Name = "fechaAlta"
            fechaAlta.HeaderText = "Fecha Alta"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True
            'grilla.DataSource = BusProductos.getProductos()

            For Each prod In BusProveedoresProductos.gerProveedoresProductos()

                If prod.estado Then
                    estado = "Activo"

                Else
                    estado = "Inactivo"
                End If
                grilla.Rows.Add(prod.idProducto, prod.Productos.nombreProducto, prod.idProveedor, prod.Proveedores.nombreComercial, prod.precioCompra, estado, prod.fechaAlta)
            Next
        Catch ex As Exception
            MsgBox("Error al listar las relaciones productos/proveedores.Es posible que aún no haya guardado datos de ninguna relación." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try
    End Sub

    Private Sub dgvProvProdCompras_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProvProdCompras.CellContentClick
        Me.moverDatosProdProv(dgvProvProdCompras)
    End Sub

    Private Sub cmbMotivoModCompra_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles cmbMotivoModCompra.SelectedIndexChanged
        If Me.cmbMotivoModCompra.Text = "Sin Modificación" Then
            Me.ckbConfModPrecio.Checked = False
        End If
    End Sub

    Private Sub chkAuPrecioCompra_CheckedChanged(sender As Object, e As EventArgs) Handles chkAuPrecioCompra.CheckedChanged
        Me.chkDescPrecioCompra.Checked = Not Me.chkAuPrecioCompra.Checked
    End Sub

    Private Sub chkDescPrecioCompra_CheckedChanged(sender As Object, e As EventArgs) Handles chkDescPrecioCompra.CheckedChanged
        Me.chkAuPrecioCompra.Checked = Not Me.chkDescPrecioCompra.Checked
    End Sub

    Private Sub realizarCompra(ByRef grilla As DataGridView)
        Try
            Dim ultimaCabeceraCompra As Integer
            Dim ultimaCabeceraMod As Integer
            Dim idProductos As New List(Of Integer)
            Dim idProveedores As New List(Of Integer)
            Dim unidadesCompra As New List(Of Integer)
            Dim montosCompra As New List(Of Double)
            Dim montosMod As New List(Of Double)
            Dim idMotivos As New List(Of Integer)
            Dim stock As New List(Of Integer)
            Dim descripciones As New List(Of String)
            Dim elementosGrilla As Integer
            Dim elementoAnalizado As Integer
            Dim montoTotal As Double
            Dim montoTotalMod As Double
            Dim montoFinal As Double

            elementosGrilla = grilla.Rows.Count
            elementoAnalizado = 0
            montoTotal = 0
            montoTotalMod = 0
            'recorro la grilla para almacenar los datos relevantes
            While elementoAnalizado < elementosGrilla
                If CDbl(grilla.Rows(elementoAnalizado).Cells("cantidadMod").Value) < 0 Then
                    montoFinal = (CDbl(grilla.Rows(elementoAnalizado).Cells("precio").Value) + CDbl(grilla.Rows(elementoAnalizado).Cells("cantidadMod").Value))
                Else
                    montoFinal = (CDbl(grilla.Rows(elementoAnalizado).Cells("precio").Value) - CDbl(grilla.Rows(elementoAnalizado).Cells("cantidadMod").Value))
                End If

                idProductos.Add(CInt(grilla.Rows(elementoAnalizado).Cells("codProd").Value))
                idProveedores.Add(CInt(grilla.Rows(elementoAnalizado).Cells("codProv").Value))
                unidadesCompra.Add(CInt(grilla.Rows(elementoAnalizado).Cells("cantidad").Value))
                montosMod.Add(CDbl(grilla.Rows(elementoAnalizado).Cells("cantidadMod").Value))
                montosCompra.Add(montoFinal)
                montoTotal = montoTotal + montosCompra(elementoAnalizado)
                montoTotalMod = montoTotalMod + montosMod(elementoAnalizado)
                idMotivos.Add(grilla.Rows(elementoAnalizado).Cells("idMotivo").Value)
                descripciones.Add(grilla.Rows(elementoAnalizado).Cells("descMod").Value)
                elementoAnalizado = elementoAnalizado + 1
            End While
            'inserto las cabeceras
            BusCompras.insertarCabeceraCompra(montoTotal, Sesion.getIdUsuario())
            BusCompras.insertarCabeceraMod(montoTotalMod)
            'obtengo los id de las últimas cabeceras insertadas
            ultimaCabeceraCompra = BusCompras.getUltimaCabeceraCompra()
            ultimaCabeceraMod = BusCompras.getUltimaCabeceraMod()
            'inserto los detalles de la compra
            BusCompras.insertarDetallesCompra(ultimaCabeceraCompra, montosCompra, unidadesCompra, idProductos, idProveedores)
            'inserto los detalles de las modificaciones
            BusCompras.insertarDetallesMod(ultimaCabeceraCompra, ultimaCabeceraMod, montosMod, idMotivos, descripciones)
            'actualizo el stock
            BusCompras.actuatlizaStockPorCompra(idProductos, unidadesCompra)
            MsgBox("Compra realizada exitosamente. Revise el stock de los productos comprados para asegurarse de que se asentaron adecuadamente.")
        Catch ex As Exception
            MsgBox("Error al realizarse al la compra. Los errores producidos fueron:" + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de compra")
        End Try
    End Sub

    Private Sub btnComprar_Click(sender As Object, e As EventArgs) Handles btnComprar.Click
        Dim respuesta As Integer
        respuesta = MsgBox("¿Desea confirmar la compra de los productos?", vbYesNo + vbDefaultButton2, "Ventana De Confirmación")
        If respuesta = vbYes Then
            If Me.dgvCarritoCompras.Rows.Count <= 0 Then
                MsgBox("No ha seleccionado ningún producto para comprar." + vbNewLine + "Por favor, seleccione uno antes de proceder.", vbOKOnly + vbExclamation, "Carrito Vacío")
            Else
                Me.realizarCompra(Me.dgvCarritoCompras)
                Me.limpiarCompra()
                Me.limpiarCarrito(Me.dgvCarritoCompras)
            End If
        End If
    End Sub
    Private Sub moverParaModificarCompra(ByRef carrito As DataGridView)
        Try

        Catch ex As Exception
            MsgBox("Error al mover los datos para modificar. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub modificarUnidadesCompradas(ByRef carrito As DataGridView)
        Try
            Dim cantidadNueva As String
            Dim valorActual As Integer
            Dim precioUnitario As Double

            cantidadNueva = InputBox("Indique la nueva cantidad de unidades a comprar. Si coloca un valor igual o menor al existente, dicho elemento se quitará del carrito", "Disminución Carrito", "0")
            cantidadNueva = Trim(cantidadNueva)
            If Not General.controlarCajaEnBlanco(cantidadNueva) And General.controlarEnterosPos(cantidadNueva) Then
                precioUnitario = CDbl(carrito.CurrentRow.Cells("precioUnitario").Value)
                If CInt(cantidadNueva) > 0 Then
                    valorActual = precioUnitario * CInt(cantidadNueva)
                    carrito.CurrentRow.Cells("cantidad").Value = cantidadNueva
                    carrito.CurrentRow.Cells("precio").Value = valorActual
                Else
                    MsgBox("Por favor, no coloque 0 como un nuevo valor", vbOKOnly + vbExclamation, "Ventana Aviso")
                End If
            Else
                MsgBox("Por favor, colque un valor numérico entero positivo", vbOKOnly + vbCritical, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error al modificar los datos en los elementos del carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub eliminarElementoCarrito(ByRef carrito As DataGridView)
        Try
            Dim respuesta As Integer
            respuesta = MsgBox("¿Desea eliminar el producto seleccionado? ", vbYesNo + vbDefaultButton2, "Ventana Consulta")
            If respuesta = vbYes Then
                carrito.Rows.Remove(carrito.CurrentRow)
            End If
        Catch ex As Exception
            MsgBox("Error al eliminar un elemento del carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub dgvCarritoCompras_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCarritoCompras.CellClick
        'Dim respuesta As MsgBoxResult
        Dim cell As DataGridViewButtonCell = TryCast(dgvCarritoCompras.CurrentCell, 
        DataGridViewButtonCell)
        If cell IsNot Nothing Then 'Verifica que la celdas tengan informacion
            Dim bc As DataGridViewButtonColumn = TryCast(dgvCarritoCompras.Columns(e.ColumnIndex), 
            DataGridViewButtonColumn) 'Genero una variable que contiene el boton en el datagrid
            If bc IsNot Nothing Then
                Dim s As String = Convert.ToString(cell.Value)
                Select Case bc.Name
                    Case "btnDisminuir"
                        Me.moverDatosModCarrito(Me.dgvCarritoCompras)
                    Case "btnEliminar"
                        Me.eliminarElementoCarrito(dgvCarritoCompras)
                End Select
            End If

        End If
    End Sub
    Private Sub moverDatosModCarrito(ByRef carrito As DataGridView)
        Try
            Dim montoDescuento As Integer

            Me.txbNroProdCompra.Text = carrito.CurrentRow.Cells("codProd").Value
            Me.txbNombreProdCompra.Text = carrito.CurrentRow.Cells("nombreProducto").Value
            Me.txbNroProvCompra.Text = carrito.CurrentRow.Cells("codProv").Value
            Me.txbProvCompra.Text = carrito.CurrentRow.Cells("nombreProveedor").Value
            Me.txbPrecioCompra.Text = carrito.CurrentRow.Cells("precioUnitario").Value
            Me.nudCantidadCompra.Value = CInt(carrito.CurrentRow.Cells("cantidad").Value)
            montoDescuento = CInt(carrito.CurrentRow.Cells("cantidadMod").Value)
            If montoDescuento < 0 Then
                montoDescuento = (-1) * montoDescuento
                Me.chkDescPrecioCompra.Checked = True
                Me.ckbConfModPrecio.Checked = True
            Else
                Me.chkAuPrecioCompra.Checked = True
                Me.ckbConfModPrecio.Checked = True
            End If
            Me.txbModiPrecioCompra.Text = CStr(montoDescuento)
            Me.cmbMotivoModCompra.SelectedValue = CInt(carrito.CurrentRow.Cells("idMotivo").Value)
            Me.txbDescModCompra.Text = carrito.CurrentRow.Cells("descMod").Value
            Me.btnAsignarCarritoCompra.Enabled = False
            Me.btnModCarrito.Enabled = True
            Me.btnCancelarModCarrito.Enabled = True
            Me.grbCarritoCompra.Enabled = False
        Catch ex As Exception
            MsgBox("Error al mover los datos del producto para modificar. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana De Error")
        End Try
    End Sub

    Private Sub btnCancelarModCarrito_Click(sender As Object, e As EventArgs) Handles btnCancelarModCarrito.Click
        Me.limpiarCompra()
    End Sub

    Private Sub nudCantidadCompra_ValueChanged(sender As Object, e As EventArgs) Handles nudCantidadCompra.ValueChanged
        If Me.nudCantidadCompra.Value <= 0 Then
            MsgBox("Por favor, coloque una cantidad para comprar que sea mayor a cero", vbOKOnly + vbCritical, "Ventana Error")
        End If
    End Sub

    Private Sub dgvCarritoCompras_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCarritoCompras.CellContentClick

    End Sub

    Private Sub txbModiPrecioCompra_EnabledChanged(sender As Object, e As EventArgs) Handles txbModiPrecioCompra.EnabledChanged
        'MsgBox(e.GetType().ToString() + vbNewLine + sender.GetType().ToString())
    End Sub

    Private Sub txbModiPrecioCompra_LostFocus(sender As Object, e As EventArgs) Handles txbModiPrecioCompra.LostFocus
        'If Not General.controlarRealPositivo(Me.txbModiPrecioCompra.Text) Then
        'MsgBox("Por favor, coloque un valor positivo a la modificacion de precio", vbOKOnly + vbExclamation, "Ventana Error")
        'End If
    End Sub

    Private Function buscarIndiceProdEnCarrito(ByRef carrito As DataGridView, ByVal idProd As Integer, ByVal idProv As Integer) As Integer
        Try
            Dim indiceBuscado As Integer
            Dim elementosCarrito As Integer
            Dim elementoAnalizado As Integer

            elementosCarrito = carrito.Rows.Count
            elementoAnalizado = 0
            indiceBuscado = -1
            While elementoAnalizado < elementosCarrito
                If CInt(carrito.Rows(elementoAnalizado).Cells("codProd").Value) = idProd And CInt(carrito.Rows(elementoAnalizado).Cells("codProv").Value) = idProv Then
                    indiceBuscado = elementoAnalizado
                    elementoAnalizado = elementoAnalizado + 1
                End If
                elementoAnalizado = elementoAnalizado + 1
            End While
            Return indiceBuscado
        Catch ex As Exception
            MsgBox("Error al controlar si el producto existe en el carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return -1
        End Try
    End Function
    Private Sub modificarDatosCarrito(ByVal carrito As DataGridView)
        Try
            Dim indiceMod As Integer
            Dim idPro As Integer
            Dim idProv As Integer
            Dim descripcion As String
            Dim precioMod As Double
            Dim textoMod As String
            'Dim idMotivo As Integer
            'Dim motivo As String

            idPro = CInt(carrito.CurrentRow.Cells("codProd").Value)
            idProv = CInt(carrito.CurrentRow.Cells("codProv").Value)
            indiceMod = Me.buscarIndiceProdEnCarrito(carrito, idPro, idProv)
            descripcion = "Sin modificación de precio"

            If indiceMod > -1 Then
                If Me.ckbConfModPrecio.Checked Then
                    If Me.chkAuPrecioCompra.Checked Then
                        precioMod = CDbl(Me.txbModiPrecioCompra.Text)
                    Else
                        precioMod = CDbl(Me.txbModiPrecioCompra.Text) * (-1)
                    End If
                    descripcion = Me.txbDescModCompra.Text
                    textoMod = Me.cmbMotivoModCompra.Text

                Else
                    textoMod = "Sin Modificación"
                    precioMod = 0
                    descripcion = "Sin descripción de modificación"
                End If
                carrito.Rows(indiceMod).Cells("cantidad").Value = Me.nudCantidadCompra.Value

                carrito.Rows(indiceMod).Cells("cantidadMod").Value = precioMod

                carrito.Rows(indiceMod).Cells("idMotivo").Value = Me.cmbMotivoModCompra.SelectedValue
                carrito.Rows(indiceMod).Cells("descMod").Value = descripcion
                Me.btnAsignarCarritoCompra.Enabled = True
                Me.btnModCarrito.Enabled = False
                Me.btnCancelarModCarrito.Enabled = True
                Me.grbCarritoCompra.Enabled = True
            End If


        Catch ex As Exception
            MsgBox("Error al modificar los datos del producto. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub btnModCarrito_Click(sender As Object, e As EventArgs) Handles btnModCarrito.Click
        Dim mensaje As String
        Try
            mensaje = ""

            'controla que haya sido seleccionado un producto
            If Not General.controlarReales(Me.txbPrecioCompra.Text) Then
                mensaje = mensaje + "Por favor, coloque una cantidad numérica al precio de compra" + vbNewLine
            ElseIf CDbl(Me.txbPrecioCompra.Text) <= 0 Then

                mensaje = mensaje + "Por favor, coloque una modificación de precio mayor a cero" + vbNewLine
            End If

            'controlo si es que hay modificación de producto y, si las hay, verifico que el precio y la
            'descripción del motivo de la modificación sean colocadas
            If ckbConfModPrecio.Checked Then
                If Not General.controlarReales(Me.txbModiPrecioCompra.Text) Then
                    mensaje = mensaje + "Por favor, coloque una cantidad numérica al precio de compra" + vbNewLine
                ElseIf (CDbl(Me.txbModiPrecioCompra.Text) <= 0) Then
                    mensaje = mensaje + "Por favor, coloque una cantidad numérica al precio de compra mayor a cero" + vbNewLine
                End If
                If General.controlarCajaEnBlanco(Me.txbDescModCompra.Text) Then
                    mensaje = mensaje + "Por favor, si realizará aumentos o descuentos en el precio final, coloque la descripción del motivo." + vbNewLine
                End If
            End If

            'si el mensaje llegó aquí en blanco, entonces ingreso los datos al carrito
            'caso contrario, muesto el error.
            If String.IsNullOrWhiteSpace(mensaje) Then
                Me.modificarDatosCarrito(Me.dgvCarritoCompras)
                MsgBox("Datos modificado con éxito", vbOKOnly, "Ventana Éxito")
                Me.limpiarCompra()
            Else
                MsgBox("Se han producido los siguientes erroes: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
            End If
        Catch ex As Exception
            MsgBox("Error al asignar un producto al carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error de carga producto")
        End Try
    End Sub


    Private Sub txbModiPrecioCompra_TextChanged(sender As Object, e As EventArgs) Handles txbModiPrecioCompra.TextChanged

    End Sub

    Private Sub grbModPrecioCompra_EnabledChanged(sender As Object, e As EventArgs) Handles grbModPrecioCompra.EnabledChanged

    End Sub

    Private Sub grbModPrecioCompra_Enter(sender As Object, e As EventArgs) Handles grbModPrecioCompra.Enter

    End Sub

    Private Sub btnFiltrarProd_Click(sender As Object, e As EventArgs) Handles btnFiltrarProd.Click
        If Not General.controlarCajaEnBlanco(Trim(Me.txbFiltrarProdProv.Text)) Then
            Me.prepararGrillaProductos(Me.dgvProvProd, False, False)
            Me.listarProductosFiltroNombre(Me.dgvProvProd, Me.txbFiltrarProdProv.Text, False, False)
        Else
            MsgBox("Por favor no deje en blanco el criterio de filtro para el producto", vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub

    Private Sub btnFiltrarProveedor_Click(sender As Object, e As EventArgs) Handles btnFiltrarProveedor.Click
        If Not General.controlarCajaEnBlanco(Trim(Me.txbFiltrarProvProdProv.Text)) Then
            Me.prepararGrillaProveedor(Me.dgvProv_ProvProd, False)
            Me.listarProveedoresFiltradoNombre(Me.dgvProv_ProvProd, Me.txbFiltrarProvProdProv.Text)
        Else
            MsgBox("Por favor no deje en blanco el criterio de filtro para el proveedor", vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub
    Private Sub filtroPorProvProd(ByRef grilla As DataGridView, ByVal nombreProducto As String, ByVal nombreProveedor As String)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codProd As New DataGridViewTextBoxColumn()
        Dim codProv As New DataGridViewTextBoxColumn()
        Dim nombreProv As New DataGridViewTextBoxColumn()
        Dim nombreProd As New DataGridViewTextBoxColumn()
        Dim precioCompra As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim fechaAlta As New DataGridViewTextBoxColumn()

        Dim estado As String

        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProd)
            grilla.Columns.Add(codProv)
            grilla.Columns.Add(nombreProv)
            grilla.Columns.Add(precioCompra)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(fechaAlta)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            codProv.Name = "codProv"
            codProv.HeaderText = "Número Proveedor"
            nombreProd.Name = "nombreProducto"
            nombreProd.HeaderText = "Producto"
            nombreProv.Name = "nombreProveedor"
            nombreProv.HeaderText = "Proveedor"
            precioCompra.Name = "precioCompra"
            precioCompra.HeaderText = "Precio Compra"
            colEstado.Name = "estado"
            colEstado.HeaderText = "Estado"
            fechaAlta.Name = "fechaAlta"
            fechaAlta.HeaderText = "Fecha Alta"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True

            Dim provprod = BusProveedoresProductos.getDatos(nombreProducto, nombreProveedor)
            If Not IsNothing(provprod) Then
                For Each prod In provprod

                    If prod.estado Then
                        estado = "Activo"

                    Else
                        estado = "Inactivo"
                    End If
                    grilla.Rows.Add(prod.idProducto, prod.Productos.nombreProducto, prod.idProveedor, prod.Proveedores.nombreComercial, prod.precioCompra, estado, prod.fechaAlta)
                Next
            Else
                MsgBox("No existen datos que cumplan con los criterios de búsqueda", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If
        Catch ex As Exception
            MsgBox("Error al listar las relaciones productos/proveedores." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try
    End Sub
    Private Sub filtroPorProveedores(ByRef grilla As DataGridView, ByVal nombreProveedor As String)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codProd As New DataGridViewTextBoxColumn()
        Dim codProv As New DataGridViewTextBoxColumn()
        Dim nombreProv As New DataGridViewTextBoxColumn()
        Dim nombreProd As New DataGridViewTextBoxColumn()
        Dim precioCompra As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim fechaAlta As New DataGridViewTextBoxColumn()

        Dim estado As String

        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProd)
            grilla.Columns.Add(codProv)
            grilla.Columns.Add(nombreProv)
            grilla.Columns.Add(precioCompra)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(fechaAlta)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            codProv.Name = "codProv"
            codProv.HeaderText = "Número Proveedor"
            nombreProd.Name = "nombreProducto"
            nombreProd.HeaderText = "Producto"
            nombreProv.Name = "nombreProveedor"
            nombreProv.HeaderText = "Proveedor"
            precioCompra.Name = "precioCompra"
            precioCompra.HeaderText = "Precio Compra"
            colEstado.Name = "estado"
            colEstado.HeaderText = "Estado"
            fechaAlta.Name = "fechaAlta"
            fechaAlta.HeaderText = "Fecha Alta"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True

            Dim provprod = BusProveedoresProductos.getDatosPorProveedor(nombreProveedor)
            If Not IsNothing(provprod) Then
                For Each prod In provprod

                    If prod.estado Then
                        estado = "Activo"

                    Else
                        estado = "Inactivo"
                    End If
                    grilla.Rows.Add(prod.idProducto, prod.Productos.nombreProducto, prod.idProveedor, prod.Proveedores.nombreComercial, prod.precioCompra, estado, prod.fechaAlta)
                Next
            Else
                MsgBox("No existen datos que cumplan con los criterios de búsqueda", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If
        Catch ex As Exception
            MsgBox("Error al listar las relaciones productos/proveedores." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try
    End Sub
    Private Sub filtroPorProductos(ByRef grilla As DataGridView, ByVal nombreProducto As String)
        Dim boton As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codProd As New DataGridViewTextBoxColumn()
        Dim codProv As New DataGridViewTextBoxColumn()
        Dim nombreProv As New DataGridViewTextBoxColumn()
        Dim nombreProd As New DataGridViewTextBoxColumn()
        Dim precioCompra As New DataGridViewTextBoxColumn()
        Dim colEstado As New DataGridViewTextBoxColumn()
        Dim fechaAlta As New DataGridViewTextBoxColumn()

        Dim estado As String

        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProd)
            grilla.Columns.Add(codProv)
            grilla.Columns.Add(nombreProv)
            grilla.Columns.Add(precioCompra)
            grilla.Columns.Add(colEstado)
            grilla.Columns.Add(fechaAlta)
            grilla.Columns.Add(boton)

            boton.HeaderText = "Alta/Baja"
            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            codProv.Name = "codProv"
            codProv.HeaderText = "Número Proveedor"
            nombreProd.Name = "nombreProducto"
            nombreProd.HeaderText = "Producto"
            nombreProv.Name = "nombreProveedor"
            nombreProv.HeaderText = "Proveedor"
            precioCompra.Name = "precioCompra"
            precioCompra.HeaderText = "Precio Compra"
            colEstado.Name = "estado"
            colEstado.HeaderText = "Estado"
            fechaAlta.Name = "fechaAlta"
            fechaAlta.HeaderText = "Fecha Alta"
            boton.Text = "Modificar"
            boton.Name = "btnAltaBaja"
            boton.UseColumnTextForButtonValue = True

            Dim provprod = BusProveedoresProductos.getDatosPorProducto(nombreProducto)
            If Not IsNothing(provprod) Then
                For Each prod In provprod

                    If prod.estado Then
                        estado = "Activo"

                    Else
                        estado = "Inactivo"
                    End If
                    grilla.Rows.Add(prod.idProducto, prod.Productos.nombreProducto, prod.idProveedor, prod.Proveedores.nombreComercial, prod.precioCompra, estado, prod.fechaAlta)
                Next
            Else
                MsgBox("No existen datos que cumplan con los criterios de búsqueda", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If
        Catch ex As Exception
            MsgBox("Error al listar las relaciones productos/proveedores." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try
    End Sub
    Private Sub btnFiltrarProdProv_Click(sender As Object, e As EventArgs) Handles btnFiltrarProdProv.Click
        Dim nombreProd As String
        Dim nombreProv As String
        Dim buscarProd As Boolean
        Dim buscarProv As Boolean

        nombreProd = Trim(Me.txbProdFilCompra.Text)
        nombreProv = Trim(Me.txbProvFilCompra.Text)
        buscarProd = Not General.controlarCajaEnBlanco(nombreProd)

        buscarProv = Not General.controlarCajaEnBlanco(nombreProv)

        If buscarProv And buscarProd Then
            Me.filtroPorProvProd(Me.dgvProvProdCompras, nombreProd, nombreProv)
        ElseIf buscarProv Then
            Me.filtroPorProveedores(Me.dgvProvProdCompras, nombreProv)
        ElseIf buscarProd Then
            Me.filtroPorProductos(Me.dgvProvProdCompras, nombreProd)
        Else
            MsgBox("No deje ambas cajas en blanco. Escriba el nombre de un producto o proveedor para iniciar la búsqueda", vbOKOnly + vbExclamation, "Ventana Error")
        End If

    End Sub
    Private Sub filtrarProdCatSub(ByRef grilla As DataGridView, ByVal idCat As Integer, ByVal idSub As Integer, ByVal conDescripcion As Boolean, ByVal baja As Boolean)
        Try
            Dim estado As String
            Dim consulta = BusProductos.filtrarPorCatSub(idCat, idSub)
            If Not IsNothing(consulta) Then
                For Each prod In consulta

                    If prod.estado Then
                        estado = "Activo"

                    Else
                        estado = "Inactivo"
                    End If
                    If conDescripcion Then
                        grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.descripcionProducto, prod.nombreFoto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                    Else
                        grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                    End If

                    If estado = "Inactivo" Then
                        grilla.Rows.Item(grilla.Rows.Count - 1).DefaultCellStyle.BackColor = Color.Red
                    End If
                Next
            Else
                MsgBox("Error al filtrar los productos. No existe ningún producto con el criterio seleccionado", vbOKOnly + vbExclamation, "Ventana Advertencia")

            End If
        Catch ex As Exception
            MsgBox("Error al listar los productos.Es posible que aún no haya guardado datos de ningún producto." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try

    End Sub
    Private Sub filtrarProdCat(ByRef grilla As DataGridView, ByVal idCat As Integer, ByVal conDescripcion As Boolean, ByVal baja As Boolean)

        Try
            Dim estado As String
            Dim consulta = BusProductos.filtrarPorIdCat(idCat)
            If Not IsNothing(consulta) Then
                For Each prod In consulta

                    If prod.estado Then
                        estado = "Activo"

                    Else
                        estado = "Inactivo"
                    End If
                    If conDescripcion Then
                        grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.descripcionProducto, prod.nombreFoto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                    Else
                        grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre, prod.stock, prod.stockMinimo, prod.garantia, estado, prod.fecha)
                    End If

                    If estado = "Inactivo" Then
                        grilla.Rows.Item(grilla.Rows.Count - 1).DefaultCellStyle.BackColor = Color.Red
                    End If
                Next
            Else
                MsgBox("Error al filtrar los productos. No existe ningún producto con el criterio seleccionado", vbOKOnly + vbExclamation, "Ventana Advertencia")

            End If
        Catch ex As Exception
            MsgBox("Error al listar los productos.Es posible que aún no haya guardado datos de ningún producto." + vbNewLine + "Los errores presentados fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error")

        End Try

    End Sub

    Private Sub cmbCatModStockPrecio_BackColorChanged(sender As Object, e As EventArgs) Handles cmbCatModStockPrecio.BackColorChanged

    End Sub

    Private Sub cmbCatModStockPrecio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCatModStockPrecio.SelectedIndexChanged
        If IsNumeric(cmbCatModStockPrecio.SelectedValue) And Me.chkFilModStockPrecio.Checked Then
            Me.prepararGrillaProductos(Me.dgvProdModstockPrecio, False, False)
            Me.filtrarProdCat(Me.dgvProdModstockPrecio, Me.cmbCatModStockPrecio.SelectedValue, False, False)
        End If
    End Sub

    Private Sub cmbFiltroSubcat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbFiltroSubcat.SelectedIndexChanged
        If Me.chkFilModStockPrecio.Checked And IsNumeric(cmbFiltroSubcat.SelectedValue) Then
            Me.prepararGrillaProductos(Me.dgvProdModstockPrecio, False, False)
            Me.filtrarProdCatSub(Me.dgvProdModstockPrecio, Me.cmbCatModStockPrecio.SelectedValue, Me.cmbFiltroSubcat.SelectedValue, False, False)
        Else

        End If

    End Sub

    Private Sub chkFilModStockPrecio_CheckedChanged(sender As Object, e As EventArgs) Handles chkFilModStockPrecio.CheckedChanged
        If Not Me.chkFilModStockPrecio.Checked Then
            Me.prepararGrillaProductos(Me.dgvProdModstockPrecio, False, False)
            Me.listarProductos(Me.dgvProdModstockPrecio, False, False)
        End If
    End Sub

    Private Sub chkMostrarTodos_CheckedChanged(sender As Object, e As EventArgs) Handles chkMostrarTodos.CheckedChanged
        If chkMostrarTodos.Checked = True Then
            Me.habilitarModProd(False)
            Me.prepararGrillaProductos(Me.dgvListaProdMod, True, True)
            Me.listarProductos(Me.dgvListaProdMod, True, True)
        End If
    End Sub

    Private Sub txbNombreProdAdmNuevo_LostFocus(sender As Object, e As EventArgs) Handles txbNombreProdAdmNuevo.LostFocus

    End Sub


End Class