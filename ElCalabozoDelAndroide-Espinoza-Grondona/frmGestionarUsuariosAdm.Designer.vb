﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGestionarUsuariosAdm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tbcUsuarios = New System.Windows.Forms.TabControl()
        Me.tpgNuevoEmp = New System.Windows.Forms.TabPage()
        Me.grbFiltradoEmpAdm = New System.Windows.Forms.GroupBox()
        Me.btnFiltrarEmp = New System.Windows.Forms.Button()
        Me.txbApeNuevoEmpFil = New System.Windows.Forms.TextBox()
        Me.lblApeFiltradoEmp = New System.Windows.Forms.Label()
        Me.txbDniNuevoEmpFil = New System.Windows.Forms.TextBox()
        Me.lblDniFiltradoEmp = New System.Windows.Forms.Label()
        Me.grbEmpleados = New System.Windows.Forms.GroupBox()
        Me.dgvEmpleados = New System.Windows.Forms.DataGridView()
        Me.grbNuevoUsuario = New System.Windows.Forms.GroupBox()
        Me.btnAgregarEmpleado = New System.Windows.Forms.Button()
        Me.txbMailNuevoEmp = New System.Windows.Forms.TextBox()
        Me.txbTelNuevoEmp = New System.Windows.Forms.TextBox()
        Me.txbDniNuevoEmp = New System.Windows.Forms.TextBox()
        Me.txbNomNuevoEmp = New System.Windows.Forms.TextBox()
        Me.txbApeNuevoEmp = New System.Windows.Forms.TextBox()
        Me.lblMailEmpleado = New System.Windows.Forms.Label()
        Me.lblTelEmpleado = New System.Windows.Forms.Label()
        Me.lblDniEmpleado = New System.Windows.Forms.Label()
        Me.lblNombreEmpleado = New System.Windows.Forms.Label()
        Me.lblApellidoEmpleado = New System.Windows.Forms.Label()
        Me.tpgNuevoUsu = New System.Windows.Forms.TabPage()
        Me.grbFiltradoEmpNuevoAdm = New System.Windows.Forms.GroupBox()
        Me.btnFilEmpNuevoUsu = New System.Windows.Forms.Button()
        Me.txbApeEmpNuevoUsu = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txbDniEmpNuevoUsu = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.grbEmNuevoUsuAdm = New System.Windows.Forms.GroupBox()
        Me.dgvEmpNuevoAdm = New System.Windows.Forms.DataGridView()
        Me.grbNuevoEmpAdm = New System.Windows.Forms.GroupBox()
        Me.txbMailEmpUsu = New System.Windows.Forms.TextBox()
        Me.txbTelEmpUsu = New System.Windows.Forms.TextBox()
        Me.txbDniEmpUsu = New System.Windows.Forms.TextBox()
        Me.txbNomEmpUsu = New System.Windows.Forms.TextBox()
        Me.txbApeEmpUsu = New System.Windows.Forms.TextBox()
        Me.txbCodEmpNuevoUsu = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbTiposUsuario = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txbContraRepeUsu = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txbContraseniaUsu = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txbNomUsu = New System.Windows.Forms.TextBox()
        Me.lblNombreUsuario = New System.Windows.Forms.Label()
        Me.btnCrearUsuario = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tpgModUsu = New System.Windows.Forms.TabPage()
        Me.grbFiltradoModUsuAdm = New System.Windows.Forms.GroupBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.txbApeModUsu = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txbDniModUsu = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.grbModUsuAdm = New System.Windows.Forms.GroupBox()
        Me.txbNumUsuMod = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txbRepContraUsu = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cmbTipoModUsu = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txbContraModUsu = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txbNomModUsu = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnModContrasenia = New System.Windows.Forms.Button()
        Me.grbUsuModUsuAdm = New System.Windows.Forms.GroupBox()
        Me.dgvUsuariosActivosAdm = New System.Windows.Forms.DataGridView()
        Me.tpgModUsuActual = New System.Windows.Forms.TabPage()
        Me.grbCambioDatosUsu = New System.Windows.Forms.GroupBox()
        Me.grbNuevaContraEmpPropio = New System.Windows.Forms.GroupBox()
        Me.cmbTipoModDatosUsu = New System.Windows.Forms.ComboBox()
        Me.txbNuevaRepContra = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txbNuevaContra = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.btnModContraPropio = New System.Windows.Forms.Button()
        Me.btnControlarUsuario = New System.Windows.Forms.Button()
        Me.txbContraModUsuPropio = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txbNomModUsuPropio = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.grbDatosPersonalesMod = New System.Windows.Forms.GroupBox()
        Me.ckbConfirmarModDni = New System.Windows.Forms.CheckBox()
        Me.btnModDatosPropios = New System.Windows.Forms.Button()
        Me.txbMailModEmpPropio = New System.Windows.Forms.TextBox()
        Me.txbTelModEmpPropio = New System.Windows.Forms.TextBox()
        Me.txbDniModEmpPropio = New System.Windows.Forms.TextBox()
        Me.txbNomModEmpProp = New System.Windows.Forms.TextBox()
        Me.txbApeModEmp = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.tbcUsuarios.SuspendLayout()
        Me.tpgNuevoEmp.SuspendLayout()
        Me.grbFiltradoEmpAdm.SuspendLayout()
        Me.grbEmpleados.SuspendLayout()
        CType(Me.dgvEmpleados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbNuevoUsuario.SuspendLayout()
        Me.tpgNuevoUsu.SuspendLayout()
        Me.grbFiltradoEmpNuevoAdm.SuspendLayout()
        Me.grbEmNuevoUsuAdm.SuspendLayout()
        CType(Me.dgvEmpNuevoAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbNuevoEmpAdm.SuspendLayout()
        Me.tpgModUsu.SuspendLayout()
        Me.grbFiltradoModUsuAdm.SuspendLayout()
        Me.grbModUsuAdm.SuspendLayout()
        Me.grbUsuModUsuAdm.SuspendLayout()
        CType(Me.dgvUsuariosActivosAdm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgModUsuActual.SuspendLayout()
        Me.grbCambioDatosUsu.SuspendLayout()
        Me.grbNuevaContraEmpPropio.SuspendLayout()
        Me.grbDatosPersonalesMod.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbcUsuarios
        '
        Me.tbcUsuarios.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbcUsuarios.Controls.Add(Me.tpgNuevoEmp)
        Me.tbcUsuarios.Controls.Add(Me.tpgNuevoUsu)
        Me.tbcUsuarios.Controls.Add(Me.tpgModUsu)
        Me.tbcUsuarios.Controls.Add(Me.tpgModUsuActual)
        Me.tbcUsuarios.Location = New System.Drawing.Point(12, 12)
        Me.tbcUsuarios.Name = "tbcUsuarios"
        Me.tbcUsuarios.SelectedIndex = 0
        Me.tbcUsuarios.Size = New System.Drawing.Size(1290, 610)
        Me.tbcUsuarios.TabIndex = 0
        '
        'tpgNuevoEmp
        '
        Me.tpgNuevoEmp.AllowDrop = True
        Me.tpgNuevoEmp.Controls.Add(Me.grbFiltradoEmpAdm)
        Me.tpgNuevoEmp.Controls.Add(Me.grbEmpleados)
        Me.tpgNuevoEmp.Controls.Add(Me.grbNuevoUsuario)
        Me.tpgNuevoEmp.Location = New System.Drawing.Point(4, 22)
        Me.tpgNuevoEmp.Name = "tpgNuevoEmp"
        Me.tpgNuevoEmp.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgNuevoEmp.Size = New System.Drawing.Size(1282, 584)
        Me.tpgNuevoEmp.TabIndex = 0
        Me.tpgNuevoEmp.Text = " Empleado"
        Me.tpgNuevoEmp.UseVisualStyleBackColor = True
        '
        'grbFiltradoEmpAdm
        '
        Me.grbFiltradoEmpAdm.BackColor = System.Drawing.Color.RoyalBlue
        Me.grbFiltradoEmpAdm.Controls.Add(Me.btnFiltrarEmp)
        Me.grbFiltradoEmpAdm.Controls.Add(Me.txbApeNuevoEmpFil)
        Me.grbFiltradoEmpAdm.Controls.Add(Me.lblApeFiltradoEmp)
        Me.grbFiltradoEmpAdm.Controls.Add(Me.txbDniNuevoEmpFil)
        Me.grbFiltradoEmpAdm.Controls.Add(Me.lblDniFiltradoEmp)
        Me.grbFiltradoEmpAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltradoEmpAdm.Location = New System.Drawing.Point(676, 48)
        Me.grbFiltradoEmpAdm.Name = "grbFiltradoEmpAdm"
        Me.grbFiltradoEmpAdm.Size = New System.Drawing.Size(387, 141)
        Me.grbFiltradoEmpAdm.TabIndex = 12
        Me.grbFiltradoEmpAdm.TabStop = False
        Me.grbFiltradoEmpAdm.Text = "Opciones De Filtrado"
        '
        'btnFiltrarEmp
        '
        Me.btnFiltrarEmp.Location = New System.Drawing.Point(280, 38)
        Me.btnFiltrarEmp.Name = "btnFiltrarEmp"
        Me.btnFiltrarEmp.Size = New System.Drawing.Size(101, 66)
        Me.btnFiltrarEmp.TabIndex = 11
        Me.btnFiltrarEmp.Text = "Filtrar"
        Me.btnFiltrarEmp.UseVisualStyleBackColor = True
        '
        'txbApeNuevoEmpFil
        '
        Me.txbApeNuevoEmpFil.Location = New System.Drawing.Point(72, 78)
        Me.txbApeNuevoEmpFil.Name = "txbApeNuevoEmpFil"
        Me.txbApeNuevoEmpFil.Size = New System.Drawing.Size(202, 26)
        Me.txbApeNuevoEmpFil.TabIndex = 8
        '
        'lblApeFiltradoEmp
        '
        Me.lblApeFiltradoEmp.AutoSize = True
        Me.lblApeFiltradoEmp.Location = New System.Drawing.Point(6, 81)
        Me.lblApeFiltradoEmp.Name = "lblApeFiltradoEmp"
        Me.lblApeFiltradoEmp.Size = New System.Drawing.Size(65, 20)
        Me.lblApeFiltradoEmp.TabIndex = 7
        Me.lblApeFiltradoEmp.Text = "Apellido"
        '
        'txbDniNuevoEmpFil
        '
        Me.txbDniNuevoEmpFil.Location = New System.Drawing.Point(72, 38)
        Me.txbDniNuevoEmpFil.Name = "txbDniNuevoEmpFil"
        Me.txbDniNuevoEmpFil.Size = New System.Drawing.Size(202, 26)
        Me.txbDniNuevoEmpFil.TabIndex = 6
        '
        'lblDniFiltradoEmp
        '
        Me.lblDniFiltradoEmp.AutoSize = True
        Me.lblDniFiltradoEmp.Location = New System.Drawing.Point(6, 41)
        Me.lblDniFiltradoEmp.Name = "lblDniFiltradoEmp"
        Me.lblDniFiltradoEmp.Size = New System.Drawing.Size(33, 20)
        Me.lblDniFiltradoEmp.TabIndex = 0
        Me.lblDniFiltradoEmp.Text = "Dni"
        '
        'grbEmpleados
        '
        Me.grbEmpleados.Controls.Add(Me.dgvEmpleados)
        Me.grbEmpleados.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbEmpleados.Location = New System.Drawing.Point(6, 248)
        Me.grbEmpleados.Name = "grbEmpleados"
        Me.grbEmpleados.Size = New System.Drawing.Size(1260, 335)
        Me.grbEmpleados.TabIndex = 1
        Me.grbEmpleados.TabStop = False
        Me.grbEmpleados.Text = "Datos Empleados Registrados"
        '
        'dgvEmpleados
        '
        Me.dgvEmpleados.AllowUserToAddRows = False
        Me.dgvEmpleados.AllowUserToDeleteRows = False
        Me.dgvEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmpleados.Location = New System.Drawing.Point(6, 24)
        Me.dgvEmpleados.MultiSelect = False
        Me.dgvEmpleados.Name = "dgvEmpleados"
        Me.dgvEmpleados.ReadOnly = True
        Me.dgvEmpleados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpleados.Size = New System.Drawing.Size(1248, 305)
        Me.dgvEmpleados.TabIndex = 0
        '
        'grbNuevoUsuario
        '
        Me.grbNuevoUsuario.BackColor = System.Drawing.Color.Aquamarine
        Me.grbNuevoUsuario.Controls.Add(Me.btnAgregarEmpleado)
        Me.grbNuevoUsuario.Controls.Add(Me.txbMailNuevoEmp)
        Me.grbNuevoUsuario.Controls.Add(Me.txbTelNuevoEmp)
        Me.grbNuevoUsuario.Controls.Add(Me.txbDniNuevoEmp)
        Me.grbNuevoUsuario.Controls.Add(Me.txbNomNuevoEmp)
        Me.grbNuevoUsuario.Controls.Add(Me.txbApeNuevoEmp)
        Me.grbNuevoUsuario.Controls.Add(Me.lblMailEmpleado)
        Me.grbNuevoUsuario.Controls.Add(Me.lblTelEmpleado)
        Me.grbNuevoUsuario.Controls.Add(Me.lblDniEmpleado)
        Me.grbNuevoUsuario.Controls.Add(Me.lblNombreEmpleado)
        Me.grbNuevoUsuario.Controls.Add(Me.lblApellidoEmpleado)
        Me.grbNuevoUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbNuevoUsuario.Location = New System.Drawing.Point(6, 6)
        Me.grbNuevoUsuario.Name = "grbNuevoUsuario"
        Me.grbNuevoUsuario.Size = New System.Drawing.Size(471, 236)
        Me.grbNuevoUsuario.TabIndex = 0
        Me.grbNuevoUsuario.TabStop = False
        Me.grbNuevoUsuario.Text = "Datos Empleado Nuevo"
        '
        'btnAgregarEmpleado
        '
        Me.btnAgregarEmpleado.Location = New System.Drawing.Point(162, 189)
        Me.btnAgregarEmpleado.Name = "btnAgregarEmpleado"
        Me.btnAgregarEmpleado.Size = New System.Drawing.Size(145, 41)
        Me.btnAgregarEmpleado.TabIndex = 10
        Me.btnAgregarEmpleado.Text = "Agregar"
        Me.btnAgregarEmpleado.UseVisualStyleBackColor = True
        '
        'txbMailNuevoEmp
        '
        Me.txbMailNuevoEmp.Location = New System.Drawing.Point(121, 157)
        Me.txbMailNuevoEmp.Name = "txbMailNuevoEmp"
        Me.txbMailNuevoEmp.Size = New System.Drawing.Size(344, 26)
        Me.txbMailNuevoEmp.TabIndex = 9
        '
        'txbTelNuevoEmp
        '
        Me.txbTelNuevoEmp.Location = New System.Drawing.Point(121, 121)
        Me.txbTelNuevoEmp.Name = "txbTelNuevoEmp"
        Me.txbTelNuevoEmp.Size = New System.Drawing.Size(344, 26)
        Me.txbTelNuevoEmp.TabIndex = 8
        '
        'txbDniNuevoEmp
        '
        Me.txbDniNuevoEmp.Location = New System.Drawing.Point(121, 89)
        Me.txbDniNuevoEmp.Name = "txbDniNuevoEmp"
        Me.txbDniNuevoEmp.Size = New System.Drawing.Size(344, 26)
        Me.txbDniNuevoEmp.TabIndex = 7
        '
        'txbNomNuevoEmp
        '
        Me.txbNomNuevoEmp.Location = New System.Drawing.Point(121, 59)
        Me.txbNomNuevoEmp.Name = "txbNomNuevoEmp"
        Me.txbNomNuevoEmp.Size = New System.Drawing.Size(344, 26)
        Me.txbNomNuevoEmp.TabIndex = 6
        '
        'txbApeNuevoEmp
        '
        Me.txbApeNuevoEmp.Location = New System.Drawing.Point(121, 27)
        Me.txbApeNuevoEmp.Name = "txbApeNuevoEmp"
        Me.txbApeNuevoEmp.Size = New System.Drawing.Size(344, 26)
        Me.txbApeNuevoEmp.TabIndex = 5
        '
        'lblMailEmpleado
        '
        Me.lblMailEmpleado.AutoSize = True
        Me.lblMailEmpleado.Location = New System.Drawing.Point(6, 160)
        Me.lblMailEmpleado.Name = "lblMailEmpleado"
        Me.lblMailEmpleado.Size = New System.Drawing.Size(53, 20)
        Me.lblMailEmpleado.TabIndex = 4
        Me.lblMailEmpleado.Text = "E-Mail"
        '
        'lblTelEmpleado
        '
        Me.lblTelEmpleado.AutoSize = True
        Me.lblTelEmpleado.Location = New System.Drawing.Point(6, 123)
        Me.lblTelEmpleado.Name = "lblTelEmpleado"
        Me.lblTelEmpleado.Size = New System.Drawing.Size(71, 20)
        Me.lblTelEmpleado.TabIndex = 3
        Me.lblTelEmpleado.Text = "Teléfono"
        '
        'lblDniEmpleado
        '
        Me.lblDniEmpleado.AutoSize = True
        Me.lblDniEmpleado.Location = New System.Drawing.Point(6, 92)
        Me.lblDniEmpleado.Name = "lblDniEmpleado"
        Me.lblDniEmpleado.Size = New System.Drawing.Size(33, 20)
        Me.lblDniEmpleado.TabIndex = 2
        Me.lblDniEmpleado.Text = "Dni"
        '
        'lblNombreEmpleado
        '
        Me.lblNombreEmpleado.AutoSize = True
        Me.lblNombreEmpleado.Location = New System.Drawing.Point(6, 62)
        Me.lblNombreEmpleado.Name = "lblNombreEmpleado"
        Me.lblNombreEmpleado.Size = New System.Drawing.Size(65, 20)
        Me.lblNombreEmpleado.TabIndex = 1
        Me.lblNombreEmpleado.Text = "Nombre"
        '
        'lblApellidoEmpleado
        '
        Me.lblApellidoEmpleado.AutoSize = True
        Me.lblApellidoEmpleado.Location = New System.Drawing.Point(6, 30)
        Me.lblApellidoEmpleado.Name = "lblApellidoEmpleado"
        Me.lblApellidoEmpleado.Size = New System.Drawing.Size(65, 20)
        Me.lblApellidoEmpleado.TabIndex = 0
        Me.lblApellidoEmpleado.Text = "Apellido"
        '
        'tpgNuevoUsu
        '
        Me.tpgNuevoUsu.Controls.Add(Me.grbFiltradoEmpNuevoAdm)
        Me.tpgNuevoUsu.Controls.Add(Me.grbEmNuevoUsuAdm)
        Me.tpgNuevoUsu.Controls.Add(Me.grbNuevoEmpAdm)
        Me.tpgNuevoUsu.Location = New System.Drawing.Point(4, 22)
        Me.tpgNuevoUsu.Name = "tpgNuevoUsu"
        Me.tpgNuevoUsu.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgNuevoUsu.Size = New System.Drawing.Size(1282, 584)
        Me.tpgNuevoUsu.TabIndex = 1
        Me.tpgNuevoUsu.Text = "Nuevo Usuario"
        Me.tpgNuevoUsu.UseVisualStyleBackColor = True
        '
        'grbFiltradoEmpNuevoAdm
        '
        Me.grbFiltradoEmpNuevoAdm.BackColor = System.Drawing.Color.SpringGreen
        Me.grbFiltradoEmpNuevoAdm.Controls.Add(Me.btnFilEmpNuevoUsu)
        Me.grbFiltradoEmpNuevoAdm.Controls.Add(Me.txbApeEmpNuevoUsu)
        Me.grbFiltradoEmpNuevoAdm.Controls.Add(Me.Label13)
        Me.grbFiltradoEmpNuevoAdm.Controls.Add(Me.txbDniEmpNuevoUsu)
        Me.grbFiltradoEmpNuevoAdm.Controls.Add(Me.Label14)
        Me.grbFiltradoEmpNuevoAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltradoEmpNuevoAdm.Location = New System.Drawing.Point(474, 6)
        Me.grbFiltradoEmpNuevoAdm.Name = "grbFiltradoEmpNuevoAdm"
        Me.grbFiltradoEmpNuevoAdm.Size = New System.Drawing.Size(802, 123)
        Me.grbFiltradoEmpNuevoAdm.TabIndex = 13
        Me.grbFiltradoEmpNuevoAdm.TabStop = False
        Me.grbFiltradoEmpNuevoAdm.Text = "Opciones De Filtrado"
        '
        'btnFilEmpNuevoUsu
        '
        Me.btnFilEmpNuevoUsu.Location = New System.Drawing.Point(669, 47)
        Me.btnFilEmpNuevoUsu.Name = "btnFilEmpNuevoUsu"
        Me.btnFilEmpNuevoUsu.Size = New System.Drawing.Size(116, 31)
        Me.btnFilEmpNuevoUsu.TabIndex = 11
        Me.btnFilEmpNuevoUsu.Text = "Filtrar"
        Me.btnFilEmpNuevoUsu.UseVisualStyleBackColor = True
        '
        'txbApeEmpNuevoUsu
        '
        Me.txbApeEmpNuevoUsu.Location = New System.Drawing.Point(452, 49)
        Me.txbApeEmpNuevoUsu.Name = "txbApeEmpNuevoUsu"
        Me.txbApeEmpNuevoUsu.Size = New System.Drawing.Size(182, 26)
        Me.txbApeEmpNuevoUsu.TabIndex = 8
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(381, 52)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(65, 20)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "Apellido"
        '
        'txbDniEmpNuevoUsu
        '
        Me.txbDniEmpNuevoUsu.Location = New System.Drawing.Point(45, 49)
        Me.txbDniEmpNuevoUsu.Name = "txbDniEmpNuevoUsu"
        Me.txbDniEmpNuevoUsu.Size = New System.Drawing.Size(282, 26)
        Me.txbDniEmpNuevoUsu.TabIndex = 6
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 52)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(33, 20)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Dni"
        '
        'grbEmNuevoUsuAdm
        '
        Me.grbEmNuevoUsuAdm.Controls.Add(Me.dgvEmpNuevoAdm)
        Me.grbEmNuevoUsuAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbEmNuevoUsuAdm.Location = New System.Drawing.Point(474, 135)
        Me.grbEmNuevoUsuAdm.Name = "grbEmNuevoUsuAdm"
        Me.grbEmNuevoUsuAdm.Size = New System.Drawing.Size(802, 451)
        Me.grbEmNuevoUsuAdm.TabIndex = 2
        Me.grbEmNuevoUsuAdm.TabStop = False
        Me.grbEmNuevoUsuAdm.Text = "Datos Empleados Registrados"
        '
        'dgvEmpNuevoAdm
        '
        Me.dgvEmpNuevoAdm.AllowUserToAddRows = False
        Me.dgvEmpNuevoAdm.AllowUserToDeleteRows = False
        Me.dgvEmpNuevoAdm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmpNuevoAdm.Location = New System.Drawing.Point(5, 19)
        Me.dgvEmpNuevoAdm.Name = "dgvEmpNuevoAdm"
        Me.dgvEmpNuevoAdm.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpNuevoAdm.Size = New System.Drawing.Size(791, 348)
        Me.dgvEmpNuevoAdm.TabIndex = 0
        '
        'grbNuevoEmpAdm
        '
        Me.grbNuevoEmpAdm.BackColor = System.Drawing.Color.PaleGreen
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbMailEmpUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbTelEmpUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbDniEmpUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbNomEmpUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbApeEmpUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbCodEmpNuevoUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label2)
        Me.grbNuevoEmpAdm.Controls.Add(Me.cmbTiposUsuario)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label18)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbContraRepeUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label12)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbContraseniaUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label11)
        Me.grbNuevoEmpAdm.Controls.Add(Me.txbNomUsu)
        Me.grbNuevoEmpAdm.Controls.Add(Me.lblNombreUsuario)
        Me.grbNuevoEmpAdm.Controls.Add(Me.btnCrearUsuario)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label6)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label7)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label8)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label9)
        Me.grbNuevoEmpAdm.Controls.Add(Me.Label10)
        Me.grbNuevoEmpAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbNuevoEmpAdm.Location = New System.Drawing.Point(6, 6)
        Me.grbNuevoEmpAdm.Name = "grbNuevoEmpAdm"
        Me.grbNuevoEmpAdm.Size = New System.Drawing.Size(462, 577)
        Me.grbNuevoEmpAdm.TabIndex = 1
        Me.grbNuevoEmpAdm.TabStop = False
        Me.grbNuevoEmpAdm.Text = "Datos Empleado Nuevo"
        '
        'txbMailEmpUsu
        '
        Me.txbMailEmpUsu.Location = New System.Drawing.Point(153, 235)
        Me.txbMailEmpUsu.Name = "txbMailEmpUsu"
        Me.txbMailEmpUsu.ReadOnly = True
        Me.txbMailEmpUsu.Size = New System.Drawing.Size(303, 26)
        Me.txbMailEmpUsu.TabIndex = 31
        '
        'txbTelEmpUsu
        '
        Me.txbTelEmpUsu.Location = New System.Drawing.Point(153, 189)
        Me.txbTelEmpUsu.Name = "txbTelEmpUsu"
        Me.txbTelEmpUsu.ReadOnly = True
        Me.txbTelEmpUsu.Size = New System.Drawing.Size(303, 26)
        Me.txbTelEmpUsu.TabIndex = 30
        '
        'txbDniEmpUsu
        '
        Me.txbDniEmpUsu.Location = New System.Drawing.Point(153, 152)
        Me.txbDniEmpUsu.Name = "txbDniEmpUsu"
        Me.txbDniEmpUsu.ReadOnly = True
        Me.txbDniEmpUsu.Size = New System.Drawing.Size(303, 26)
        Me.txbDniEmpUsu.TabIndex = 29
        '
        'txbNomEmpUsu
        '
        Me.txbNomEmpUsu.Location = New System.Drawing.Point(153, 111)
        Me.txbNomEmpUsu.Name = "txbNomEmpUsu"
        Me.txbNomEmpUsu.ReadOnly = True
        Me.txbNomEmpUsu.Size = New System.Drawing.Size(303, 26)
        Me.txbNomEmpUsu.TabIndex = 28
        '
        'txbApeEmpUsu
        '
        Me.txbApeEmpUsu.Location = New System.Drawing.Point(153, 73)
        Me.txbApeEmpUsu.Name = "txbApeEmpUsu"
        Me.txbApeEmpUsu.ReadOnly = True
        Me.txbApeEmpUsu.Size = New System.Drawing.Size(303, 26)
        Me.txbApeEmpUsu.TabIndex = 27
        '
        'txbCodEmpNuevoUsu
        '
        Me.txbCodEmpNuevoUsu.Location = New System.Drawing.Point(153, 33)
        Me.txbCodEmpNuevoUsu.Name = "txbCodEmpNuevoUsu"
        Me.txbCodEmpNuevoUsu.ReadOnly = True
        Me.txbCodEmpNuevoUsu.Size = New System.Drawing.Size(100, 26)
        Me.txbCodEmpNuevoUsu.TabIndex = 26
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(141, 20)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Número Empleado"
        '
        'cmbTiposUsuario
        '
        Me.cmbTiposUsuario.FormattingEnabled = True
        Me.cmbTiposUsuario.Location = New System.Drawing.Point(182, 445)
        Me.cmbTiposUsuario.Name = "cmbTiposUsuario"
        Me.cmbTiposUsuario.Size = New System.Drawing.Size(274, 28)
        Me.cmbTiposUsuario.TabIndex = 18
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 448)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(98, 20)
        Me.Label18.TabIndex = 17
        Me.Label18.Text = "Tipo Usuario"
        '
        'txbContraRepeUsu
        '
        Me.txbContraRepeUsu.Location = New System.Drawing.Point(182, 397)
        Me.txbContraRepeUsu.Name = "txbContraRepeUsu"
        Me.txbContraRepeUsu.Size = New System.Drawing.Size(274, 26)
        Me.txbContraRepeUsu.TabIndex = 16
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 400)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(148, 20)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Repetir Contraseña"
        '
        'txbContraseniaUsu
        '
        Me.txbContraseniaUsu.Location = New System.Drawing.Point(182, 353)
        Me.txbContraseniaUsu.Name = "txbContraseniaUsu"
        Me.txbContraseniaUsu.Size = New System.Drawing.Size(274, 26)
        Me.txbContraseniaUsu.TabIndex = 14
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 356)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(92, 20)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Contraseña"
        '
        'txbNomUsu
        '
        Me.txbNomUsu.Location = New System.Drawing.Point(182, 304)
        Me.txbNomUsu.Name = "txbNomUsu"
        Me.txbNomUsu.Size = New System.Drawing.Size(274, 26)
        Me.txbNomUsu.TabIndex = 12
        '
        'lblNombreUsuario
        '
        Me.lblNombreUsuario.AutoSize = True
        Me.lblNombreUsuario.Location = New System.Drawing.Point(6, 307)
        Me.lblNombreUsuario.Name = "lblNombreUsuario"
        Me.lblNombreUsuario.Size = New System.Drawing.Size(124, 20)
        Me.lblNombreUsuario.TabIndex = 11
        Me.lblNombreUsuario.Text = "Nombre Usuario"
        '
        'btnCrearUsuario
        '
        Me.btnCrearUsuario.Location = New System.Drawing.Point(140, 489)
        Me.btnCrearUsuario.Name = "btnCrearUsuario"
        Me.btnCrearUsuario.Size = New System.Drawing.Size(143, 82)
        Me.btnCrearUsuario.TabIndex = 10
        Me.btnCrearUsuario.Text = "Crear Usuario"
        Me.btnCrearUsuario.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 241)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 20)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "E-Mail"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 192)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 20)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Teléfono"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 158)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(33, 20)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Dni"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 117)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 20)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Nombre"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 73)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 20)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Apellido"
        '
        'tpgModUsu
        '
        Me.tpgModUsu.BackColor = System.Drawing.Color.Silver
        Me.tpgModUsu.Controls.Add(Me.grbFiltradoModUsuAdm)
        Me.tpgModUsu.Controls.Add(Me.grbModUsuAdm)
        Me.tpgModUsu.Controls.Add(Me.grbUsuModUsuAdm)
        Me.tpgModUsu.Location = New System.Drawing.Point(4, 22)
        Me.tpgModUsu.Name = "tpgModUsu"
        Me.tpgModUsu.Size = New System.Drawing.Size(1282, 584)
        Me.tpgModUsu.TabIndex = 2
        Me.tpgModUsu.Text = "Modificar Usuario"
        '
        'grbFiltradoModUsuAdm
        '
        Me.grbFiltradoModUsuAdm.BackColor = System.Drawing.Color.Aqua
        Me.grbFiltradoModUsuAdm.Controls.Add(Me.Button9)
        Me.grbFiltradoModUsuAdm.Controls.Add(Me.txbApeModUsu)
        Me.grbFiltradoModUsuAdm.Controls.Add(Me.Label21)
        Me.grbFiltradoModUsuAdm.Controls.Add(Me.txbDniModUsu)
        Me.grbFiltradoModUsuAdm.Controls.Add(Me.Label22)
        Me.grbFiltradoModUsuAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbFiltradoModUsuAdm.Location = New System.Drawing.Point(3, 3)
        Me.grbFiltradoModUsuAdm.Name = "grbFiltradoModUsuAdm"
        Me.grbFiltradoModUsuAdm.Size = New System.Drawing.Size(454, 159)
        Me.grbFiltradoModUsuAdm.TabIndex = 14
        Me.grbFiltradoModUsuAdm.TabStop = False
        Me.grbFiltradoModUsuAdm.Text = "Opciones De Filtrado"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(347, 46)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(101, 76)
        Me.Button9.TabIndex = 11
        Me.Button9.Text = "Filtrar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'txbApeModUsu
        '
        Me.txbApeModUsu.Location = New System.Drawing.Point(103, 94)
        Me.txbApeModUsu.Name = "txbApeModUsu"
        Me.txbApeModUsu.Size = New System.Drawing.Size(238, 26)
        Me.txbApeModUsu.TabIndex = 8
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(6, 97)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(65, 20)
        Me.Label21.TabIndex = 7
        Me.Label21.Text = "Apellido"
        '
        'txbDniModUsu
        '
        Me.txbDniModUsu.Location = New System.Drawing.Point(103, 46)
        Me.txbDniModUsu.Name = "txbDniModUsu"
        Me.txbDniModUsu.Size = New System.Drawing.Size(238, 26)
        Me.txbDniModUsu.TabIndex = 6
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 49)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(33, 20)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Dni"
        '
        'grbModUsuAdm
        '
        Me.grbModUsuAdm.BackColor = System.Drawing.Color.LightGreen
        Me.grbModUsuAdm.Controls.Add(Me.txbNumUsuMod)
        Me.grbModUsuAdm.Controls.Add(Me.Label1)
        Me.grbModUsuAdm.Controls.Add(Me.txbRepContraUsu)
        Me.grbModUsuAdm.Controls.Add(Me.Label20)
        Me.grbModUsuAdm.Controls.Add(Me.cmbTipoModUsu)
        Me.grbModUsuAdm.Controls.Add(Me.Label19)
        Me.grbModUsuAdm.Controls.Add(Me.txbContraModUsu)
        Me.grbModUsuAdm.Controls.Add(Me.Label15)
        Me.grbModUsuAdm.Controls.Add(Me.txbNomModUsu)
        Me.grbModUsuAdm.Controls.Add(Me.Label17)
        Me.grbModUsuAdm.Controls.Add(Me.btnModContrasenia)
        Me.grbModUsuAdm.Enabled = False
        Me.grbModUsuAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbModUsuAdm.Location = New System.Drawing.Point(463, 3)
        Me.grbModUsuAdm.Name = "grbModUsuAdm"
        Me.grbModUsuAdm.Size = New System.Drawing.Size(813, 159)
        Me.grbModUsuAdm.TabIndex = 4
        Me.grbModUsuAdm.TabStop = False
        Me.grbModUsuAdm.Text = "Datos Usuario"
        '
        'txbNumUsuMod
        '
        Me.txbNumUsuMod.Location = New System.Drawing.Point(197, 25)
        Me.txbNumUsuMod.Name = "txbNumUsuMod"
        Me.txbNumUsuMod.ReadOnly = True
        Me.txbNumUsuMod.Size = New System.Drawing.Size(182, 26)
        Me.txbNumUsuMod.TabIndex = 24
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(124, 20)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Número Usuario"
        '
        'txbRepContraUsu
        '
        Me.txbRepContraUsu.Location = New System.Drawing.Point(565, 61)
        Me.txbRepContraUsu.Name = "txbRepContraUsu"
        Me.txbRepContraUsu.Size = New System.Drawing.Size(242, 26)
        Me.txbRepContraUsu.TabIndex = 22
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(411, 64)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(148, 20)
        Me.Label20.TabIndex = 21
        Me.Label20.Text = "Repetir Contraseña"
        '
        'cmbTipoModUsu
        '
        Me.cmbTipoModUsu.FormattingEnabled = True
        Me.cmbTipoModUsu.Location = New System.Drawing.Point(197, 94)
        Me.cmbTipoModUsu.Name = "cmbTipoModUsu"
        Me.cmbTipoModUsu.Size = New System.Drawing.Size(182, 28)
        Me.cmbTipoModUsu.TabIndex = 20
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 97)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(98, 20)
        Me.Label19.TabIndex = 19
        Me.Label19.Text = "Tipo Usuario"
        '
        'txbContraModUsu
        '
        Me.txbContraModUsu.Location = New System.Drawing.Point(197, 61)
        Me.txbContraModUsu.Name = "txbContraModUsu"
        Me.txbContraModUsu.Size = New System.Drawing.Size(182, 26)
        Me.txbContraModUsu.TabIndex = 16
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 64)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(173, 20)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "Contraseña Modificada"
        '
        'txbNomModUsu
        '
        Me.txbNomModUsu.Location = New System.Drawing.Point(565, 28)
        Me.txbNomModUsu.Name = "txbNomModUsu"
        Me.txbNomModUsu.ReadOnly = True
        Me.txbNomModUsu.Size = New System.Drawing.Size(242, 26)
        Me.txbNomModUsu.TabIndex = 12
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(411, 28)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(124, 20)
        Me.Label17.TabIndex = 11
        Me.Label17.Text = "Nombre Usuario"
        '
        'btnModContrasenia
        '
        Me.btnModContrasenia.Location = New System.Drawing.Point(583, 104)
        Me.btnModContrasenia.Name = "btnModContrasenia"
        Me.btnModContrasenia.Size = New System.Drawing.Size(224, 49)
        Me.btnModContrasenia.TabIndex = 10
        Me.btnModContrasenia.Text = "Modificar Contraseña" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnModContrasenia.UseVisualStyleBackColor = True
        '
        'grbUsuModUsuAdm
        '
        Me.grbUsuModUsuAdm.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.grbUsuModUsuAdm.Controls.Add(Me.dgvUsuariosActivosAdm)
        Me.grbUsuModUsuAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbUsuModUsuAdm.Location = New System.Drawing.Point(3, 168)
        Me.grbUsuModUsuAdm.Name = "grbUsuModUsuAdm"
        Me.grbUsuModUsuAdm.Size = New System.Drawing.Size(1273, 418)
        Me.grbUsuModUsuAdm.TabIndex = 3
        Me.grbUsuModUsuAdm.TabStop = False
        Me.grbUsuModUsuAdm.Text = "Datos Usuarios "
        '
        'dgvUsuariosActivosAdm
        '
        Me.dgvUsuariosActivosAdm.AllowUserToAddRows = False
        Me.dgvUsuariosActivosAdm.AllowUserToDeleteRows = False
        Me.dgvUsuariosActivosAdm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuariosActivosAdm.Location = New System.Drawing.Point(6, 19)
        Me.dgvUsuariosActivosAdm.MultiSelect = False
        Me.dgvUsuariosActivosAdm.Name = "dgvUsuariosActivosAdm"
        Me.dgvUsuariosActivosAdm.ReadOnly = True
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgvUsuariosActivosAdm.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvUsuariosActivosAdm.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUsuariosActivosAdm.Size = New System.Drawing.Size(1092, 393)
        Me.dgvUsuariosActivosAdm.TabIndex = 0
        '
        'tpgModUsuActual
        '
        Me.tpgModUsuActual.Controls.Add(Me.grbCambioDatosUsu)
        Me.tpgModUsuActual.Controls.Add(Me.grbDatosPersonalesMod)
        Me.tpgModUsuActual.Location = New System.Drawing.Point(4, 22)
        Me.tpgModUsuActual.Name = "tpgModUsuActual"
        Me.tpgModUsuActual.Size = New System.Drawing.Size(1282, 584)
        Me.tpgModUsuActual.TabIndex = 3
        Me.tpgModUsuActual.Text = "Modificar Datos Propios"
        Me.tpgModUsuActual.UseVisualStyleBackColor = True
        '
        'grbCambioDatosUsu
        '
        Me.grbCambioDatosUsu.BackColor = System.Drawing.Color.LightGreen
        Me.grbCambioDatosUsu.Controls.Add(Me.grbNuevaContraEmpPropio)
        Me.grbCambioDatosUsu.Controls.Add(Me.btnControlarUsuario)
        Me.grbCambioDatosUsu.Controls.Add(Me.txbContraModUsuPropio)
        Me.grbCambioDatosUsu.Controls.Add(Me.Label36)
        Me.grbCambioDatosUsu.Controls.Add(Me.txbNomModUsuPropio)
        Me.grbCambioDatosUsu.Controls.Add(Me.Label37)
        Me.grbCambioDatosUsu.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbCambioDatosUsu.Location = New System.Drawing.Point(645, 3)
        Me.grbCambioDatosUsu.Name = "grbCambioDatosUsu"
        Me.grbCambioDatosUsu.Size = New System.Drawing.Size(637, 362)
        Me.grbCambioDatosUsu.TabIndex = 13
        Me.grbCambioDatosUsu.TabStop = False
        Me.grbCambioDatosUsu.Text = "Datos Usuario"
        '
        'grbNuevaContraEmpPropio
        '
        Me.grbNuevaContraEmpPropio.Controls.Add(Me.cmbTipoModDatosUsu)
        Me.grbNuevaContraEmpPropio.Controls.Add(Me.txbNuevaRepContra)
        Me.grbNuevaContraEmpPropio.Controls.Add(Me.Label33)
        Me.grbNuevaContraEmpPropio.Controls.Add(Me.Label34)
        Me.grbNuevaContraEmpPropio.Controls.Add(Me.txbNuevaContra)
        Me.grbNuevaContraEmpPropio.Controls.Add(Me.Label35)
        Me.grbNuevaContraEmpPropio.Controls.Add(Me.btnModContraPropio)
        Me.grbNuevaContraEmpPropio.Location = New System.Drawing.Point(10, 116)
        Me.grbNuevaContraEmpPropio.Name = "grbNuevaContraEmpPropio"
        Me.grbNuevaContraEmpPropio.Size = New System.Drawing.Size(621, 240)
        Me.grbNuevaContraEmpPropio.TabIndex = 15
        Me.grbNuevaContraEmpPropio.TabStop = False
        Me.grbNuevaContraEmpPropio.Text = "Datos Nueva Contraseña"
        '
        'cmbTipoModDatosUsu
        '
        Me.cmbTipoModDatosUsu.Enabled = False
        Me.cmbTipoModDatosUsu.FormattingEnabled = True
        Me.cmbTipoModDatosUsu.Location = New System.Drawing.Point(224, 18)
        Me.cmbTipoModDatosUsu.Name = "cmbTipoModDatosUsu"
        Me.cmbTipoModDatosUsu.Size = New System.Drawing.Size(391, 28)
        Me.cmbTipoModDatosUsu.TabIndex = 30
        '
        'txbNuevaRepContra
        '
        Me.txbNuevaRepContra.Location = New System.Drawing.Point(224, 112)
        Me.txbNuevaRepContra.Name = "txbNuevaRepContra"
        Me.txbNuevaRepContra.Size = New System.Drawing.Size(391, 26)
        Me.txbNuevaRepContra.TabIndex = 29
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(6, 116)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(148, 20)
        Me.Label33.TabIndex = 28
        Me.Label33.Text = "Repetir Contraseña"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(6, 24)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(98, 20)
        Me.Label34.TabIndex = 27
        Me.Label34.Text = "Tipo Usuario"
        '
        'txbNuevaContra
        '
        Me.txbNuevaContra.Location = New System.Drawing.Point(224, 62)
        Me.txbNuevaContra.Name = "txbNuevaContra"
        Me.txbNuevaContra.Size = New System.Drawing.Size(391, 26)
        Me.txbNuevaContra.TabIndex = 26
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(6, 66)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(173, 20)
        Me.Label35.TabIndex = 25
        Me.Label35.Text = "Contraseña Modificada"
        '
        'btnModContraPropio
        '
        Me.btnModContraPropio.Location = New System.Drawing.Point(197, 166)
        Me.btnModContraPropio.Name = "btnModContraPropio"
        Me.btnModContraPropio.Size = New System.Drawing.Size(224, 59)
        Me.btnModContraPropio.TabIndex = 24
        Me.btnModContraPropio.Text = "Guardar Nueva Contraseña "
        Me.btnModContraPropio.UseVisualStyleBackColor = True
        '
        'btnControlarUsuario
        '
        Me.btnControlarUsuario.Location = New System.Drawing.Point(515, 71)
        Me.btnControlarUsuario.Name = "btnControlarUsuario"
        Me.btnControlarUsuario.Size = New System.Drawing.Size(116, 34)
        Me.btnControlarUsuario.TabIndex = 11
        Me.btnControlarUsuario.Text = "Controlar"
        Me.btnControlarUsuario.UseVisualStyleBackColor = True
        '
        'txbContraModUsuPropio
        '
        Me.txbContraModUsuPropio.Location = New System.Drawing.Point(240, 75)
        Me.txbContraModUsuPropio.Name = "txbContraModUsuPropio"
        Me.txbContraModUsuPropio.Size = New System.Drawing.Size(269, 26)
        Me.txbContraModUsuPropio.TabIndex = 14
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(6, 78)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(92, 20)
        Me.Label36.TabIndex = 13
        Me.Label36.Text = "Contraseña"
        '
        'txbNomModUsuPropio
        '
        Me.txbNomModUsuPropio.Location = New System.Drawing.Point(240, 27)
        Me.txbNomModUsuPropio.Name = "txbNomModUsuPropio"
        Me.txbNomModUsuPropio.Size = New System.Drawing.Size(391, 26)
        Me.txbNomModUsuPropio.TabIndex = 12
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(6, 30)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(124, 20)
        Me.Label37.TabIndex = 11
        Me.Label37.Text = "Nombre Usuario"
        '
        'grbDatosPersonalesMod
        '
        Me.grbDatosPersonalesMod.BackColor = System.Drawing.Color.Teal
        Me.grbDatosPersonalesMod.Controls.Add(Me.ckbConfirmarModDni)
        Me.grbDatosPersonalesMod.Controls.Add(Me.btnModDatosPropios)
        Me.grbDatosPersonalesMod.Controls.Add(Me.txbMailModEmpPropio)
        Me.grbDatosPersonalesMod.Controls.Add(Me.txbTelModEmpPropio)
        Me.grbDatosPersonalesMod.Controls.Add(Me.txbDniModEmpPropio)
        Me.grbDatosPersonalesMod.Controls.Add(Me.txbNomModEmpProp)
        Me.grbDatosPersonalesMod.Controls.Add(Me.txbApeModEmp)
        Me.grbDatosPersonalesMod.Controls.Add(Me.Label28)
        Me.grbDatosPersonalesMod.Controls.Add(Me.Label29)
        Me.grbDatosPersonalesMod.Controls.Add(Me.Label30)
        Me.grbDatosPersonalesMod.Controls.Add(Me.Label31)
        Me.grbDatosPersonalesMod.Controls.Add(Me.Label32)
        Me.grbDatosPersonalesMod.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosPersonalesMod.Location = New System.Drawing.Point(3, 3)
        Me.grbDatosPersonalesMod.Name = "grbDatosPersonalesMod"
        Me.grbDatosPersonalesMod.Size = New System.Drawing.Size(636, 362)
        Me.grbDatosPersonalesMod.TabIndex = 12
        Me.grbDatosPersonalesMod.TabStop = False
        Me.grbDatosPersonalesMod.Text = "Datos Personales"
        '
        'ckbConfirmarModDni
        '
        Me.ckbConfirmarModDni.AutoSize = True
        Me.ckbConfirmarModDni.Location = New System.Drawing.Point(421, 133)
        Me.ckbConfirmarModDni.Name = "ckbConfirmarModDni"
        Me.ckbConfirmarModDni.Size = New System.Drawing.Size(120, 24)
        Me.ckbConfirmarModDni.TabIndex = 11
        Me.ckbConfirmarModDni.Text = "Modificar Dni"
        Me.ckbConfirmarModDni.UseVisualStyleBackColor = True
        '
        'btnModDatosPropios
        '
        Me.btnModDatosPropios.Location = New System.Drawing.Point(243, 281)
        Me.btnModDatosPropios.Name = "btnModDatosPropios"
        Me.btnModDatosPropios.Size = New System.Drawing.Size(150, 59)
        Me.btnModDatosPropios.TabIndex = 10
        Me.btnModDatosPropios.Text = "Modificar Datos"
        Me.btnModDatosPropios.UseVisualStyleBackColor = True
        '
        'txbMailModEmpPropio
        '
        Me.txbMailModEmpPropio.Location = New System.Drawing.Point(203, 228)
        Me.txbMailModEmpPropio.Name = "txbMailModEmpPropio"
        Me.txbMailModEmpPropio.Size = New System.Drawing.Size(427, 26)
        Me.txbMailModEmpPropio.TabIndex = 9
        '
        'txbTelModEmpPropio
        '
        Me.txbTelModEmpPropio.Location = New System.Drawing.Point(203, 178)
        Me.txbTelModEmpPropio.Name = "txbTelModEmpPropio"
        Me.txbTelModEmpPropio.Size = New System.Drawing.Size(427, 26)
        Me.txbTelModEmpPropio.TabIndex = 8
        '
        'txbDniModEmpPropio
        '
        Me.txbDniModEmpPropio.Location = New System.Drawing.Point(203, 131)
        Me.txbDniModEmpPropio.Name = "txbDniModEmpPropio"
        Me.txbDniModEmpPropio.Size = New System.Drawing.Size(212, 26)
        Me.txbDniModEmpPropio.TabIndex = 7
        '
        'txbNomModEmpProp
        '
        Me.txbNomModEmpProp.Location = New System.Drawing.Point(203, 75)
        Me.txbNomModEmpProp.Name = "txbNomModEmpProp"
        Me.txbNomModEmpProp.Size = New System.Drawing.Size(427, 26)
        Me.txbNomModEmpProp.TabIndex = 6
        '
        'txbApeModEmp
        '
        Me.txbApeModEmp.Location = New System.Drawing.Point(203, 27)
        Me.txbApeModEmp.Name = "txbApeModEmp"
        Me.txbApeModEmp.Size = New System.Drawing.Size(427, 26)
        Me.txbApeModEmp.TabIndex = 5
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(6, 231)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(53, 20)
        Me.Label28.TabIndex = 4
        Me.Label28.Text = "E-Mail"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(0, 181)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(71, 20)
        Me.Label29.TabIndex = 3
        Me.Label29.Text = "Teléfono"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(6, 134)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(33, 20)
        Me.Label30.TabIndex = 2
        Me.Label30.Text = "Dni"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(6, 78)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(65, 20)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Nombre"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(6, 30)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(65, 20)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "Apellido"
        '
        'frmGestionarUsuariosAdm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1304, 646)
        Me.Controls.Add(Me.tbcUsuarios)
        Me.Name = "frmGestionarUsuariosAdm"
        Me.Text = "Gestion De Empleados y Usuarios"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcUsuarios.ResumeLayout(False)
        Me.tpgNuevoEmp.ResumeLayout(False)
        Me.grbFiltradoEmpAdm.ResumeLayout(False)
        Me.grbFiltradoEmpAdm.PerformLayout()
        Me.grbEmpleados.ResumeLayout(False)
        CType(Me.dgvEmpleados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbNuevoUsuario.ResumeLayout(False)
        Me.grbNuevoUsuario.PerformLayout()
        Me.tpgNuevoUsu.ResumeLayout(False)
        Me.grbFiltradoEmpNuevoAdm.ResumeLayout(False)
        Me.grbFiltradoEmpNuevoAdm.PerformLayout()
        Me.grbEmNuevoUsuAdm.ResumeLayout(False)
        CType(Me.dgvEmpNuevoAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbNuevoEmpAdm.ResumeLayout(False)
        Me.grbNuevoEmpAdm.PerformLayout()
        Me.tpgModUsu.ResumeLayout(False)
        Me.grbFiltradoModUsuAdm.ResumeLayout(False)
        Me.grbFiltradoModUsuAdm.PerformLayout()
        Me.grbModUsuAdm.ResumeLayout(False)
        Me.grbModUsuAdm.PerformLayout()
        Me.grbUsuModUsuAdm.ResumeLayout(False)
        CType(Me.dgvUsuariosActivosAdm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgModUsuActual.ResumeLayout(False)
        Me.grbCambioDatosUsu.ResumeLayout(False)
        Me.grbCambioDatosUsu.PerformLayout()
        Me.grbNuevaContraEmpPropio.ResumeLayout(False)
        Me.grbNuevaContraEmpPropio.PerformLayout()
        Me.grbDatosPersonalesMod.ResumeLayout(False)
        Me.grbDatosPersonalesMod.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbcUsuarios As System.Windows.Forms.TabControl
    Friend WithEvents tpgNuevoEmp As System.Windows.Forms.TabPage
    Friend WithEvents grbFiltradoEmpAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnFiltrarEmp As System.Windows.Forms.Button
    Friend WithEvents txbApeNuevoEmpFil As System.Windows.Forms.TextBox
    Friend WithEvents lblApeFiltradoEmp As System.Windows.Forms.Label
    Friend WithEvents txbDniNuevoEmpFil As System.Windows.Forms.TextBox
    Friend WithEvents lblDniFiltradoEmp As System.Windows.Forms.Label
    Friend WithEvents grbEmpleados As System.Windows.Forms.GroupBox
    Friend WithEvents dgvEmpleados As System.Windows.Forms.DataGridView
    Friend WithEvents grbNuevoUsuario As System.Windows.Forms.GroupBox
    Friend WithEvents btnAgregarEmpleado As System.Windows.Forms.Button
    Friend WithEvents txbMailNuevoEmp As System.Windows.Forms.TextBox
    Friend WithEvents txbTelNuevoEmp As System.Windows.Forms.TextBox
    Friend WithEvents txbDniNuevoEmp As System.Windows.Forms.TextBox
    Friend WithEvents txbNomNuevoEmp As System.Windows.Forms.TextBox
    Friend WithEvents txbApeNuevoEmp As System.Windows.Forms.TextBox
    Friend WithEvents lblMailEmpleado As System.Windows.Forms.Label
    Friend WithEvents lblTelEmpleado As System.Windows.Forms.Label
    Friend WithEvents lblDniEmpleado As System.Windows.Forms.Label
    Friend WithEvents lblNombreEmpleado As System.Windows.Forms.Label
    Friend WithEvents lblApellidoEmpleado As System.Windows.Forms.Label
    Friend WithEvents tpgNuevoUsu As System.Windows.Forms.TabPage
    Friend WithEvents tpgModUsu As System.Windows.Forms.TabPage
    Friend WithEvents grbFiltradoEmpNuevoAdm As System.Windows.Forms.GroupBox
    Friend WithEvents btnFilEmpNuevoUsu As System.Windows.Forms.Button
    Friend WithEvents txbApeEmpNuevoUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txbDniEmpNuevoUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents grbEmNuevoUsuAdm As System.Windows.Forms.GroupBox
    Friend WithEvents dgvEmpNuevoAdm As System.Windows.Forms.DataGridView
    Friend WithEvents grbNuevoEmpAdm As System.Windows.Forms.GroupBox
    Friend WithEvents txbContraRepeUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txbContraseniaUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txbNomUsu As System.Windows.Forms.TextBox
    Friend WithEvents lblNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents btnCrearUsuario As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbTiposUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents grbFiltradoModUsuAdm As System.Windows.Forms.GroupBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents txbApeModUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txbDniModUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents grbModUsuAdm As System.Windows.Forms.GroupBox
    Friend WithEvents txbRepContraUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoModUsu As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txbContraModUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnModContrasenia As System.Windows.Forms.Button
    Friend WithEvents grbUsuModUsuAdm As System.Windows.Forms.GroupBox
    Friend WithEvents dgvUsuariosActivosAdm As System.Windows.Forms.DataGridView
    Friend WithEvents tpgModUsuActual As System.Windows.Forms.TabPage
    Friend WithEvents grbCambioDatosUsu As System.Windows.Forms.GroupBox
    Friend WithEvents txbContraModUsuPropio As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txbNomModUsuPropio As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents grbDatosPersonalesMod As System.Windows.Forms.GroupBox
    Friend WithEvents btnModDatosPropios As System.Windows.Forms.Button
    Friend WithEvents txbMailModEmpPropio As System.Windows.Forms.TextBox
    Friend WithEvents txbTelModEmpPropio As System.Windows.Forms.TextBox
    Friend WithEvents txbDniModEmpPropio As System.Windows.Forms.TextBox
    Friend WithEvents txbNomModEmpProp As System.Windows.Forms.TextBox
    Friend WithEvents txbApeModEmp As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents btnControlarUsuario As System.Windows.Forms.Button
    Friend WithEvents txbNumUsuMod As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txbNomModUsu As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txbMailEmpUsu As System.Windows.Forms.TextBox
    Friend WithEvents txbTelEmpUsu As System.Windows.Forms.TextBox
    Friend WithEvents txbDniEmpUsu As System.Windows.Forms.TextBox
    Friend WithEvents txbNomEmpUsu As System.Windows.Forms.TextBox
    Friend WithEvents txbApeEmpUsu As System.Windows.Forms.TextBox
    Friend WithEvents txbCodEmpNuevoUsu As System.Windows.Forms.TextBox
    Friend WithEvents ckbConfirmarModDni As System.Windows.Forms.CheckBox
    Friend WithEvents grbNuevaContraEmpPropio As System.Windows.Forms.GroupBox
    Friend WithEvents cmbTipoModDatosUsu As System.Windows.Forms.ComboBox
    Friend WithEvents txbNuevaRepContra As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txbNuevaContra As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents btnModContraPropio As System.Windows.Forms.Button
End Class
