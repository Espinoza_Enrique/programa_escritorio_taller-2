﻿'Formulario para el inicio de sesión de los usuarios
Imports BusinessLayer
Imports Entities
'Imports Controladores
Public Class frmInicioSesion
    'Dim negocioUsu As New UsuarioBus
    ' Dim entidadUsu As New Usuario

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        'Dim perfil As Byte
        Dim nombreUsu As String
        Dim password As String
        Dim idLogueo As Integer
        Dim ventanaInicial As New frmVentanaInicialAdm

        ' Dim idUsuario As Byte
        'perfil = 1
        Try
            nombreUsu = Trim(Me.txbNombreUsuario.Text)
            password = Trim(Me.txbPassword.Text)
            If Not General.controlarCajaEnBlanco(nombreUsu) Or Not General.controlarCajaEnBlanco(password) Then
                Dim usuario As Entities.Usuarios
                usuario = BusUsuarios.controlarAcceso(nombreUsu, password)
                If Not IsNothing(usuario) Then
                    MsgBox("Acceso concedido", vbOKOnly, "Éxito en el acceso")
                    Me.limpiarCampos()
                    Me.Visible = False
                    BusLogueos.nuevoLogueo(usuario.idUsuario)
                    idLogueo = BusLogueos.idLogueoSesionActual()
                    Sesion.establecerValores(usuario.idUsuario, usuario.Empleados.nombreEmpleado, usuario.Empleados.apellidoEmpleado, usuario.Empleados.dni, usuario.idPerfil, usuario.idEmpleado, idLogueo, usuario.Perfiles.descripcionPerfil)
                    ventanaInicial.abrirVentana(Me)
                Else
                    MsgBox("Usuario y contraseña no válidos", vbOKOnly + vbExclamation, "Error De Ingreso De Datos")
                End If
            Else
                MsgBox("Por favor, coloque el nombre de usuario y contraseña", vbOKOnly + vbExclamation, "Error De Ingreso De Datos")
            End If
        Catch ex As Exception
            MsgBox("Error al iniciar sesión", vbOKOnly + vbCritical, "Error de inicio de sesión")
        End Try
    End Sub
    Public Sub abrirVentana(ByRef ventanaInicial As Form)
        Try
            ventanaInicial.Dispose()
            Me.Show()
        Catch ex As Exception
            MsgBox("Error al intentar abrir la ventana de inicio de sesión", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub
    Private Sub frmInicioSesion_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            If General.consultarSalida() Then
                End
            Else
                e.Cancel = True
            End If
        Catch ex As Exception
            MsgBox("Error al cerrar la ventana de inicio de sesión", vbOKOnly + vbCritical, "Error de cierre")
        End Try
    End Sub
    Private Sub limpiarCampos()
        Try
            Me.txbNombreUsuario.Text = ""
            Me.txbPassword.Text = ""
        Catch ex As Exception
            MsgBox("Error al limpiar campos de nombre usuario y contraseña", vbOKOnly + vbCritical, "Error de refresco")
        End Try
    End Sub
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub

    Private Sub frmInicioSesion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            
            'ModGlobal.setCerrarPorX(True)
        Catch ex As Exception
            MsgBox("Error al cargar la ventana de acceso", vbOKOnly + vbCritical, "Error de carga")
        End Try
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    
End Class
