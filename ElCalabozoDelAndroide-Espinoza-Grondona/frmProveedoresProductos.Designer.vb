﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProveedoresProductos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvProvProd = New System.Windows.Forms.DataGridView()
        Me.grbDatosProdProv = New System.Windows.Forms.GroupBox()
        Me.grbPrecioProdProv = New System.Windows.Forms.GroupBox()
        Me.btnModPrecioProdProv = New System.Windows.Forms.Button()
        Me.txbPrecioCompra = New System.Windows.Forms.TextBox()
        Me.chkModPrecioProdProv = New System.Windows.Forms.CheckBox()
        Me.lblTituloPrecio = New System.Windows.Forms.Label()
        Me.txbNombreProv = New System.Windows.Forms.TextBox()
        Me.txbNombreProd = New System.Windows.Forms.TextBox()
        Me.txbNroProv = New System.Windows.Forms.TextBox()
        Me.txbNroProd = New System.Windows.Forms.TextBox()
        Me.lblTituloProv = New System.Windows.Forms.Label()
        Me.lblTituloProd = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTituloNroProd = New System.Windows.Forms.Label()
        CType(Me.dgvProvProd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosProdProv.SuspendLayout()
        Me.grbPrecioProdProv.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvProvProd
        '
        Me.dgvProvProd.AllowUserToAddRows = False
        Me.dgvProvProd.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvProvProd.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProvProd.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvProvProd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvProvProd.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvProvProd.Location = New System.Drawing.Point(12, 262)
        Me.dgvProvProd.Name = "dgvProvProd"
        Me.dgvProvProd.ReadOnly = True
        Me.dgvProvProd.Size = New System.Drawing.Size(994, 178)
        Me.dgvProvProd.TabIndex = 0
        '
        'grbDatosProdProv
        '
        Me.grbDatosProdProv.BackColor = System.Drawing.Color.MediumSpringGreen
        Me.grbDatosProdProv.Controls.Add(Me.grbPrecioProdProv)
        Me.grbDatosProdProv.Controls.Add(Me.txbNombreProv)
        Me.grbDatosProdProv.Controls.Add(Me.txbNombreProd)
        Me.grbDatosProdProv.Controls.Add(Me.txbNroProv)
        Me.grbDatosProdProv.Controls.Add(Me.txbNroProd)
        Me.grbDatosProdProv.Controls.Add(Me.lblTituloProv)
        Me.grbDatosProdProv.Controls.Add(Me.lblTituloProd)
        Me.grbDatosProdProv.Controls.Add(Me.Label1)
        Me.grbDatosProdProv.Controls.Add(Me.lblTituloNroProd)
        Me.grbDatosProdProv.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProdProv.Location = New System.Drawing.Point(12, 35)
        Me.grbDatosProdProv.Name = "grbDatosProdProv"
        Me.grbDatosProdProv.Size = New System.Drawing.Size(989, 213)
        Me.grbDatosProdProv.TabIndex = 1
        Me.grbDatosProdProv.TabStop = False
        Me.grbDatosProdProv.Text = "Datos Producto/Proveedor"
        '
        'grbPrecioProdProv
        '
        Me.grbPrecioProdProv.BackColor = System.Drawing.Color.PaleTurquoise
        Me.grbPrecioProdProv.Controls.Add(Me.btnModPrecioProdProv)
        Me.grbPrecioProdProv.Controls.Add(Me.txbPrecioCompra)
        Me.grbPrecioProdProv.Controls.Add(Me.chkModPrecioProdProv)
        Me.grbPrecioProdProv.Controls.Add(Me.lblTituloPrecio)
        Me.grbPrecioProdProv.Location = New System.Drawing.Point(22, 115)
        Me.grbPrecioProdProv.Name = "grbPrecioProdProv"
        Me.grbPrecioProdProv.Size = New System.Drawing.Size(855, 79)
        Me.grbPrecioProdProv.TabIndex = 11
        Me.grbPrecioProdProv.TabStop = False
        Me.grbPrecioProdProv.Text = "Datos Precio"
        '
        'btnModPrecioProdProv
        '
        Me.btnModPrecioProdProv.Location = New System.Drawing.Point(382, 32)
        Me.btnModPrecioProdProv.Name = "btnModPrecioProdProv"
        Me.btnModPrecioProdProv.Size = New System.Drawing.Size(137, 29)
        Me.btnModPrecioProdProv.TabIndex = 11
        Me.btnModPrecioProdProv.Text = "Modificar Precio"
        Me.btnModPrecioProdProv.UseVisualStyleBackColor = True
        '
        'txbPrecioCompra
        '
        Me.txbPrecioCompra.Location = New System.Drawing.Point(136, 33)
        Me.txbPrecioCompra.Name = "txbPrecioCompra"
        Me.txbPrecioCompra.ReadOnly = True
        Me.txbPrecioCompra.Size = New System.Drawing.Size(201, 26)
        Me.txbPrecioCompra.TabIndex = 9
        '
        'chkModPrecioProdProv
        '
        Me.chkModPrecioProdProv.AutoSize = True
        Me.chkModPrecioProdProv.Location = New System.Drawing.Point(563, 35)
        Me.chkModPrecioProdProv.Name = "chkModPrecioProdProv"
        Me.chkModPrecioProdProv.Size = New System.Drawing.Size(158, 24)
        Me.chkModPrecioProdProv.TabIndex = 10
        Me.chkModPrecioProdProv.Text = "¿Modificar Precio?"
        Me.chkModPrecioProdProv.UseVisualStyleBackColor = True
        '
        'lblTituloPrecio
        '
        Me.lblTituloPrecio.AutoSize = True
        Me.lblTituloPrecio.Location = New System.Drawing.Point(6, 36)
        Me.lblTituloPrecio.Name = "lblTituloPrecio"
        Me.lblTituloPrecio.Size = New System.Drawing.Size(113, 20)
        Me.lblTituloPrecio.TabIndex = 8
        Me.lblTituloPrecio.Text = "Precio Compra"
        '
        'txbNombreProv
        '
        Me.txbNombreProv.Location = New System.Drawing.Point(461, 70)
        Me.txbNombreProv.Name = "txbNombreProv"
        Me.txbNombreProv.ReadOnly = True
        Me.txbNombreProv.Size = New System.Drawing.Size(492, 26)
        Me.txbNombreProv.TabIndex = 7
        '
        'txbNombreProd
        '
        Me.txbNombreProd.Location = New System.Drawing.Point(461, 24)
        Me.txbNombreProd.Name = "txbNombreProd"
        Me.txbNombreProd.ReadOnly = True
        Me.txbNombreProd.Size = New System.Drawing.Size(492, 26)
        Me.txbNombreProd.TabIndex = 6
        '
        'txbNroProv
        '
        Me.txbNroProv.Location = New System.Drawing.Point(166, 76)
        Me.txbNroProv.Name = "txbNroProv"
        Me.txbNroProv.ReadOnly = True
        Me.txbNroProv.Size = New System.Drawing.Size(94, 26)
        Me.txbNroProv.TabIndex = 5
        '
        'txbNroProd
        '
        Me.txbNroProd.Location = New System.Drawing.Point(166, 25)
        Me.txbNroProd.Name = "txbNroProd"
        Me.txbNroProd.ReadOnly = True
        Me.txbNroProd.Size = New System.Drawing.Size(100, 26)
        Me.txbNroProd.TabIndex = 4
        '
        'lblTituloProv
        '
        Me.lblTituloProv.AutoSize = True
        Me.lblTituloProv.Location = New System.Drawing.Point(374, 73)
        Me.lblTituloProv.Name = "lblTituloProv"
        Me.lblTituloProv.Size = New System.Drawing.Size(81, 20)
        Me.lblTituloProv.TabIndex = 3
        Me.lblTituloProv.Text = "Proveedor"
        '
        'lblTituloProd
        '
        Me.lblTituloProd.AutoSize = True
        Me.lblTituloProd.Location = New System.Drawing.Point(378, 24)
        Me.lblTituloProd.Name = "lblTituloProd"
        Me.lblTituloProd.Size = New System.Drawing.Size(77, 20)
        Me.lblTituloProd.TabIndex = 2
        Me.lblTituloProd.Text = " Producto"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(141, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Número Proveedor"
        '
        'lblTituloNroProd
        '
        Me.lblTituloNroProd.AutoSize = True
        Me.lblTituloNroProd.Location = New System.Drawing.Point(19, 27)
        Me.lblTituloNroProd.Name = "lblTituloNroProd"
        Me.lblTituloNroProd.Size = New System.Drawing.Size(133, 20)
        Me.lblTituloNroProd.TabIndex = 0
        Me.lblTituloNroProd.Text = "Número Producto"
        '
        'frmProveedoresProductos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(1078, 499)
        Me.Controls.Add(Me.grbDatosProdProv)
        Me.Controls.Add(Me.dgvProvProd)
        Me.Name = "frmProveedoresProductos"
        Me.Text = "Datos Proveedores/Productos"
        CType(Me.dgvProvProd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosProdProv.ResumeLayout(False)
        Me.grbDatosProdProv.PerformLayout()
        Me.grbPrecioProdProv.ResumeLayout(False)
        Me.grbPrecioProdProv.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvProvProd As System.Windows.Forms.DataGridView
    Friend WithEvents grbDatosProdProv As System.Windows.Forms.GroupBox
    Friend WithEvents grbPrecioProdProv As System.Windows.Forms.GroupBox
    Friend WithEvents btnModPrecioProdProv As System.Windows.Forms.Button
    Friend WithEvents txbPrecioCompra As System.Windows.Forms.TextBox
    Friend WithEvents chkModPrecioProdProv As System.Windows.Forms.CheckBox
    Friend WithEvents lblTituloPrecio As System.Windows.Forms.Label
    Friend WithEvents txbNombreProv As System.Windows.Forms.TextBox
    Friend WithEvents txbNombreProd As System.Windows.Forms.TextBox
    Friend WithEvents txbNroProv As System.Windows.Forms.TextBox
    Friend WithEvents txbNroProd As System.Windows.Forms.TextBox
    Friend WithEvents lblTituloProv As System.Windows.Forms.Label
    Friend WithEvents lblTituloProd As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTituloNroProd As System.Windows.Forms.Label
End Class
