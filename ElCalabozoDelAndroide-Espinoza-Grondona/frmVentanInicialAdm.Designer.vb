﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVentanaInicialAdm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVentanaInicialAdm))
        Me.PtbDevoluciones = New System.Windows.Forms.PictureBox()
        Me.PtbUsuarios = New System.Windows.Forms.PictureBox()
        Me.PtbProductos = New System.Windows.Forms.PictureBox()
        Me.PtbVentas = New System.Windows.Forms.PictureBox()
        Me.ptbInformes = New System.Windows.Forms.PictureBox()
        Me.lblDatosSesion = New System.Windows.Forms.Label()
        Me.menuVentanaPrincipal = New System.Windows.Forms.MenuStrip()
        Me.menuProductos = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemNuevoProd = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemModificarProd = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemModInfoProd = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemModStockPrecioProd = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemNuevoProv = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemProdProv = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemAsentarCompras = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuVentas = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemRealizarVenta = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuUsuarios = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemNuevoEmp = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemNuevoSub = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemEliminarModUsu = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemModDatosPropios = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuDevoluciones = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemNuevaDev = New System.Windows.Forms.ToolStripMenuItem()
        Me.itemAyuda = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemManual = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemAcercaDe = New System.Windows.Forms.ToolStripMenuItem()
        Me.itemSistema = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemRealizarRespaldo = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemCerrarSesion = New System.Windows.Forms.ToolStripMenuItem()
        Me.subItemSalir = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblNombreGrupo = New System.Windows.Forms.Label()
        CType(Me.PtbDevoluciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PtbVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ptbInformes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.menuVentanaPrincipal.SuspendLayout()
        Me.SuspendLayout()
        '
        'PtbDevoluciones
        '
        Me.PtbDevoluciones.BackgroundImage = CType(resources.GetObject("PtbDevoluciones.BackgroundImage"), System.Drawing.Image)
        Me.PtbDevoluciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PtbDevoluciones.Location = New System.Drawing.Point(691, 355)
        Me.PtbDevoluciones.Name = "PtbDevoluciones"
        Me.PtbDevoluciones.Size = New System.Drawing.Size(325, 319)
        Me.PtbDevoluciones.TabIndex = 8
        Me.PtbDevoluciones.TabStop = False
        '
        'PtbUsuarios
        '
        Me.PtbUsuarios.BackgroundImage = CType(resources.GetObject("PtbUsuarios.BackgroundImage"), System.Drawing.Image)
        Me.PtbUsuarios.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PtbUsuarios.Location = New System.Drawing.Point(360, 355)
        Me.PtbUsuarios.Name = "PtbUsuarios"
        Me.PtbUsuarios.Size = New System.Drawing.Size(325, 319)
        Me.PtbUsuarios.TabIndex = 7
        Me.PtbUsuarios.TabStop = False
        '
        'PtbProductos
        '
        Me.PtbProductos.BackgroundImage = CType(resources.GetObject("PtbProductos.BackgroundImage"), System.Drawing.Image)
        Me.PtbProductos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PtbProductos.Location = New System.Drawing.Point(1022, 30)
        Me.PtbProductos.Name = "PtbProductos"
        Me.PtbProductos.Size = New System.Drawing.Size(325, 319)
        Me.PtbProductos.TabIndex = 6
        Me.PtbProductos.TabStop = False
        '
        'PtbVentas
        '
        Me.PtbVentas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PtbVentas.Image = CType(resources.GetObject("PtbVentas.Image"), System.Drawing.Image)
        Me.PtbVentas.Location = New System.Drawing.Point(691, 30)
        Me.PtbVentas.Name = "PtbVentas"
        Me.PtbVentas.Size = New System.Drawing.Size(325, 319)
        Me.PtbVentas.TabIndex = 5
        Me.PtbVentas.TabStop = False
        '
        'ptbInformes
        '
        Me.ptbInformes.BackgroundImage = CType(resources.GetObject("ptbInformes.BackgroundImage"), System.Drawing.Image)
        Me.ptbInformes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ptbInformes.Location = New System.Drawing.Point(1022, 355)
        Me.ptbInformes.Name = "ptbInformes"
        Me.ptbInformes.Size = New System.Drawing.Size(325, 319)
        Me.ptbInformes.TabIndex = 9
        Me.ptbInformes.TabStop = False
        '
        'lblDatosSesion
        '
        Me.lblDatosSesion.AutoSize = True
        Me.lblDatosSesion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatosSesion.Location = New System.Drawing.Point(12, 70)
        Me.lblDatosSesion.Name = "lblDatosSesion"
        Me.lblDatosSesion.Size = New System.Drawing.Size(423, 24)
        Me.lblDatosSesion.TabIndex = 10
        Me.lblDatosSesion.Text = "Datos Del Usuario Cuya Sesión Está Iniciada"
        '
        'menuVentanaPrincipal
        '
        Me.menuVentanaPrincipal.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuProductos, Me.menuVentas, Me.menuUsuarios, Me.menuDevoluciones, Me.itemAyuda, Me.itemSistema})
        Me.menuVentanaPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.menuVentanaPrincipal.Name = "menuVentanaPrincipal"
        Me.menuVentanaPrincipal.Size = New System.Drawing.Size(1370, 24)
        Me.menuVentanaPrincipal.TabIndex = 12
        Me.menuVentanaPrincipal.Text = "Productos"
        '
        'menuProductos
        '
        Me.menuProductos.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemNuevoProd, Me.subItemModificarProd, Me.ProveedoresToolStripMenuItem, Me.subItemAsentarCompras})
        Me.menuProductos.Name = "menuProductos"
        Me.menuProductos.Size = New System.Drawing.Size(73, 20)
        Me.menuProductos.Text = "Productos"
        '
        'subItemNuevoProd
        '
        Me.subItemNuevoProd.Name = "subItemNuevoProd"
        Me.subItemNuevoProd.Size = New System.Drawing.Size(177, 22)
        Me.subItemNuevoProd.Text = "Nuevo Producto"
        '
        'subItemModificarProd
        '
        Me.subItemModificarProd.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemModInfoProd, Me.subItemModStockPrecioProd})
        Me.subItemModificarProd.Name = "subItemModificarProd"
        Me.subItemModificarProd.Size = New System.Drawing.Size(177, 22)
        Me.subItemModificarProd.Text = "Modificar Producto"
        '
        'subItemModInfoProd
        '
        Me.subItemModInfoProd.Name = "subItemModInfoProd"
        Me.subItemModInfoProd.Size = New System.Drawing.Size(195, 22)
        Me.subItemModInfoProd.Text = "Modificar Información"
        '
        'subItemModStockPrecioProd
        '
        Me.subItemModStockPrecioProd.Name = "subItemModStockPrecioProd"
        Me.subItemModStockPrecioProd.Size = New System.Drawing.Size(195, 22)
        Me.subItemModStockPrecioProd.Text = "Modificar Stock/Precio"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemNuevoProv, Me.subItemProdProv})
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores"
        '
        'subItemNuevoProv
        '
        Me.subItemNuevoProv.Name = "subItemNuevoProv"
        Me.subItemNuevoProv.Size = New System.Drawing.Size(198, 22)
        Me.subItemNuevoProv.Text = "Agregar Proveedor"
        '
        'subItemProdProv
        '
        Me.subItemProdProv.Name = "subItemProdProv"
        Me.subItemProdProv.Size = New System.Drawing.Size(198, 22)
        Me.subItemProdProv.Text = "Productos/Proveedores"
        '
        'subItemAsentarCompras
        '
        Me.subItemAsentarCompras.Name = "subItemAsentarCompras"
        Me.subItemAsentarCompras.Size = New System.Drawing.Size(177, 22)
        Me.subItemAsentarCompras.Text = "Asentar Compras"
        '
        'menuVentas
        '
        Me.menuVentas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemRealizarVenta})
        Me.menuVentas.Name = "menuVentas"
        Me.menuVentas.Size = New System.Drawing.Size(54, 20)
        Me.menuVentas.Text = "Ventas"
        '
        'subItemRealizarVenta
        '
        Me.subItemRealizarVenta.Name = "subItemRealizarVenta"
        Me.subItemRealizarVenta.Size = New System.Drawing.Size(152, 22)
        Me.subItemRealizarVenta.Text = "Realizar Venta"
        '
        'menuUsuarios
        '
        Me.menuUsuarios.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemNuevoEmp, Me.subItemNuevoSub, Me.subItemEliminarModUsu, Me.subItemModDatosPropios})
        Me.menuUsuarios.Name = "menuUsuarios"
        Me.menuUsuarios.Size = New System.Drawing.Size(64, 20)
        Me.menuUsuarios.Text = "Usuarios"
        '
        'subItemNuevoEmp
        '
        Me.subItemNuevoEmp.Name = "subItemNuevoEmp"
        Me.subItemNuevoEmp.Size = New System.Drawing.Size(216, 22)
        Me.subItemNuevoEmp.Text = "Agregar Nuevo Empleado"
        '
        'subItemNuevoSub
        '
        Me.subItemNuevoSub.Name = "subItemNuevoSub"
        Me.subItemNuevoSub.Size = New System.Drawing.Size(216, 22)
        Me.subItemNuevoSub.Text = "Nuevo Usuario"
        '
        'subItemEliminarModUsu
        '
        Me.subItemEliminarModUsu.Name = "subItemEliminarModUsu"
        Me.subItemEliminarModUsu.Size = New System.Drawing.Size(216, 22)
        Me.subItemEliminarModUsu.Text = "Modificar/Eliminar Usuario"
        '
        'subItemModDatosPropios
        '
        Me.subItemModDatosPropios.Name = "subItemModDatosPropios"
        Me.subItemModDatosPropios.Size = New System.Drawing.Size(216, 22)
        Me.subItemModDatosPropios.Text = "Modificar Datos Propios"
        '
        'menuDevoluciones
        '
        Me.menuDevoluciones.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemNuevaDev})
        Me.menuDevoluciones.Name = "menuDevoluciones"
        Me.menuDevoluciones.Size = New System.Drawing.Size(90, 20)
        Me.menuDevoluciones.Text = "Devoluciones"
        '
        'subItemNuevaDev
        '
        Me.subItemNuevaDev.Name = "subItemNuevaDev"
        Me.subItemNuevaDev.Size = New System.Drawing.Size(176, 22)
        Me.subItemNuevaDev.Text = "Nueva Devolución"
        '
        'itemAyuda
        '
        Me.itemAyuda.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemManual, Me.subItemAcercaDe})
        Me.itemAyuda.Name = "itemAyuda"
        Me.itemAyuda.Size = New System.Drawing.Size(53, 20)
        Me.itemAyuda.Text = "Ayuda"
        '
        'subItemManual
        '
        Me.subItemManual.Name = "subItemManual"
        Me.subItemManual.Size = New System.Drawing.Size(174, 22)
        Me.subItemManual.Text = "Manual De Usuario"
        '
        'subItemAcercaDe
        '
        Me.subItemAcercaDe.Name = "subItemAcercaDe"
        Me.subItemAcercaDe.Size = New System.Drawing.Size(174, 22)
        Me.subItemAcercaDe.Text = "Acerca De"
        '
        'itemSistema
        '
        Me.itemSistema.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.subItemRealizarRespaldo, Me.subItemCerrarSesion, Me.subItemSalir})
        Me.itemSistema.Name = "itemSistema"
        Me.itemSistema.Size = New System.Drawing.Size(60, 20)
        Me.itemSistema.Text = "Sistema"
        '
        'subItemRealizarRespaldo
        '
        Me.subItemRealizarRespaldo.Name = "subItemRealizarRespaldo"
        Me.subItemRealizarRespaldo.Size = New System.Drawing.Size(165, 22)
        Me.subItemRealizarRespaldo.Text = "Realizar Respaldo"
        '
        'subItemCerrarSesion
        '
        Me.subItemCerrarSesion.Name = "subItemCerrarSesion"
        Me.subItemCerrarSesion.Size = New System.Drawing.Size(165, 22)
        Me.subItemCerrarSesion.Text = "Cerrar Sesión"
        '
        'subItemSalir
        '
        Me.subItemSalir.Name = "subItemSalir"
        Me.subItemSalir.Size = New System.Drawing.Size(165, 22)
        Me.subItemSalir.Text = "Salir"
        '
        'lblNombreGrupo
        '
        Me.lblNombreGrupo.AutoSize = True
        Me.lblNombreGrupo.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreGrupo.ForeColor = System.Drawing.Color.Red
        Me.lblNombreGrupo.Location = New System.Drawing.Point(12, 30)
        Me.lblNombreGrupo.Name = "lblNombreGrupo"
        Me.lblNombreGrupo.Size = New System.Drawing.Size(324, 18)
        Me.lblNombreGrupo.TabIndex = 13
        Me.lblNombreGrupo.Text = "Grupo 7: Espinoza Enrique-Grondona Matías"
        '
        'frmVentanaInicialAdm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightGray
        Me.ClientSize = New System.Drawing.Size(1370, 749)
        Me.Controls.Add(Me.lblNombreGrupo)
        Me.Controls.Add(Me.lblDatosSesion)
        Me.Controls.Add(Me.ptbInformes)
        Me.Controls.Add(Me.PtbDevoluciones)
        Me.Controls.Add(Me.PtbUsuarios)
        Me.Controls.Add(Me.PtbProductos)
        Me.Controls.Add(Me.PtbVentas)
        Me.Controls.Add(Me.menuVentanaPrincipal)
        Me.MainMenuStrip = Me.menuVentanaPrincipal
        Me.Name = "frmVentanaInicialAdm"
        Me.Text = "Ventana Inicial Administrador"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PtbDevoluciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PtbVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ptbInformes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.menuVentanaPrincipal.ResumeLayout(False)
        Me.menuVentanaPrincipal.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PtbDevoluciones As System.Windows.Forms.PictureBox
    Friend WithEvents PtbUsuarios As System.Windows.Forms.PictureBox
    Friend WithEvents PtbProductos As System.Windows.Forms.PictureBox
    Friend WithEvents PtbVentas As System.Windows.Forms.PictureBox
    Friend WithEvents ptbInformes As System.Windows.Forms.PictureBox
    Friend WithEvents lblDatosSesion As System.Windows.Forms.Label
    Friend WithEvents menuVentanaPrincipal As System.Windows.Forms.MenuStrip
    Friend WithEvents menuProductos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemNuevoProd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemModificarProd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuVentas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuUsuarios As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemNuevoEmp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemNuevoSub As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemEliminarModUsu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuDevoluciones As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemNuevaDev As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents itemAyuda As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemManual As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemAcercaDe As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents itemSistema As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemCerrarSesion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemSalir As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemRealizarRespaldo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemModDatosPropios As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemRealizarVenta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblNombreGrupo As System.Windows.Forms.Label
    Friend WithEvents ProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemNuevoProv As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemProdProv As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemAsentarCompras As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemModInfoProd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents subItemModStockPrecioProd As System.Windows.Forms.ToolStripMenuItem
End Class
