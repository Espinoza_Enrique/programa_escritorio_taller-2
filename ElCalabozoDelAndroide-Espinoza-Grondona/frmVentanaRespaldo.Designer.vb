﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVentanaRespaldo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVentanaRespaldo))
        Me.ofdRespaldo = New System.Windows.Forms.OpenFileDialog()
        Me.lblInformes = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnRespaldoBD = New System.Windows.Forms.Button()
        Me.btnRecuperacionBD = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ofdRespaldo
        '
        Me.ofdRespaldo.FileName = "OpenFileDialog1"
        '
        'lblInformes
        '
        Me.lblInformes.AutoSize = True
        Me.lblInformes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInformes.Location = New System.Drawing.Point(196, 9)
        Me.lblInformes.Name = "lblInformes"
        Me.lblInformes.Size = New System.Drawing.Size(167, 20)
        Me.lblInformes.TabIndex = 6
        Me.lblInformes.Text = "Ventana De Respaldo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(93, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 20)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Respaldo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(384, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 20)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Recuperación"
        '
        'btnRespaldoBD
        '
        Me.btnRespaldoBD.Image = CType(resources.GetObject("btnRespaldoBD.Image"), System.Drawing.Image)
        Me.btnRespaldoBD.Location = New System.Drawing.Point(61, 112)
        Me.btnRespaldoBD.Name = "btnRespaldoBD"
        Me.btnRespaldoBD.Size = New System.Drawing.Size(200, 200)
        Me.btnRespaldoBD.TabIndex = 9
        Me.btnRespaldoBD.UseVisualStyleBackColor = True
        '
        'btnRecuperacionBD
        '
        Me.btnRecuperacionBD.Image = CType(resources.GetObject("btnRecuperacionBD.Image"), System.Drawing.Image)
        Me.btnRecuperacionBD.Location = New System.Drawing.Point(344, 107)
        Me.btnRecuperacionBD.Name = "btnRecuperacionBD"
        Me.btnRecuperacionBD.Size = New System.Drawing.Size(200, 200)
        Me.btnRecuperacionBD.TabIndex = 10
        Me.btnRecuperacionBD.UseVisualStyleBackColor = True
        '
        'frmVentanaRespaldo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkGray
        Me.ClientSize = New System.Drawing.Size(623, 324)
        Me.Controls.Add(Me.btnRecuperacionBD)
        Me.Controls.Add(Me.btnRespaldoBD)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblInformes)
        Me.Name = "frmVentanaRespaldo"
        Me.Text = "Respaldo Base De Datos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ofdRespaldo As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblInformes As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnRespaldoBD As System.Windows.Forms.Button
    Friend WithEvents btnRecuperacionBD As System.Windows.Forms.Button
End Class
