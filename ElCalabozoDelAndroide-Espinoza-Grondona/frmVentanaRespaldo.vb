﻿Imports BusinessLayer
Imports System.Data.SqlClient

Public Class frmVentanaRespaldo

    Public Sub abrirVentana()
        Me.ShowDialog()
    End Sub
    Private Sub frmVentanaRespaldo_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Me.Dispose()
    End Sub
    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub
    Private Sub frmVentanaRespaldo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub buscarArchivoRespaldo(ByRef dialogo As FileDialog)

        Try
            MsgBox("Busque el archivo que desea respaldar con extensión .bak. Una vez elegido el archivo, espere unos segundos hasta que la transacción se complete", vbOKOnly, "Ventana Información")
            Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)
            Dim ruta = cadena + "\Respaldos\"
            dialogo.Filter = "Archivo de Respaldo(*.bak)|*.bak|Todos los Archivos(*.*)|*.*"
            dialogo.InitialDirectory = ruta
            If dialogo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Me.restaurarBD(dialogo.FileName)
            End If
        Catch ex As Exception
            MsgBox("Error al restrablecer la base de datos. Los errores producidos fueron: " + vbNewLine + ex.Message, vbCritical + vbOKOnly, "Error Restaurar La Base De Datos")
        End Try
    End Sub
    Private Function restaurarBD(ByVal ruta As String) As Boolean
        'Dim cn As New SqlConnection("data source=.\SQLEXPRESS;initial catalog=master;integrated security=True")
        'Try

        'cn.Open()
        'Dim sCmd As String = "RESTORE DATABASE ['Calabozo'] FROM DISK = '" + ruta + "' WITH REPLACE;"
        'Dim cmd As New SqlCommand(sCmd, cn)
        'cmd.ExecuteNonQuery()
        'cn.Close()
        'restaurarBD = True
        'Catch ex As Exception
        'restaurarBD = False
        'MsgBox("Error al restaurar la base de datos de la rutá.Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        'Finally
        'cn.Close()
        'cn.Dispose()
        'End Try
     
        Dim query1 As String = "Alter database [Calabozo] set offline with rollback immediate"
        Dim sBackup As String = "RESTORE DATABASE " & "Calabozo" & _
                                " FROM DISK = '" & ruta & "'" & _
                                " WITH REPLACE"
        Dim queryFinal As String = " alter database [Calabozo] set online"
        Dim csb As New SqlConnectionStringBuilder
        csb.DataSource = ".\SQLEXPRESS"
        ' Es mejor abrir la conexión con la base Master
        csb.InitialCatalog = "master"
        csb.IntegratedSecurity = True

        Using con As New SqlConnection(csb.ConnectionString)
            Try
                con.Open()

                Dim cmdBackUp As New SqlCommand(sBackup, con)
                Dim cmdQuery1 As New SqlCommand(query1, con)
                Dim cmdQueryFinal As New SqlCommand(queryFinal, con)
                cmdQuery1.ExecuteNonQuery()
                cmdBackUp.ExecuteNonQuery()
                cmdQueryFinal.ExecuteNonQuery()
                MessageBox.Show("Se ha restaurado la copia de la base de datos.", _
                                "Restaurar base de datos", _
                                MessageBoxButtons.OK, MessageBoxIcon.Information)

                con.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, _
                                "Error al restaurar la base de datos", _
                                MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Using

        
        Return True
    End Function


    Private Sub respaldarBD()
        Try
            Dim nombreRespaldo As String
            Dim cadena As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6)

            nombreRespaldo = "BACKUP_" & Now.Day & "_" & Now.Month & "_" & Now.Year & ".bak"

            Dim conexion As New SqlConnection("data source=.\SQLEXPRESS;initial catalog=Calabozo;integrated security=True")

            Dim ruta As String = cadena + "\Respaldos\" + nombreRespaldo

            Dim cmd As New SqlCommand("BACKUP DATABASE Calabozo TO DISK = '" & ruta & nombreRespaldo & "'", conexion)

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox("Error al respaldar la base de datos. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub


    Private Sub btnRespaldoBD_Click(sender As Object, e As EventArgs) Handles btnRespaldoBD.Click
            Me.lblInformes.Text = "Se Está Respaldando La Base De Datos. Por favor, Espere"
            Me.lblInformes.ForeColor = Color.Red
            Me.respaldarBD()
            MsgBox("Respaldo de la base de datos con éxito", vbOKOnly, "Ventana Éxito")
            Me.lblInformes.Text = "Ventana De Respaldo"
            Me.lblInformes.ForeColor = Color.Black
    End Sub

    Private Sub btnRecuperacionBD_Click(sender As Object, e As EventArgs) Handles btnRecuperacionBD.Click
        Me.lblInformes.Text = "Se Está Recuperando La Base De Datos. Por favor, Espere"
        Me.lblInformes.ForeColor = Color.Red
        Me.buscarArchivoRespaldo(Me.ofdRespaldo)
        Me.lblInformes.Text = "Ventana De Respaldo"
        Me.lblInformes.ForeColor = Color.Black
    End Sub
End Class