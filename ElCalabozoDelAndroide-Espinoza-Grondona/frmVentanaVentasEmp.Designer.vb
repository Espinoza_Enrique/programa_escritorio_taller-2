﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVentanaVentasEmp
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbcVentasEmpleado = New System.Windows.Forms.TabControl()
        Me.tpgVentas = New System.Windows.Forms.TabPage()
        Me.grbCarritoProdEmp = New System.Windows.Forms.GroupBox()
        Me.lblTotalPago = New System.Windows.Forms.Label()
        Me.lblTituloTotalPago = New System.Windows.Forms.Label()
        Me.btnConfirmarVenta = New System.Windows.Forms.Button()
        Me.dgvCarritoProdEmp = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.grbListaProdEmp = New System.Windows.Forms.GroupBox()
        Me.grbFiltradoProdEmo = New System.Windows.Forms.GroupBox()
        Me.txbNombreProdEmp = New System.Windows.Forms.TextBox()
        Me.lblTituloIdProdEmp = New System.Windows.Forms.Label()
        Me.dgvListaProdEmp = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grbDatosProdEmp = New System.Windows.Forms.GroupBox()
        Me.grbDatosVentas = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnModCarrito = New System.Windows.Forms.Button()
        Me.chkModVentas = New System.Windows.Forms.CheckBox()
        Me.grbDatosModVentas = New System.Windows.Forms.GroupBox()
        Me.chkDescVenta = New System.Windows.Forms.CheckBox()
        Me.chkAuVenta = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txbDetDescVentasEmp = New System.Windows.Forms.TextBox()
        Me.cmbMotDescVentasEmp = New System.Windows.Forms.ComboBox()
        Me.txbDescVentasEmp = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblMontoFinalVenta = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnAgregarCarrito = New System.Windows.Forms.Button()
        Me.grbDatosProductosVenta = New System.Windows.Forms.GroupBox()
        Me.txbTotalSinMod = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txbPrecioUniVenta = New System.Windows.Forms.TextBox()
        Me.txbNomProdVenta = New System.Windows.Forms.TextBox()
        Me.nudCantProdVenta = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txbNroProdVenta = New System.Windows.Forms.TextBox()
        Me.lblCantProdEmp = New System.Windows.Forms.Label()
        Me.lblPrecioProdEmp = New System.Windows.Forms.Label()
        Me.lblIdProductoEmp = New System.Windows.Forms.Label()
        Me.tbcVentasEmpleado.SuspendLayout()
        Me.tpgVentas.SuspendLayout()
        Me.grbCarritoProdEmp.SuspendLayout()
        CType(Me.dgvCarritoProdEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbListaProdEmp.SuspendLayout()
        Me.grbFiltradoProdEmo.SuspendLayout()
        CType(Me.dgvListaProdEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDatosProdEmp.SuspendLayout()
        Me.grbDatosVentas.SuspendLayout()
        Me.grbDatosModVentas.SuspendLayout()
        Me.grbDatosProductosVenta.SuspendLayout()
        CType(Me.nudCantProdVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbcVentasEmpleado
        '
        Me.tbcVentasEmpleado.Alignment = System.Windows.Forms.TabAlignment.Right
        Me.tbcVentasEmpleado.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbcVentasEmpleado.Controls.Add(Me.tpgVentas)
        Me.tbcVentasEmpleado.Location = New System.Drawing.Point(12, 24)
        Me.tbcVentasEmpleado.Multiline = True
        Me.tbcVentasEmpleado.Name = "tbcVentasEmpleado"
        Me.tbcVentasEmpleado.SelectedIndex = 0
        Me.tbcVentasEmpleado.Size = New System.Drawing.Size(1310, 703)
        Me.tbcVentasEmpleado.TabIndex = 40
        '
        'tpgVentas
        '
        Me.tpgVentas.BackColor = System.Drawing.Color.Silver
        Me.tpgVentas.Controls.Add(Me.grbCarritoProdEmp)
        Me.tpgVentas.Controls.Add(Me.grbListaProdEmp)
        Me.tpgVentas.Controls.Add(Me.grbDatosProdEmp)
        Me.tpgVentas.Location = New System.Drawing.Point(4, 4)
        Me.tpgVentas.Name = "tpgVentas"
        Me.tpgVentas.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgVentas.Size = New System.Drawing.Size(1283, 695)
        Me.tpgVentas.TabIndex = 0
        Me.tpgVentas.Text = "Venta"
        '
        'grbCarritoProdEmp
        '
        Me.grbCarritoProdEmp.BackColor = System.Drawing.Color.LightSeaGreen
        Me.grbCarritoProdEmp.Controls.Add(Me.lblTotalPago)
        Me.grbCarritoProdEmp.Controls.Add(Me.lblTituloTotalPago)
        Me.grbCarritoProdEmp.Controls.Add(Me.btnConfirmarVenta)
        Me.grbCarritoProdEmp.Controls.Add(Me.dgvCarritoProdEmp)
        Me.grbCarritoProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbCarritoProdEmp.Location = New System.Drawing.Point(532, 428)
        Me.grbCarritoProdEmp.Name = "grbCarritoProdEmp"
        Me.grbCarritoProdEmp.Size = New System.Drawing.Size(742, 263)
        Me.grbCarritoProdEmp.TabIndex = 41
        Me.grbCarritoProdEmp.TabStop = False
        Me.grbCarritoProdEmp.Text = "Carrito"
        '
        'lblTotalPago
        '
        Me.lblTotalPago.AutoSize = True
        Me.lblTotalPago.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPago.Location = New System.Drawing.Point(145, 230)
        Me.lblTotalPago.Name = "lblTotalPago"
        Me.lblTotalPago.Size = New System.Drawing.Size(26, 27)
        Me.lblTotalPago.TabIndex = 33
        Me.lblTotalPago.Text = "0"
        '
        'lblTituloTotalPago
        '
        Me.lblTituloTotalPago.AutoSize = True
        Me.lblTituloTotalPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloTotalPago.Location = New System.Drawing.Point(6, 235)
        Me.lblTituloTotalPago.Name = "lblTituloTotalPago"
        Me.lblTituloTotalPago.Size = New System.Drawing.Size(116, 25)
        Me.lblTituloTotalPago.TabIndex = 32
        Me.lblTituloTotalPago.Text = "Total Pago"
        '
        'btnConfirmarVenta
        '
        Me.btnConfirmarVenta.Location = New System.Drawing.Point(597, 214)
        Me.btnConfirmarVenta.Name = "btnConfirmarVenta"
        Me.btnConfirmarVenta.Size = New System.Drawing.Size(139, 43)
        Me.btnConfirmarVenta.TabIndex = 42
        Me.btnConfirmarVenta.Text = "Confirmar Venta"
        Me.btnConfirmarVenta.UseVisualStyleBackColor = True
        '
        'dgvCarritoProdEmp
        '
        Me.dgvCarritoProdEmp.AllowUserToAddRows = False
        Me.dgvCarritoProdEmp.AllowUserToDeleteRows = False
        Me.dgvCarritoProdEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCarritoProdEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.Column1, Me.Column4, Me.Column5, Me.Column6, Me.Column2, Me.Column3})
        Me.dgvCarritoProdEmp.Location = New System.Drawing.Point(6, 19)
        Me.dgvCarritoProdEmp.Name = "dgvCarritoProdEmp"
        Me.dgvCarritoProdEmp.ReadOnly = True
        Me.dgvCarritoProdEmp.Size = New System.Drawing.Size(730, 189)
        Me.dgvCarritoProdEmp.TabIndex = 29
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nombre Producto"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'Column1
        '
        Me.Column1.HeaderText = "Cantidad"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Monto Descuento"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Motivo Descuento"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "Detalle Descuento"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "Quitar 1"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'Column3
        '
        Me.Column3.HeaderText = "Eliminar"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'grbListaProdEmp
        '
        Me.grbListaProdEmp.BackColor = System.Drawing.Color.DodgerBlue
        Me.grbListaProdEmp.Controls.Add(Me.grbFiltradoProdEmo)
        Me.grbListaProdEmp.Controls.Add(Me.dgvListaProdEmp)
        Me.grbListaProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbListaProdEmp.ForeColor = System.Drawing.Color.Crimson
        Me.grbListaProdEmp.Location = New System.Drawing.Point(532, 6)
        Me.grbListaProdEmp.Name = "grbListaProdEmp"
        Me.grbListaProdEmp.Size = New System.Drawing.Size(764, 416)
        Me.grbListaProdEmp.TabIndex = 40
        Me.grbListaProdEmp.TabStop = False
        Me.grbListaProdEmp.Text = "Lista De Productos"
        '
        'grbFiltradoProdEmo
        '
        Me.grbFiltradoProdEmo.BackColor = System.Drawing.Color.SteelBlue
        Me.grbFiltradoProdEmo.Controls.Add(Me.txbNombreProdEmp)
        Me.grbFiltradoProdEmo.Controls.Add(Me.lblTituloIdProdEmp)
        Me.grbFiltradoProdEmo.ForeColor = System.Drawing.Color.Black
        Me.grbFiltradoProdEmo.Location = New System.Drawing.Point(6, 23)
        Me.grbFiltradoProdEmo.Name = "grbFiltradoProdEmo"
        Me.grbFiltradoProdEmo.Size = New System.Drawing.Size(752, 86)
        Me.grbFiltradoProdEmo.TabIndex = 29
        Me.grbFiltradoProdEmo.TabStop = False
        Me.grbFiltradoProdEmo.Text = "Filtrado Productos"
        '
        'txbNombreProdEmp
        '
        Me.txbNombreProdEmp.Location = New System.Drawing.Point(172, 38)
        Me.txbNombreProdEmp.Name = "txbNombreProdEmp"
        Me.txbNombreProdEmp.Size = New System.Drawing.Size(306, 24)
        Me.txbNombreProdEmp.TabIndex = 2
        '
        'lblTituloIdProdEmp
        '
        Me.lblTituloIdProdEmp.AutoSize = True
        Me.lblTituloIdProdEmp.Location = New System.Drawing.Point(6, 41)
        Me.lblTituloIdProdEmp.Name = "lblTituloIdProdEmp"
        Me.lblTituloIdProdEmp.Size = New System.Drawing.Size(127, 18)
        Me.lblTituloIdProdEmp.TabIndex = 0
        Me.lblTituloIdProdEmp.Text = "Nombre Producto"
        '
        'dgvListaProdEmp
        '
        Me.dgvListaProdEmp.AllowUserToAddRows = False
        Me.dgvListaProdEmp.AllowUserToDeleteRows = False
        Me.dgvListaProdEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListaProdEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33})
        Me.dgvListaProdEmp.Location = New System.Drawing.Point(6, 115)
        Me.dgvListaProdEmp.Name = "dgvListaProdEmp"
        Me.dgvListaProdEmp.ReadOnly = True
        Me.dgvListaProdEmp.Size = New System.Drawing.Size(742, 295)
        Me.dgvListaProdEmp.TabIndex = 28
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Número Producto"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "Nombre Producto"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "Precio Unitario"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "Categoria"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "Subcategoria"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "Stock"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        '
        'grbDatosProdEmp
        '
        Me.grbDatosProdEmp.BackColor = System.Drawing.Color.DarkSlateGray
        Me.grbDatosProdEmp.Controls.Add(Me.grbDatosVentas)
        Me.grbDatosProdEmp.Controls.Add(Me.grbDatosProductosVenta)
        Me.grbDatosProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosProdEmp.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.grbDatosProdEmp.Location = New System.Drawing.Point(6, 6)
        Me.grbDatosProdEmp.Name = "grbDatosProdEmp"
        Me.grbDatosProdEmp.Size = New System.Drawing.Size(520, 685)
        Me.grbDatosProdEmp.TabIndex = 39
        Me.grbDatosProdEmp.TabStop = False
        Me.grbDatosProdEmp.Text = "Datos De La Venta"
        '
        'grbDatosVentas
        '
        Me.grbDatosVentas.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.grbDatosVentas.Controls.Add(Me.btnCancelar)
        Me.grbDatosVentas.Controls.Add(Me.btnModCarrito)
        Me.grbDatosVentas.Controls.Add(Me.chkModVentas)
        Me.grbDatosVentas.Controls.Add(Me.grbDatosModVentas)
        Me.grbDatosVentas.Controls.Add(Me.lblMontoFinalVenta)
        Me.grbDatosVentas.Controls.Add(Me.Label7)
        Me.grbDatosVentas.Controls.Add(Me.btnAgregarCarrito)
        Me.grbDatosVentas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.grbDatosVentas.Location = New System.Drawing.Point(6, 255)
        Me.grbDatosVentas.Name = "grbDatosVentas"
        Me.grbDatosVentas.Size = New System.Drawing.Size(508, 424)
        Me.grbDatosVentas.TabIndex = 20
        Me.grbDatosVentas.TabStop = False
        Me.grbDatosVentas.Text = "Detalles Venta"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(333, 339)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(153, 49)
        Me.btnCancelar.TabIndex = 33
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnModCarrito
        '
        Me.btnModCarrito.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModCarrito.Location = New System.Drawing.Point(174, 339)
        Me.btnModCarrito.Name = "btnModCarrito"
        Me.btnModCarrito.Size = New System.Drawing.Size(153, 49)
        Me.btnModCarrito.TabIndex = 32
        Me.btnModCarrito.Text = "Modificar"
        Me.btnModCarrito.UseVisualStyleBackColor = True
        '
        'chkModVentas
        '
        Me.chkModVentas.AutoSize = True
        Me.chkModVentas.Location = New System.Drawing.Point(301, 32)
        Me.chkModVentas.Name = "chkModVentas"
        Me.chkModVentas.Size = New System.Drawing.Size(192, 22)
        Me.chkModVentas.TabIndex = 31
        Me.chkModVentas.Text = "¿Modificar Precio Venta?"
        Me.chkModVentas.UseVisualStyleBackColor = True
        '
        'grbDatosModVentas
        '
        Me.grbDatosModVentas.Controls.Add(Me.chkDescVenta)
        Me.grbDatosModVentas.Controls.Add(Me.chkAuVenta)
        Me.grbDatosModVentas.Controls.Add(Me.Label4)
        Me.grbDatosModVentas.Controls.Add(Me.txbDetDescVentasEmp)
        Me.grbDatosModVentas.Controls.Add(Me.cmbMotDescVentasEmp)
        Me.grbDatosModVentas.Controls.Add(Me.txbDescVentasEmp)
        Me.grbDatosModVentas.Controls.Add(Me.Label3)
        Me.grbDatosModVentas.Controls.Add(Me.Label2)
        Me.grbDatosModVentas.Location = New System.Drawing.Point(6, 60)
        Me.grbDatosModVentas.Name = "grbDatosModVentas"
        Me.grbDatosModVentas.Size = New System.Drawing.Size(493, 224)
        Me.grbDatosModVentas.TabIndex = 30
        Me.grbDatosModVentas.TabStop = False
        Me.grbDatosModVentas.Text = "Datos Modificación"
        '
        'chkDescVenta
        '
        Me.chkDescVenta.AutoSize = True
        Me.chkDescVenta.Location = New System.Drawing.Point(388, 23)
        Me.chkDescVenta.Name = "chkDescVenta"
        Me.chkDescVenta.Size = New System.Drawing.Size(99, 22)
        Me.chkDescVenta.TabIndex = 34
        Me.chkDescVenta.Text = "Descuento"
        Me.chkDescVenta.UseVisualStyleBackColor = True
        '
        'chkAuVenta
        '
        Me.chkAuVenta.AutoSize = True
        Me.chkAuVenta.Location = New System.Drawing.Point(388, 65)
        Me.chkAuVenta.Name = "chkAuVenta"
        Me.chkAuVenta.Size = New System.Drawing.Size(86, 22)
        Me.chkAuVenta.TabIndex = 32
        Me.chkAuVenta.Text = "Aumento"
        Me.chkAuVenta.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(4, 98)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(129, 18)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Detalle Descuento"
        '
        'txbDetDescVentasEmp
        '
        Me.txbDetDescVentasEmp.Location = New System.Drawing.Point(9, 119)
        Me.txbDetDescVentasEmp.Multiline = True
        Me.txbDetDescVentasEmp.Name = "txbDetDescVentasEmp"
        Me.txbDetDescVentasEmp.Size = New System.Drawing.Size(478, 98)
        Me.txbDetDescVentasEmp.TabIndex = 32
        '
        'cmbMotDescVentasEmp
        '
        Me.cmbMotDescVentasEmp.FormattingEnabled = True
        Me.cmbMotDescVentasEmp.Location = New System.Drawing.Point(156, 61)
        Me.cmbMotDescVentasEmp.Name = "cmbMotDescVentasEmp"
        Me.cmbMotDescVentasEmp.Size = New System.Drawing.Size(212, 26)
        Me.cmbMotDescVentasEmp.TabIndex = 31
        '
        'txbDescVentasEmp
        '
        Me.txbDescVentasEmp.Location = New System.Drawing.Point(156, 31)
        Me.txbDescVentasEmp.Name = "txbDescVentasEmp"
        Me.txbDescVentasEmp.Size = New System.Drawing.Size(126, 24)
        Me.txbDescVentasEmp.TabIndex = 30
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 18)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Motivo Descuento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 18)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Modificación"
        '
        'lblMontoFinalVenta
        '
        Me.lblMontoFinalVenta.AutoSize = True
        Me.lblMontoFinalVenta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMontoFinalVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoFinalVenta.Location = New System.Drawing.Point(117, 294)
        Me.lblMontoFinalVenta.Name = "lblMontoFinalVenta"
        Me.lblMontoFinalVenta.Size = New System.Drawing.Size(288, 20)
        Me.lblMontoFinalVenta.TabIndex = 21
        Me.lblMontoFinalVenta.Text = "Resultado= (precio/u*cantidad)-descuento"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 296)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 18)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Monto Final"
        '
        'btnAgregarCarrito
        '
        Me.btnAgregarCarrito.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregarCarrito.Location = New System.Drawing.Point(15, 339)
        Me.btnAgregarCarrito.Name = "btnAgregarCarrito"
        Me.btnAgregarCarrito.Size = New System.Drawing.Size(153, 49)
        Me.btnAgregarCarrito.TabIndex = 19
        Me.btnAgregarCarrito.Text = "Agregar"
        Me.btnAgregarCarrito.UseVisualStyleBackColor = True
        '
        'grbDatosProductosVenta
        '
        Me.grbDatosProductosVenta.BackColor = System.Drawing.Color.CornflowerBlue
        Me.grbDatosProductosVenta.Controls.Add(Me.txbTotalSinMod)
        Me.grbDatosProductosVenta.Controls.Add(Me.Label5)
        Me.grbDatosProductosVenta.Controls.Add(Me.txbPrecioUniVenta)
        Me.grbDatosProductosVenta.Controls.Add(Me.txbNomProdVenta)
        Me.grbDatosProductosVenta.Controls.Add(Me.nudCantProdVenta)
        Me.grbDatosProductosVenta.Controls.Add(Me.Label1)
        Me.grbDatosProductosVenta.Controls.Add(Me.txbNroProdVenta)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblCantProdEmp)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblPrecioProdEmp)
        Me.grbDatosProductosVenta.Controls.Add(Me.lblIdProductoEmp)
        Me.grbDatosProductosVenta.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.grbDatosProductosVenta.Location = New System.Drawing.Point(6, 23)
        Me.grbDatosProductosVenta.Name = "grbDatosProductosVenta"
        Me.grbDatosProductosVenta.Size = New System.Drawing.Size(508, 226)
        Me.grbDatosProductosVenta.TabIndex = 19
        Me.grbDatosProductosVenta.TabStop = False
        Me.grbDatosProductosVenta.Text = "Datos Producto"
        '
        'txbTotalSinMod
        '
        Me.txbTotalSinMod.Location = New System.Drawing.Point(173, 163)
        Me.txbTotalSinMod.Name = "txbTotalSinMod"
        Me.txbTotalSinMod.ReadOnly = True
        Me.txbTotalSinMod.Size = New System.Drawing.Size(165, 24)
        Me.txbTotalSinMod.TabIndex = 31
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 166)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(164, 18)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Precio Sin Modificación"
        '
        'txbPrecioUniVenta
        '
        Me.txbPrecioUniVenta.Location = New System.Drawing.Point(173, 89)
        Me.txbPrecioUniVenta.Name = "txbPrecioUniVenta"
        Me.txbPrecioUniVenta.ReadOnly = True
        Me.txbPrecioUniVenta.Size = New System.Drawing.Size(98, 24)
        Me.txbPrecioUniVenta.TabIndex = 29
        '
        'txbNomProdVenta
        '
        Me.txbNomProdVenta.Location = New System.Drawing.Point(173, 59)
        Me.txbNomProdVenta.Name = "txbNomProdVenta"
        Me.txbNomProdVenta.ReadOnly = True
        Me.txbNomProdVenta.Size = New System.Drawing.Size(270, 24)
        Me.txbNomProdVenta.TabIndex = 28
        '
        'nudCantProdVenta
        '
        Me.nudCantProdVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudCantProdVenta.Location = New System.Drawing.Point(173, 126)
        Me.nudCantProdVenta.Name = "nudCantProdVenta"
        Me.nudCantProdVenta.Size = New System.Drawing.Size(98, 24)
        Me.nudCantProdVenta.TabIndex = 29
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 18)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Nombre Producto"
        '
        'txbNroProdVenta
        '
        Me.txbNroProdVenta.Location = New System.Drawing.Point(173, 21)
        Me.txbNroProdVenta.Name = "txbNroProdVenta"
        Me.txbNroProdVenta.ReadOnly = True
        Me.txbNroProdVenta.Size = New System.Drawing.Size(100, 24)
        Me.txbNroProdVenta.TabIndex = 26
        '
        'lblCantProdEmp
        '
        Me.lblCantProdEmp.AutoSize = True
        Me.lblCantProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantProdEmp.Location = New System.Drawing.Point(10, 128)
        Me.lblCantProdEmp.Name = "lblCantProdEmp"
        Me.lblCantProdEmp.Size = New System.Drawing.Size(66, 18)
        Me.lblCantProdEmp.TabIndex = 28
        Me.lblCantProdEmp.Text = "Cantidad"
        '
        'lblPrecioProdEmp
        '
        Me.lblPrecioProdEmp.AutoSize = True
        Me.lblPrecioProdEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecioProdEmp.Location = New System.Drawing.Point(6, 92)
        Me.lblPrecioProdEmp.Name = "lblPrecioProdEmp"
        Me.lblPrecioProdEmp.Size = New System.Drawing.Size(106, 18)
        Me.lblPrecioProdEmp.TabIndex = 19
        Me.lblPrecioProdEmp.Text = "Precio Unitario"
        '
        'lblIdProductoEmp
        '
        Me.lblIdProductoEmp.AutoSize = True
        Me.lblIdProductoEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdProductoEmp.Location = New System.Drawing.Point(6, 27)
        Me.lblIdProductoEmp.Name = "lblIdProductoEmp"
        Me.lblIdProductoEmp.Size = New System.Drawing.Size(127, 18)
        Me.lblIdProductoEmp.TabIndex = 17
        Me.lblIdProductoEmp.Text = "Número Producto"
        '
        'frmVentanaVentasEmp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1334, 749)
        Me.Controls.Add(Me.tbcVentasEmpleado)
        Me.Name = "frmVentanaVentasEmp"
        Me.Text = "Ventas"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbcVentasEmpleado.ResumeLayout(False)
        Me.tpgVentas.ResumeLayout(False)
        Me.grbCarritoProdEmp.ResumeLayout(False)
        Me.grbCarritoProdEmp.PerformLayout()
        CType(Me.dgvCarritoProdEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbListaProdEmp.ResumeLayout(False)
        Me.grbFiltradoProdEmo.ResumeLayout(False)
        Me.grbFiltradoProdEmo.PerformLayout()
        CType(Me.dgvListaProdEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDatosProdEmp.ResumeLayout(False)
        Me.grbDatosVentas.ResumeLayout(False)
        Me.grbDatosVentas.PerformLayout()
        Me.grbDatosModVentas.ResumeLayout(False)
        Me.grbDatosModVentas.PerformLayout()
        Me.grbDatosProductosVenta.ResumeLayout(False)
        Me.grbDatosProductosVenta.PerformLayout()
        CType(Me.nudCantProdVenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbcVentasEmpleado As System.Windows.Forms.TabControl
    Friend WithEvents tpgVentas As System.Windows.Forms.TabPage
    Friend WithEvents btnConfirmarVenta As System.Windows.Forms.Button
    Friend WithEvents grbCarritoProdEmp As System.Windows.Forms.GroupBox
    Friend WithEvents dgvCarritoProdEmp As System.Windows.Forms.DataGridView
    Friend WithEvents grbListaProdEmp As System.Windows.Forms.GroupBox
    Friend WithEvents grbFiltradoProdEmo As System.Windows.Forms.GroupBox
    Friend WithEvents txbNombreProdEmp As System.Windows.Forms.TextBox
    Friend WithEvents lblTituloIdProdEmp As System.Windows.Forms.Label
    Friend WithEvents dgvListaProdEmp As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grbDatosProdEmp As System.Windows.Forms.GroupBox
    Friend WithEvents lblTotalPago As System.Windows.Forms.Label
    Friend WithEvents lblTituloTotalPago As System.Windows.Forms.Label
    Friend WithEvents grbDatosVentas As System.Windows.Forms.GroupBox
    Friend WithEvents nudCantProdVenta As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblCantProdEmp As System.Windows.Forms.Label
    Friend WithEvents lblMontoFinalVenta As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnAgregarCarrito As System.Windows.Forms.Button
    Friend WithEvents grbDatosProductosVenta As System.Windows.Forms.GroupBox
    Friend WithEvents lblPrecioProdEmp As System.Windows.Forms.Label
    Friend WithEvents lblIdProductoEmp As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents chkModVentas As System.Windows.Forms.CheckBox
    Friend WithEvents grbDatosModVentas As System.Windows.Forms.GroupBox
    Friend WithEvents chkDescVenta As System.Windows.Forms.CheckBox
    Friend WithEvents chkAuVenta As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txbDetDescVentasEmp As System.Windows.Forms.TextBox
    Friend WithEvents cmbMotDescVentasEmp As System.Windows.Forms.ComboBox
    Friend WithEvents txbDescVentasEmp As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txbTotalSinMod As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txbPrecioUniVenta As System.Windows.Forms.TextBox
    Friend WithEvents txbNomProdVenta As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txbNroProdVenta As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnModCarrito As System.Windows.Forms.Button
End Class
