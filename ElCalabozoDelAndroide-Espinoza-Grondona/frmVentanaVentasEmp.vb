﻿Imports Entities
Imports BusinessLayer
Public Class frmVentanaVentasEmp
    '--------------------------------------------------------------------------------------------------------
    'Permite seleccionar una de las opciones de los tab control de los formularios. Se usará para que, al
    'presionar sobre la barra de opciones, pueda dirigirse a la opción del tab control correspondiente
    'Parámetros:
    'control-Tipo: TabControl-Representa el tab control cuyas tab pages se quiere seleccionar
    'elemento-tipo: Integer-Representa la tab page que se seleccionará
    '--------------------------------------------------------------------------------------------------------
    Public Sub seleccionarTabControl(ByVal elemento As Byte)
        Try
            If elemento >= 1 And elemento <= Me.tbcVentasEmpleado.TabCount Then
                Me.tbcVentasEmpleado.SelectedIndex = elemento - 1
                Me.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox("Error al seleccionar una opción en las opciones para el empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Seleccionar Una Opción De Ventana")
        End Try
    End Sub

    Public Sub abrirVentana()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            MsgBox("Error al abrir la ventana de ventas para el empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error")
        End Try
    End Sub

    Private Sub cargarFondoPantalla(ByVal nombreImagen As String)
        Try
            Me.BackgroundImage = Image.FromFile(General.retornarRutaFoto(nombreImagen))
        Catch ex As Exception
            MsgBox("Error al cargar la imagen de fondo: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Error carga")
        End Try
    End Sub

    Private Sub realizarVenta(ByRef carrito As DataGridView)
        Try
            Dim cabeceraVenta As Integer
            Dim cabeceraModVenta As Integer
            Dim montoTotalVenta As Double
            Dim idUsuario As Integer
            Dim idProductos As New List(Of Integer)
            Dim unidadesVendidas As New List(Of Integer)
            Dim preciosVentas As New List(Of Double)
            Dim montoTotalMod As Double
            Dim montoDetMod As New List(Of Double)
            Dim idMotivos As New List(Of Integer)
            Dim descripciones As New List(Of String)
            Dim elementosCarrito As Integer
            Dim elementoAnalizado

            elementosCarrito = carrito.Rows.Count()
            elementoAnalizado = 0
            idUsuario = Sesion.getIdUsuario()
            montoTotalVenta = CDbl(Me.lblTotalPago.Text)
            montoTotalMod = 0
            cabeceraVenta = 0
            cabeceraModVenta = 0
            While elementoAnalizado < elementosCarrito
                idProductos.Add(CInt(carrito.Rows(elementoAnalizado).Cells("codProd").Value))
                unidadesVendidas.Add(CInt(carrito.Rows(elementoAnalizado).Cells("cantidad").Value))
                preciosVentas.Add((CDbl(carrito.Rows(elementoAnalizado).Cells("precioFinal").Value)))
                montoDetMod.Add((CDbl(carrito.Rows(elementoAnalizado).Cells("cantidadMod").Value)))
                montoTotalMod = montoTotalMod + montoDetMod(elementoAnalizado)
                idMotivos.Add((CInt(carrito.Rows(elementoAnalizado).Cells("idMotivo").Value)))
                descripciones.Add(carrito.Rows(elementoAnalizado).Cells("descMod").Value)
                elementoAnalizado = elementoAnalizado + 1
            End While
            'inserto la cabecera de la venta
            BusVentas.insertarCabeceraVenta(idUsuario, montoTotalVenta)
            'recupero el id de la última cabecera insertada
            cabeceraVenta = BusVentas.getUltimoIdCabecera()
            'inserto la cabecera de modificación de precios en la compra
            BusVentas.insertarCabeceraMod(montoTotalMod)
            'recupero el id de la última cabecera de modificaciones ingresada
            cabeceraModVenta = BusVentas.getUltimoIdCabeceraMod()
            'inserto los detalles de ventas
            BusVentas.insertarDetallesVenta(cabeceraVenta, preciosVentas, unidadesVendidas, idProductos)
            'inserto los detalles de modificaciones de precio
            BusVentas.insertarDetallesMod(cabeceraVenta, cabeceraModVenta, montoDetMod, idMotivos, descripciones)
            'actualizo el stock de los productos vendidos
            BusVentas.actualizarStockPorVenta(idProductos, unidadesVendidas)

        Catch ex As Exception
            MsgBox("Error al cargar los datos del carrito para realizar la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Venta Errores")
        End Try
    End Sub

    Private Sub deshabilitarModificaciones()
        Try
            Me.grbDatosModVentas.Enabled = False
        Catch ex As Exception
            MsgBox("Error al deshabilitar las modificaciones de precio:" + vbNewLine + ex.Message, vbOKOnly, "Ventana Erorr")
        End Try
    End Sub
    Private Sub frmVentanaVentasEmp_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            Me.cargarFondoPantalla("cobaltoplata.jpg")
            'Me.btnAntProdEmp.Image.RotateFlip(RotateFlipType.Rotate180FlipY)
            Me.cargarProductos(Me.dgvListaProdEmp)
            Me.prepararCarritoVenta(Me.dgvCarritoProdEmp)
            Me.rellenarMotivosModPrecio(Me.cmbMotDescVentasEmp)
            Me.deshabilitarModificaciones()
        Catch ex As Exception
            MsgBox("Error al cargar la ventana del empleado:" + vbNewLine + ex.Message, vbOKOnly, "Error De Carga De Ventana")
        End Try

    End Sub


    Private Sub btnAgregarCarrito_Click(sender As Object, e As EventArgs) Handles btnAgregarCarrito.Click

        Try
            Dim mensaje As String
            Dim modificacion As Boolean
            mensaje = ""
            modificacion = Me.chkModVentas.Checked

            If General.controlarCajaEnBlanco(Me.txbDescVentasEmp.Text) And modificacion Then
                mensaje = mensaje + "Por favor, coloque un valor a la modificación de precio" + vbNewLine
            End If

            If General.controlarCajaEnBlanco(Me.txbDetDescVentasEmp.Text) And modificacion Then
                mensaje = mensaje + "Por favor, no deje el motivo del descuento en blanco" + vbNewLine
            End If

            If (General.controlarCajaEnBlanco(Me.txbDescVentasEmp.Text) Or Not General.controlarRealPositivo(Me.txbDescVentasEmp.Text)) And modificacion Then
                mensaje = mensaje + "Por favor, coloque un valor numérico positivo si va a realizar alguna modificación al precio de venta."

            End If
            mensaje = mensaje + Me.cargarCarrito(Me.dgvCarritoProdEmp, modificacion)

            If String.IsNullOrWhiteSpace(mensaje) Then
                MsgBox("Datos Correctos", vbOKOnly, "Ventana Éxito")
                Me.calcularPrecioFinalCompra(Me.dgvCarritoProdEmp)
                Me.limpiarDatosVenta()
            Else
                MsgBox("Se han presentado errores. Los errores fueron: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
            End If

        Catch ex As Exception
            MsgBox("Error al ingresar productos al carrito:" + vbNewLine + ex.Message, vbOKOnly, "Error Al Seleccionar Una Opción De Ventana")
        End Try
    End Sub

    Private Sub filtrarProdCatSub(ByVal grilla As DataGridView, ByVal idCat As Integer, ByVal idSub As Integer)
        Try
            Dim codProd As New DataGridViewTextBoxColumn()
            Dim nombreProducto As New DataGridViewTextBoxColumn()
            Dim precio As New DataGridViewTextBoxColumn()
            Dim categoria As New DataGridViewTextBoxColumn()
            Dim subcategoria As New DataGridViewTextBoxColumn()
            Dim productos As New List(Of ProductosHabilitados)
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProducto)
            grilla.Columns.Add(precio)
            grilla.Columns.Add(categoria)
            grilla.Columns.Add(subcategoria)

            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            nombreProducto.Name = "nombreProducto"
            nombreProducto.HeaderText = "Producto"
            precio.Name = "precio"
            precio.HeaderText = "Precio"
            categoria.Name = "categoria"
            categoria.HeaderText = "Categoria"
            subcategoria.Name = "subcategoria"
            subcategoria.HeaderText = "Subcategoria"
            grilla.AutoGenerateColumns = False
            grilla.Columns(0).DataPropertyName = "nroProducto"
            grilla.Columns(1).DataPropertyName = "nombre"
            grilla.Columns(2).DataPropertyName = "precio"
            grilla.Columns(3).DataPropertyName = "categoria"
            grilla.Columns(4).DataPropertyName = "subcategoria"
            productos = BusProductos.filtrarParaVentaPorCatSub(idCat, idSub)
            If Not IsNothing(productos) Then
                grilla.DataSource = productos
            Else
                MsgBox("No existen productos que cumplan con el criterio establecido", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If




            'For Each prod In BusProductos.getProductosHabilitados()
            'grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre)
            'Next

        Catch ex As Exception

        End Try
    End Sub
    Private Sub filtrarProdCat(ByVal grilla As DataGridView, ByVal idCat As Integer)
        Try
            Dim codProd As New DataGridViewTextBoxColumn()
            Dim nombreProducto As New DataGridViewTextBoxColumn()
            Dim precio As New DataGridViewTextBoxColumn()
            Dim categoria As New DataGridViewTextBoxColumn()
            Dim subcategoria As New DataGridViewTextBoxColumn()
            Dim productos As New List(Of ProductosHabilitados)

            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProducto)
            grilla.Columns.Add(precio)
            grilla.Columns.Add(categoria)
            grilla.Columns.Add(subcategoria)

            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            nombreProducto.Name = "nombreProducto"
            nombreProducto.HeaderText = "Producto"
            precio.Name = "precio"
            precio.HeaderText = "Precio"
            categoria.Name = "categoria"
            categoria.HeaderText = "Categoria"
            subcategoria.Name = "subcategoria"
            subcategoria.HeaderText = "Subcategoria"
            grilla.AutoGenerateColumns = False
            grilla.Columns(0).DataPropertyName = "nroProducto"
            grilla.Columns(1).DataPropertyName = "nombre"
            grilla.Columns(2).DataPropertyName = "precio"
            grilla.Columns(3).DataPropertyName = "categoria"
            grilla.Columns(4).DataPropertyName = "subcategoria"
            productos = BusProductos.filtrarParaVentaPorCat(idCat)
            If Not IsNothing(productos) Then
                grilla.DataSource = productos
            Else
                MsgBox("No existen productos que cumplan con el criterio establecido", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If



            'For Each prod In BusProductos.getProductosHabilitados()
            'grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre)
            'Next

        Catch ex As Exception

        End Try
    End Sub
    Private Sub filtrarProdNom(ByVal grilla As DataGridView, ByVal nombre As String)
        Try
            Dim codProd As New DataGridViewTextBoxColumn()
            Dim nombreProducto As New DataGridViewTextBoxColumn()
            Dim precio As New DataGridViewTextBoxColumn()
            Dim categoria As New DataGridViewTextBoxColumn()
            Dim subcategoria As New DataGridViewTextBoxColumn()
            Dim productos As New List(Of ProductosHabilitados)

            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProducto)
            grilla.Columns.Add(precio)
            grilla.Columns.Add(categoria)
            grilla.Columns.Add(subcategoria)

            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            nombreProducto.Name = "nombreProducto"
            nombreProducto.HeaderText = "Producto"
            precio.Name = "precio"
            precio.HeaderText = "Precio"
            categoria.Name = "categoria"
            categoria.HeaderText = "Categoria"
            subcategoria.Name = "subcategoria"
            subcategoria.HeaderText = "Subcategoria"
            grilla.AutoGenerateColumns = False
            grilla.Columns(0).DataPropertyName = "nroProducto"
            grilla.Columns(1).DataPropertyName = "nombre"
            grilla.Columns(2).DataPropertyName = "precio"
            grilla.Columns(3).DataPropertyName = "categoria"
            grilla.Columns(4).DataPropertyName = "subcategoria"
            productos = BusProductos.filtrarParaVentaPorNom(nombre)
            If Not IsNothing(productos) Then
                grilla.DataSource = productos
            Else
                MsgBox("No existen productos que cumplan con el criterio establecido", vbOKOnly + vbExclamation, "Ventana Advertencia")
            End If



            'For Each prod In BusProductos.getProductosHabilitados()
            'grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre)
            'Next

        Catch ex As Exception

        End Try
    End Sub
    Private Sub cargarProductos(ByVal grilla As DataGridView)
        Try
            Dim codProd As New DataGridViewTextBoxColumn()
            Dim nombreProducto As New DataGridViewTextBoxColumn()
            Dim precio As New DataGridViewTextBoxColumn()
            Dim categoria As New DataGridViewTextBoxColumn()
            Dim subcategoria As New DataGridViewTextBoxColumn()

            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProducto)
            grilla.Columns.Add(precio)
            grilla.Columns.Add(categoria)
            grilla.Columns.Add(subcategoria)

            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            nombreProducto.Name = "nombreProducto"
            nombreProducto.HeaderText = "Producto"
            precio.Name = "precio"
            precio.HeaderText = "Precio"
            categoria.Name = "categoria"
            categoria.HeaderText = "Categoria"
            subcategoria.Name = "subcategoria"
            subcategoria.HeaderText = "Subcategoria"
            grilla.AutoGenerateColumns = False
            grilla.Columns(0).DataPropertyName = "nroProducto"
            grilla.Columns(1).DataPropertyName = "nombre"
            grilla.Columns(2).DataPropertyName = "precio"
            grilla.Columns(3).DataPropertyName = "categoria"
            grilla.Columns(4).DataPropertyName = "subcategoria"
            grilla.DataSource = BusProductos.getProductosHabilitados()


            
            'For Each prod In BusProductos.getProductosHabilitados()
            'grilla.Rows.Add(prod.idProducto, prod.nombreProducto, prod.precio, prod.CategoriasSubcategorias.Categorias.nombre, prod.CategoriasSubcategorias.Subcategorias.nombre)
            'Next

        Catch ex As Exception

        End Try
    End Sub
    Private Sub prepararCarritoVenta(ByRef grilla As DataGridView)
        Dim btnModificar As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim btnEliminar As New System.Windows.Forms.DataGridViewButtonColumn()
        Dim codProd As New DataGridViewTextBoxColumn()
        Dim nombreProducto As New DataGridViewTextBoxColumn()
        Dim cantidad As New DataGridViewTextBoxColumn()
        Dim precioTotal As New DataGridViewTextBoxColumn()
        Dim motivoMod As New DataGridViewTextBoxColumn()
        Dim cantidadMod As New DataGridViewTextBoxColumn()
        Dim precioFinal As New DataGridViewTextBoxColumn()
        Dim idMotivo As New DataGridViewTextBoxColumn()
        Dim precioUnitario As New DataGridViewTextBoxColumn()
        Dim descripcion As New DataGridViewTextBoxColumn()


        'grilla.ColumnCount = 8
        Try
            grilla.Columns.Clear()
            grilla.Columns.Add(codProd)
            grilla.Columns.Add(nombreProducto)
            grilla.Columns.Add(cantidad)
            grilla.Columns.Add(precioTotal)
            grilla.Columns.Add(cantidadMod)
            grilla.Columns.Add(precioFinal)
            grilla.Columns.Add(motivoMod)
            grilla.Columns.Add(descripcion)
            grilla.Columns.Add(idMotivo)
            grilla.Columns.Add(precioUnitario)
            grilla.Columns.Add(btnModificar)
            grilla.Columns.Add(btnEliminar)


            codProd.Name = "codProd"
            codProd.HeaderText = "Número Producto"
            nombreProducto.Name = "nombreProducto"
            nombreProducto.HeaderText = "Producto"
            cantidad.Name = "cantidad"
            cantidad.HeaderText = "Cantidad"
            precioTotal.Name = "precio"
            precioTotal.HeaderText = "Precio"
            precioFinal.Name = "precioFinal"
            precioFinal.HeaderText = "Precio Final"
            motivoMod.Name = "motivoMod"
            motivoMod.HeaderText = "Modificación Precio"
            cantidadMod.Name = "cantidadMod"
            cantidadMod.HeaderText = "Monto Modificado"
            descripcion.Name = "descMod"
            descripcion.HeaderText = "Descripción Modificación"
            idMotivo.Name = "idMotivo"
            idMotivo.Visible = False
            precioUnitario.Name = "precioUnitario"
            precioUnitario.Visible = False
            btnModificar.Text = "Modificar"
            btnModificar.Name = "btnModificar"
            btnModificar.HeaderText = "Modificar Unidades"
            btnModificar.UseColumnTextForButtonValue = True
            btnEliminar.Text = "Eliminar"
            btnEliminar.Name = "btnEliminar"
            btnEliminar.HeaderText = "Eliminar"
            btnEliminar.UseColumnTextForButtonValue = True

        Catch ex As Exception
            MsgBox(ex.Message, vbOKOnly, "Error")

        End Try
    End Sub

    Private Sub moverDatosProductos(ByVal grilla As DataGridView)
        Try
           
            Me.txbNroProdVenta.Text = grilla.CurrentRow.Cells("codProd").Value
            Me.txbNomProdVenta.Text = grilla.CurrentRow.Cells("nombreProducto").Value
            Me.txbPrecioUniVenta.Text = grilla.CurrentRow.Cells("precio").Value
            Me.nudCantProdVenta.Value = 1
            Me.grbDatosModVentas.Enabled = False
            Me.btnModCarrito.Enabled = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub nudCantProdVenta_ValueChanged(sender As Object, e As EventArgs) Handles nudCantProdVenta.ValueChanged
        If Me.nudCantProdVenta.Value <= 0 Then
            MsgBox("La cantidad comprada no puede ser menor a uno", vbOKOnly + vbExclamation, "Ventana Error")
            Me.nudCantProdVenta.Value = 1
        ElseIf Not General.controlarCajaEnBlanco(Me.txbPrecioUniVenta.Text) And General.controlarRealPositivo(Me.txbPrecioUniVenta.Text) Then
            Me.txbTotalSinMod.Text = CDbl(Me.txbPrecioUniVenta.Text) * Me.nudCantProdVenta.Value
        Else
            Me.txbPrecioUniVenta.Text=""
        End If
    End Sub

    Private Sub chkModVentas_CheckedChanged(sender As Object, e As EventArgs) Handles chkModVentas.CheckedChanged
        Me.grbDatosModVentas.Enabled = Me.chkModVentas.Checked
        Me.chkDescVenta.Checked = True
        If Not Me.grbDatosModVentas.Enabled Then
            Me.lblMontoFinalVenta.Text = Me.txbTotalSinMod.Text
        End If
    End Sub

    Private Sub chkAuVenta_CheckedChanged(sender As Object, e As EventArgs) Handles chkAuVenta.CheckedChanged
        Me.chkAuVenta.Checked = Not Me.chkDescVenta.Checked
    End Sub

    Private Sub chkDescVenta_CheckedChanged(sender As Object, e As EventArgs) Handles chkDescVenta.CheckedChanged
        Me.chkDescVenta.Checked = Not Me.chkAuVenta.Checked
    End Sub

    Private Sub dgvListaProdEmp_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvListaProdEmp.CellClick
        If Not Me.controlarExistenciaEnCarrito(Me.dgvListaProdEmp.CurrentRow.Cells("codProd").Value, Me.dgvCarritoProdEmp) Then
            Me.moverDatosProductos(Me.dgvListaProdEmp)
        Else
            MsgBox("El producto que desea comprar ya se encuentra en el carrito." + vbNewLine + "Presione el botón 'Modificar' en el item del carrito si desea realizar cambios", vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub

    Private Function controlarExistenciaEnCarrito(ByVal idProducto As String, ByRef carrito As DataGridView) As Boolean
        Try
            Dim elementosEnCarrito As Integer
            Dim elementoAnalizado As Integer
            Dim resultado As Boolean

            resultado = False
            elementosEnCarrito = carrito.Rows.Count
            elementoAnalizado = 0
            While elementoAnalizado < elementosEnCarrito
                If carrito.Rows(elementoAnalizado).Cells("codProd").Value = idProducto Then
                    resultado = True
                    elementoAnalizado = elementoAnalizado + 1
                End If
                elementoAnalizado = elementoAnalizado + 1
            End While
            Return resultado
        Catch ex As Exception
            MsgBox("Se ha producido un error al intentar controlar si el producto en cuestión ya existe en el carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
            Return Nothing
        End Try

    End Function
    Private Sub lblMontoFinalVenta_Click(sender As Object, e As EventArgs) Handles lblMontoFinalVenta.Click

    End Sub

    Private Sub txbTotalSinMod_TextChanged(sender As Object, e As EventArgs) Handles txbTotalSinMod.TextChanged
        Me.lblMontoFinalVenta.Text = Me.txbTotalSinMod.Text
    End Sub

    Private Sub rellenarMotivosModPrecio(ByRef cmbModificacion As ComboBox)
        Try
            Dim indiceSinMod As Integer
            cmbModificacion.DataSource = BusVentas.getMotivosModVentas()
            cmbModificacion.ValueMember = "idMotivoModVentas"
            cmbModificacion.DisplayMember = "idNombreMotivo"
            indiceSinMod = cmbModificacion.FindStringExact("Sin Modificación")
            cmbModificacion.SelectedIndex = indiceSinMod
        Catch ex As Exception
            MsgBox("Error al cargar los motivos de modificación de ventas. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub txbDescVentasEmp_LostFocus(sender As Object, e As EventArgs) Handles txbDescVentasEmp.LostFocus
        If Not General.controlarCajaEnBlanco(Me.txbDescVentasEmp.Text) And General.controlarRealPositivo(Me.txbDescVentasEmp.Text) Then
            If Me.chkDescVenta.Checked Then
                Me.lblMontoFinalVenta.Text = CDbl(Me.txbTotalSinMod.Text) - CDbl(Me.txbDescVentasEmp.Text)
            Else
                Me.lblMontoFinalVenta.Text = CDbl(Me.txbTotalSinMod.Text) + CDbl(Me.txbDescVentasEmp.Text)
            End If

        End If
    End Sub

    Private Sub txbDescVentasEmp_TextChanged(sender As Object, e As EventArgs) Handles txbDescVentasEmp.TextChanged
        'If Not General.controlarRealPositivo(Me.txbDescVentasEmp.Text) And Not String.IsNullOrWhiteSpace(Me.txbDescVentasEmp.Text) Then
        'MsgBox("Por favor, coloque un valor real positivo.", vbOKOnly + vbExclamation, "Ventana Error")
        'End If
    End Sub

    Private Sub cmbMotDescVentasEmp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMotDescVentasEmp.SelectedIndexChanged
        If Me.cmbMotDescVentasEmp.Text = "Sin Modificación" Then
            Me.chkModVentas.Checked = False
        End If
    End Sub

    Private Sub eliminarElementoCarrito(ByRef carrito As DataGridView)
        Try
            Dim respuesta As Integer
            respuesta = MsgBox("¿Desea eliminar el producto seleccionado? ", vbYesNo + vbDefaultButton2, "Ventana Consulta")
            If respuesta = vbYes Then
                carrito.Rows.Remove(carrito.CurrentRow)
            End If
        Catch ex As Exception
            MsgBox("Error al eliminar un elemento del carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Function cargarCarrito(ByVal carrito As DataGridView, ByVal modificacion As Boolean) As String
        Try
            Dim codProd As String
            Dim producto As String
            Dim cantidad As Integer
            Dim precioTotal As Double
            Dim precioFinal As Double
            Dim precioUnitario As Double
            Dim precioMod As Double
            Dim motivo As String
            Dim idMotivo As Integer
            Dim descripcion As String
            Dim mensaje As String

            mensaje = ""
            codProd = Me.txbNroProdVenta.Text
            producto = Me.txbNomProdVenta.Text
            cantidad = Me.nudCantProdVenta.Value
            precioTotal = CDbl(Me.txbTotalSinMod.Text)
            precioFinal = CDbl(Me.lblMontoFinalVenta.Text)
            precioUnitario = CDbl(Me.txbPrecioUniVenta.Text)
            precioMod = 0
            descripcion = "No hubo modificación para este producto"
            If modificacion And Me.chkAuVenta.Checked Then
                precioMod = CDbl(Me.txbDescVentasEmp.Text)
                descripcion = Me.txbDetDescVentasEmp.Text
            ElseIf modificacion And Not Me.chkAuVenta.Checked Then
                precioMod = CDbl(Me.txbDescVentasEmp.Text) * (-1)
                descripcion = Me.txbDetDescVentasEmp.Text
            End If

            motivo = Me.cmbMotDescVentasEmp.Text
            idMotivo = Me.cmbMotDescVentasEmp.SelectedValue


            If BusProductos.controlarStock(codProd, cantidad) Then
                carrito.Rows.Add(codProd, producto, cantidad, precioTotal, precioMod, precioFinal, motivo, descripcion, idMotivo, precioUnitario)
            Else
                mensaje = mensaje + "La cantidad de unidades que desea comprar supera el stock disponible. Por favor, compre una cantidad menor." + vbNewLine
            End If

            Return mensaje

        Catch ex As Exception
            MsgBox("Error al cargar el artículo al carrito. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbExclamation, "Ventana Error")
            Return Nothing
        End Try
    End Function
    Private Sub calcularPrecioFinalCompra(ByRef carrito As DataGridView)
        Try
            Dim precioFinal As Double
            Dim elementosCarrito As Integer
            Dim elementoAnalizado As Integer

            precioFinal = 0
            elementosCarrito = carrito.Rows.Count
            elementoAnalizado = 0

            While elementoAnalizado < elementosCarrito
                precioFinal = precioFinal + CDbl(carrito.Rows(elementoAnalizado).Cells("precioFinal").Value)
                elementoAnalizado = elementoAnalizado + 1
            End While
            Me.lblTotalPago.Text = precioFinal
        Catch ex As Exception
            MsgBox("Error al calcular el precio final de la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub dgvCarritoProdEmp_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCarritoProdEmp.CellClick
        Dim cell As DataGridViewButtonCell = TryCast(dgvCarritoProdEmp.CurrentCell, 
       DataGridViewButtonCell)
        If cell IsNot Nothing Then 'Verifica que la celdas tengan informacion
            Dim bc As DataGridViewButtonColumn = TryCast(dgvCarritoProdEmp.Columns(e.ColumnIndex), 
            DataGridViewButtonColumn) 'Genero una variable que contiene el boton en el datagrid
            If bc IsNot Nothing Then
                Dim s As String = Convert.ToString(cell.Value)
                Select Case bc.Name
                    Case "btnModificar"
                        Me.moverDatosParaModificar(Me.dgvCarritoProdEmp)
                    Case "btnEliminar"
                        Me.eliminarElementoCarrito(dgvCarritoProdEmp)
                        Me.limpiarDatosVenta()
                End Select
            End If

        End If
    End Sub

    Private Sub moverDatosParaModificar(ByRef carrito As DataGridView)
        Try
            Me.btnAgregarCarrito.Enabled = False
            Me.btnModCarrito.Enabled = True
            Me.txbNroProdVenta.Text = carrito.CurrentRow.Cells("codProd").Value
            Me.txbNomProdVenta.Text = carrito.CurrentRow.Cells("nombreProducto").Value
            Me.txbPrecioUniVenta.Text = carrito.CurrentRow.Cells("precioUnitario").Value
            Me.nudCantProdVenta.Value = carrito.CurrentRow.Cells("cantidad").Value
            Me.txbTotalSinMod.Text = CDbl(Me.txbPrecioUniVenta.Text) * Me.nudCantProdVenta.Value
            If CInt(carrito.CurrentRow.Cells("cantidadMod").Value) > 0 Then
                Me.chkDescVenta.Checked = False
            ElseIf CInt(carrito.CurrentRow.Cells("cantidadMod").Value) < 0 Then
                Me.chkDescVenta.Checked = True
                Me.txbDescVentasEmp.Text = CDbl(carrito.CurrentRow.Cells("cantidadMod").Value) * (-1)
            End If
            Me.cmbMotDescVentasEmp.SelectedValue = carrito.CurrentRow.Cells("idMotivo").Value
            Me.txbDetDescVentasEmp.Text = carrito.CurrentRow.Cells("descMod").Value
            Me.lblMontoFinalVenta.Text = carrito.CurrentRow.Cells("precioFinal").Value
        Catch ex As Exception
            MsgBox("Error al mover los datos del carrito para modificar. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub
    Private Sub modificar(ByRef carrito As DataGridView, ByVal modificacion As Boolean)
        Try
            Dim totalElementos As Integer
            Dim elementoAnalizado As Integer
            Dim indice As Integer
            Dim codigoProd As String
            Dim codProd As String
            Dim producto As String
            Dim cantidad As Integer
            Dim precioTotal As Double
            Dim precioFinal As Double
            Dim precioUnitario As Double
            Dim precioMod As Double
            Dim motivo As String
            Dim idMotivo As Integer
            Dim descripcion As String
            Dim mensaje As String

            mensaje = ""
            codProd = Me.txbNroProdVenta.Text
            producto = Me.txbNomProdVenta.Text
            cantidad = Me.nudCantProdVenta.Value
            precioTotal = CDbl(Me.txbTotalSinMod.Text)
            precioFinal = CDbl(Me.lblMontoFinalVenta.Text)
            precioUnitario = CDbl(Me.txbPrecioUniVenta.Text)
            precioMod = 0
            descripcion = "No hubo modificación para este producto"
            If modificacion And Me.chkAuVenta.Checked Then
                precioMod = CDbl(Me.txbDescVentasEmp.Text)
                descripcion = Me.txbDetDescVentasEmp.Text
            ElseIf modificacion And Not Me.chkAuVenta.Checked Then
                precioMod = CDbl(Me.txbDescVentasEmp.Text) * (-1)
                descripcion = Me.txbDetDescVentasEmp.Text
            End If

            motivo = Me.cmbMotDescVentasEmp.Text
            idMotivo = Me.cmbMotDescVentasEmp.SelectedValue

            indice = -1
            totalElementos = carrito.Rows.Count
            elementoAnalizado = 0
            codigoProd = carrito.CurrentRow.Cells("codProd").Value
            While elementoAnalizado < totalElementos
                If carrito.Rows(elementoAnalizado).Cells("codProd").Value = codigoProd Then
                    indice = elementoAnalizado
                    elementoAnalizado = totalElementos
                End If
                elementoAnalizado = elementoAnalizado + 1
            End While
            If indice <> -1 Then
                carrito.Rows(indice).Cells("codProd").Value = codProd
                carrito.Rows(indice).Cells("nombreProducto").Value = producto
                carrito.Rows(indice).Cells("cantidad").Value = cantidad
                carrito.Rows(indice).Cells("precio").Value = precioTotal
                carrito.Rows(indice).Cells("cantidadMod").Value = precioMod
                carrito.Rows(indice).Cells("precioFinal").Value = precioFinal
                carrito.Rows(indice).Cells("motivoMod").Value = motivo
                carrito.Rows(indice).Cells("descMod").Value = descripcion
                carrito.Rows(indice).Cells("idMotivo").Value = idMotivo
                carrito.Rows(indice).Cells("precioUnitario").Value = precioUnitario
            End If

        Catch ex As Exception
            MsgBox("Error al cargar los datos modificados de la compra. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbExclamation, "Ventana Error")
        End Try
    End Sub

    Private Sub limpiarDatosVenta()
        Try
            Me.txbNroProdVenta.Text = ""
            Me.txbNomProdVenta.Text = ""
            Me.txbPrecioUniVenta.Text = ""
            Me.nudCantProdVenta.Value = 1
            Me.txbTotalSinMod.Text = ""
            Me.chkDescVenta.Checked = False
            Me.txbDescVentasEmp.Text = ""
            Me.cmbMotDescVentasEmp.SelectedIndex = Me.cmbMotDescVentasEmp.FindStringExact("Sin Modificación")
            Me.txbDetDescVentasEmp.Text = ""
            Me.lblMontoFinalVenta.Text = ""
            Me.btnModCarrito.Enabled = False
            Me.btnAgregarCarrito.Enabled = True
        Catch ex As Exception
            MsgBox("Error al limpiar los datos de las compras. Los errores producidos fueron: " + vbNewLine + ex.Message, vbOKOnly + vbCritical, "Ventana Error")
        End Try
    End Sub

    Private Sub btnModCarrito_Click(sender As Object, e As EventArgs) Handles btnModCarrito.Click
        Dim modificacion As Boolean
        Dim mensaje As String

        mensaje = ""
        modificacion = Me.chkModVentas.Checked

        If modificacion Then
            If General.controlarCajaEnBlanco(Me.txbDetDescVentasEmp.Text) Then
                mensaje = mensaje + "Por favor, no deje la descripcion de la modificación en blanco" + vbNewLine
            End If

            If Not General.controlarRealPositivo(Me.txbDescVentasEmp.Text) Then
                mensaje = mensaje + "Por favor, coloque un valor entero positivo para el precio de modificación" + vbNewLine
            End If
        End If
        If String.IsNullOrWhiteSpace(mensaje) Then
            Me.modificar(Me.dgvCarritoProdEmp, modificacion)
            Me.limpiarDatosVenta()
        Else
            MsgBox("Error al querer modificar los datos. Los errores producidos fueron: " + vbNewLine + mensaje, vbOKOnly + vbExclamation, "Ventana Error")
        End If
    End Sub

    Private Sub dgvListaProdEmp_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvListaProdEmp.CellContentClick

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.limpiarDatosVenta()
    End Sub

    Private Sub dgvCarritoProdEmp_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCarritoProdEmp.CellContentClick

    End Sub

    Private Sub btnConfirmarVenta_Click(sender As Object, e As EventArgs) Handles btnConfirmarVenta.Click
        Dim respuesta As Integer
        respuesta = MsgBox("¿Desea compras los productos en el carrito?", vbYesNo + vbDefaultButton2, "Ventana Consulta")
        If respuesta = vbYes Then
            Me.realizarVenta(Me.dgvCarritoProdEmp)
            dgvCarritoProdEmp.Rows.Clear()
            Me.cargarProductos(Me.dgvListaProdEmp)
        End If
    End Sub

    Private Sub txbNombreProdEmp_TextChanged(sender As Object, e As EventArgs) Handles txbNombreProdEmp.TextChanged
        If Not String.IsNullOrWhiteSpace(Me.txbNombreProdEmp.Text) Then
            Me.filtrarProdNom(Me.dgvListaProdEmp, Trim(Me.txbNombreProdEmp.Text))
        Else
            Me.cargarProductos(Me.dgvListaProdEmp)
        End If
    End Sub
End Class
