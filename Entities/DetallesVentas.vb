'------------------------------------------------------------------------------
' <auto-generated>
'     Este código se generó a partir de una plantilla.
'
'     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
'     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class DetallesVentas
    Public Property idVenta As Integer
    Public Property idDetalle As Integer
    Public Property idProducto As Integer
    Public Property cantidad As Integer
    Public Property precioDetalle As Double
    Public Property estado As Nullable(Of Boolean)
    Public Property fechaModDet As Nullable(Of DateTime)

    Public Overridable Property Productos As Productos
    Public Overridable Property DevolucionesDetalles As ICollection(Of DevolucionesDetalles) = New HashSet(Of DevolucionesDetalles)
    Public Overridable Property OrdenesVentas As OrdenesVentas
    Public Overridable Property DetallesModVentas As ICollection(Of DetallesModVentas) = New HashSet(Of DetallesModVentas)

End Class
